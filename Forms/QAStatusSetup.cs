using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class QAStatusSetup : Form
    {
        public QAStatusSetup()
        {
            InitializeComponent();
        }

        #region Properties
        private Boolean m_FlagQ = true;
        public Boolean FlagQ
        {
            get { return m_FlagQ; }
            set { m_FlagQ = value; }
        }
        private Boolean m_FlagM = false;
        public Boolean FlagM
        {
            get { return m_FlagM; }
            set { m_FlagM = value; }
        }
        private Boolean m_FlagE = false;
        public Boolean FlagE
        {
            get { return m_FlagE; }
            set { m_FlagE = value; }
        }
        private Boolean m_FlagP = false;
        public Boolean FlagP
        {
            get { return m_FlagP; }
            set { m_FlagP = value; }
        }
        private Boolean m_TNStatus = false;
        public Boolean TNStatus
        {
            get { return m_TNStatus; }
            set { m_TNStatus = value; }
        }
        private Boolean m_Verbose = false;
        public Boolean Verbose
        {
            get { return m_Verbose; }
            set { m_Verbose = value; }
        }
        #endregion

        #region Ok Pressed
        private void btnOk_Click(object sender, EventArgs e)
        {
            FlagQ = chkPassedQA.Checked;
            FlagM = chkModified.Checked;
            FlagP = chkProjectHold.Checked;
            FlagE = chkExecHold.Checked;
            Verbose = rdoVerboseReport.Checked;
            TNStatus = chkModifiedTNs.Checked;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region Child Form Close
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        private void rdoVerboseReport_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoVerboseReport.Checked)
            {
                chkModifiedTNs.Enabled = true;
            }
            else
            {
                chkModifiedTNs.Checked = false;
                chkModifiedTNs.Enabled = false;
            }
        }

        private void rdoIDListOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoIDListOnly.Checked)
            {
                chkModifiedTNs.Checked = false;
                chkModifiedTNs.Enabled = false;
            }
        }
    }
}