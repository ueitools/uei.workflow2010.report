using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class IntronFilterForm : Form
    {
        #region Proerties
        private Boolean m_IncludeBrand = false;
        public Boolean IncludeBrand
        {
            get { return m_IncludeBrand; }
            set { m_IncludeBrand = value; }
        }
        #endregion

        public IntronFilterForm()
        {
            InitializeComponent();
        }      
        private void btnOk_Click(object sender, EventArgs e)
        {
            if(this.chkIncludeBrand.Checked == true)
                this.IncludeBrand = true;
            else
                this.IncludeBrand = false;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }            
    }
}