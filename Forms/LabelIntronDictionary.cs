using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Service;

namespace UEI.Workflow2010.Report
{
    public delegate void LabelIntronSelectedEventHandler(object sender, LabelIntronSelectedEventArgs e);

    public partial class LabelIntronDictionary : Form
    {
        private Model.IntronLabelDictionary dictionary = null;
        private const string ALLCATEGORY = "All";
        private CheckBox headerCheck = null;
        private bool avoidCheckedEvent = false;
        private List<int> checkedIndices = new List<int>();
        private DataTable selectedDataTable = null;
        
        public event LabelIntronSelectedEventHandler LabelAndIntronSelected;

        public LabelIntronDictionary()
        {
            InitializeComponent();

            //get dictionary
            this.dictionary = GetIntronLabelDictionary();
            //fill the category combo
            FillCategoryCombo();
            //
            CustomGridviewSettings();
        }

        private void FillCategoryCombo()
        {
            if (this.dictionary!=null)
            {
                List<string> categoryList = this.dictionary.GetCategories();
                cmbBxSelectCategory.Items.Clear();
                //Add 'All' item
                cmbBxSelectCategory.Items.Add(ALLCATEGORY);
                foreach (string category in categoryList)
                {
                    if (category != Model.IntronLabelDictionary.UnassignedCategory)
                        cmbBxSelectCategory.Items.Add(category);
                }
                cmbBxSelectCategory.SelectedIndex = 0;
            }
        }
        
        private Model.IntronLabelDictionary GetIntronLabelDictionary()
        {
            try
            {
                Reports reportService = new Reports();
                
                 return reportService.GetIntronLabelDictionary();
            }
            catch
            {
                throw;
            }
        }

        private void cmbBxSelectCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string category = cmbBxSelectCategory.SelectedItem.ToString();
            string key = txtBxEnterText.Text.Trim();

            LoadIntronLabelDictionary(category, key);

        }

        private void CustomGridviewSettings()
        {
            
            //settings

            //1.enable editing only for checkbox column
            foreach (DataGridViewColumn column in this.grdVwLabelIntronDictionary.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                if (column.Index != 0)//avoid the first column
                    column.ReadOnly = true;
               
            }
            //2.Add check box to header of checkbox column
            //Rectangle headerCellPosition = this.grdVwLabelIntronDictionary.GetCellDisplayRectangle(0, -1, false);
            Rectangle headerCellPosition = this.grdVwLabelIntronDictionary.Columns[0].HeaderCell.ContentBounds;
            

            headerCheck = new CheckBox();
            headerCheck.Name = "chkHeader";
            headerCheck.AutoSize = true;
            headerCheck.BackColor = Color.Transparent;
            headerCheck.Location = new Point(7,5);
            headerCheck.CheckStateChanged += new EventHandler(headerCheck_CheckStateChanged);
            
            this.grdVwLabelIntronDictionary.Controls.Add(headerCheck);
            this.grdVwLabelIntronDictionary.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            //this.grdVwLabelIntronDictionary.MultiSelect = true;

        }

        void headerCheck_CheckStateChanged(object sender, EventArgs e)
        {
            if (!this.avoidCheckedEvent)
            {
                CheckBox headerCheck = (CheckBox)sender;
                //check all the rows.
                //get the column

                //the current cell is a checkbox and change is not getting reflected to it.Next line
                //is a workaround for this.
                this.grdVwLabelIntronDictionary.CurrentCell = null;

                foreach (DataGridViewRow row in this.grdVwLabelIntronDictionary.Rows)
                {
                    DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)row.Cells[0];
                    checkBoxCell.Value = headerCheck.Checked;
                }

                
           }
            
            this.avoidCheckedEvent = false;
           
          }

        private void grdVwLabelIntronDictionary_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)//to avoid header row.
            {
                if (e.ColumnIndex == 0)//for the checkbox column
                {
                    DataGridViewCheckBoxCell checkBoxCell = (DataGridViewCheckBoxCell)this.grdVwLabelIntronDictionary.Rows[e.RowIndex].Cells[0];
                    bool cellChecked = (bool)checkBoxCell.Value;

                    if (cellChecked) //if it is checked
                    {
                        //add to the selected indices list
                        this.checkedIndices.Add(e.RowIndex);
                        
                        if (this.grdVwLabelIntronDictionary.Rows.Count.Equals(this.checkedIndices.Count))
                        {
                            if (!this.headerCheck.Checked)
                            {
                                this.avoidCheckedEvent = true;//to avoid raising headerchecked event.
                                this.headerCheck.Checked = true;
                            }
                        }

                        //highlight the selected cells
                        this.grdVwLabelIntronDictionary.Rows[e.RowIndex].DefaultCellStyle.BackColor = this.grdVwLabelIntronDictionary.DefaultCellStyle.SelectionBackColor;
                        this.grdVwLabelIntronDictionary.Rows[e.RowIndex].DefaultCellStyle.ForeColor = this.grdVwLabelIntronDictionary.DefaultCellStyle.SelectionForeColor;
                        

                    }
                    else
                    {
                        //remove from selected indices list
                        this.checkedIndices.Remove(e.RowIndex);
                        if (this.headerCheck.Checked)
                        {
                            this.avoidCheckedEvent = true;//to avoid raising headerchecked event.
                            this.headerCheck.Checked = false;
                        }

                        //remove highlight from selected cells
                        this.grdVwLabelIntronDictionary.Rows[e.RowIndex].DefaultCellStyle.BackColor = this.grdVwLabelIntronDictionary.DefaultCellStyle.BackColor;
                        this.grdVwLabelIntronDictionary.Rows[e.RowIndex].DefaultCellStyle.ForeColor = this.grdVwLabelIntronDictionary.DefaultCellStyle.ForeColor;
                        this.grdVwLabelIntronDictionary.Rows[e.RowIndex].Selected = false;

                    }


                   
                }
            }

            
        }

        private void grdVwLabelIntronDictionary_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.grdVwLabelIntronDictionary.IsCurrentCellDirty)
            {
                this.grdVwLabelIntronDictionary.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private string previousTextEntered = String.Empty;

        private void txtBxEnterText_KeyUp(object sender, KeyEventArgs e)
        {
            //if it is a character key
            string key = txtBxEnterText.Text.Trim();
            string category = cmbBxSelectCategory.SelectedItem.ToString();


            if ((e.KeyCode != Keys.Alt) && (e.KeyCode != Keys.ControlKey) && (e.KeyCode != Keys.ShiftKey) && (e.KeyCode != Keys.Enter) && (e.KeyCode != Keys.Escape) && (e.KeyCode != Keys.CapsLock) && (e.KeyCode != Keys.RWin)
                && (e.KeyCode != Keys.LWin) && e.KeyCode!= Keys.Home && (e.KeyCode != Keys.End) && (e.KeyCode != Keys.PageUp) && (e.KeyCode != Keys.PageDown)
                && (e.KeyCode != Keys.Up) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Down) && (e.KeyCode != Keys.Right)
                && !(e.KeyCode >= Keys.F1 && e.KeyCode <= Keys.F12))
            {
                if (this.previousTextEntered != key)
                {
                    LoadIntronLabelDictionary(category,key);

                    this.previousTextEntered = key;
                }
                
            }
         
            
        }


        private void LoadIntronLabelDictionary(string category,string key)
        {
            
            if (category == ALLCATEGORY)
                this.selectedDataTable = this.dictionary.GetLabelIntronSet(String.Empty, key);
            else
                this.selectedDataTable = this.dictionary.GetLabelIntronSet(category, key);

            this.grdVwLabelIntronDictionary.DataSource = this.selectedDataTable;

            
            //reset the header checkbox cell
            if (this.headerCheck != null)
            {
                if (this.selectedDataTable.Rows.Count > 0)
                    this.headerCheck.Enabled = true;
                else
                    this.headerCheck.Enabled = false;


                this.headerCheck.Checked = false;
            }
            //clear the selected indices list
            this.checkedIndices.Clear();
        }

        private void btnAddItems_Click(object sender, EventArgs e)
        {
            if (this.checkedIndices.Count > 0)
            {
                string category = cmbBxSelectCategory.SelectedItem.ToString();
                LabelIntronSelectedEventArgs labelIntronArgs = new LabelIntronSelectedEventArgs();
                List<object[]> labelWithIntron = new List<object[]>();

                foreach (int selectedRowIndex in this.checkedIndices)
                {
                    //get from the category
                    DataRow selectedRow = this.selectedDataTable.Rows[selectedRowIndex];
                    object[] selectedLabelWithIntron = selectedRow.ItemArray;
                    labelWithIntron.Add(selectedLabelWithIntron);
                }

                labelIntronArgs.SelectedLabelsWithIntrons = labelWithIntron;

                if(LabelAndIntronSelected!=null)
                    LabelAndIntronSelected(this, labelIntronArgs);

                this.Close();
            }
        }
        

        
    }

    public class LabelIntronSelectedEventArgs
    {
        private List<object[]> labelWithIntron = new List<object[]>();

        public List<object[]> SelectedLabelsWithIntrons
        {
            get { return labelWithIntron; }
            set { labelWithIntron = value; }
        }



    }
}