using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class SynthesizerSelection : Form
    {
        #region Properits
        private Boolean m_IncludeIntrons = true;
        public Boolean IncludeIntrons
        {
            get { return m_IncludeIntrons; }
            set { m_IncludeIntrons = value; }
        }
        private Boolean m_UseDataBase = true;
        public Boolean UseDataBase
        {
            get { return m_UseDataBase; }
            set { m_UseDataBase = value; }
        }
        #endregion

        public SynthesizerSelection()
        {
            InitializeComponent();
            this.btnOK.Click += new EventHandler(btnOK_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        void btnOK_Click(object sender, EventArgs e)
        {
            this.UseDataBase = this.rdoUEI2DB.Checked;
            this.IncludeIntrons = this.chkIncludeIntrons.Checked;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}