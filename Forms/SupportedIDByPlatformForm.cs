﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Model;
using UEI.Workflow2010.Report.Service;

namespace UEI.Workflow2010.Report
{
    public partial class SupportedIDByPlatformForm : Form
    {
        #region Variables
        #endregion

        #region Properties
        public IdSetupCodeInfo Parameter { get; set; }
        private IList<Hashtable> _Platforms = null;
        public IList<Hashtable> Platforms
        {
            get { return _Platforms; }
            set { _Platforms = value; }
        }
        #endregion

        #region Coctructor
        public SupportedIDByPlatformForm()
        {
            InitializeComponent();
            this.Load += SupportedIDByPlatformForm_Load;            
        }
        void SupportedIDByPlatformForm_Load(object sender, EventArgs e)
        {
            this.Parameter = new IdSetupCodeInfo();
            this.btnCancel.Click += btnCancel_Click;
            this.btnOK.Click += btnOK_Click;
            this.chkIncludeUnsupportedIds.CheckedChanged += chkIncludeUnsupportedIds_CheckedChanged;
            this.treeViewPlatforms.AfterCheck += treeViewPlatforms_AfterCheck;
            LoadPlatforms();
        }               
        #endregion

        #region OK
        void btnOK_Click(object sender, EventArgs e)
        {
            GetSupportedPlatform();
            if (this.Parameter.IncludeUnsupportedPlatforms == false)
            {
                if (this.Parameter.SupportedPlatforms == null)
                {
                    MessageBox.Show("Please select Platform you want to include.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (this.Parameter.SupportedPlatforms.Count == 0)
                {
                    MessageBox.Show("Please select Platform you want to include.", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {                    
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        #endregion

        #region Cancel
        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Supported Ids By Platforms
        private void LoadPlatforms()
        {
            Reports _Report = new Reports();
            this.Platforms = _Report.GetAllSupportedPlatforms();
        }
        private Int32 GetCheckedNodes(TreeNode node)
        {
            Int32 checkedNodes = 0;
            if (node != null)
                foreach (TreeNode child in node.Nodes)
                {
                    if (child.Checked)
                        checkedNodes++;
                }
            return checkedNodes;
        }
        private void checkNodes(TreeNode node, bool check)
        {
            if (node != null)
                foreach (TreeNode child in node.Nodes)
                {
                    child.Checked = check;
                    checkNodes(child, check);
                }
        }
        private void chkIncludeUnsupportedIds_CheckedChanged(object sender, EventArgs e)
        {
            if (treeViewPlatforms.Nodes.Count > 0)
                treeViewPlatforms.Nodes.Clear();
            if (((CheckBox)sender).Checked == true)
            {
                grpPlatforms.Enabled = false;                
            }
            else
            {                
                grpPlatforms.Enabled = true;
                if (this.Platforms != null)
                {
                    treeViewPlatforms.Nodes.Add("All", "All Platforms");
                    treeViewPlatforms.Nodes["All"].Tag = "All";
                    foreach (Hashtable item in this.Platforms)
                    {
                        treeViewPlatforms.Nodes["All"].Nodes.Add(item["Platform_RID"].ToString(), item["Name"].ToString());
                        treeViewPlatforms.Nodes["All"].Nodes[item["Platform_RID"].ToString()].Tag = "Platform";
                    }
                    checkNodes(treeViewPlatforms.Nodes["All"], true);
                    treeViewPlatforms.Nodes["All"].Checked = true;
                    treeViewPlatforms.ExpandAll();
                }
            }
        }
        private void treeViewPlatforms_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    if (node.Tag.ToString() == "All")
                    {
                        checkNodes(node, node.Checked);
                    }
                    else
                    {
                        checkNodes(node, node.Checked);
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Checked = false;
                        }
                    }
                }
            }
        }
        private void GetSupportedPlatform()
        {
            if (chkIncludeUnsupportedIds.Checked == false)
            {
                if (this.Parameter == null)
                    this.Parameter = new IdSetupCodeInfo();
                this.Parameter.IncludeUnsupportedPlatforms = false;
                if (treeViewPlatforms.Nodes.Count > 0)
                {
                    if (this.Parameter.SupportedPlatforms == null)
                        this.Parameter.SupportedPlatforms = new List<String>();
                    else
                        this.Parameter.SupportedPlatforms.Clear();
                    foreach (TreeNode parentNode in treeViewPlatforms.Nodes)
                    {
                        if (parentNode.Nodes != null)
                        {
                            foreach (TreeNode subNode in parentNode.Nodes)
                            {
                                if (subNode.Checked == true)
                                {
                                    this.Parameter.SupportedPlatforms.Add(subNode.Text);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                this.Parameter.IncludeUnsupportedPlatforms = true;
                this.Parameter.SupportedPlatforms = null;
            }
        }
        #endregion        
    }
}