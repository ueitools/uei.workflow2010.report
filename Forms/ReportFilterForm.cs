using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Model;
using UEI.Workflow2010.Report.Service;
using System.IO;
using System.Collections;
using System.Data.OleDb;
using CommonForms;
namespace UEI.Workflow2010.Report
{
    public partial class ReportFilterForm : Form
    {
        #region Variables
        private IList<AliasId> _PICIDAliasList;
        public List<String> _selectedModes = new List<String>();       
        private const String IDBASEDDEVICE = "IDBased_Device";
        Dictionary<String, String> _eliminationList = new Dictionary<String, String>();        
        private BackgroundWorker workerProcess = new BackgroundWorker();
        private BackgroundWorker quicksetProcess = null;
        private Boolean backWorkerCancelled = false;
        private Boolean _LoadDevicesFlag = true;        
        private LocationModel _locationCollection = null;        
        private ReportType _ReportType;
        public SelType SelectedType = SelType.MODE;
        private IDModeRecordModel allModeNames = null;
        private IDictionary m_GroupList = null;
        Forms.ProgressForm m_Progress = null;
        #endregion

        #region Properties
        private IList<Hashtable> _Platforms = null;
        public IList<Hashtable> Platforms
        {
            get { return _Platforms; }
            set { _Platforms = value; }
        }
        private IList<Id> _ids = new List<Id>();
        public IList<Id> Ids
        {
            get { return _ids; }
            set { _ids = value; }
        }
        private IList<Model.Region> _regions = null;
        public IList<Model.Region> Regions
        {
            get { return _regions; }
        }
        private IList<DeviceType> _deviceTypes = null;
        public IList<DeviceType> DeviceTypes
        {
            get { return _deviceTypes; }
        }
        private IList<DataSources> _dataSources = null;
        public IList<DataSources> DataSources
        {
            get { return _dataSources; }
        }
        private Boolean _RemoveIDSForSelectedSubDevices = false;
        public Boolean RemoveIDSForSelectedSubDevices
        {
            get { return _RemoveIDSForSelectedSubDevices; }
        }
        private String m_ProjectName = String.Empty;
        public String ProjectName
        {
            get { return m_ProjectName; }
            set
            {
                txtMIProjectName.Text = value;
                txtProjectName.Text = value;
                m_ProjectName = value;
            }
        }
        private IdSetupCodeInfo _IdSetupCodeInfoParam = null;
        public IdSetupCodeInfo IdSetupCodeInfoParam
        {
            get { return _IdSetupCodeInfoParam; }
        }
        private BrandFilter _brandSpecificSel;
        public BrandFilter BrandSpecificSel
        {
            get { return _brandSpecificSel; }
            set { _brandSpecificSel = value; }
        }
        private ModelFilter _modelSpecificSel;
        public ModelFilter ModelSpecificSel
        {
            get { return _modelSpecificSel; }
            set { _modelSpecificSel = value; }
        }
        private ModelInfoSpecific _modelInfoSpecificSel;
        public ModelInfoSpecific ModelInfoSpecificSel
        {
            get { return _modelInfoSpecificSel; }
            set { _modelInfoSpecificSel = value; }
        }
        private BrandBasedSeach m_brandbasedSearch;
        public BrandBasedSeach brandbasedSeach
        {
            get { return m_brandbasedSearch; }
            set { m_brandbasedSearch = value; }
        }
        private LabelIntronFilters _LabelIntronSearchInputs;
        public LabelIntronFilters LabelIntronSearchInputs
        {
            get { return _LabelIntronSearchInputs; }
            set { _LabelIntronSearchInputs = value; }
        }
        private QuickSetInfo m_QuickSetInputs = null;
        public QuickSetInfo QuickSetInputs
        {
            get { return m_QuickSetInputs; }
            set { m_QuickSetInputs = value; }
        }
        private int m_KeyFunctionReportType = 1;
        public int KeyFunctionReportType
        {
            get { return m_KeyFunctionReportType; }
            set { m_KeyFunctionReportType = value; }
        }
        #endregion

        #region Tab Selection
        public void TabSelection()
        {
            switch (_ReportType)
            {               
                case ReportType.IDList:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    chkDisplayBrandNumber.Visible = false;
                    lblIDOffset.Visible = false;
                    txtIDOffset.Visible = false;
                    grpAliasIDFile.Visible = false;
                    rdoSortByPriorityIDListFile.Visible = false;
                    txtPriorityIDListFile.Visible = false;
                    rdoSortByName.Visible = false;
                    chkDisplayMajorBrands.Visible = false;
                    cmbSetupCodeFormating.Visible = false;
                    lblSetupCodeFormat.Visible = false;
                    grpProject.Visible = false;
                    lblStartDate.Enabled = false;
                    lblEndDate.Enabled = false;
                    txtStartDate.Enabled = false;
                    txtEndDate.Enabled = false;
                    btnStartDate.Enabled = false;
                    btnEndDate.Enabled = false;
                    chkRemoveIDs.Visible = false;
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;
                    this.Text = "ID Selection";
                    break;
                case ReportType.EdId:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    //tabReportFilter.Controls.Remove(tabBrand);
                    //tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    groupBox4.Visible = false;
                    chkDisplayBrandNumber.Visible = false;
                    lblIDOffset.Visible = false;
                    txtIDOffset.Visible = false;
                    grpAliasIDFile.Visible = false;
                    rdoSortByPriorityIDListFile.Visible = false;
                    txtPriorityIDListFile.Visible = false;
                    rdoSortByName.Visible = false;
                    chkDisplayMajorBrands.Visible = false;
                    cmbSetupCodeFormating.Visible = false;
                    lblSetupCodeFormat.Visible = false;
                    grpProject.Visible = false;
                    lblStartDate.Enabled = false;
                    lblEndDate.Enabled = false;
                    txtStartDate.Enabled = false;
                    txtEndDate.Enabled = false;
                    btnStartDate.Enabled = false;
                    btnEndDate.Enabled = false;
                    chkRemoveIDs.Visible = false;
                    chkRecordWithoutModelName.Visible = false;
                    chkRemoveNonCodeBookModels.Visible = false;
                    groupBox6.Visible = false;
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;
                    chkNoUEIBrandNumbers.Visible = false;
                    this.Text = "EDID";
                    this._brandSpecificSel = new BrandFilter();
                    this._modelSpecificSel = new ModelFilter();
                    break;
                case ReportType.IDBrandList:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    lblStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    txtStartDate.Visible = false;
                    txtEndDate.Visible = false;
                    btnStartDate.Visible = false;
                    btnEndDate.Visible = false;
                    grpProject.Visible = false;
                    lblIDOffset.Visible = false;
                    txtIDOffset.Visible = false;
                    grpAliasIDFile.Visible = false;
                    rdoSortByPriorityIDListFile.Visible = false;
                    txtPriorityIDListFile.Visible = false;
                    rdoSortByIDNumber.Visible = false;
                    chkDisplayBrandNumber.Visible = false;
                    chkRemoveIDs.Visible = false;
                    rdoSortByPOSData.Visible = false;
                    rdoSortBySearchStatistics.Visible = false;
                    chkDisplayMajorBrands.Visible = false;
                    cmbSetupCodeFormating.Visible = false;
                    lblSetupCodeFormat.Visible = false;
                    chkIncludeAliasBrand.Visible = true;
                    chkIncludeAliasBrand.Enabled = true;
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;
                    this.Text = "ID By Brand";
                    break;
                case ReportType.KeyFunction:
                    {
                        tabReportFilter.Controls.Remove(tabQuickSetInfo);
                        tabReportFilter.Controls.Remove(tabBrand);
                        tabReportFilter.Controls.Remove(tabModel);
                        tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                        tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                        tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                        tabReportFilter.Controls.Remove(tabUnsupportedId);
                        chkDisplayMajorBrands.Visible = false;
                        rdoSortByName.Visible = false;
                        rdoSortByPOSData.Visible = false;
                        rdoSortBySearchStatistics.Visible = false;
                        lblStartDate.Visible = false;
                        lblEndDate.Visible = false;
                        txtStartDate.Visible = false;
                        txtEndDate.Visible = false;
                        btnStartDate.Visible = false;
                        btnEndDate.Visible = false;
                        grpProject.Visible = false;
                        chkRemoveIDs.Visible = false;
                        chkIncludeAliasBrand.Visible = false;
                        chkIncludeAliasBrand.Enabled = false;      
                 
                        chkDisplayBrandNumber.Visible = false;
                        lblIDOffset.Visible = false;
                        txtIDOffset.Visible = false;
                        chkBxIncludeBlankModelNames.Visible = false;
                        lblSetupCodeFormat.Visible = false;
                        cmbSetupCodeFormating.Visible = false;
                        chkFormatStartDigitSwitch.Visible = false;
                        gbSort.Visible = false;
                        grpAliasIDFile.Visible = false;
                        this.Text = "Key Function Count";
                    }
                    break;
                case ReportType.BSC:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    chkDisplayMajorBrands.Visible = false;
                    rdoSortByName.Visible = false;
                    rdoSortByPOSData.Visible = false;
                    rdoSortBySearchStatistics.Visible = false;
                    lblStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    txtStartDate.Visible = false;
                    txtEndDate.Visible = false;
                    btnStartDate.Visible = false;
                    btnEndDate.Visible = false;
                    grpProject.Visible = false;
                    chkRemoveIDs.Visible = false;
                    chkIncludeAliasBrand.Visible = true;
                    chkIncludeAliasBrand.Enabled = true;
                    txtIDOffset.Text = "0";
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;
                    this.Text = "BSC";
                    break;
                case ReportType.BSCPIC:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    chkDisplayMajorBrands.Visible = false;
                    rdoSortByName.Visible = false;
                    rdoSortByPOSData.Visible = false;
                    rdoSortBySearchStatistics.Visible = false;
                    lblStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    txtStartDate.Visible = false;
                    txtEndDate.Visible = false;
                    btnStartDate.Visible = false;
                    btnEndDate.Visible = false;
                    grpProject.Visible = true;
                    chkIncludeAliasBrand.Visible = true;
                    chkIncludeAliasBrand.Enabled = true;
                    txtIDOffset.Text = "0";
                    chkRemoveIDs.Visible = false;
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;
                    this.Text = "BSC";
                    break;
                case ReportType.ModelInfoWithArdKey:
                    enableDateFilterChk.Enabled = false;
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    chkRemoveIDs.Visible = false;
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    grpMIProject.Visible = false;
                    this._brandSpecificSel = new BrandFilter();
                    this._modelSpecificSel = new ModelFilter();
                    this._modelInfoSpecificSel = new ModelInfoSpecific();
                    this.Text = "Model Info Report with Ard Key";
                    break;
                case ReportType.ModelInfoWithArdKeyRegenerate:
                    {
                        tabReportFilter.Controls.Remove(tabQuickSetInfo);
                        tabReportFilter.Controls.Remove(tabBrand);
                        tabReportFilter.Controls.Remove(tabModel);
                        tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                        tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                        tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                        tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                        tabReportFilter.Controls.Remove(tabKeyFunction);
                        tabReportFilter.Controls.Remove(tabUnsupportedId);
                        this.enableDateFilterChk.Enabled = false;
                        this.Text = "Model Info Report with Ard Key"; 
                    }
                    break;
                case ReportType.QuickSetRegenerate:
                    {
                        tabReportFilter.Controls.Remove(tabQuickSetInfo);
                        tabReportFilter.Controls.Remove(tabBrand);
                        tabReportFilter.Controls.Remove(tabModel);
                        tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                        tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                        tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                        tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                        tabReportFilter.Controls.Remove(tabKeyFunction);
                        tabReportFilter.Controls.Remove(tabUnsupportedId);
                        this.Text = "QuickSet Model Info Report";                        
                    }
                    break;
                case ReportType.QuickSet:
                    {
                        chkRemoveIDs.Visible = false;
                        LoadModeList();                       
                        m_QuickSetInputs = new QuickSetInfo();
                        tabReportFilter.Controls.Remove(tabQuickSetInfo);
                        tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                        tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                        tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                        tabReportFilter.Controls.Remove(tabKeyFunction);
                        tabReportFilter.Controls.Remove(tabUnsupportedId);
                        grpMIProject.Visible = false;
                        //_LoadDevicesFlag = true;
                        //quicksetProcess = new BackgroundWorker();
                        //quicksetProcess.DoWork += quicksetProcess_DoWork;
                        //quicksetProcess.RunWorkerCompleted += quicksetProcess_RunWorkerCompleted;
                        this._brandSpecificSel = new BrandFilter();
                        this._modelSpecificSel = new ModelFilter();
                        this._modelInfoSpecificSel = new ModelInfoSpecific();
                        this.Text = "QuickSet Model Info Report";
                    }
                    break;
                case ReportType.ModelInformation:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    chkRemoveIDs.Visible = false;                    
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    grpMIProject.Visible = false;                    
                    this._brandSpecificSel = new BrandFilter();
                    this._modelSpecificSel = new ModelFilter();
                    this._modelInfoSpecificSel = new ModelInfoSpecific();
                    this.Text = "Model Information Report";
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    chkRemoveIDs.Visible = false;
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabDevice);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    grpMIProject.Visible = false;
                    this._brandSpecificSel = new BrandFilter();
                    this._modelSpecificSel = new ModelFilter();
                    this._modelInfoSpecificSel = new ModelInfoSpecific();
                    this.Text = "Model Information Report";
                    break;
                case ReportType.MODELINFOPIC:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    chkRemoveIDs.Visible = false;
                    grpMIDateSelection.Visible = false;
                    grpMIOption.Visible = false;
                    enableDateFilterChk.Visible = false;
                    grpMIProject.Visible = true;                    
                    dateBasedSearchGroup.Enabled = true;
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    this._brandSpecificSel = new BrandFilter();
                    this._modelSpecificSel = new ModelFilter();
                    this._modelInfoSpecificSel = new ModelInfoSpecific();
                    this.Text = "Model Information Report";
                    break;
                case ReportType.BrandBasedSearch:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabLabelIntronSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    this.m_brandbasedSearch = new BrandBasedSeach();
                    cmbBxModelTypeBBS.Enabled = chkModel.Checked;
                    cmbBxModelTypeBBS.SelectedIndex = 2;
                    chkRemoveIDs.Visible = false;
                    this.Text = "Brand Based Search";
                    break;
                case ReportType.LabelSearch:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabMarket);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabDataSourceLocation);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabDevice);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    tabLabelIntronSearch.Text = "Label Search";
                    grpBxEnterPatterns.Text = "Enter labels to search";
                    this._LabelIntronSearchInputs = new LabelIntronFilters();
                    this.Text = "Label Search";
                    break;
                case ReportType.IntronSearch:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabMarket);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabDataSourceLocation);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabDevice);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    tabLabelIntronSearch.Text = "Intron Search";
                    grpBxEnterPatterns.Text = "Enter introns to search";
                    this._LabelIntronSearchInputs = new LabelIntronFilters();
                    this.Text = "Intron Search";
                     
                    break;
                case ReportType.ExcludeLabelSearch:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabMarket);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabDataSourceLocation);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabDevice);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    tabLabelIntronSearch.Text = "Label Search";
                    grpBxEnterPatterns.Text = "Enter labels to Exclude search";
                    this._LabelIntronSearchInputs = new LabelIntronFilters();
                    this.Text = "Label Search";
                    chkBxGetCompleteMatch.Checked = true;
                    _LabelIntronSearchInputs.IsCompleteMatch = true;
                    break;
                case ReportType.ExcludeIntronSearch:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabMarket);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabDataSourceLocation);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabDevice);
                    tabReportFilter.Controls.Remove(tabIDSetupCodeInfo);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    tabLabelIntronSearch.Text = "Intron Search";
                    grpBxEnterPatterns.Text = "Enter introns to Exclude search";
                    this._LabelIntronSearchInputs = new LabelIntronFilters();
                    this.Text = "Intron Search";
                    chkBxGetCompleteMatch.Checked = true;
                    _LabelIntronSearchInputs.IsCompleteMatch = true;
                    break;
                case ReportType.IntronSearchWithBrand:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    lblStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    txtStartDate.Visible = false;
                    txtEndDate.Visible = false;
                    btnStartDate.Visible = false;
                    btnEndDate.Visible = false;
                    grpProject.Visible = false;
                    lblIDOffset.Visible = false;
                    txtIDOffset.Visible = false;
                    grpAliasIDFile.Visible = false;
                    rdoSortByPriorityIDListFile.Visible = false;
                    txtPriorityIDListFile.Visible = false;
                    rdoSortByIDNumber.Visible = false;
                    chkDisplayBrandNumber.Visible = false;
                    chkRemoveIDs.Visible = false;
                    rdoSortByPOSData.Visible = false;
                    rdoSortBySearchStatistics.Visible = false;
                    chkDisplayMajorBrands.Visible = false;
                    cmbSetupCodeFormating.Visible = false;
                    lblSetupCodeFormat.Visible = false;
                    chkIncludeAliasBrand.Visible = true;
                    chkIncludeAliasBrand.Enabled = true;
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;
                    tabLabelIntronSearch.Text = "Intron Search";
                    grpBxEnterPatterns.Text = "Enter introns to search";
                    this._LabelIntronSearchInputs = new LabelIntronFilters();
                    this.Text = "Intron Search";
                    break;
                case ReportType.LabelSearchWithBrand:
                    tabReportFilter.Controls.Remove(tabQuickSetInfo);
                    tabReportFilter.Controls.Remove(tabBrand);
                    tabReportFilter.Controls.Remove(tabModel);
                    tabReportFilter.Controls.Remove(tabModelInfoSpecific);
                    tabReportFilter.Controls.Remove(tabBrandBasedSearch);
                    tabReportFilter.Controls.Remove(tabKeyFunction);
                    tabReportFilter.Controls.Remove(tabUnsupportedId);
                    lblStartDate.Visible = false;
                    lblEndDate.Visible = false;
                    txtStartDate.Visible = false;
                    txtEndDate.Visible = false;
                    btnStartDate.Visible = false;
                    btnEndDate.Visible = false;
                    grpProject.Visible = false;
                    lblIDOffset.Visible = false;
                    txtIDOffset.Visible = false;
                    grpAliasIDFile.Visible = false;
                    rdoSortByPriorityIDListFile.Visible = false;
                    txtPriorityIDListFile.Visible = false;
                    rdoSortByIDNumber.Visible = false;
                    chkDisplayBrandNumber.Visible = false;
                    chkRemoveIDs.Visible = false;
                    rdoSortByPOSData.Visible = false;
                    rdoSortBySearchStatistics.Visible = false;
                    chkDisplayMajorBrands.Visible = false;
                    cmbSetupCodeFormating.Visible = false;
                    lblSetupCodeFormat.Visible = false;
                    chkIncludeAliasBrand.Visible = true;
                    chkIncludeAliasBrand.Enabled = true;
                    cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;
                    cmbBxModelTypeISCI.SelectedIndex = 0;

                    tabLabelIntronSearch.Text = "Label Search";
                    grpBxEnterPatterns.Text = "Enter labels to search";
                    this._LabelIntronSearchInputs = new LabelIntronFilters();
                    this.Text = "Label Search";                    
                    break;
            }
        }        
        #endregion

        #region Constructor
        public ReportFilterForm(ReportType _reportTYpe):this(_reportTYpe,null)
        {
           
        }
        public ReportFilterForm(ReportType _reportTYpe, IList<String> idList)
        {
            InitializeComponent();
            _ReportType = _reportTYpe;            
            _ids = new List<Id>();
            _regions = new List<Model.Region>();                        
            _deviceTypes = new List<DeviceType>();
            _dataSources = new List<DataSources>();
            _IdSetupCodeInfoParam = new IdSetupCodeInfo();
            TabSelection();
            cmbSetupCodeFormating.Text = "Base 10";
            if (idList != null)
            {
                foreach (String id in idList)
                {
                    Id idObj = new Id(id);
                    _ids.Add(idObj);
                    if (!_selectedModes.Contains(id.Substring(0, 1).ToUpper()))
                    {
                        _selectedModes.Add(id.Substring(0, 1).ToUpper());
                    }
                }
            }
            if (this._ReportType != ReportType.EdId)
            {
                LoadPlatforms();
            }
            LoadRegions();           
            LoadSubRegions();
            LoadCountries();
            LoadDataSources();
            tabReportFilter.TabChanged += new EventHandler(tabReportFilter_TabChanged);                        
            workerProcess.WorkerReportsProgress = true;
            workerProcess.WorkerSupportsCancellation = true;
            workerProcess.DoWork += new DoWorkEventHandler(workerProcess_DoWork);
            workerProcess.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workerProcess_RunWorkerCompleted);
        }
        public ReportFilterForm(ReportType _reportTYpe, IList<String> idList, String _projectname)
        {                        
            InitializeComponent();
            ProjectName = _projectname;
            _ReportType = _reportTYpe;
            
            //TabSelection();
            _ids = new List<Id>();
            _regions = new List<Model.Region>();                        
            _deviceTypes = new List<DeviceType>();
            _dataSources = new List<DataSources>();
            _IdSetupCodeInfoParam = new IdSetupCodeInfo();
            TabSelection();
            cmbSetupCodeFormating.Text = "Base 10";

            //Get Pick Id With Alias ID
            Reports _Report = new Reports();
            ArrayList _PICIDList = null;
            if(idList != null)
            {
                foreach(String id in idList)
                {
                    if(_PICIDList == null)
                        _PICIDList = new ArrayList();
                    _PICIDList.Add(id);
                }
            }
            _PICIDAliasList = _Report.GetPickIdWithAlias(ProjectName,_PICIDList);
            if (_PICIDAliasList != null)
            {
                idList = _Report.ProcessPicIDWithAlias(idList, _PICIDAliasList);
            }

            if (idList != null)
            {
                foreach (String id in idList)
                {
                    Id idObj = new Id(id);
                    _ids.Add(idObj);
                    if (!_selectedModes.Contains(id.Substring(0, 1).ToUpper()))
                    {
                        _selectedModes.Add(id.Substring(0, 1).ToUpper());
                    }
                }
            }
            if (this._ReportType != ReportType.EdId)
            {
                LoadPlatforms();
            }
            LoadRegions();
            LoadSubRegions();
            LoadCountries();
            LoadDataSources();
            tabReportFilter.TabChanged += new EventHandler(tabReportFilter_TabChanged);            
            workerProcess.WorkerReportsProgress = true;
            workerProcess.WorkerSupportsCancellation = true;
            workerProcess.DoWork += new DoWorkEventHandler(workerProcess_DoWork);
            workerProcess.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workerProcess_RunWorkerCompleted);
        }
        #endregion
        void workerProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {            
            if (e.Cancelled)
            {
            }
            else if (e.Error != null)
            {

            }
            else
            {
                if (!backWorkerCancelled)
                {
                    BindDeviceTypes();
                    this._LoadDevicesFlag = false;
                }
            }
        }
        private void workerProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            GetDeviceTypes();
        }
        private void LoadPlatforms()
        {
            Reports _Report = new Reports();
            this.Platforms = _Report.GetAllSupportedPlatforms();
        }
        private void LoadRegions()
        {
            Reports _Report = new Reports();
            _locationCollection = _Report.GetLocations();
            //IList<Model.Region> _regionList = _Report.GetRegions();
            List<Model.Region> _regionList = _locationCollection.GetRegions();
            if (_regionList.Count > 0)
            {
                chkRegions.Nodes.Add("0", "All Markets");
                foreach (Model.Region item in _regionList)
                {
                    chkRegions.Nodes[0].Nodes.Add(item.RegionID, item.Name);
                    chkRegions.Nodes[0].Nodes[item.RegionID].Tag = "Region";
                    chkRegions.Nodes[0].Nodes[item.RegionID].Checked = true;
                }
                chkRegions.ExpandAll();
                chkRegions.Nodes["0"].Checked = true;             
            }
        }
        private void LoadSubRegions()
        {
            _LoadDevicesFlag = true;
            if (chkRegions.Nodes != null)
            {
                Reports _Report = new Reports();
                foreach (TreeNode node in chkRegions.Nodes[0].Nodes)
                {
                    if (node.Checked == true)
                    {
                        Model.Region region = new Model.Region();
                        region.Name = node.Text;
                        region.RegionID = node.Name;
                        //IList<Model.Region> _subregionList = _Report.GetSubRegions(region);
                        List<Model.Region> _subregionList = _locationCollection.GetSubRegions(region);
                        if (_subregionList.Count > 0)
                        {
                            chkSubRegions.Nodes.Add(region.RegionID, region.Name);
                            chkSubRegions.Nodes[region.RegionID].Tag = "Region";
                            foreach (Model.Region item in _subregionList)
                            {
                                chkSubRegions.Nodes[region.RegionID].Nodes.Add(item.RegionID, item.Name);                                
                                //if (SubRegionSelected(region, item.RegionID))
                                //{
                                //    chkSubRegions.Nodes[region.RegionID].Nodes[item.RegionID].Checked = true;
                                //}
                                //All Sub Region Need to selected By Default
                                chkSubRegions.Nodes[region.RegionID].Nodes[item.RegionID].Tag = "SubRegion";
                                chkSubRegions.Nodes[region.RegionID].Nodes[item.RegionID].Checked = true;
                            }
                        }
                        if (!RegionExists(region))
                            _regions.Add(region);
                    }
                    else
                    {
                        Int32 _index = GetRegion(node.Name);
                        if (_index != -1)
                        {
                            _regions.RemoveAt(_index);
                        }
                    }
                }
                chkSubRegions.ExpandAll(); ;
            }
        }
        private Boolean RegionExists(Model.Region reg)
        {
            Boolean isRegionExists = false;
            if(_regions != null)
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == reg.RegionID)
                    {
                        isRegionExists = true;
                        return isRegionExists;
                    }
                }
            return isRegionExists;
        }
        private Int32 GetRegion(String _reg)
        {
            if(_regions != null)
                for (Int32 i=0;i<_regions.Count;i++)
                {
                    if (_regions[i].RegionID == _reg)
                        return i;
                }
            return -1;
        }
        private void LoadCountries()
        {
            _LoadDevicesFlag = true;
            Reports _Report = new Reports();
            foreach (TreeNode nodes in chkRegions.Nodes[0].Nodes)
            {
                if (nodes.Checked == true)
                {
                    Model.Region region = new Model.Region(nodes.Name, nodes.Text);
                    if (chkSubRegions.Nodes[nodes.Name] != null)
                    {
                        Int32 chkAll = 0;
                        foreach (TreeNode node in chkSubRegions.Nodes[nodes.Name].Nodes)
                        {
                            if (node.Checked == true)
                            {
                                if (region.SubRegions == null)
                                    region.SubRegions = new List<Model.Region>();
                                region.SubRegions.Add(new Model.Region(node.Name, node.Text));
                                chkAll++;
                            }
                            else
                            {
                                ClearSubRegions(region);
                            }
                        }
                        if (chkSubRegions.Nodes[nodes.Name].Nodes.Count == chkAll)
                        {
                            chkSubRegions.Nodes[nodes.Name].Checked = true;
                        }
                    }
                    IList<Model.Country> _countList = new List<Model.Country>();
                    if (region.SubRegions != null)
                    {
                        Int32 chkAllMain = 0;
                        foreach (Model.Region subreg in region.SubRegions)
                        {
                            //IList<Country> _countriesList = _Report.GetCountries(region, subreg, true);
                            List<Country> _countriesList = _locationCollection.GetCountries(region, subreg);
                            if (_countriesList.Count > 0)
                            {
                                if (chkCountries.Nodes != null)
                                {
                                    if (!chkCountries.Nodes.ContainsKey(region.RegionID))
                                    {
                                        chkCountries.Nodes.Add(region.RegionID, region.Name);
                                        chkCountries.Nodes[region.RegionID].Tag = "Region";
                                    }
                                }
                                else
                                {
                                    chkCountries.Nodes.Add(region.RegionID, region.Name);
                                    chkCountries.Nodes[region.RegionID].Tag = "Region";
                                }
                                if (chkCountries.Nodes[region.RegionID].Nodes != null)
                                {
                                    if (!chkCountries.Nodes[region.RegionID].Nodes.ContainsKey(subreg.RegionID))
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes.Add(subreg.RegionID, subreg.Name);
                                        chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Tag = "SubRegion";
                                    }
                                }
                                else
                                {
                                    chkCountries.Nodes[region.RegionID].Nodes.Add(subreg.RegionID, subreg.Name);
                                    chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Tag = "SubRegion";
                                }
                                Int32 chkAllSub = 0;
                                foreach (Model.Country item in _countriesList)
                                {
                                    _countList.Add(item);
                                    if (chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes != null)
                                    {
                                        if (!chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes.ContainsKey(item.CountryID))
                                        {
                                            chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes.Add(item.CountryID, item.Name);
                                            chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes[item.CountryID].Tag = "Country";
                                        }
                                    }
                                    else
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes.Add(item.CountryID, item.Name);
                                        chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes[item.CountryID].Tag = "Country";
                                    }
                                    //if (CountryExist(region, item.CountryID))
                                    //{
                                    //    Country _tempCountry = new Country(item.CountryID, item.Name, subreg.RegionID, subreg.Name);
                                    //    UpdateSubRegionforCountry(region, _tempCountry);
                                    //    chkAllSub++;
                                    //    chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes[item.CountryID].Checked = true;
                                    //}
                                    //By Default all Countries need to be Selected
                                    Country _tempCountry = new Country(item.CountryID, item.Name, subreg.RegionID, subreg.Name);
                                    UpdateSubRegionforCountry(region, _tempCountry);
                                    chkAllSub++;
                                    chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes[item.CountryID].Checked = true;
                                }
                                if (chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID] != null)
                                {
                                    if (chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes.Count == chkAllSub)
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Checked = true;
                                        chkAllMain++;
                                    }
                                }
                            }
                        }
                        //Add Reset Countries don't have Sub Devices
                        {
                            //IList<Country> _countriesList = _Report.GetCountries(region, subreg, true);
                            List<Country> _countriesList = _locationCollection.GetCountriesWithoutSubDevices(region);
                            if (_countriesList.Count > 0)
                            {
                                if (chkCountries.Nodes != null)
                                {
                                    if (!chkCountries.Nodes.ContainsKey(region.RegionID))
                                    {
                                        chkCountries.Nodes.Add(region.RegionID, region.Name);
                                        chkCountries.Nodes[region.RegionID].Tag = "Region";
                                    }
                                }
                                else
                                {
                                    chkCountries.Nodes.Add(region.RegionID, region.Name);
                                    chkCountries.Nodes[region.RegionID].Tag = "Region";
                                }
                                if (chkCountries.Nodes[region.RegionID].Nodes != null)
                                {
                                    if (!chkCountries.Nodes[region.RegionID].Nodes.ContainsKey("-1"))
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes.Add("-1", "");
                                        chkCountries.Nodes[region.RegionID].Nodes["-1"].Tag = "SubRegion";
                                    }
                                }
                                else
                                {
                                    chkCountries.Nodes[region.RegionID].Nodes.Add("-1", "");
                                    chkCountries.Nodes[region.RegionID].Nodes["-1"].Tag = "SubRegion";
                                }
                                Int32 chkAllSub = 0;
                                foreach (Model.Country item in _countriesList)
                                {
                                    _countList.Add(item);
                                    if (chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes != null)
                                    {
                                        if (!chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.ContainsKey(item.CountryID))
                                        {
                                            chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.Add(item.CountryID, item.Name);
                                            chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes[item.CountryID].Tag = "Country";
                                        }
                                    }
                                    else
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.Add(item.CountryID, item.Name);
                                        chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes[item.CountryID].Tag = "Country";
                                    }
                                    //if (CountryExist(region, item.CountryID))
                                    //{
                                    //    Country _tempCountry = new Country(item.CountryID, item.Name, subreg.RegionID, subreg.Name);
                                    //    UpdateSubRegionforCountry(region, _tempCountry);
                                    //    chkAllSub++;
                                    //    chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes[item.CountryID].Checked = true;
                                    //}
                                    //By Default all Countries need to be Selected
                                    Country _tempCountry = new Country(item.CountryID, item.Name, String.Empty, String.Empty);
                                    UpdateSubRegionforCountry(region, _tempCountry);
                                    chkAllSub++;
                                    chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes[item.CountryID].Checked = true;
                                }
                                if (chkCountries.Nodes[region.RegionID].Nodes["-1"] != null)
                                {
                                    if (chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.Count == chkAllSub)
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes["-1"].Checked = true;
                                        chkAllMain++;
                                    }
                                }
                            }
                        }
                        if (chkCountries.Nodes[region.RegionID] != null)
                            if (chkCountries.Nodes[region.RegionID].Nodes.Count == chkAllMain)
                            {
                                chkCountries.Nodes[region.RegionID].Checked = true;
                            }
                            else
                            {
                                chkCountries.Nodes[region.RegionID].Checked = false;
                            }
                        AddSubRegion(region);
                    }
                    else
                    {
                        Int32 chkAllMain = 0;
                        //IList<Country> _countriesList = _Report.GetCountries(region, subreg, true);
                        List<Country> _countriesList = _locationCollection.GetCountriesWithoutSubDevices(region);
                        if (_countriesList.Count > 0)
                        {
                            if (chkCountries.Nodes != null)
                            {
                                if (!chkCountries.Nodes.ContainsKey(region.RegionID))
                                {
                                    chkCountries.Nodes.Add(region.RegionID, region.Name);
                                    chkCountries.Nodes[region.RegionID].Tag = "Region";
                                }
                            }
                            else
                            {
                                chkCountries.Nodes.Add(region.RegionID, region.Name);
                                chkCountries.Nodes[region.RegionID].Tag = "Region";
                            }
                            if (chkCountries.Nodes[region.RegionID].Nodes != null)
                            {
                                if (!chkCountries.Nodes[region.RegionID].Nodes.ContainsKey("-1"))
                                {
                                    chkCountries.Nodes[region.RegionID].Nodes.Add("-1", "");
                                    chkCountries.Nodes[region.RegionID].Nodes["-1"].Tag = "SubRegion";
                                }
                            }
                            else
                            {
                                chkCountries.Nodes[region.RegionID].Nodes.Add("-1", "");
                                chkCountries.Nodes[region.RegionID].Nodes["-1"].Tag = "SubRegion";
                            }
                            Int32 chkAllSub = 0;
                            foreach (Model.Country item in _countriesList)
                            {
                                _countList.Add(item);
                                if (chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes != null)
                                {
                                    if (!chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.ContainsKey(item.CountryID))
                                    {
                                        chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.Add(item.CountryID, item.Name);
                                        chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes[item.CountryID].Tag = "Country";
                                    }
                                }
                                else
                                {
                                    chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.Add(item.CountryID, item.Name);
                                    chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes[item.CountryID].Tag = "Country";
                                }
                                //if (CountryExist(region, item.CountryID))
                                //{
                                //    Country _tempCountry = new Country(item.CountryID, item.Name, subreg.RegionID, subreg.Name);
                                //    UpdateSubRegionforCountry(region, _tempCountry);
                                //    chkAllSub++;
                                //    chkCountries.Nodes[region.RegionID].Nodes[subreg.RegionID].Nodes[item.CountryID].Checked = true;
                                //}
                                //By Default all Countries need to be Selected
                                Country _tempCountry = new Country(item.CountryID, item.Name, String.Empty, String.Empty);
                                UpdateSubRegionforCountry(region, _tempCountry);
                                chkAllSub++;
                                chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes[item.CountryID].Checked = true;
                            }
                            if (chkCountries.Nodes[region.RegionID].Nodes["-1"] != null)
                            {
                                if (chkCountries.Nodes[region.RegionID].Nodes["-1"].Nodes.Count == chkAllSub)
                                {
                                    chkCountries.Nodes[region.RegionID].Nodes["-1"].Checked = true;
                                    chkAllMain++;
                                }
                            }
                        }
                        if (chkCountries.Nodes[region.RegionID] != null)
                            if (chkCountries.Nodes[region.RegionID].Nodes.Count == chkAllMain)
                            {
                                chkCountries.Nodes[region.RegionID].Checked = true;
                            }
                            else
                            {
                                chkCountries.Nodes[region.RegionID].Checked = false;
                            }
                        AddSubRegion(region);
                    }
                    RemoveCountries(region, _countList);
                }                
            }            
            chkCountries.ExpandAll();
        }
        private Boolean  SubRegionSelected(Model.Region _reg, String _subreg)
        {
            Boolean isSubRegionSelected = false;
            if (_regions != null)
            {
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if(item.SubRegions != null)
                            foreach (Model.Region item1 in item.SubRegions)
                            {
                                if (item1.RegionID == _subreg)
                                {
                                    isSubRegionSelected = true;
                                    break;
                                }
                            }
                    }
                }
            }
            return isSubRegionSelected;
        }
        private Boolean CountryExist(Model.Region _reg, String _countr)
        {
            Boolean isCountrySelected = false;
            if (_regions != null)
            {
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if (item.Countries != null)
                        {
                            foreach (Model.Country item1 in item.Countries)
                            {
                                if (item1.CountryID == _countr)
                                {
                                    isCountrySelected = true;
                                    return isCountrySelected;
                                }
                            }

                        }
                    }
                }
            }
            return isCountrySelected;
        }
        private void UpdateSubRegionforCountry(Model.Region _reg, Model.Country _countr)
        {
            if (_regions != null)
            {
                Boolean isUpdated = false;
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if (item.Countries != null)
                        {
                            foreach (Model.Country item1 in item.Countries)
                            {
                                if (item1.CountryID == _countr.CountryID)
                                {
                                    if (item1.SubRegionID == String.Empty)
                                    {
                                        item1.SubRegion = _countr.SubRegion;
                                        item1.SubRegionID = _countr.SubRegionID;
                                        isUpdated = true;
                                    }
                                }
                            }
                        }
                        if (isUpdated == false)
                        {
                            if(item.Countries == null)
                                item.Countries = new List<Country>();

                            item.Countries.Add(_countr);
                        }
                    }
                }
            }            
        }
        private Boolean CountryExist(IList<Model.Country> _countryList, String _count)
        {
            Boolean isExits = false;
            foreach (Model.Country item in _countryList)
            {
                if (item.CountryID == _count)
                {
                    return true;
                }
            }
            return isExits;
        }
        private void ClearSubRegions(Model.Region _reg)
        {
            if (_regions != null)
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if (item.SubRegions != null)
                            item.SubRegions.Clear();
                        break;
                    }
                }
        }
        private void RemoveCountries(Model.Region _reg, IList<Model.Country> _countList)
        {
            if (_regions != null)
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if (item.Countries != null)
                        {
                            for (int i = 0; i < item.Countries.Count; i++)
                            {
                                if (!CountryExist(_countList, item.Countries[i].CountryID))
                                {
                                    item.Countries.RemoveAt(i);
                                    i = -1;
                                }
                            } 
                        }
                    }
                }
        }
        private void AddSubRegion(Model.Region _reg)
        {
            if(_regions != null)
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if (item.SubRegions == null)
                            item.SubRegions = new List<Model.Region>();
                        else
                            item.SubRegions.Clear();
                        item.SubRegions = _reg.SubRegions;
                        break;
                    }
                }
        }
        private void AddCountries(Model.Region _reg)
        {
            if (_regions != null)
                foreach (Model.Region item in _regions)
                {
                    if (item.RegionID == _reg.RegionID)
                    {
                        if (item.Countries == null)
                            item.Countries = new List<Model.Country>();
                        else                       
                            item.Countries.Clear();
                        item.Countries = _reg.Countries;
                        break;
                    }
                }
        }
        private Int32 GetCheckedNodes(TreeNode node)
        {
            Int32 checkedNodes = 0;
            if (node != null)
                foreach (TreeNode child in node.Nodes)
                {
                    if (child.Checked)
                        checkedNodes++;
                }
            return checkedNodes;
        }
        private void checkNodes(TreeNode node, bool check)
        {
            if(node != null)
                foreach (TreeNode child in node.Nodes)
                {
                    child.Checked = check;
                    checkNodes(child, check);
                }
        }
        private void chkRegions_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {                
                TreeNode node = ((TreeNode)e.Node);                
                if (node != null)
                {
                    if (node.Text == "All Markets")
                    {
                        chkRegions.Nodes["0"].Checked = node.Checked;
                        checkNodes(chkRegions.Nodes["0"], node.Checked);
                        if(node.Checked == true)
                            chkUnknownLocation.Checked = true;
                        else
                        {
                            chkUnknownLocation.Checked = false;                            
                        }
                    }
                    else
                    {
                        if (GetCheckedNodes(chkRegions.Nodes["0"]) == chkRegions.Nodes["0"].Nodes.Count)
                        {
                            chkUnknownLocation.Checked = true;
                            chkRegions.Nodes["0"].Checked = true;
                        }
                        else if (GetCheckedNodes(chkRegions.Nodes["0"]) == 0)
                        {
                            chkUnknownLocation.Checked = true;                          
                            chkRegions.Nodes["0"].Checked = true;
                            checkNodes(chkRegions.Nodes["0"], true);
                        }
                        else
                        {                            
                            chkUnknownLocation.Checked = false;                            
                            chkRegions.Nodes["0"].Checked = false;
                        }
                    }
                    if (chkSubRegions.Nodes != null)
                        chkSubRegions.Nodes.Clear();
                    if (chkCountries.Nodes != null)
                        chkCountries.Nodes.Clear();
                    LoadSubRegions();
                    LoadCountries();
                }                
            }
        }
        private void chkSubRegions_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    if (node.Tag.ToString() == "Region")
                    {
                        checkNodes(node, node.Checked);
                    }
                    else if (node.Tag.ToString() == "SubRegion")
                    {
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Checked = false;
                        }
                    }
                    if (chkCountries.Nodes != null)
                        chkCountries.Nodes.Clear();
                    LoadCountries();
                }                
            }
        }
        private void chkCountries_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    if (node.Tag.ToString() == "Region")
                    {
                        checkNodes(node, node.Checked);
                    }
                    else if (node.Tag.ToString() == "SubRegion")
                    {
                        checkNodes(node, node.Checked);
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Checked = false;
                        }
                    }
                    else if (node.Tag.ToString() == "Country")
                    {
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                            if (node.Parent.Parent != null)
                            {
                                if (node.Parent.Parent.Nodes.Count == GetCheckedNodes(node.Parent.Parent))
                                {
                                    node.Parent.Parent.Checked = true;
                                }
                                else
                                {
                                    node.Parent.Parent.Checked = false;
                                }
                            }
                        }
                        else
                        {
                            node.Parent.Checked = false;
                            if (node.Parent.Parent != null)
                                node.Parent.Parent.Checked = false;
                        }
                    }
                    BindCountries();
                }
            }
        }
        private void BindCountries()
        {
            _LoadDevicesFlag = true;
            if (chkCountries.Nodes != null)
            {
                foreach (TreeNode nodes in chkCountries.Nodes)
                {
                    Model.Region region = new Model.Region(nodes.Name, nodes.Text);
                    foreach (TreeNode node in nodes.Nodes)
                    {
                        if (node.Nodes.Count > 0)
                        {
                            foreach (TreeNode no in node.Nodes)
                            {
                                if (no.Checked == true)
                                {
                                    //if sub region is present for the region
                                    //add Countries
                                    if (region.Countries == null)
                                        region.Countries = new List<Model.Country>();
                                    region.Countries.Add(new Country(no.Name, no.Text,node.Name,node.Text));
                                }
                            }
                        }
                        else
                        {
                            //if No Sub regions are availbale for the region
                            //Add Countries
                            if (node.Checked == true)
                            {
                                if (region.Countries == null)
                                    region.Countries = new List<Model.Country>();
                                region.Countries.Add(new Country(node.Name, node.Text));
                            }
                        }
                    }
                    AddCountries(region);
                }
            }
        }
        IList<DeviceType> deviceList = new List<DeviceType>();
        DeviceModel deviceCollection = new DeviceModel();
        #region DeviceTypes
        private void LoadDeviceTypes()
        {
            if(_deviceTypes.Count>0)
                _deviceTypes.Clear();            

            if (chkMainDevices.Nodes != null)
                chkMainDevices.Nodes.Clear();
            if (chkSubDevices.Nodes != null)
                chkSubDevices.Nodes.Clear();
            if (chkComponents.Nodes != null)
                chkComponents.Nodes.Clear();

            chkMainDevices.Nodes.Add("0", "Please Wait...");                                                
            chkMainDevices.Enabled = false;
            if (workerProcess != null && workerProcess.IsBusy == false)
                workerProcess.RunWorkerAsync();            
        }
        private void BindDeviceTypes()
        {            
            if (chkMainDevices.Nodes != null)
                chkMainDevices.Nodes.Clear();
            if (chkSubDevices.Nodes != null)
                chkSubDevices.Nodes.Clear();
            if (chkComponents.Nodes != null)
                chkComponents.Nodes.Clear();

            if (deviceCollection != null && deviceCollection.IsDeviceAvailable)
            {
                chkMainDevices.Enabled = true;
                foreach (String mode in _selectedModes)
                {                                        
                    List<DeviceType> devices = deviceCollection.GetDevices(mode);
                    if (devices.Count > 0)
                    {
                        TreeNode modeNode = new TreeNode(mode);
                        modeNode.Name = mode;
                        modeNode.Tag = "Mode";
                        foreach (DeviceType item in devices)
                        {
                            modeNode.Nodes.Add(item.Name, item.Name);
                            modeNode.Nodes[item.Name].Tag = "MainDevice";
                        }
                        chkMainDevices.Nodes.Add(modeNode);
                    }                    
                }
                chkMainDevices.ExpandAll();  
            }
            else
            {
                chkMainDevices.Nodes.Add("0", "No Device Found");
                chkMainDevices.Enabled = false;
            }            
        }        
        private void GetDeviceTypes()
        {
            Reports _Report = new Reports();
            //temporarly Handling Datasource for Telematry EDID records to load uei2 device types
            IList<DataSources> tempDataSources = new List<DataSources>();
            foreach (DataSources item in _dataSources)
            {
                if (item.Name.Contains("Telemetry EDID"))
                {
                    tempDataSources.Add(new DataSources("Capture EDID"));
                }
                else
                {
                    tempDataSources.Add(new DataSources(item.Name));
                }
            }
            //deviceCollection = _Report.LoadCompleteDevices(SelectedType.ToString(), _selectedModes, _ids, _dataSources, _regions, IdSetupCodeInfoParam);
            deviceCollection = _Report.LoadCompleteDevices(SelectedType.ToString(), _selectedModes, _ids, tempDataSources, _regions, IdSetupCodeInfoParam);
       }
        #endregion
        private IList<Model.DeviceType> GetSubDevices(String mode,String mD)
        {
            List<DeviceType> mainDevices = deviceCollection.GetDevices(mode);
            List<DeviceType> subDevices = new List<DeviceType>();
            if (mainDevices != null && mainDevices.Count > 0)
            {
                foreach (DeviceType mainDevice in mainDevices)
                {
                    if (mainDevice.Name == mD)
                        subDevices = mainDevice.SubDevices;
                }
            }
            return subDevices;
        }
        private void LoadSubDeviceTypes()
        {            
            if (chkMainDevices.Nodes != null)
            {
                foreach (TreeNode modeNode in chkMainDevices.Nodes)
                {
                    if (modeNode.Nodes != null)
                    {
                        Int32 chkMode = 0;
                        foreach (TreeNode deviceNode in modeNode.Nodes)
                        {
                            if (deviceNode.Checked == true)
                            {
                                Model.DeviceType device = new Model.DeviceType();
                                device.Name = deviceNode.Text;
                                device.DeviceTypeID = deviceNode.Name;
                                device.Mode = deviceNode.Parent.Name;
                                IList<Model.DeviceType> _subdeviceList = GetSubDevices(modeNode.Text, deviceNode.Text);

                                if (_subdeviceList.Count > 0)
                                {
                                    if (!chkSubDevices.Nodes.ContainsKey(device.Mode))
                                    {
                                        chkSubDevices.Nodes.Add(device.Mode,device.Mode);
                                        chkSubDevices.Nodes[device.Mode].Tag = "Mode";
                                    }
                                    if (chkSubDevices.Nodes != null)
                                    {
                                        if (!chkSubDevices.Nodes[device.Mode].Nodes.ContainsKey(device.Name))
                                        {
                                            chkSubDevices.Nodes[device.Mode].Nodes.Add(device.Name, device.Name);
                                            chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Tag = "MainDevice";
                                        }
                                    }
                                    Int32 chkMainDevice = 0;                  
                                    foreach (Model.DeviceType subdevice in _subdeviceList)
                                    {
                                        if (chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Nodes != null)
                                        {
                                            if (!chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Nodes.ContainsKey(subdevice.Name))
                                            {
                                                chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Nodes.Add(subdevice.Name, subdevice.Name);
                                                chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Nodes[subdevice.Name].Tag = "SubDevice";
                                            }
                                        }                                       
                                        //if (SubDeviceExist(device, subdevice.Name))
                                        //{
                                        //    chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Nodes[subdevice.Name].Checked = true;
                                        //}
                                        //By Default all Sub Devices need to be Selected
                                        chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Nodes[subdevice.Name].Checked = true;
                                        chkMainDevice++;
                                    }

                                    if(GetCheckedNodes(chkSubDevices.Nodes[device.Mode].Nodes[device.Name]) == chkMainDevice)
                                    {
                                        chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Checked = true;
                                        chkMode++;
                                    }
                                    else
                                    {
                                        chkSubDevices.Nodes[device.Mode].Nodes[device.Name].Checked = false;
                                    }                                    
                                }
                                if (!isMainDeviceExist(device))
                                {
                                    AddMainDevices(device);
                                }                                
                            }
                            else
                            {
                                Int32 index = GetMainDeviceIndex(deviceNode.Name,deviceNode.Parent.Name);
                                if (index != -1)
                                {
                                    _deviceTypes.RemoveAt(index);
                                }
                            }
                        }
                        if(chkSubDevices.Nodes.ContainsKey(modeNode.Text))
                        {
                            if(GetCheckedNodes(chkSubDevices.Nodes[modeNode.Text]) == chkMode)
                            {
                                chkSubDevices.Nodes[modeNode.Text].Checked = true;
                            }
                            else
                            {
                                chkSubDevices.Nodes[modeNode.Text].Checked = false;
                            }
                        }
                    }
                }
                chkSubDevices.ExpandAll();
            }            
        }        
        private void LoadComponents()
        {
            if (chkComponents.Nodes != null)
                chkComponents.Nodes.Clear();
            Int32 chkMode = 0, chkMainDevice = 0, chkAllSubDevice = 0;
            if(chkSubDevices.Nodes != null)
            {
                foreach(TreeNode modeNode in chkSubDevices.Nodes)
                {
                    if(modeNode.Nodes != null)
                    {
                        chkMode = 0;
                        foreach(TreeNode deviceNode in modeNode.Nodes)
                        {
                            if(deviceNode.Nodes != null)
                            {
                                chkMainDevice = 0;
                                Model.DeviceType device = new Model.DeviceType();
                                device.Name = deviceNode.Text;
                                device.DeviceTypeID = deviceNode.Name;
                                device.Mode = modeNode.Name;
                                foreach(TreeNode subDeviceNode in deviceNode.Nodes)
                                {                                   
                                    if(subDeviceNode.Checked == true)
                                    {
                                        if(device.SubDevices == null)
                                            device.SubDevices = new List<Model.DeviceType>();
                                        device.SubDevices.Add(new DeviceType(subDeviceNode.Name, subDeviceNode.Text));
                                    }
                                    else
                                    {
                                        ClearSubDevices(device);
                                    }
                                }
                                //IList<Model.Component> _compoList = new List<Model.Component>();
                                if(device.SubDevices.Count != 0)//subdevice is selected
                                {                                   
                                    foreach(Model.DeviceType subdev in device.SubDevices)
                                    {
                                        IList<Model.Component> _componentList = GetComponents(modeNode.Text, device.Name, subdev.Name);
                                        if(_componentList.Count > 0)
                                        {
                                            if(!chkComponents.Nodes.ContainsKey(device.Mode))
                                            {
                                                chkComponents.Nodes.Add(device.Mode, device.Mode);
                                                chkComponents.Nodes[device.Mode].Tag = "Mode";
                                            }
                                            if(!chkComponents.Nodes[device.Mode].Nodes.ContainsKey(device.Name))
                                            {
                                                chkComponents.Nodes[device.Mode].Nodes.Add(device.Name, device.Name);
                                                chkComponents.Nodes[device.Mode].Nodes[device.Name].Tag = "MainDevice";
                                            }
                                            if(!chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes.ContainsKey(subdev.Name))
                                            {
                                                chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes.Add(subdev.Name, subdev.Name);
                                                chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Tag = "SubDevice";
                                            }
                                            chkAllSubDevice = 0;
                                            foreach(Model.Component compo in _componentList)
                                            {
                                                //_compoList.Add(compo);
                                                if(!chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Nodes.ContainsKey(compo.Name))
                                                {
                                                    chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Nodes.Add(compo.Name, compo.Name);
                                                    chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Nodes[compo.Name].Tag = "Component";

                                                    //By Default All Component need to be Selected
                                                    Model.Component _tempComponent = new Model.Component(compo.CompanentID, compo.Name, subdev.DeviceTypeID, subdev.Name);
                                                    UpdateSubDevicesforComponent(device, _tempComponent);                                                    
                                                    chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Nodes[compo.Name].Checked = true;
                                                    chkAllSubDevice++;
                                                }
                                            }
                                            if(chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name] != null)
                                            {
                                                if(GetCheckedNodes(chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name]) == chkAllSubDevice)
                                                {
                                                    chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Checked = true;
                                                    chkMainDevice++;
                                                }
                                                else
                                                {
                                                    chkComponents.Nodes[device.Mode].Nodes[device.Name].Nodes[subdev.Name].Checked = false;
                                                }
                                            }
                                        }
                                    }
                                }
                                AddSubDevice(device);
                                //RemoveComponents(device, _compoList);
                                if(chkComponents.Nodes[device.Mode] != null)
                                {
                                    if(chkComponents.Nodes[device.Mode].Nodes[device.Name] != null)
                                    {
                                        if(GetCheckedNodes(chkComponents.Nodes[device.Mode].Nodes[device.Name]) == chkMainDevice)
                                        {
                                            chkComponents.Nodes[device.Mode].Nodes[device.Name].Checked = true;
                                            chkMode++;
                                        }
                                        else
                                        {
                                            chkComponents.Nodes[device.Mode].Nodes[device.Name].Checked = false;
                                        }
                                    }
                                }
                            }                           
                        }
                        if(chkComponents.Nodes[modeNode.Name] != null)
                        {
                            if(GetCheckedNodes(chkComponents.Nodes[modeNode.Name]) == chkMode)
                            {
                                chkComponents.Nodes[modeNode.Name].Checked = true;
                            }
                            else
                            {
                                chkComponents.Nodes[modeNode.Name].Checked = false;
                            }
                        }
                        if(chkComponents.Nodes != null)
                        {
                            chkComponents.ExpandAll();
                        }
                    }
                }
            }
        }
        private IList<Model.Component> GetComponents(string mode, string device, string subdevice)
        {
            DeviceType mainDevice = deviceCollection.FindDevice(mode, device);
            List<Model.Component> compList = new List<UEI.Workflow2010.Report.Model.Component>();
            if (mainDevice != null)
            {
                if (!String.IsNullOrEmpty(subdevice))
                {
                    DeviceType subDeviceType = mainDevice.FindSubDevice(subdevice);
                    if (subDeviceType != null)
                    {
                        compList = subDeviceType.Components;
                    }
                }
                else
                {
                    
                    foreach (DeviceType subDeviceType in mainDevice.SubDevices)
                    {
                        foreach (Model.Component comp in subDeviceType.Components)
                        {
                            if (!compList.Contains(comp))
                                compList.Add(comp);
                            
                        }
                    }

                   
                }
            }
            return compList;
        }
        private void chkMainDevices_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {                    
                    if (node.Tag.ToString() == "Mode")
                    {
                        checkNodes(node, node.Checked);
                    }
                    else
                    {                        
                        Int32 checkedNodes = GetCheckedNodes(node.Parent);
                        if (node.Parent.Nodes.Count > checkedNodes)
                        {
                            chkMainDevices.Nodes[node.Parent.Name].Checked = false;
                        }
                        if (node.Parent.Nodes.Count == checkedNodes)
                        {
                            chkMainDevices.Nodes[node.Parent.Name].Checked = true;
                        }
                    }
                    if (chkSubDevices.Nodes != null)
                        chkSubDevices.Nodes.Clear();
                    if (chkComponents.Nodes != null)
                        chkComponents.Nodes.Clear();
                    LoadSubDeviceTypes();
                    LoadComponents();
                }
            }
        }
        private void chkSubDevices_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    if (node.Tag.ToString() == "Mode")
                    {
                        checkNodes(node, node.Checked);
                    }
                    else if (node.Tag.ToString() == "MainDevice")
                    {
                        checkNodes(node, node.Checked);
                        Int32 checkedNodes = GetCheckedNodes(node.Parent);
                        if (node.Parent.Nodes.Count > checkedNodes)
                        {
                            chkSubDevices.Nodes[node.Parent.Name].Checked = false;
                        }
                        if (node.Parent.Nodes.Count == checkedNodes)
                        {
                            chkSubDevices.Nodes[node.Parent.Name].Checked = true;
                        }                        
                    }
                    else
                    {
                        //Check all Sub Device under Main Device
                        Int32 checkedNodes = GetCheckedNodes(node.Parent);
                        if (node.Parent.Nodes.Count > checkedNodes)
                        {
                            chkSubDevices.Nodes[node.Parent.Parent.Name].Nodes[node.Parent.Name].Checked = false;
                        }
                        if (node.Parent.Nodes.Count == checkedNodes)
                        {
                            chkSubDevices.Nodes[node.Parent.Parent.Name].Nodes[node.Parent.Name].Checked = true;
                        }
                        //Check All Main Devices under Mode
                        checkedNodes = GetCheckedNodes(node.Parent.Parent);
                        if (node.Parent.Parent.Nodes.Count > checkedNodes)
                        {
                            chkSubDevices.Nodes[node.Parent.Parent.Name].Checked = false;
                        }
                        if (node.Parent.Parent.Nodes.Count == checkedNodes)
                        {
                            chkSubDevices.Nodes[node.Parent.Parent.Name].Checked = true;
                        }
                    }
                    if (chkComponents.Nodes != null)
                        chkComponents.Nodes.Clear();
                    LoadComponents();
                }
            }
        }
        private void chkComponents_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    if (node.Tag.ToString() == "Mode")
                    {
                        checkNodes(node, node.Checked);
                    }
                    if (node.Tag.ToString() == "MainDevice")
                    {
                        checkNodes(node, node.Checked);
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Checked = false;
                        }
                    }
                    else if (node.Tag.ToString() == "SubDevice")
                    {
                        checkNodes(node, node.Checked);
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Checked = false;
                        }
                        if (node.Parent.Parent.Nodes.Count == GetCheckedNodes(node.Parent.Parent))
                        {
                            node.Parent.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Parent.Checked = false;
                        }
                    }
                    else if (node.Tag.ToString() == "Component")
                    {
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                            if (node.Parent.Parent != null)
                            {
                                if (node.Parent.Parent.Nodes.Count == GetCheckedNodes(node.Parent.Parent))
                                {
                                    node.Parent.Parent.Checked = true;
                                }
                                else
                                {
                                    node.Parent.Parent.Checked = false;
                                }
                            }
                            if (node.Parent.Parent.Parent != null)
                            {
                                if (node.Parent.Parent.Parent.Nodes.Count == GetCheckedNodes(node.Parent.Parent.Parent))
                                {
                                    node.Parent.Parent.Parent.Checked = true;
                                }
                                else
                                {
                                    node.Parent.Parent.Parent.Checked = false;
                                }
                            }
                        }
                        else
                        {
                            node.Parent.Checked = false;
                            if (node.Parent.Parent != null)
                            {
                                node.Parent.Parent.Checked = false;
                            }
                            if (node.Parent.Parent.Parent != null)
                            {
                                node.Parent.Parent.Parent.Checked = false;
                            }
                        }
                    }
                    BindComponents();
                }
            }
        }
        private Boolean isMainDeviceExist(Model.DeviceType dev)
        {
            Boolean isExists = false;
            if(_deviceTypes != null)
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if (item.Name == dev.Name && item.Mode == dev.Mode)
                    {
                        isExists = true;
                        return isExists;
                    }
                }
            return isExists;
        }
        private void AddMainDevices(Model.DeviceType dev)
        {            
            if (_deviceTypes == null)
                _deviceTypes = new List<Model.DeviceType>();            
            _deviceTypes.Add(dev);
        }
        private Int32 GetMainDeviceIndex(String dev, String mode)
        {
            if (_deviceTypes != null)
                for (Int32 i = 0; i < _deviceTypes.Count;i++ )
                {
                    if (_deviceTypes[i].Name == dev && _deviceTypes[i].Mode == mode)
                    {
                        return i;
                    }
                }
            return -1;
        }
        private void AddSubDevice(Model.DeviceType _dev)
        {
            if (_deviceTypes != null)
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if (item.Name == _dev.Name && item.Mode == _dev.Mode)
                    {
                        if (item.SubDevices == null)
                            item.SubDevices = new List<Model.DeviceType>();
                        else
                            item.SubDevices.Clear();
                        item.SubDevices = _dev.SubDevices;
                        break;
                    }
                }
        }
        private void BindComponents()
        {
            if (chkComponents.Nodes != null)
            {
                //Mode
                foreach (TreeNode modeNode in chkComponents.Nodes)
                {
                    if (modeNode.Nodes != null)
                    {
                        //Main Device
                        foreach (TreeNode nodes in modeNode.Nodes)
                        {
                            Model.DeviceType device = new Model.DeviceType(modeNode.Name,nodes.Name, nodes.Text);
                            foreach (TreeNode node in nodes.Nodes)
                            {
                                if (node.Nodes.Count > 0)
                                {
                                    foreach (TreeNode no in node.Nodes)
                                    {
                                        if (no.Checked == true)
                                        {
                                            //if sub device is present for the device
                                            //Add Component
                                            if (device.Components == null)
                                                device.Components = new List<Model.Component>();
                                            device.Components.Add(new Model.Component(no.Name, no.Text, node.Name, node.Text));
                                        }
                                    }
                                }
                                else
                                {
                                    //if No Sub Devices are availbale for the device
                                    //Add Components
                                    if (node.Checked == true)
                                    {
                                        if (device.Components == null)
                                            device.Components = new List<Model.Component>();
                                        device.Components.Add(new Model.Component(node.Name, node.Text));
                                    }
                                }
                            }
                            AddComponents(device);
                        }
                    }                    
                }
            }
        }
        private void AddComponents(Model.DeviceType _dev)
        {
            if (_deviceTypes != null)
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if (item.Name == _dev.Name && item.Mode == _dev.Mode)
                    {
                        if (item.Components == null)
                            item.Components = new List<Model.Component>();
                        else
                            item.Components.Clear();
                        item.Components = _dev.Components;                        
                    }
                }
        }
        private void UpdateSubDevicesforComponent(Model.DeviceType _dev, Model.Component _compo)
        {
            Boolean isUpdated = false;
            if (_deviceTypes != null)
            {
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if(item.Name == _dev.Name && item.Mode == _dev.Mode)
                    {
                        if(item.Components == null)
                            item.Components = new List<Model.Component>();
                        item.Components.Add(_compo);
                    }
                }
            }            
        }       
        private Boolean ComponentExist(IList<Model.Component> _compoList, Model.Component _compo)
        {
            Boolean isExits = false;
            foreach (Model.Component item in _compoList)
            {
                if (item.Name == _compo.Name && item.SubDeviceType == _compo.SubDeviceType)
                {
                    return true;
                }
            }
            return isExits;
        }
        private void RemoveComponents(Model.DeviceType _dev, IList<Model.Component> _compo)
        {            
            if (_deviceTypes != null)
            {
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if (item.Name == _dev.Name && item.Mode == _dev.Mode)
                    {
                        if (item.Components != null)
                        {                            
                            for (int i = 0; i < item.Components.Count; i++)
                            {
                                if (ComponentExist( _compo,item.Components[i]))
                                {
                                    item.Components.RemoveAt(i);
                                    i = -1;                                    
                                }
                            }                           
                        }
                    }
                }
            }         
        }
        private Boolean SubDeviceExist(Model.DeviceType _dev, String _subdev)
        {
            Boolean isExists = false;
            if (_deviceTypes != null)
            {
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if (item.Name == _dev.Name && item.Mode == _dev.Mode)
                    {
                        if (item.SubDevices != null)
                            foreach (Model.DeviceType item1 in item.SubDevices)
                            {
                                if (item1.Name == _subdev)
                                {
                                    isExists = true;
                                    return isExists;
                                }
                            }
                    }
                }
            }
            return isExists;
        }
        private void ClearSubDevices(Model.DeviceType _dev)
        {
            if (_deviceTypes != null)
                foreach (Model.DeviceType item in _deviceTypes)
                {
                    if (item.Name == _dev.Name && item.Mode == _dev.Mode)
                    {                        
                        RemoveComponents(item, item.Components);
                        if (item.SubDevices != null)
                            item.SubDevices.Clear();
                        break;
                    }
                }
        }
        
        #region Load DataSources
        private void LoadDataSources()
        {            
            Reports _Report = new Reports();
            IList<DataSources> _dataSourcesList = _Report.GetAllDataSources();
            if (_dataSourcesList.Count > 0)
            {
                treeDataSources.Nodes.Add("DataSources", "DataSources");
                treeDataSources.Nodes["DataSources"].Tag = "All";
                treeDataSources.Nodes["DataSources"].Checked = true; 
                foreach (DataSources item in _dataSourcesList)
                {
                    if (item.Name.Contains("CF"))
                    {
                        if (item.Name.Contains(":"))
                        {
                            String[] _SubDataSource = item.Name.Split(':');
                            if(treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]] == null)
                            {
                                treeDataSources.Nodes["DataSources"].Nodes.Add(_SubDataSource[0], _SubDataSource[0]);
                                treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Tag = "DataSource";
                                treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Checked = true;
                            }
                            treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Nodes.Add(_SubDataSource[1], _SubDataSource[1]);
                            treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Nodes[_SubDataSource[1]].Tag = "SubDataSource";
                            treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Nodes[_SubDataSource[1]].Checked = true;
                        }
                        else
                        {
                            treeDataSources.Nodes["DataSources"].Nodes.Add(item.Name, item.Name);
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Tag = "DataSource";
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Checked = true;

                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Nodes.Add(item.Name, item.Name);
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Nodes[item.Name].Tag = "SubDataSource";
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Nodes[item.Name].Checked = true;
                        }
                    }
                    else
                    {
                        treeDataSources.Nodes["DataSources"].Nodes.Add(item.Name, item.Name);
                        treeDataSources.Nodes["DataSources"].Nodes[item.Name].Tag = "DataSource";
                        treeDataSources.Nodes["DataSources"].Nodes[item.Name].Checked = true;
                    }
                    _dataSources.Add(new DataSources(item.Name));
                }
                treeDataSources.Nodes["DataSources"].Expand();
            }
        }
        private void treeDataSources_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    switch (node.Tag.ToString())
                    {
                        case "All":
                            checkNodes(node, node.Checked);
                            break;
                        case "DataSource":
                            if (node.Text == "CF")
                            {
                                checkNodes(node, node.Checked);
                            }
                            if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                            {
                                node.Parent.Checked = true;
                            }
                            else
                            {
                                node.Parent.Checked = false;
                            }
                            break;
                        case "SubDataSource":
                            if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                            {
                                node.Parent.Checked = true;
                            }
                            else
                            {
                                node.Parent.Checked = false;
                            }
                            if (node.Parent.Parent.Nodes.Count == GetCheckedNodes(node.Parent.Parent))
                            {
                                node.Parent.Parent.Checked = true;
                            }
                            else
                            {
                                node.Parent.Parent.Checked = false;
                            }
                            break;
                    }
                    SelectedDataSources();
                }
            }
        }
        private void SelectedDataSources()
        {
            if (treeDataSources.Nodes != null)
            {
                _LoadDevicesFlag = true;
                _dataSources = new List<DataSources>();
                foreach (TreeNode mainNode in treeDataSources.Nodes)
                {
                    if (mainNode.Nodes != null)
                    {
                        foreach (TreeNode datasourceNode in mainNode.Nodes)
                        {
                            if (datasourceNode.Checked == true)
                            {
                                if (datasourceNode.Text != "CF")
                                    _dataSources.Add(new DataSources(datasourceNode.Text));                                                                 
                            }
                            if (datasourceNode.Nodes != null)
                            {
                                foreach (TreeNode subdatasourceNode in datasourceNode.Nodes)
                                {
                                    if (subdatasourceNode.Checked == true)
                                    {
                                        if (subdatasourceNode.Text != "CF")
                                            _dataSources.Add(new DataSources(datasourceNode.Text + ":" + subdatasourceNode.Text));
                                        else
                                            _dataSources.Add(new DataSources(subdatasourceNode.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {                           
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        private void tabReportFilter_TabChanged(object sender, EventArgs e)
        {
            if (tabReportFilter.SelectedTab.Text == "QuickSet Info")
            {
                if (this._regions == null || this._regions.Count == 0)
                {                   
                    MessageBox.Show("Region is a mandatory input, It cannot be left blank.", "Invalid Inputs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else if (this._dataSources == null || this._dataSources.Count == 0)
                {                   
                    MessageBox.Show("Datasource is a mandatory input, It cannot be left blank.", "Invalid Inputs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    if (this._LoadDevicesFlag)
                        LoadQuickSetSubDeviceTypes();
                }
            }
            if (tabReportFilter.SelectedTab.Text == "Device")
            {               
                if (this._regions == null || this._regions.Count == 0)
                {
                    if (chkMainDevices.Nodes != null)
                        chkMainDevices.Nodes.Clear();
                    if (chkSubDevices.Nodes != null)
                        chkSubDevices.Nodes.Clear();
                    if (chkComponents.Nodes != null)
                        chkComponents.Nodes.Clear();
                    MessageBox.Show("Region is a mandatory input, It cannot be left blank.", "Invalid Inputs", MessageBoxButtons.OK, MessageBoxIcon.Stop);                    
                }
                else if (this._dataSources == null || this._dataSources.Count == 0)
                {
                    if (chkMainDevices.Nodes != null)
                        chkMainDevices.Nodes.Clear();
                    if (chkSubDevices.Nodes != null)
                        chkSubDevices.Nodes.Clear();
                    if (chkComponents.Nodes != null)
                        chkComponents.Nodes.Clear();
                    MessageBox.Show("Datasource is a mandatory input, It cannot be left blank.", "Invalid Inputs", MessageBoxButtons.OK, MessageBoxIcon.Stop);                    
                }
                else
                {
                    if (workerProcess != null)
                        workerProcess.CancelAsync();
                    //check whether the _LoadDevicesFlag is true
                    if (chkMainDevices.Nodes.Count != 0 && this._LoadDevicesFlag == true)
                    {
                        chkMainDevices.Nodes.Clear();
                    }
                    if (this._LoadDevicesFlag)
                        LoadDeviceTypes();
                }
            }          
        }       
        private void btnOk_Click(object sender, EventArgs e)
        {                       
            if (this._ReportType == ReportType.EdId)
            {
                if (this._brandSpecificSel == null)
                    this._brandSpecificSel = new BrandFilter();
                ListBox.ObjectCollection listBoxObjects = null;
                if (lstMatchBrands.Items.Count > 0)
                {
                    this._brandSpecificSel.MatchSelectedBrands = true;
                    listBoxObjects = new ListBox.ObjectCollection(lstMatchBrands);

                    foreach (object item in lstMatchBrands.Items)
                        if (lstFilterBrands.FindStringExact(item.ToString()) == ListBox.NoMatches)
                            listBoxObjects.Add(item);

                    if (listBoxObjects != null)
                    {
                        this._brandSpecificSel.SelectedBrands.Clear();
                        foreach (object item in listBoxObjects)
                            this._brandSpecificSel.SelectedBrands.Add(item.ToString());
                    }
                }

                if (this._modelSpecificSel == null)
                    this._modelSpecificSel = new ModelFilter();
                listBoxObjects = null;
                if (lstMatchModels.Items.Count > 0)
                {
                    this._modelSpecificSel.MatchSelectedModels = true;
                    listBoxObjects = new ListBox.ObjectCollection(lstMatchModels);

                    foreach (object item in lstMatchModels.Items)
                        if (lstFilterModels.FindStringExact(item.ToString()) == ListBox.NoMatches)
                            listBoxObjects.Add(item);

                    if (listBoxObjects != null)
                    {
                        this._modelSpecificSel.SelectedModels.Clear();
                        foreach (object item in listBoxObjects)
                            this._modelSpecificSel.SelectedModels.Add(item.ToString());
                    }
                }
            }
            if ((this._ReportType == ReportType.ModelInformation) || (this._ReportType == ReportType.ModelInfoWithArdKey) || (this._ReportType == ReportType.ModelInformation_EmptyDevice) || (this._ReportType == ReportType.QuickSet))
            {
                GetSupportedPlatform();
                GetModelInfoReportSpecificInputs();
            }                        
            if (this._ReportType == ReportType.IDList)
            {
                GetSupportedPlatform();
                _IdSetupCodeInfoParam.StartDate = txtStartDate.Text;
                _IdSetupCodeInfoParam.EndDate = txtEndDate.Text;
            }          
            if (this._ReportType == ReportType.BrandBasedSearch)
            {
                if (chkPOSAvail.Checked == true && chkOSSAvail.Checked == true)
                    brandbasedSeach.Avail = 3;
                else if (chkOSSAvail.Checked == true)
                    brandbasedSeach.Avail = 2;
                else if (chkPOSAvail.Checked)
                    brandbasedSeach.Avail = 1;
                else if (chkUEI2Avail.Checked == true)
                    brandbasedSeach.Avail = 0;
                if (chkMode.Checked == true)
                    brandbasedSeach.Mode = true;
                if (chkDataSource.Checked == true)
                    brandbasedSeach.DataSource = true;
                if (chkModel.Checked == true)
                {
                    brandbasedSeach.Model = true;
                    //get model related inputs.
                    brandbasedSeach.IncludeBlankModelNames = chkBxBlankModelNameBBS.Checked;
                    brandbasedSeach.ModelType = GetModelTypeValue(chkBxBlankModelNameBBS.Text);
                }
                brandbasedSeach.POSStartDate = txtPOSStartDate.Text;
                brandbasedSeach.POSEndDate = txtPOSEndDate.Text;
                brandbasedSeach.OSSStartDate = txtOSSStartDate.Text;
                brandbasedSeach.OSSEndDate = txtOSSEndDate.Text;
            }
            if (this._ReportType == ReportType.LabelSearch || this._ReportType == ReportType.IntronSearch || this._ReportType == ReportType.ExcludeLabelSearch || this._ReportType == ReportType.ExcludeIntronSearch)
            {
                GetLabelIntronSearchInputs();
            }
            if (this._ReportType == ReportType.IntronSearchWithBrand)
            {
                GetLabelIntronSearchInputs();
                this._LabelIntronSearchInputs.IsIntronSearch = true;
            }
            if (this._ReportType == ReportType.LabelSearchWithBrand)
            {
                GetLabelIntronSearchInputs();
                this._LabelIntronSearchInputs.IsIntronSearch = false;
            }
            if (this._ReportType == ReportType.BSCPIC)
            {
                GetSupportedPlatform();
                if (chkIncludeIdenticalId.Checked == true)
                {
                    //Include Identical ID Elimination List
                    Reports _Report = new Reports();
                    _IdSetupCodeInfoParam.IncludeIdenticalIdList = true;
                    IList<AliasId> m_identicalidlist = _Report.GetIdenticalIDList(ProjectName);
                    if (m_identicalidlist != null)
                    {
                        if (_IdSetupCodeInfoParam.IdenticalIdList == null)
                            _IdSetupCodeInfoParam.IdenticalIdList = new List<AliasId>();
                        foreach (AliasId aliasid in m_identicalidlist)
                        {
                            _IdSetupCodeInfoParam.IdenticalIdList.Add(aliasid);
                        }
                    }                    
                }
                if (_IdSetupCodeInfoParam.AliasIDList == null)
                {
                    _IdSetupCodeInfoParam.AliasIDList = new List<AliasId>();
                    foreach (AliasId m_AliasID in _PICIDAliasList)
                    {
                        _IdSetupCodeInfoParam.AliasIDList.Add(new AliasId(m_AliasID.AliasID, m_AliasID.ID));
                    }
                }
                else
                {
                    if (_PICIDAliasList != null)
                    {
                        foreach (AliasId m_AliasID in _PICIDAliasList)
                        {
                            _IdSetupCodeInfoParam.AliasIDList.Add(new AliasId(m_AliasID.AliasID, m_AliasID.ID));
                        }
                    }
                }
            }
            if (this._ReportType == ReportType.MODELINFOPIC)
            {
                GetSupportedPlatform();
                GetModelInfoReportSpecificInputs();
                if (chkMIIncludeIdenticalId.Checked == true)
                {
                    //Include Identical ID Elemination List 
                    Reports _Report = new Reports();
                    _modelInfoSpecificSel.IncludeIdenticalIdList = true;
                    IList<AliasId> m_identicalidlist = _Report.GetIdenticalIDList(ProjectName);
                    if(m_identicalidlist != null)
                    {
                        if(_modelInfoSpecificSel.IdenticalIdList == null)
                            _modelInfoSpecificSel.IdenticalIdList = new List<AliasId>();
                        foreach (AliasId aliasid in m_identicalidlist)
                        {
                            _modelInfoSpecificSel.IdenticalIdList.Add(aliasid);
                        }
                    }                    
                }
                if(_IdSetupCodeInfoParam.AliasIDList == null)
                {
                    _IdSetupCodeInfoParam.AliasIDList = new List<AliasId>();
                    foreach(AliasId m_AliasID in _PICIDAliasList)
                    {
                        _IdSetupCodeInfoParam.AliasIDList.Add(new AliasId(m_AliasID.AliasID, m_AliasID.ID));
                    }
                }
                else
                {
                    if(_PICIDAliasList != null)
                    {
                        foreach(AliasId m_AliasID in _PICIDAliasList)
                        {
                            _IdSetupCodeInfoParam.AliasIDList.Add(new AliasId(m_AliasID.AliasID, m_AliasID.ID));
                        }
                    }
                }
            }
            if (this._ReportType == ReportType.IDBrandList)
            {
                GetSupportedPlatform();
            }
            this.DialogResult = DialogResult.OK;
        }
        private void GetModelInfoReportSpecificInputs()
        {
            ListBox.ObjectCollection listBoxObjects = null;

            if (!this.enableDateFilterChk.Checked)//get brand and model names only if date based search is not enabled.
            {
                //get the brand filters
                if (lstMatchBrands.Items.Count > 0)
                {
                    this._brandSpecificSel.MatchSelectedBrands = true;
                    listBoxObjects = new ListBox.ObjectCollection(lstMatchBrands);

                    foreach (object item in lstMatchBrands.Items)
                        if (lstFilterBrands.FindStringExact(item.ToString()) == ListBox.NoMatches)
                            listBoxObjects.Add(item);

                }
                else if (lstFilterBrands.Items.Count > 0)
                {
                    this._brandSpecificSel.MatchSelectedBrands = false;
                    listBoxObjects = lstFilterBrands.Items;
                }

                if (listBoxObjects != null)
                {
                    this._brandSpecificSel.SelectedBrands.Clear();
                    foreach (object item in listBoxObjects)
                        this._brandSpecificSel.SelectedBrands.Add(item.ToString());
                }
                //this._brandSpecificSel.RecordWithoutBrandNumber = this.chkNoUEIBrandNumbers.Checked;
                //get model filters
                listBoxObjects = null;
                if (lstMatchModels.Items.Count > 0)
                {
                    this._modelSpecificSel.MatchSelectedModels = true;
                    listBoxObjects = new ListBox.ObjectCollection(lstMatchModels);

                    foreach (object item in lstMatchModels.Items)
                        if (lstFilterModels.FindStringExact(item.ToString()) == ListBox.NoMatches)
                            listBoxObjects.Add(item);
                }
                else if (lstFilterModels.Items.Count > 0)
                {
                    this._modelSpecificSel.MatchSelectedModels = false;
                    listBoxObjects = lstFilterModels.Items;
                }

                if (listBoxObjects != null)
                {
                    this._modelSpecificSel.SelectedModels.Clear();
                    foreach (object item in listBoxObjects)
                        this._modelSpecificSel.SelectedModels.Add(item.ToString());
                }

                //set the model info report options
                this._modelInfoSpecificSel.ModelInfoOptions = ModelInfoSpecific.ModelReportOptions.ModelBased;
            }
            else
            {
                //if date based search is enabled.
                this._modelInfoSpecificSel.FromDate = this.dBSFromDateTxt.Text.Trim();
                this._modelInfoSpecificSel.ToDate = this.dBSToDateTxt.Text.Trim();
                
               
            }

            this._brandSpecificSel.RecordWithoutBrandNumber = this.chkNoUEIBrandNumbers.Checked;

            this._modelSpecificSel.RecordWithoutModelName = this.chkRecordWithoutModelName.Checked;
            this._modelSpecificSel.RemoveNonCodeBookModels = this.chkRemoveNonCodeBookModels.Checked;
            //misc filters
            this._modelInfoSpecificSel.DateBasedSearchEnabled = this.enableDateFilterChk.Checked;
            this._modelInfoSpecificSel.KeepDuplicatedOutputs = this.chkKeepDuplicateModels.Checked;
            this._modelInfoSpecificSel.SelectTNLink = this.chkTNLinks.Checked;
            this._modelInfoSpecificSel.IncludeRestrictedIds = (this.chkBxIncludeRestrictedIDs_ModelInfo.Checked) ? "0" : "1";            
        }
        private void GetLabelIntronSearchInputs()
        {
           
            //get the labels or introns from the list box.
            List<string> searchInputs = new List<string>();
            foreach (object item in lstBxSearchPatterns.Items)
                if (!searchInputs.Contains(item.ToString()))//remove the duplicates
                    searchInputs.Add(item.ToString());
                
            //create a pattern string
            StringBuilder patternBuilder = new StringBuilder();
            foreach (string pattern in searchInputs)
                patternBuilder.Append(GetSearchPattern(pattern) + ",");

            if (patternBuilder.Length > 0)
                patternBuilder.Remove(patternBuilder.Length - 1, 1);

            //get the type of search
            this._LabelIntronSearchInputs.IsIntronSearch = ((this._ReportType == ReportType.IntronSearch) || (this._ReportType == ReportType.ExcludeIntronSearch)) ? true : false;
            this._LabelIntronSearchInputs.IsCompleteMatch = chkBxGetCompleteMatch.Checked;//partial or complete
            //add to the filter class
            this._LabelIntronSearchInputs.SearchPattern = patternBuilder.ToString();

        }
        private string GetSearchPattern(String searchString)
        {

            string modifiedString = searchString;

            if (modifiedString.StartsWith("*"))
            {
                modifiedString = modifiedString.Remove(0, 1).Insert(0, "%");
            }

            if (modifiedString.EndsWith("*"))
            {
                int index = modifiedString.LastIndexOf('*');
                modifiedString = modifiedString.Remove(index, 1) + "%";
            }

            return modifiedString;
        }
        private void chkRemoveIDs_CheckedChanged(object sender, EventArgs e)
        {           
            if (chkRemoveIDs.Checked == true)
            {
                _RemoveIDSForSelectedSubDevices = true;
            }
            else
            {
                _RemoveIDSForSelectedSubDevices = false;
            }
        }               
        //private void chkDisplayMajorBrands_CheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBox majorBrandsChk = (CheckBox)sender;
        //    _IdSetupCodeInfoParam.DisplayMajorBrandsOnly = majorBrandsChk.Checked;

        //    if (majorBrandsChk.Checked)
        //    {
        //        gbSort.Enabled = false;
        //    }
        //    else
        //    {
        //        gbSort.Enabled = true;
        //    }
        //}
        private void chkIncludeRestrictedIDs_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox includeRestrictedIdsChk = (CheckBox)sender;
            if (includeRestrictedIdsChk.Checked)
            {
                _IdSetupCodeInfoParam.IncludeRestrictedIds = "0";
            }
            else
            {
                _IdSetupCodeInfoParam.IncludeRestrictedIds = "1";
            }
        }
        private void chkDisplayBrandNumber_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox displayBrandNumber = (CheckBox)sender;
            if (displayBrandNumber.Checked)
                _IdSetupCodeInfoParam.DisplayBrandNumber = true;
            else
                _IdSetupCodeInfoParam.DisplayBrandNumber = false;
        }
        private void rdoSortByModelCount_CheckedChanged(object sender, EventArgs e)
        {
            if(this._ReportType != ReportType.BSC)
                cmbBxModelTypeISCI.Enabled = rdoSortByModelCount.Checked;

            if (rdoSortByModelCount.Checked)
                _IdSetupCodeInfoParam.SortType = ReportSortBy.ModelCount;
        }
        private void rdoSortByName_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSortByName.Checked)
                _IdSetupCodeInfoParam.SortType = ReportSortBy.AlphabeticalBrandNames;
        }
        private void rdoIDNumber_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSortByIDNumber.Checked)
                _IdSetupCodeInfoParam.SortType = ReportSortBy.IDNumber;
        }
        private void rdoPriorityIDListFile_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSortByPriorityIDListFile.Checked)
            {
                _IdSetupCodeInfoParam.SortType = ReportSortBy.PriorityIDListFile;
                GetInputFile(FileType.PriorityIDListFile);
            }
            else
            {
                txtPriorityIDListFile.Text = "";
                _IdSetupCodeInfoParam.PriorityIDList = null;
            }
        }
        private void rdoSortByPOSData_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSortByPOSData.Checked)
            {
                _IdSetupCodeInfoParam.SortType = ReportSortBy.POS;
                lblStartDate.Enabled = true;
                lblEndDate.Enabled = true;
                txtStartDate.Enabled = true;
                txtEndDate.Enabled = true;
                btnStartDate.Enabled = true;
                btnEndDate.Enabled = true;
                txtStartDate.Text = "";
                txtEndDate.Text = "";
            }
            else
            {
                lblStartDate.Enabled = false;
                lblEndDate.Enabled = false;
                txtStartDate.Enabled = false;
                txtEndDate.Enabled = false;
                btnStartDate.Enabled = false;
                btnEndDate.Enabled = false;
                //dateTime1.Visible = false;
                //dateTime2.Visible = false;
            }
        }
        private void rdoSortBySearchStatistics_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSortBySearchStatistics.Checked)
            {
                _IdSetupCodeInfoParam.SortType = ReportSortBy.SearchStatistics;
                lblStartDate.Enabled = true;
                lblEndDate.Enabled = true;
                txtStartDate.Enabled = true;
                txtEndDate.Enabled = true;
                btnStartDate.Enabled = true;
                btnEndDate.Enabled = true;
                txtStartDate.Text = "";
                txtEndDate.Text = "";
            }
            else
            {
                lblStartDate.Enabled = false;
                lblEndDate.Enabled = false;
                txtStartDate.Enabled = false;
                txtEndDate.Enabled = false;
                btnStartDate.Enabled = false;
                btnEndDate.Enabled = false;
                //dateTime1.Visible = false;
                //dateTime2.Visible = false;
            }
        }
        private void chkDisplayMajorBrands_CheckedChanged(object sender, EventArgs e)
        {
            _IdSetupCodeInfoParam.DisplayMajorBrandsOnly = chkDisplayMajorBrands.Checked;

            if (chkDisplayMajorBrands.Checked)
            {
                rdoSortByModelCount.Visible = false;
                rdoSortByName.Visible = false;
            }
            else
            {
                rdoSortByModelCount.Visible = true;
                rdoSortByName.Visible = true;
            }
        }  
        private void GetInputFile(FileType fileType)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            switch (fileType)
            {
                case FileType.AliasIDListFile:
                    openFileDialog.Filter = "Alias File (*.als)|*.als|ID List File (*.ids)|*.ids|List File (*.lst)|*.lst";
                    openFileDialog.DefaultExt = "als";
                    break;
                case FileType.PriorityIDListFile:
                    openFileDialog.Filter = "ID List File (*.ids)|*.ids|List File (*.lst)|*.lst";
                    openFileDialog.DefaultExt = "ids";
                    break;
                case FileType.IdenticalIDListFile:
                    openFileDialog.Filter = "Identical ID List File (*.ids)|*.ids|List File (*.lst)|*.lst";
                    openFileDialog.DefaultExt = "ids";
                    break;
                case FileType.QuickSetPriorityIDListFile:
                    openFileDialog.Filter = "Priority ID List File (*.xls,*.xlsx)|*.xlsx;*.xls";
                    openFileDialog.DefaultExt = "xlsx";
                    break;
                case FileType.BrandSearchListFile:
                    openFileDialog.Filter = "Brand Search List File (*.xls)|*.xls";
                    openFileDialog.DefaultExt = "txt";
                    break;
                default:
                    openFileDialog.DefaultExt = "txt";
                    break;
            }
            openFileDialog.Filter += "|Text File (*.txt)|*.txt";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                switch (fileType)
                {
                    case FileType.PriorityIDListFile:
                        txtPriorityIDListFile.Text = openFileDialog.FileName;
                        GetIDListFromPriorityIDListFile();
                        break;
                    case FileType.QuickSetPriorityIDListFile:
                        txtQSPriorityIdListFile.Text = openFileDialog.FileName;
                        GetQuickSetPriorityIDList();
                        break;
                    case FileType.AliasIDListFile:
                        txtAliasIDFile.Text = openFileDialog.FileName;
                        GetIDListFromAliasListFile();
                        break;
                    case FileType.IdenticalIDListFile:
                        if (_ReportType == ReportType.BSCPIC)
                        {
                            txtIdenticalIdListFile.Text = openFileDialog.FileName;
                        }
                        if (_ReportType == ReportType.MODELINFOPIC)
                        {
                            txtMIIdenticalFile.Text = openFileDialog.FileName;
                        }
                        GetIdenticalIdListFile();
                        break;
                    case FileType.BrandSearchListFile:
                        txtBrandSeachListFile.Text = openFileDialog.FileName;
                        GetBrandSearchList();
                        break;
                }
            }
            else
            {
                rdoSortByModelCount.Checked = true;
            }
        }
        private void GetBrandSearchList()
        {
            brandbasedSeach = null;
            FileInfo file = new FileInfo(txtBrandSeachListFile.Text);
            if (file.Extension.ToLower() == ".txt")
            {
                StreamReader stream = file.OpenText();
                String brand = stream.ReadLine();
                //Get Brand List
                while (brand != null)
                {                    
                    if(!(brand.Trim().ToLower().Contains("brand")) && (brand.Trim() != String.Empty))
                    {
                        if (brandbasedSeach == null)
                            brandbasedSeach = new BrandBasedSeach();
                        //Handle duplicate Brand entry
                        if(!brandbasedSeach.Brand.Contains(brand))
                            brandbasedSeach.Brand.Add(brand);
                    }
                    brand = stream.ReadLine();
                }
                stream.Close();
                if (brandbasedSeach.Brand.Count == 0)
                {
                    MessageBox.Show("Brand List is Empty");
                    txtBrandSeachListFile.Text = "";
                }
            }
            else if (file.Extension.ToLower() == ".xls")
            {                
                OleDbConnection excelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + txtBrandSeachListFile.Text + ";Extended Properties=Excel 8.0;");
                excelConnection.Open();
                DataTable dtExcelSheets = excelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                excelConnection.Close();
                if (dtExcelSheets.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtExcelSheets.Rows)
                    {
                        OleDbDataAdapter excelAdapter = new OleDbDataAdapter("SELECT * FROM [" + dr["TABLE_NAME"].ToString() + "]", excelConnection);
                        DataTable dtBrandList = new DataTable("BrandList");
                        excelAdapter.Fill(dtBrandList);
                        for (Int32 row = 0; row < dtBrandList.Rows.Count; row++)
                        {
                            String brand = dtBrandList.Rows[row][0].ToString();
                            if (!(brand.ToLower().Contains("brand")) && (brand.Trim() != String.Empty))
                            {
                                if (brandbasedSeach == null)
                                    brandbasedSeach = new BrandBasedSeach();
                                //Handle duplicate Brand entry
                                if (!brandbasedSeach.Brand.Contains(brand))
                                    brandbasedSeach.Brand.Add(brand);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Brand List is Empty");
                    txtBrandSeachListFile.Text = "";
                }
                if (brandbasedSeach.Brand.Count == 0)
                {
                    MessageBox.Show("Brand List is Empty");
                    txtBrandSeachListFile.Text = "";
                }
            }
        }
        private void GetIdenticalIdListFile()
        {
            if (_ReportType == ReportType.MODELINFOPIC)
            {
                _modelInfoSpecificSel.IdenticalIdList = null;
                FileInfo file = new FileInfo(txtMIIdenticalFile.Text);
                StreamReader stream = file.OpenText();
                String bothid = stream.ReadLine();
                IList<String> allModes = new List<String>();
                Char chr = 'A';
                for (Int32 item = 0; item < 26; item++)
                {
                    allModes.Add(chr.ToString());
                    chr++;
                }
                //Get All Identical Ids and split them using comma(,)
                Char[] separator = { '\t', ' ', ',' };
                while (bothid != null)
                {
                    String[] id = bothid.Trim().Split(separator);
                    if (id.Length >= 2)
                    {
                        String firstId = String.Empty;
                        String secondId = String.Empty;
                        Int32 intLength = 0;

                        String tempfirstId = String.Empty;
                        while (intLength < id.Length)
                        {
                            String tempsecondId = String.Empty;
                            if (tempfirstId == String.Empty && id[intLength] != String.Empty && intLength == 0)
                            {
                                tempfirstId = id[intLength];
                            }
                            else if (id[intLength] != String.Empty)
                            {
                                tempsecondId = id[intLength];
                            }
                            if (tempfirstId != String.Empty && tempsecondId != String.Empty)
                            {
                                //Check for Valide ID
                                int IDNumber;
                                if (tempfirstId.Length == 5 && allModes.Contains(tempfirstId.Substring(0, 1)) && (int.TryParse(tempfirstId.Substring(1), out IDNumber)))
                                {
                                    firstId = tempfirstId;
                                }
                                if (tempsecondId.Length == 5 && allModes.Contains(tempsecondId.Substring(0, 1)) && (int.TryParse(tempsecondId.Substring(1), out IDNumber)))
                                {
                                    secondId += tempsecondId + ",";
                                }
                            }
                            intLength++;
                        }
                        if (firstId != String.Empty && secondId != String.Empty)
                        {
                            secondId = secondId.Remove(secondId.Length - 1, 1);
                            if (_modelInfoSpecificSel.IdenticalIdList == null)
                                _modelInfoSpecificSel.IdenticalIdList = new List<AliasId>();
                            _modelInfoSpecificSel.IdenticalIdList.Add(new AliasId(firstId, secondId));
                        }
                    }
                    bothid = stream.ReadLine();
                }
                stream.Close();
                if (_modelInfoSpecificSel.IdenticalIdList == null)
                {
                    txtMIIdenticalFile.Text = "Validation Fail";
                }
                else if (_modelInfoSpecificSel.IdenticalIdList.Count == 0)
                {
                    txtMIIdenticalFile.Text = "Validation Fail";
                }
            }
            if (_ReportType == ReportType.BSCPIC)
            {
                _IdSetupCodeInfoParam.IdenticalIdList = null;
                FileInfo file = new FileInfo(txtIdenticalIdListFile.Text);
                StreamReader stream = file.OpenText();
                String bothid = stream.ReadLine();
                IList<String> allModes = new List<String>();
                Char chr = 'A';
                for (Int32 item = 0; item < 26; item++)
                {
                    allModes.Add(chr.ToString());
                    chr++;
                }
                //Get All Identical Ids and split them using comma(,)
                Char[] separator = { '\t', ' ', ',' };
                while (bothid != null)
                {
                    String[] id = bothid.Trim().Split(separator);
                    if (id.Length >= 2)
                    {
                        String firstId = String.Empty;
                        String secondId = String.Empty;
                        Int32 intLength = 0;

                        String tempfirstId = String.Empty;
                        while (intLength < id.Length)
                        {
                            String tempsecondId = String.Empty;
                            if (tempfirstId == String.Empty && id[intLength] != String.Empty && intLength == 0)
                            {
                                tempfirstId = id[intLength];
                            }
                            else if (id[intLength] != String.Empty)
                            {
                                tempsecondId = id[intLength];
                            }
                            if (tempfirstId != String.Empty && tempsecondId != String.Empty)
                            {
                                //Check for Valide ID
                                int IDNumber;
                                if (tempfirstId.Length == 5 && allModes.Contains(tempfirstId.Substring(0, 1)) && (int.TryParse(tempfirstId.Substring(1), out IDNumber)))
                                {
                                    firstId = tempfirstId;
                                }
                                if (tempsecondId.Length == 5 && allModes.Contains(tempsecondId.Substring(0, 1)) && (int.TryParse(tempsecondId.Substring(1), out IDNumber)))
                                {
                                    secondId += tempsecondId + ",";
                                }
                            }
                            intLength++;
                        }
                        if (firstId != String.Empty && secondId != String.Empty)
                        {
                            secondId = secondId.Remove(secondId.Length - 1, 1);
                            if (_IdSetupCodeInfoParam.IdenticalIdList == null)
                                _IdSetupCodeInfoParam.IdenticalIdList = new List<AliasId>();
                            _IdSetupCodeInfoParam.IdenticalIdList.Add(new AliasId(firstId, secondId));
                        }
                    }
                    bothid = stream.ReadLine();
                }
                stream.Close();
                if (_IdSetupCodeInfoParam.IdenticalIdList == null)
                {
                    txtIdenticalIdListFile.Text = "Validation Fail";
                }
                else if (_IdSetupCodeInfoParam.IdenticalIdList.Count == 0)
                {
                    txtIdenticalIdListFile.Text = "Validation Fail";
                }
            }
            
        }
        private void GetIDListFromPriorityIDListFile()
        {
            _IdSetupCodeInfoParam.PriorityIDList = null;
            FileInfo file = new FileInfo(txtPriorityIDListFile.Text);
            StreamReader stream = file.OpenText();
            String id = stream.ReadLine();
            IList<String> allModes = new List<String>();
            Char chr = 'A';
            for (Int32 item = 0; item < 26; item++)
            {
                allModes.Add(chr.ToString());
                chr++;
            }
            //Get All the Modes
            //select Mode,Name from Modes
            while(id != null)
            {
                id = id.Trim().ToUpper(); 
                //Check for Valide ID                
                int IDNumber;
                if (id.Length == 5 && (allModes.Contains(id.Substring(0, 1)) == true) && (int.TryParse(id.Substring(1),out IDNumber)))
                {                    
                    if (_IdSetupCodeInfoParam.PriorityIDList == null)
                        _IdSetupCodeInfoParam.PriorityIDList = new List<Id>();
                    _IdSetupCodeInfoParam.PriorityIDList.Add(new Id(id));
                }
                //else
                //{
                //    txtPriorityIDListFile.Text = "Validation Fail";
                //    rdoSortByModelCount.Checked = true;
                //    break;
                //}
                id = stream.ReadLine();
            }
            if (_IdSetupCodeInfoParam.PriorityIDList == null)
            {
                txtPriorityIDListFile.Text = "Validation Fail";
                rdoSortByModelCount.Checked = true;
            }
            else if (_IdSetupCodeInfoParam.PriorityIDList.Count == 0)
            {
                txtPriorityIDListFile.Text = "Validation Fail";
                rdoSortByModelCount.Checked = true;
            }
            stream.Close();
        }
        private void GetQuickSetPriorityIDList()
        {
            if (File.Exists(txtQSPriorityIdListFile.Text))
            {
                if (QuickSetInputs == null)
                    QuickSetInputs = new QuickSetInfo();

                if (QuickSetInputs.PriorityIDList == null)
                    QuickSetInputs.PriorityIDList = new Dictionary<String, List<String>>();

                IList<String> allModes = new List<String>();
                Char chr = 'A';
                for (Int32 item = 0; item < 26; item++)
                {
                    allModes.Add(chr.ToString());
                    chr++;
                }
                FileInfo m_File = new FileInfo(txtQSPriorityIdListFile.Text);                
                switch (m_File.Extension)
                {
                    case ".txt":
                        {                           
                            StreamReader stream = m_File.OpenText();
                            String strLine = stream.ReadLine();
                            while (!String.IsNullOrEmpty(strLine))
                            {
                                if (!(strLine.Trim().ToLower().Contains("brand")) && (String.IsNullOrEmpty(strLine)))
                                {
                                    
                                    String[] strItems = strLine.Split(' ');
                                    if (strItems.Length > 0)
                                    {
                                        List<String> ids = new List<String>();
                                        foreach (String id in strItems)
                                        {
                                            //Check for Valide ID                
                                            int IDNumber;
                                            if (id.Length == 5 && (allModes.Contains(id.Substring(0, 1)) == true) && (int.TryParse(id.Substring(1), out IDNumber)))
                                            {
                                                ids.Add(id.Trim().ToUpper());
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(strItems[0]) && !String.IsNullOrEmpty(strItems[1]))
                                        {
                                            //Consider Device Code and Brand as Key, DeviceCode,Brand
                                            //check if key value exists
                                            String strDeviceCodeandBrand = String.Empty;
                                            strDeviceCodeandBrand = strItems[0].Trim().ToUpper() + ',' + strItems[1].Trim();
                                            if (!QuickSetInputs.PriorityIDList.ContainsKey(strDeviceCodeandBrand))
                                                QuickSetInputs.PriorityIDList.Add(strDeviceCodeandBrand, ids);
                                        }
                                    }
                                }
                                strLine = stream.ReadLine();
                            }
                            stream.Close();
                            if (QuickSetInputs.PriorityIDList.Count == 0)
                            {
                                MessageBox.Show("Priority ID List File is Empty", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtQSPriorityIdListFile.Text = "";
                            }
                        }
                        break;
                    case ".xls":
                        {
                            try
                            {
                                OleDbConnection m_ExcelConnection = null;
                                m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + m_File.FullName + ";Extended Properties=Excel 8.0;");
                                m_ExcelConnection.Open();
                                DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                                m_ExcelConnection.Close();
                                if (dtExcelSheets.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtExcelSheets.Rows)
                                    {
                                        try
                                        {
                                            OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + dr["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                            DataTable dtPriorityIdList = new DataTable("PrioriyIDList");
                                            m_ExcelAdapter.Fill(dtPriorityIdList);
                                            if (dtPriorityIdList.Rows.Count > 0)
                                            {
                                                DataRow[] dtEmptyRows = dtPriorityIdList.Select("DeviceCode = '' or Brand = ''");
                                                if (dtEmptyRows.Length > 0)
                                                {
                                                    foreach (DataRow item in dtEmptyRows)
                                                    {
                                                        item.Delete();
                                                    }
                                                    dtPriorityIdList.AcceptChanges();
                                                }
                                                dtEmptyRows = dtPriorityIdList.Select("DeviceCode is null or Brand is null");
                                                if (dtEmptyRows.Length > 0)
                                                {
                                                    foreach (DataRow item in dtEmptyRows)
                                                    {
                                                        item.Delete();
                                                    }
                                                    dtPriorityIdList.AcceptChanges();
                                                }                                               
                                                foreach (DataRow dataRow in dtPriorityIdList.Rows)
                                                {
                                                    List<String> ids = new List<String>();
                                                    String strDeviceCode = String.Empty;
                                                    String strBrand = String.Empty;
                                                    foreach (DataColumn dataColumn in dtPriorityIdList.Columns)
                                                    {
                                                        String id = dataRow[dataColumn.ColumnName].ToString().Trim();
                                                        //Check for Valide ID                
                                                        int IDNumber;
                                                        if (id.Length == 5 && (allModes.Contains(id.Substring(0, 1)) == true) && (int.TryParse(id.Substring(1), out IDNumber)))
                                                        {
                                                            ids.Add(id.Trim().ToUpper());
                                                        }
                                                        if (dataColumn.ColumnName.ToLower() == "devicecode" && !String.IsNullOrEmpty(dataRow[dataColumn.ColumnName].ToString()))
                                                        {
                                                            strDeviceCode = dataRow[dataColumn.ColumnName].ToString().Trim();
                                                        }
                                                        if (dataColumn.ColumnName.ToLower() == "brand")
                                                        {
                                                            strBrand = dataRow[dataColumn.ColumnName].ToString().Trim();
                                                        }
                                                    }
                                                    if (!String.IsNullOrEmpty(strDeviceCode) && !String.IsNullOrEmpty(strBrand))
                                                    {
                                                        //Consider Device Code and Brand as Key, DeviceCode,Brand
                                                        //check if key value exists
                                                        String strDeviceCodeandBrand = String.Empty;
                                                        strDeviceCodeandBrand = strDeviceCode.Trim().ToUpper() + ',' + strBrand.Trim();
                                                        if (!QuickSetInputs.PriorityIDList.ContainsKey(strDeviceCodeandBrand))
                                                            QuickSetInputs.PriorityIDList.Add(strDeviceCodeandBrand, ids);
                                                    }
                                                }
                                               
                                            }
                                        }
                                        catch { }
                                    }
                                    if (QuickSetInputs.PriorityIDList.Count == 0)
                                    {
                                        MessageBox.Show("Priority ID List File is Empty", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        txtQSPriorityIdListFile.Text = "";
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Priority ID List File is Empty", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Couldn't able access BSC Priority ID List file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        break;
                    case ".xlsx":
                        {
                            try
                            {
                                OleDbConnection m_ExcelConnection = null;
                                m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + m_File.FullName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                m_ExcelConnection.Open();
                                DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                                m_ExcelConnection.Close();
                                if (dtExcelSheets.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtExcelSheets.Rows)
                                    {
                                        try
                                        {
                                            OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + dr["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                            DataTable dtPriorityIdList = new DataTable("PrioriyIDList");
                                            m_ExcelAdapter.Fill(dtPriorityIdList);
                                            if (dtPriorityIdList.Rows.Count > 0)
                                            {
                                                DataRow[] dtEmptyRows = dtPriorityIdList.Select("DeviceCode = '' or Brand = ''");
                                                if (dtEmptyRows.Length > 0)
                                                {
                                                    foreach (DataRow item in dtEmptyRows)
                                                    {
                                                        item.Delete();
                                                    }
                                                    dtPriorityIdList.AcceptChanges();
                                                }
                                                dtEmptyRows = dtPriorityIdList.Select("DeviceCode is null or Brand is null");
                                                if (dtEmptyRows.Length > 0)
                                                {
                                                    foreach (DataRow item in dtEmptyRows)
                                                    {
                                                        item.Delete();
                                                    }
                                                    dtPriorityIdList.AcceptChanges();
                                                }
                                                foreach (DataRow dataRow in dtPriorityIdList.Rows)
                                                {
                                                    List<String> ids = new List<String>();
                                                    String strDeviceCode = String.Empty;
                                                    String strBrand = String.Empty;
                                                    foreach (DataColumn dataColumn in dtPriorityIdList.Columns)
                                                    {
                                                        String id = dataRow[dataColumn.ColumnName].ToString().Trim();
                                                        //Check for Valide ID                
                                                        int IDNumber;
                                                        if (id.Length == 5 && (allModes.Contains(id.Substring(0, 1)) == true) && (int.TryParse(id.Substring(1), out IDNumber)))
                                                        {
                                                            ids.Add(id.Trim().ToUpper());
                                                        }
                                                        if (dataColumn.ColumnName.ToLower() == "devicecode" && !String.IsNullOrEmpty(dataRow[dataColumn.ColumnName].ToString()))
                                                        {
                                                            strDeviceCode = dataRow[dataColumn.ColumnName].ToString().Trim();
                                                        }
                                                        if (dataColumn.ColumnName.ToLower() == "brand")
                                                        {
                                                            strBrand = dataRow[dataColumn.ColumnName].ToString().Trim();
                                                        }
                                                    }
                                                    if (!String.IsNullOrEmpty(strDeviceCode) && !String.IsNullOrEmpty(strBrand))
                                                    {
                                                        //Consider Device Code and Brand as Key, DeviceCode,Brand
                                                        //check if key value exists
                                                        String strDeviceCodeandBrand = String.Empty;
                                                        strDeviceCodeandBrand = strDeviceCode.Trim().ToUpper() + ',' + strBrand.Trim();
                                                        if (!QuickSetInputs.PriorityIDList.ContainsKey(strDeviceCodeandBrand))
                                                            QuickSetInputs.PriorityIDList.Add(strDeviceCodeandBrand, ids);
                                                    }
                                                }
                                            }
                                        }
                                        catch { }
                                    }
                                    if (QuickSetInputs.PriorityIDList.Count == 0)
                                    {
                                        MessageBox.Show("Priority ID List File is Empty", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        txtQSPriorityIdListFile.Text = "";
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Priority ID List File is Empty", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Couldn't able access BSC Priority ID List file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        break;
                }
            }
        }
        private void GetIDListFromAliasListFile()
        {
            _IdSetupCodeInfoParam.AliasIDList = null;
            FileInfo file = new FileInfo(txtAliasIDFile.Text);
            StreamReader stream = file.OpenText();
            String bothid = stream.ReadLine();
            IList<String> allModes = new List<String>();
            Char chr = 'A';
            for (Int32 item = 0; item < 26; item++)
            {
                allModes.Add(chr.ToString());
                chr++;
            }
            //Get All the Modes
            //select Mode,Name from Modes
            while (bothid != null)
            {
                Char[] separator = {'\t',' ',','};
                String[] id = bothid.Trim().Split(separator);

                if (id.Length >= 2)
                {
                    String firstId = String.Empty;  //id[0].Trim();
                    String secondId = String.Empty; //id[1].Trim();
                    Int32 intLength = 0;
                    while (intLength < id.Length)
                    {
                        if (firstId == String.Empty && id[intLength] != String.Empty)
                        {
                            firstId = id[intLength];
                        }
                        else if (id[intLength] != String.Empty)
                        {
                            secondId = id[intLength];
                        }
                        intLength++;
                    }

                    //Check for Valide ID
                    int IDNumber;
                    if (firstId.Length == 5 && allModes.Contains(firstId.Substring(0, 1)) && (int.TryParse(firstId.Substring(1), out IDNumber)) &&
                        secondId.Length == 5 && allModes.Contains(secondId.Substring(0, 1)) && (int.TryParse(secondId.Substring(1), out IDNumber)))
                    {
                        if (_IdSetupCodeInfoParam.AliasIDList == null)
                            _IdSetupCodeInfoParam.AliasIDList = new List<AliasId>();
                        _IdSetupCodeInfoParam.AliasIDList.Add(new AliasId(firstId, secondId));
                    }
                    //else
                    //{
                    //    txtAliasIDFile.Text = "Validation Fail";
                    //    rdoSortByModelCount.Checked = true;
                    //    break;
                    //}
                }                
                bothid = stream.ReadLine();
            }
            stream.Close();
            if (_IdSetupCodeInfoParam.AliasIDList == null)
            {
                txtAliasIDFile.Text = "Validation Fail";                
            }
            else if (_IdSetupCodeInfoParam.AliasIDList.Count == 0)
            {
                txtAliasIDFile.Text = "Validation Fail";                
            }
        }
        private bool ValidateInputs(out String message)
        {
            message = String.Empty;
            if (this._regions == null || this._regions.Count == 0)
            {
                message = "Region is a mandatory input.It cannot be left blank.";
                return false;//not valid.
            }
            if (this._dataSources == null || this._dataSources.Count == 0)
            {
                message = "Datasource is a mandatory input, it cannot be left blank.";
                return false;//not valid.
            }
            if(this._ReportType == ReportType.IntronSearchWithBrand)
            {
                if(this._LabelIntronSearchInputs.SearchPattern == String.Empty)
                {
                    message = "Intron is mandatory input, it cannot left blank.";
                    return false;
                }
            }
            if (this._ReportType == ReportType.EdId)
            {
                if (this._deviceTypes == null || this._deviceTypes.Count == 0)
                {
                    message = "Main Device Type is a mandatory input.It cannot be left blank.";
                    return false;//not valid.
                }
            }
            #region for Quickset Model Info Report disabled BSC Generation Feature
            if (this._ReportType == ReportType.QuickSet)
            {
                //if (this.dataGridQuickSet.Rows.Count < 1)
                //{
                //    message = "Please enter Mode Setup Information";
                //    txtModeName.Focus();
                //    return false;
                //}
                //if (this.dataGridQuickSet.Rows.Count > 0)
                //{
                //    Dictionary<String, Boolean> checkmissingModes = new Dictionary<String, Boolean>();
                //    foreach (String item in _selectedModes)
                //    {
                //        checkmissingModes.Add(item.ToString(), false);
                //    }
                //    foreach (DataGridViewRow item in this.dataGridQuickSet.Rows)
                //    {
                //        if (!String.IsNullOrEmpty(item.Cells[2].Value.ToString()))
                //        {
                //            Char[] checkValidModes = item.Cells[2].Value.ToString().ToCharArray();
                //            foreach (Char item1 in checkValidModes)
                //            {
                //                if (_selectedModes.Contains(item1.ToString()))
                //                    checkmissingModes[item1.ToString()] = true;                  
                //            }
                //        }
                //    }
                //    message = String.Empty;
                //    foreach (KeyValuePair<String,Boolean> item in checkmissingModes)
                //    {
                //        if (item.Value == false)
                //        {
                //            message += item.Key + ",";
                //        }
                //    }
                //    if (message.Length > 0)
                //    {
                //        message = message.Remove(message.Length - 1, 1);
                //        message += " Modes are missing in Mode Setup Information";
                //        return false;
                //    }
                //}
            }
            #endregion

            if (this._ReportType == ReportType.LabelSearchWithBrand)
            {
                if(this._LabelIntronSearchInputs.SearchPattern == String.Empty)
                {
                    message = "Label is mandatory input, it cannot left blank.";
                    return false;
                }
            }
            if(this._ReportType == ReportType.ModelInformation && this.enableDateFilterChk.Checked)
                if (String.IsNullOrEmpty(this._modelInfoSpecificSel.FromDate) || String.IsNullOrEmpty(this._modelInfoSpecificSel.ToDate))
                {
                    message = "From and To date should be entered for date based search.";
                    return false;
                }
            if (this._ReportType == ReportType.ModelInformation_EmptyDevice && this.enableDateFilterChk.Checked)
                if (String.IsNullOrEmpty(this._modelInfoSpecificSel.FromDate) || String.IsNullOrEmpty(this._modelInfoSpecificSel.ToDate))
                {
                    message = "From and To date should be entered for date based search.";
                    return false;
                }
            if (this._ReportType == ReportType.MODELINFOPIC && this.enableDateFilterChk.Checked)
                if (String.IsNullOrEmpty(this._modelInfoSpecificSel.FromDate) || String.IsNullOrEmpty(this._modelInfoSpecificSel.ToDate))
                {
                    message = "From and To date should be entered for date based search.";
                    return false;
                }
            if (this._ReportType == ReportType.QuickSet && this.enableDateFilterChk.Checked)
                if (String.IsNullOrEmpty(this._modelInfoSpecificSel.FromDate) || String.IsNullOrEmpty(this._modelInfoSpecificSel.ToDate))
                {
                    message = "From and To date should be entered for date based search.";
                    return false;
                }
            //if (this._ReportType == ReportType.ModelInfoWithArdKey && this.enableDateFilterChk.Checked)
            //    if (String.IsNullOrEmpty(this._modelInfoSpecificSel.FromDate) || String.IsNullOrEmpty(this._modelInfoSpecificSel.ToDate))
            //    {
            //        message = "From and To date should be entered for date based search.";
            //        return false;
            //    }
            if (this._ReportType == ReportType.IDList)
            {
                if (((rdoSortByPOSData.Checked == true)||(rdoSortBySearchStatistics.Checked == true)) && (this.txtStartDate.Text == String.Empty) && (this.txtEndDate.Text != String.Empty))
                {
                    message = "From and To date should be entered for POS search.";
                    txtStartDate.Focus();
                    return false;
                }
            }
            if (this._ReportType == ReportType.BrandBasedSearch)
            {
                if (brandbasedSeach.Brand.Count == 0)
                {
                    message = "Please provide brand list file.";                                       
                    return false;
                }
                if ((chkPOSAvail.Checked == true)&&(this.txtPOSStartDate.Text == String.Empty) && (this.txtPOSEndDate.Text != String.Empty))
                {
                    message = "From and To date should be entered for POS search.";
                    txtPOSStartDate.Focus();
                    return false;
                }
                if ((chkPOSAvail.Checked == true) && (this.txtPOSStartDate.Text != String.Empty) && (this.txtPOSEndDate.Text == String.Empty))
                {
                    message = "From and To date should be entered for POS search.";
                    txtPOSEndDate.Focus();
                    return false;
                }
                if ((chkOSSAvail.Checked == true)&&(this.txtOSSStartDate.Text == String.Empty) && (this.txtOSSEndDate.Text != String.Empty))
                {
                    message = "From and To date should be entered for Search Statistics search.";
                    txtOSSStartDate.Focus();
                    return false;
                }
                if ((chkOSSAvail.Checked == true) && (this.txtOSSStartDate.Text != String.Empty) && (this.txtOSSEndDate.Text == String.Empty))
                {
                    message = "From and To date should be entered for Search Statistics search.";
                    txtOSSEndDate.Focus();
                    return false;
                }
            }
            if (_IdSetupCodeInfoParam != null)
            {
                if (_IdSetupCodeInfoParam.IncludeUnsupportedPlatforms == false)
                {
                    if (_IdSetupCodeInfoParam.SupportedPlatforms == null)
                    {
                        message = "Please select Platform you want to include.";
                        return false;
                    }
                    else if (_IdSetupCodeInfoParam.SupportedPlatforms.Count == 0)
                    {
                        message = "Please select Platform you want to include.";
                        return false;
                    }
                }
            }
            return true;
        }                
        private List<String> LoadFromFile()
        {
            try
            {
                List<string> dataList = new List<string>();

                using (OpenFileDialog fileDialog = new OpenFileDialog())
                {
                    fileDialog.RestoreDirectory = true;
                    fileDialog.Filter = "Text file (*.txt)|*.txt";
                    fileDialog.DefaultExt = "txt";

                    if (fileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string filePath = fileDialog.FileName;

                        FileInfo fileInfo = new FileInfo(filePath);
                        StreamReader reader = new StreamReader(fileInfo.FullName, Encoding.Default);// fileInfo.OpenText();

                        if (reader != null)
                        {
                            string data = String.Empty;
                            do
                            {
                                data = reader.ReadLine();

                                if (!String.IsNullOrEmpty(data.Trim()))
                                    dataList.Add(data);

                            } while (!reader.EndOfStream);


                            reader.Close();
                        }

                    }
                }

              
                return dataList;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                return new List<string>();
            }
        }        
        private void ReportFilterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool isValid = true;
            string message = null;
            if (this.DialogResult == DialogResult.OK)
            {
               //validate.
                isValid = ValidateInputs(out message);                
            }
            if (!isValid)
                MessageBox.Show(message,"Invalid Inputs",MessageBoxButtons.OK,MessageBoxIcon.Stop);
            e.Cancel = !isValid;

            if (quicksetProcess != null && quicksetProcess.IsBusy == true)
            {
                quicksetProcess.CancelAsync();                
            }

            if (workerProcess != null && workerProcess.IsBusy == true)
            {
                workerProcess.CancelAsync();
                backWorkerCancelled = workerProcess.CancellationPending;
            }
           
        }
        private void cmbSetupCodeFormating_SelectedIndexChanged(object sender, EventArgs e)
        {
            _IdSetupCodeInfoParam.SetupCodeFormat = cmbSetupCodeFormating.Text;
            if (_IdSetupCodeInfoParam.SetupCodeFormat == "Base 10")
            {
                _IdSetupCodeInfoParam.StartingDigitofSetupCode = 0;
                chkFormatStartDigitSwitch.Enabled = false;
            }
            else
            {
                chkFormatStartDigitSwitch.Enabled = true;
                _IdSetupCodeInfoParam.StartingDigitofSetupCode = (chkFormatStartDigitSwitch.Checked) ? 1 : 0;
            }
        }

        #region Vaildate Date
        public Boolean IsDate(String attemptedDate)
        {
            Boolean Success = false;
            try
            {
                System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                dtf.ShortDatePattern = "MM-dd-yyyy";
                DateTime dtParse = DateTime.ParseExact(attemptedDate,"d",dtf);
                Success = true;
            }
            catch (FormatException e)
            {
                Success = false;
            }
            return Success;
        }
        public Boolean IsDate(string attemptedDate, out DateTime dateObj)
        {
            Boolean Success = false;
            dateObj = DateTime.Now;
            try
            {
                System.Globalization.DateTimeFormatInfo dtf = new System.Globalization.DateTimeFormatInfo();
                dtf.ShortDatePattern = "MM-dd-yyyy";
                dateObj = DateTime.ParseExact(attemptedDate, "d", dtf);
                Success = true;
            }
            catch (FormatException e)
            {
                Success = false;
            }
            return Success;
        }
        #endregion

        #region Browse File
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            GetInputFile(FileType.AliasIDListFile);
        }
        #endregion

        #region Start Date
        private void btnStartDate_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            DialogResult result = calendar.ShowDialog();
            if (result == DialogResult.OK)
            {
                DateTime startDate = calendar.SelectedDate;

                if (startDate.CompareTo(DateTime.Today) != 1)
                {
                    if (!String.IsNullOrEmpty(txtEndDate.Text))
                    {
                        DateTime endDate = DateTime.Parse(txtEndDate.Text);
                        //compare start and end date.
                        if (startDate.CompareTo(endDate) == 1)
                        {
                            MessageBox.Show("Start Date should be less than End Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {

                            txtStartDate.Text = startDate.ToString("MM-dd-yyyy");
                            ///_IdSetupCodeInfoParam.StartDate = txtStartDate.Text;
                        }

                    }
                    else
                    {
                        txtStartDate.Text = startDate.ToString("MM-dd-yyyy");
                        //_IdSetupCodeInfoParam.StartDate = txtStartDate.Text;
                    }
                }
                else
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        #region End Date
        private void btnEndDate_Click(object sender, EventArgs e)
        {                       
            CalendarForm calendar = new CalendarForm();            
            DialogResult result = calendar.ShowDialog();
            if (result == DialogResult.OK)
            {
                DateTime endDate = calendar.SelectedDate;
                //check wether the date is greater than todays date
                if (endDate.CompareTo(DateTime.Today) != 1)
                {
                    if (!String.IsNullOrEmpty(txtStartDate.Text))
                    {
                        DateTime startDate = DateTime.Parse(txtStartDate.Text);

                        //compare start and end date.
                        if (startDate.CompareTo(endDate) == 1)
                        {
                            MessageBox.Show("End Date should be greater than Start Date.","Invalid Entry",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        }
                        else
                        {
                            txtEndDate.Text = endDate.ToString("MM-dd-yyyy");
                            //_IdSetupCodeInfoParam.EndDate = txtEndDate.Text;

                        }
                    }
                    else
                    {
                        txtEndDate.Text = endDate.ToString("MM-dd-yyyy");
                        //_IdSetupCodeInfoParam.EndDate = txtEndDate.Text;

                    }
                }
                else
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                
            }

        }
        #endregion

        #region Start Date Leave
        private void txtStartDate_Leave(object sender, EventArgs e)
        {
            string enteredVal = txtStartDate.Text.Trim();
            if (!String.IsNullOrEmpty(enteredVal))
            {
                if (!IsDate(enteredVal))
                {
                   MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                   txtStartDate.Text = String.Empty;

                }
            }
        }
        #endregion

        #region End Date Leave
        private void txtEndDate_Leave(object sender, EventArgs e)
        {
            string enteredVal = txtEndDate.Text.Trim();
            if (!String.IsNullOrEmpty(enteredVal))
            {
                if (!IsDate(enteredVal))
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error");
                    txtEndDate.Text = String.Empty;
                }
            }
        }
        #endregion

        #region Add Match Brand
        private void btnAddMatchBrand_Click(object sender, EventArgs e)
        {
            string brandToMatch = txtMatchBrand.Text.Trim();

            if (!String.IsNullOrEmpty(brandToMatch))
            {
                lstMatchBrands.Items.Add(brandToMatch);
                //clear the text box
                txtMatchBrand.Text = String.Empty;
            }
        }
        #endregion

        #region Match Brand Preview
        private void txtMatchBrand_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                e.IsInputKey = true;
        }
        #endregion

        #region Match Brand Keyup
        private void txtMatchBrand_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string userEntry = txtMatchBrand.Text.Trim();
                if (!String.IsNullOrEmpty(userEntry))
                {
                    lstMatchBrands.Items.Add(userEntry);
                    txtMatchBrand.Text = String.Empty;
                }
           }
       }
        #endregion

        #region Remove Matched Brands
        private void btnRemoveMatchBrand_Click(object sender, EventArgs e)
        {
            while (lstMatchBrands.SelectedItems.Count > 0)
            {
                lstMatchBrands.Items.Remove(lstMatchBrands.SelectedItem);
            }
        }
        #endregion

        #region Add Filtered Barnds
        private void btnAddFilterBrand_Click(object sender, EventArgs e)
        {
            string brandToFilterOut = txtFilterBrand.Text.Trim();
            //check whether matched brands are empty
            if (!String.IsNullOrEmpty(brandToFilterOut))
            {
                lstFilterBrands.Items.Add(brandToFilterOut);

                txtFilterBrand.Text = String.Empty;
            }
        }
        #endregion

        #region Filter Brand Previrew
        private void txtFilterBrand_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                e.IsInputKey = true;
        }
        #endregion

        #region Filter Brand Keyup
        private void txtFilterBrand_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string userEntry = txtFilterBrand.Text.Trim();
                if (!String.IsNullOrEmpty(userEntry))
                {
                    lstFilterBrands.Items.Add(userEntry);
                    txtFilterBrand.Text = String.Empty;
                }
            }
        }
        #endregion

        #region Remove Brands from Filter Brand List
        private void btnRemoveFilterBrand_Click(object sender, EventArgs e)
        {
            while (lstFilterBrands.SelectedItems.Count > 0)
            {
                lstFilterBrands.Items.Remove(lstFilterBrands.SelectedItem);
                
            }
        }
        #endregion

        #region Add Matched Models
        private void btnAddMatchModel_Click(object sender, EventArgs e)
        {
            string modelToMatch = txtMatchModel.Text.Trim();

            if (!String.IsNullOrEmpty(modelToMatch))
            {
                lstMatchModels.Items.Add(modelToMatch);
                txtMatchModel.Text = String.Empty;
            }
        }
        #endregion

        #region Matched Model Keyup
        private void txtMatchModel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string userEntry = txtMatchModel.Text.Trim();
                if (!String.IsNullOrEmpty(userEntry))
                {
                    lstMatchModels.Items.Add(userEntry);
                    txtMatchModel.Text = String.Empty;
                }
            }
        }
        #endregion

        #region Matched Model Preview
        private void txtMatchModel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //code is needed for allowing user to use "enter" key
            if (e.KeyCode == Keys.Enter)
                e.IsInputKey = true;
        }
        #endregion

        #region Remove Matched Models
        private void btnRemoveMatchModel_Click(object sender, EventArgs e)
        {
            while (lstMatchModels.SelectedItems.Count > 0)
            {
                lstMatchModels.Items.Remove(lstMatchModels.SelectedItem);
            }

        }
        #endregion

        #region Add Filtered Models
        private void btnAddFilterModel_Click(object sender, EventArgs e)
        {
            string modelToFilter = txtFilterModel.Text.Trim();

            if (!String.IsNullOrEmpty(modelToFilter))
            {
                lstFilterModels.Items.Add(modelToFilter);
                txtFilterModel.Text = String.Empty;
            }
        }
        #endregion

        #region Filtered Model Preview
        private void txtFilterModel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //code is needed for allowing user to use "enter" key
            if (e.KeyCode == Keys.Enter)
                e.IsInputKey = true;
        }
        #endregion

        #region Filtered Model Keyup
        private void txtFilterModel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string userEntry = txtFilterModel.Text.Trim();
                if (!String.IsNullOrEmpty(userEntry))
                {
                    lstFilterModels.Items.Add(userEntry);
                    txtFilterModel.Text = String.Empty;
                }
            }
        }
        #endregion

        #region Remove Filtered Models
        private void btnRemoveFilterModel_Click(object sender, EventArgs e)
        {
            
            while (lstFilterModels.SelectedItems.Count > 0)
            {
                lstFilterModels.Items.Remove(lstFilterModels.SelectedItem);
            }
        }
        #endregion

        private void btnMatchBrandFile_Click(object sender, EventArgs e)
        {
            List<string> brandList =  LoadFromFile();

            foreach (string brand in brandList)
                lstMatchBrands.Items.Add(brand);
            
        }
        private void btnFilterBrandFile_Click(object sender, EventArgs e)
        {
            List<string> brandListToFilter = LoadFromFile();

            foreach (string brand in brandListToFilter)
                lstFilterBrands.Items.Add(brand);            
        }
        private void btnMatchModelFile_Click(object sender, EventArgs e)
        {
            List<string> matchModelList = LoadFromFile();

            foreach (string model in matchModelList)
                lstMatchModels.Items.Add(model);            
        }
        private void btnFilterModelFile_Click(object sender, EventArgs e)
        {
            List<string> filterModelList = LoadFromFile();

            foreach (string model in filterModelList)
                lstFilterModels.Items.Add(model);            
        }
        private void txtIDOffset_Leave(object sender, EventArgs e)
        {
            //Check for decimal
            //_IdSetupCodeInfoParam
            Int32 idOffset = 0;
            _IdSetupCodeInfoParam.IdOffset = 0;
            if (Int32.TryParse(txtIDOffset.Text, out idOffset))
            {
                if (idOffset >= 0 && idOffset < 100)
                {
                    _IdSetupCodeInfoParam.IdOffset = idOffset;
                }
                else
                {
                    MessageBox.Show("Note: ID Offset Value Range between 0 - 100.", "Report");
                    txtIDOffset.Text = "0";
                    txtIDOffset.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please entered valid ID Offset Value.","Report");
                txtIDOffset.Text = "0";
                txtIDOffset.Focus();
            }
        }
        private void btnBrowseIdenticalIdListFile_Click(object sender, EventArgs e)
        {
            GetInputFile(FileType.IdenticalIDListFile);
        }
        private void dBSFromDateTxt_Leave(object sender, EventArgs e)
        {
            string fromDateStr = dBSFromDateTxt.Text.Trim();
            if (!String.IsNullOrEmpty(fromDateStr))
            {
                DateTime fromDate;
                if (!IsDate(fromDateStr,out fromDate))
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dBSFromDateTxt.Text = String.Empty;

                }
                else if(fromDate.CompareTo(DateTime.Today) == 1)
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dBSFromDateTxt.Text = String.Empty;
                }
                else
                {
                    //if its valid, compare with to date.
                    string toDateStr = dBSToDateTxt.Text.Trim();
                    if (!string.IsNullOrEmpty(toDateStr))
                    {
                        DateTime toDate = DateTime.Parse(toDateStr);
                        if (fromDate.CompareTo(toDate) == 1)//from date greater
                        {
                            MessageBox.Show("From Date should be less than To Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            dBSFromDateTxt.Text = String.Empty;
                        }
                    }
                }
            }
        }
        private void dBSToDateTxt_Leave(object sender, EventArgs e)
        {
            string toDateStr = dBSToDateTxt.Text.Trim();
            if (!String.IsNullOrEmpty(toDateStr))
            {
                DateTime toDate;
                if (!IsDate(toDateStr,out toDate))//check date format
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dBSToDateTxt.Text = String.Empty;

                }
                else if (toDate.CompareTo(DateTime.Today) == 1)
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dBSToDateTxt.Text = String.Empty;
                }
                else//format ok.compare with from date if exists.
                {
                    string fromDateStr = dBSFromDateTxt.Text.Trim();
                    if (!String.IsNullOrEmpty(fromDateStr))//if exists
                    {
                        if (DateTime.Parse(fromDateStr).CompareTo(toDate) == 1)//compare dates
                        {
                            //from date > to date-wrong case
                            MessageBox.Show("From Date should be less than To Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            dBSToDateTxt.Text = String.Empty;

                        }
                    }
                }
            }
        }
        private void dBSFromDateBtn_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            if (calendar.ShowDialog() == DialogResult.OK)
            {
                dBSFromDateTxt.Text = calendar.SelectedDate.ToString("MM-dd-yyyy");
                dBSFromDateTxt.Focus();

            }
        }
        private void dBSToDateBtn_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            if (calendar.ShowDialog() == DialogResult.OK)
            {
                dBSToDateTxt.Text = calendar.SelectedDate.ToString("MM-dd-yyyy");
                dBSToDateTxt.Focus();

            }
        }
        private void enableDateFilterChk_CheckedChanged(object sender, EventArgs e)
        {
            dateBasedSearchGroup.Enabled = enableDateFilterChk.Checked;
            dBSToDateTxt.Text = DateTime.Today.ToString("MM-dd-yyyy");

            if (enableDateFilterChk.Checked)
                MessageBox.Show("Brand and Model filters will be disabled.","Information",MessageBoxButtons.OK,MessageBoxIcon.Information);

            brandFilterPanel.Enabled = modelFilterPanel.Enabled = !enableDateFilterChk.Checked;
            
            chkTNLinks.Enabled = true;

         }        
        private void dBSOptionRd_Checked(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;
            switch (radioBtn.Name)
            {
                case "dbsModelsRd":
                    this._modelInfoSpecificSel.ModelInfoOptions = ModelInfoSpecific.ModelReportOptions.ModelBased;
                    chkTNLinks.Enabled = true;
                    break;
                case "dBSBrandsRd":
                    this._modelInfoSpecificSel.ModelInfoOptions = ModelInfoSpecific.ModelReportOptions.BrandBased;
                    chkTNLinks.Enabled = false;//tn links not in brand based report.
                    break;
                case "dBSIDRd":
                    this._modelInfoSpecificSel.ModelInfoOptions = ModelInfoSpecific.ModelReportOptions.IDBased;
                    chkTNLinks.Enabled = false;
                    break;
                case "dBSTNRd":
                    this._modelInfoSpecificSel.ModelInfoOptions = ModelInfoSpecific.ModelReportOptions.TNBased;
                    chkTNLinks.Enabled = false;
                    break;
               
            }
        }

        private void chkFormatStartDigitSwitch_CheckedChanged(object sender, EventArgs e)
        {
            _IdSetupCodeInfoParam.StartingDigitofSetupCode = (chkFormatStartDigitSwitch.Checked) ? 1 : 0;
        }

        #region Get Brand Search List
        private void btnBrowseBrandListFile_Click(object sender, EventArgs e)
        {
            GetInputFile(FileType.BrandSearchListFile);
        }
        #endregion

        #region Start Date and End Date for POS
        private void btnPOSStartDate_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            DialogResult result = calendar.ShowDialog();
            if (result == DialogResult.OK)
            {
                DateTime startDate = calendar.SelectedDate;

                if (startDate.CompareTo(DateTime.Today) != 1)
                {
                    if (!String.IsNullOrEmpty(txtPOSEndDate.Text))
                    {
                        DateTime endDate = DateTime.Parse(txtPOSEndDate.Text);
                        //compare start and end date.
                        if (startDate.CompareTo(endDate) == 1)
                        {
                            MessageBox.Show("Start Date should be less than End Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            txtPOSStartDate.Text = startDate.ToString("MM-dd-yyyy");
                        }
                    }
                    else
                    {
                        txtPOSStartDate.Text = startDate.ToString("MM-dd-yyyy");
                    }
                }
                else
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btnPOSEndDate_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            DialogResult result = calendar.ShowDialog();
            if (result == DialogResult.OK)
            {
                DateTime endDate = calendar.SelectedDate;
                //check wether the date is greater than todays date
                if (endDate.CompareTo(DateTime.Today) != 1)
                {
                    if (!String.IsNullOrEmpty(txtPOSStartDate.Text))
                    {
                        DateTime startDate = DateTime.Parse(txtPOSStartDate.Text);

                        //compare start and end date.
                        if (startDate.CompareTo(endDate) == 1)
                        {
                            MessageBox.Show("End Date should be greater than Start Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            txtPOSEndDate.Text = endDate.ToString("MM-dd-yyyy");
                        }
                    }
                    else
                    {
                        txtPOSEndDate.Text = endDate.ToString("MM-dd-yyyy");
                    }
                }
                else
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void txtPOSStartDate_Leave(object sender, EventArgs e)
        {
            string enteredVal = txtPOSStartDate.Text.Trim();
            if (!String.IsNullOrEmpty(enteredVal))
            {
                if (!IsDate(enteredVal))
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPOSStartDate.Text = String.Empty;

                }
            }
        }
        private void txtPOSEndDate_Leave(object sender, EventArgs e)
        {
            string enteredVal = txtPOSEndDate.Text.Trim();
            if (!String.IsNullOrEmpty(enteredVal))
            {
                if (!IsDate(enteredVal))
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error");
                    txtPOSEndDate.Text = String.Empty;

                }
            }
        }
        #endregion

        #region Start Date and End Date for OSS
        private void btnOSSStartDate_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            DialogResult result = calendar.ShowDialog();
            if (result == DialogResult.OK)
            {
                DateTime startDate = calendar.SelectedDate;

                if (startDate.CompareTo(DateTime.Today) != 1)
                {
                    if (!String.IsNullOrEmpty(txtOSSEndDate.Text))
                    {
                        DateTime endDate = DateTime.Parse(txtOSSEndDate.Text);
                        //compare start and end date.
                        if (startDate.CompareTo(endDate) == 1)
                        {
                            MessageBox.Show("Start Date should be less than End Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            txtOSSStartDate.Text = startDate.ToString("MM-dd-yyyy");
                        }
                    }
                    else
                    {
                        txtOSSStartDate.Text = startDate.ToString("MM-dd-yyyy");
                    }
                }
                else
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btnOSSEndDate_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            DialogResult result = calendar.ShowDialog();
            if (result == DialogResult.OK)
            {
                DateTime endDate = calendar.SelectedDate;
                //check wether the date is greater than todays date
                if (endDate.CompareTo(DateTime.Today) != 1)
                {
                    if (!String.IsNullOrEmpty(txtOSSStartDate.Text))
                    {
                        DateTime startDate = DateTime.Parse(txtOSSStartDate.Text);

                        //compare start and end date.
                        if (startDate.CompareTo(endDate) == 1)
                        {
                            MessageBox.Show("End Date should be greater than Start Date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            txtOSSEndDate.Text = endDate.ToString("MM-dd-yyyy");
                        }
                    }
                    else
                    {
                        txtOSSEndDate.Text = endDate.ToString("MM-dd-yyyy");
                    }
                }
                else
                {
                    MessageBox.Show("Enter a date smaller than today's date.", "Invalid entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void txtOSSStartDate_Leave(object sender, EventArgs e)
        {
            string enteredVal = txtOSSStartDate.Text.Trim();
            if (!String.IsNullOrEmpty(enteredVal))
            {
                if (!IsDate(enteredVal))
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtOSSStartDate.Text = String.Empty;

                }
            }
        }
        private void txtOSSEndDate_Leave(object sender, EventArgs e)
        {
            string enteredVal = txtOSSStartDate.Text.Trim();
            if (!String.IsNullOrEmpty(enteredVal))
            {
                if (!IsDate(enteredVal))
                {
                    MessageBox.Show("Please enter a valid date in MM-dd-yyyy format", "Date Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtOSSStartDate.Text = String.Empty;

                }
            }
        }
        #endregion                

        #region On State Change of POS Check Box
        private void chkPOSAvail_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPOSAvail.Checked == true)
            {
                btnPOSStartDate.Enabled = true;
                btnPOSEndDate.Enabled = true;
                txtPOSStartDate.Enabled = true;
                txtPOSEndDate.Enabled = true;
            }
            else
            {
                txtPOSStartDate.Text = "";
                txtPOSEndDate.Text = "";
                btnPOSStartDate.Enabled = false;
                btnPOSEndDate.Enabled = false;
                txtPOSStartDate.Enabled = false;
                txtPOSEndDate.Enabled = false;
            }
        }
        #endregion

        #region On State Change of Online Search Statics Check Box
        private void chkOSSAvail_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOSSAvail.Checked == true)
            {
                txtOSSStartDate.Enabled = true;
                txtOSSEndDate.Enabled = true;
                btnOSSStartDate.Enabled = true;
                btnOSSEndDate.Enabled = true;
            }
            else
            {
                txtOSSStartDate.Text = "";
                txtOSSEndDate.Text = "";
                txtOSSStartDate.Enabled = false;
                txtOSSEndDate.Enabled = false;
                btnOSSStartDate.Enabled = false;
                btnOSSEndDate.Enabled = false;
            }
        }
        #endregion
        
        #region On State Change of Model
        private void chkModel_CheckedChanged(object sender, EventArgs e)
        {
            if (chkModel.Checked == true)
            {
                chkPOSAvail.Checked = false;
                chkPOSAvail.Enabled = false;
                chkOSSAvail.Checked = false;
                chkOSSAvail.Enabled = false;
            }
            else
            {                
                chkPOSAvail.Enabled = true;
                chkOSSAvail.Enabled = true;
            }
            cmbBxModelTypeBBS.Enabled = chkModel.Checked;
            chkBxBlankModelNameBBS.Enabled = chkModel.Checked;
        }
        #endregion

        #region LabelIntronSearch
        private void AddLabelOrIntron(string userInput)
        {
            if (!String.IsNullOrEmpty(userInput))
            {
                String m_ModifiedString = userInput;
                //if(m_ModifiedString.StartsWith("*"))
                //{
                //    m_ModifiedString = m_ModifiedString.Remove(0, 1).Insert(0, "%");
                //}

                //if(m_ModifiedString.EndsWith("*"))
                //{
                //    int index = m_ModifiedString.LastIndexOf('*');
                //    m_ModifiedString = m_ModifiedString.Remove(index, 1) + "%";
                //}

                if(!lstBxSearchPatterns.Items.Contains(m_ModifiedString))
                    lstBxSearchPatterns.Items.Add(m_ModifiedString);
            }
        }
        private void bttnLoadLabelIntronFile_Click(object sender, EventArgs e)
        {
            List<string> fileInputs = LoadFromFile();

            foreach (string input in fileInputs)
            {
                //lstBxSearchPatterns.Items.Add(input);
                AddLabelOrIntron(input);
            }
        }
        private void bttnRemoveLabelIntron_Click(object sender, EventArgs e)
        {
            while (lstBxSearchPatterns.SelectedItems.Count > 0)
            {
                lstBxSearchPatterns.Items.Remove(lstBxSearchPatterns.SelectedItem);
            }
        }
        private void bttnAddLabelIntron_Click(object sender, EventArgs e)
        {
            string userInput = txtBxEnterLabelIntron.Text.Trim();
            AddLabelOrIntron(userInput);
            txtBxEnterLabelIntron.Text = String.Empty;                            
        }
        private void txtBxEnterLabelIntron_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.IsInputKey = true;
            }
        }
        private void txtBxEnterLabelIntron_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                string userInput = txtBxEnterLabelIntron.Text.Trim();
                AddLabelOrIntron(userInput);
                txtBxEnterLabelIntron.Text = String.Empty;
                
            }
        }
        private void btnShowDictionary_Click(object sender, EventArgs e)
        {
            LabelIntronDictionary dict = new LabelIntronDictionary();
            dict.LabelAndIntronSelected += new LabelIntronSelectedEventHandler(dict_LabelAndIntronSelected);
            dict.ShowDialog();
        }
        void dict_LabelAndIntronSelected(object sender, LabelIntronSelectedEventArgs e)
        {
            List<object[]> selectedItems = e.SelectedLabelsWithIntrons;

            foreach (object[] item in selectedItems)
            {
                object itemToBeAdded = null;

                if (this._ReportType == ReportType.LabelSearch)
                    itemToBeAdded = item[0];
                else if (this._ReportType == ReportType.IntronSearch)
                    itemToBeAdded = item[1];
                else if(this._ReportType == ReportType.IntronSearchWithBrand)
                    itemToBeAdded = item[1];
                else if(this._ReportType == ReportType.LabelSearchWithBrand)
                    itemToBeAdded = item[0];
                else if(this._ReportType == ReportType.ExcludeLabelSearch)
                    itemToBeAdded = item[0];
                else if(this._ReportType == ReportType.ExcludeIntronSearch)
                    itemToBeAdded = item[1];
                AddLabelOrIntron(itemToBeAdded.ToString());
            }
        }
        #endregion

        private void chkBxIncludeBlankModelNames_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.IncludeBlankModelNames = chkBxIncludeBlankModelNames.Checked;
        }       
        private void cmbBxModelTypeISCI_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedType = cmbBxModelTypeISCI.Text;
            this._IdSetupCodeInfoParam.ModelType = GetModelTypeValue(selectedType);
        }
        private void cmbBxModelTypeBBS_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedType = cmbBxModelTypeBBS.Text;
            this._IdSetupCodeInfoParam.ModelType = GetModelTypeValue(selectedType);
           
        }
        private int GetModelTypeValue(String modelType)
        {
            Int32 returnValue = 0;
            if (!String.IsNullOrEmpty(modelType))
            {
                switch (modelType)
                {
                    case "Remote Model":
                       returnValue = 2;
                       break; 
                    case "Target Model":
                        returnValue = 1;
                        break;
                    case "All":
                        returnValue = 0;
                        break;
                }
            }
            return returnValue;
        }

        #region File Types
        private enum FileType
        {
            PriorityIDListFile,
            AliasIDListFile,
            IdenticalIDListFile,
            BrandSearchListFile,
            QuickSetPriorityIDListFile
        }
        #endregion

        #region Inlude Alias Brand
        private void chkIncludeAliasBrand_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.InclideAliasBrand = chkIncludeAliasBrand.Checked;
        }
        #endregion

        #region Include Non Country Linked Records
        private void chkIncludeNonCountryRecords_CheckedChanged(object sender, EventArgs e)
        {
            _LoadDevicesFlag = true;
            this._IdSetupCodeInfoParam.IncludeNonCountryLinkedRecords = chkIncludeNonCountryRecords.Checked;
        }
        #endregion

        #region Empty Device Type (IDBased_Device)
        private void chkIncludeEmptyDeviceType_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.IncludeEmptyDeviceType = chkIncludeEmptyDeviceType.Checked;
        }
        #endregion

        #region Include Non Component Linked Records
        private void chkIncludeNonComponentLinkedRecords_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.IncludeNonComponentLinkedRecords = chkIncludeNonComponentLinkedRecords.Checked;
        }   
        #endregion

        #region Component Status
        private void rdoCompStatusBuiltIn_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.ComponentStatus = "1";
        }
        private void rdoCompStatusNonBuiltIn_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.ComponentStatus = "0";
        }
        private void rdoCompStatusBoth_CheckedChanged(object sender, EventArgs e)
        {
            this._IdSetupCodeInfoParam.ComponentStatus = "";
        }
        #endregion       

        #region Browse Identical File
        private void btnMIIdenticalBrowse_Click(object sender, EventArgs e)
        {
            GetInputFile(FileType.IdenticalIDListFile);              
        }
        #endregion

        #region Include Unknown Locations
        private void chkUnknownLocation_CheckedChanged(object sender, EventArgs e)
        {
            _LoadDevicesFlag = true;
            this._IdSetupCodeInfoParam.IncludeUnknownLocation = chkUnknownLocation.Checked;
        }
        #endregion            

        #region QuickSet Sort Setup Code Selected
        private void rdoQSSortByModelCount_CheckedChanged(object sender, EventArgs e)
        {
            QuickSetInputs.PriorityIDList = null;
            txtQSPriorityIdListFile.Text = String.Empty;            
            if (rdoQSSortByModelCount.Checked)
                QuickSetInputs.SortType = ReportSortBy.ModelCount;
        }
        private void rdoQSSortByIDNo_CheckedChanged(object sender, EventArgs e)
        {
            QuickSetInputs.PriorityIDList = null;
            txtQSPriorityIdListFile.Text = String.Empty;
            if (rdoQSSortByIDNo.Checked)
                QuickSetInputs.SortType = ReportSortBy.IDNumber;
        }
        private void rdoQSSortByPriorityIDList_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoQSSortByPriorityIDList.Checked)
            {
                QuickSetInputs.SortType = ReportSortBy.PriorityIDListFile;
                GetInputFile(FileType.QuickSetPriorityIDListFile);
            }
            else
            {
                txtQSPriorityIdListFile.Text = String.Empty;
                QuickSetInputs.PriorityIDList = null;
            }           
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            ReadInputAddMode();
        }
        private void ReadInputAddMode()
        {
            if (!ValidateModeInputs(txtModeName.Text))
            {
                txtModeName.Focus();
                return;
            }

            txtModeList.Text = RemoveDuplicateDevicesInGroup(txtModeList.Text.ToUpper());
            if (!ValidateDeviceGroups(txtModeList.Text.ToUpper()))
            {
                txtModeList.Focus();
                return;
            }
            AddMode(txtModeName.Text, txtModeList.Text);
        }
        private void AddMode(String modeName, String devices)
        {
            int newRow = this.dataGridQuickSet.Rows.Add(1);
            dataGridQuickSet.Rows[newRow].Cells[0].Value = newRow;
            dataGridQuickSet.Rows[newRow].Cells[1].Value = modeName;
            dataGridQuickSet.Rows[newRow].Cells[2].Value = devices.ToUpper();
            dataGridQuickSet.FirstDisplayedScrollingRowIndex = newRow;
            if (this.QuickSetInputs.ModeSetup == null)
                this.QuickSetInputs.ModeSetup = new Dictionary<Int32, QuickSetModeSetup>();
            this.QuickSetInputs.ModeSetup.Add(newRow, new QuickSetModeSetup(modeName, devices.ToUpper(),new List<String>()));
            
            txtModeName.Text = String.Empty;
            txtModeList.Text = String.Empty;
            txtModeName.Focus();
        }
        private void btnUp_Click(object sender, EventArgs e)
        {
            if (this.dataGridQuickSet.Rows.Count < 2)
                return;
            if (MessageBox.Show("Selected Sub Devices will be reset \r\n Want to Continuee...", "QuickSet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                DataGridViewRow r = dataGridQuickSet.SelectedRows[0];
                if (r.Index > 0)
                {
                    int index = r.Index;
                    dataGridQuickSet.Rows.RemoveAt(index--);
                    InsertRow(r, index);
                }
            }
        }
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (dataGridQuickSet.Rows.Count < 2)
                return;
            if (MessageBox.Show("Selected Sub Devices will be reset \r\n Want to Continuee...", "QuickSet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                DataGridViewRow r = dataGridQuickSet.SelectedRows[0];
                if (r.Index < dataGridQuickSet.Rows.Count - 1)
                {
                    int index = r.Index;
                    dataGridQuickSet.Rows.RemoveAt(index++);
                    InsertRow(r, index);
                }
            }
        }
        private void dataGridQuickSet_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (MessageBox.Show("Selected Sub Devices will be reset \r\n Want to Continuee...", "QuickSet", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }       
        private void dataGridQuickSet_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {            
            RenumberGridRows();
        }
        private void dataGridQuickSet_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            validateGridCellEdits(e.RowIndex, e.ColumnIndex);
        }
        private void InsertRow(DataGridViewRow source, int index)
        {
            dataGridQuickSet.Rows.Insert(index, index, source.Cells[1].Value, source.Cells[2].Value);
            RenumberGridRows();
            dataGridQuickSet.ClearSelection();
            dataGridQuickSet.Rows[index].Selected = true;
            dataGridQuickSet.FirstDisplayedScrollingRowIndex = index;
        }
        private void RenumberGridRows()
        {            
            this.QuickSetInputs.ModeSetup = new Dictionary<Int32, QuickSetModeSetup>();            
            int count = 0;
            foreach (DataGridViewRow Row in dataGridQuickSet.Rows)
            {
                Row.Cells[0].Value = count++;
                this.QuickSetInputs.ModeSetup.Add(Convert.ToInt32(Row.Cells[0].Value.ToString()), new QuickSetModeSetup(Row.Cells[1].Value.ToString(), Row.Cells[2].Value.ToString().ToUpper(), new List<String>()));
            }
        }
        private void validateGridCellEdits(int row, int column)
        {
            DataGridViewRow item = dataGridQuickSet.Rows[row];
            string mode = (string)item.Cells[1].Value;
            string devices = (string)item.Cells[2].Value;            
            switch (column)
            {
                case 1:
                    if (!ValidateModeInputs(mode))
                        return;
                    break;
                case 2:
                    devices = RemoveDuplicateDevicesInGroup(devices.ToUpper());
                    if (ValidateDeviceGroups(devices.ToUpper()))
                        item.Cells[2].Value = devices.ToUpper();
                    else
                    {
                        item.Cells[2].Value = "";
                        return;
                    }                    
                    break;                              
            }
        }
        private Boolean ValidateModeInputs(String text)
        {
            if (String.IsNullOrEmpty(text))
            {
                MessageBox.Show("Please enter Mode Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private Boolean ValidateDeviceGroups(String text)
        {
            if (String.IsNullOrEmpty(text))
            {
                MessageBox.Show("Please enter Device info", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (!String.IsNullOrEmpty(text))
            {
                Char[] checkValidModes = text.ToCharArray();
                Boolean isValid = true;
                foreach (Char item in checkValidModes)
                {
                    String modeExist = allModeNames.getModeName(item.ToString());
                    if (String.IsNullOrEmpty(modeExist))
                    {
                        isValid = false;
                    }
                }
                if (isValid == false)
                {
                    MessageBox.Show("Please enter valid Mode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return CheckForDeviceOverlaps(text);
        }        
        private Boolean CheckForDeviceOverlaps(String text)
        {
            IDictionary devGrps = new Dictionary<String, String>();
            foreach (Char c in text)
            {
                String dev = c.ToString();
                foreach (DictionaryEntry group in m_GroupList)
                {
                    if (group.Value.ToString().Contains(dev))
                        if (!devGrps.Contains(group.Key))
                            devGrps.Add(group.Key, dev);
                        else
                        {
                            String value = devGrps[group.Key] + dev;
                            devGrps.Remove(group.Key);
                            devGrps.Add(group.Key, value);
                        }
                }
            }
            if (devGrps.Count > 1)
            {
                String message = "Some of your selected Devices may contain overlapping ID numbers:\r\n\r\n";
                foreach (DictionaryEntry entry in devGrps)
                {
                    message += String.Format("\t{0}:{1}\r\n", entry.Key, entry.Value);
                }
                message += "\r\nIf you continue overlapping IDs will be renumbered.\r\nDo you want to continue?";
                if (MessageBox.Show(message, "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return false;
            }
            return true;
        }
        private String RemoveDuplicateDevicesInGroup(String text)
        {
            String result = String.Empty;
            Boolean duplicates = false;
            foreach (Char c in text)
            {
                if (result.IndexOf(c) < 0)
                    result += c.ToString();
                else
                    duplicates = true;
            }

            if (duplicates)
                MessageBox.Show("Duplicate devices are not allowed.\r\n Duplicates were removed", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return result;
        }
        private void LoadModeList()
        {
            m_GroupList = new Dictionary<String, String>();
            Reports report = new Reports();
            allModeNames = report.GetAllModeNames();                        
            if (allModeNames != null)
            {
                this.dataGridModeList.DataSource = allModeNames;                                        
            }
            BuildDeviceGroupList();
        }
        private void BuildDeviceGroupList()
        {
            Reports report = new Reports();
            IList<String> deviceGroups = report.GetDeviceGroupList();
            foreach (String s in deviceGroups)
            {
                IList<String> test = report.GetModeListForDeviceGroup(s);
                String list = String.Empty;
                foreach (String device in test)
                    list += device;
                m_GroupList.Add(s, list);
            }
        }
        private void txtModeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 0x0d)
            {
                txtModeName.Focus();
                e.Handled = true;
            }
        }
        private void txtModeList_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }
        #endregion    
        
        #region QuickSet Sub Device Selection
        private CheckedListBox checkedListBox = null;
        private ToolStripDropDown toolStripDropDown = null;
        private Int32 intMaxHeight = 300;        
        public Int32 MaxHeight
        {
            get { return intMaxHeight; }
            set { intMaxHeight = value; }
        }       
        private CheckedListBox QuickSetCheckedListBox
        {
            get { return checkedListBox; }
            set { checkedListBox = value; }
        }
        private ToolStripDropDown QuickSetToolStripDropDown
        {
            get { return toolStripDropDown; }
            set { toolStripDropDown = value; }
        }
        private void dataGridQuickSet_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right && e.RowIndex != -1 && e.ColumnIndex != -1 && QuickSetCheckedListBox != null)
            if (e.RowIndex != -1 && e.ColumnIndex == -1 && QuickSetCheckedListBox != null)
            {
                this.dataGridQuickSet.ClearSelection();
                QuickSetCheckedListBox.Items.Clear();
                if (deviceCollection != null)
                {
                    QuickSetModeSetup modeSetup = this.QuickSetInputs.ModeSetup[Convert.ToInt32(dataGridQuickSet.Rows[e.RowIndex].Cells[0].Value.ToString())];
                    List<String> subdevicetypes = deviceCollection.GetSubDevices(dataGridQuickSet.Rows[e.RowIndex].Cells[2].Value.ToString().ToUpper());
                    foreach (String item in subdevicetypes)
                    {
                        if (modeSetup.SubDevices.Contains(item))
                            QuickSetCheckedListBox.Items.Add(item, true);
                        else
                            QuickSetCheckedListBox.Items.Add(item, false);
                    }
                }
                if (QuickSetCheckedListBox.Items.Count > 0)
                {
                    Int32 PreferredHeight = (QuickSetCheckedListBox.Items.Count * 16) + 7;
                    QuickSetCheckedListBox.Height = (PreferredHeight < MaxHeight) ? PreferredHeight : MaxHeight;
                    QuickSetCheckedListBox.Width = this.Width;
                    this.dataGridQuickSet.Rows[e.RowIndex].Selected = true;
                    QuickSetToolStripDropDown.Show(dataGridQuickSet, dataGridQuickSet.PointToClient(System.Windows.Forms.Cursor.Position));
                }
            }
        }
        private void ShowQuickSetContextMenu()
        {
            QuickSetCheckedListBox = new CheckedListBox();
            QuickSetCheckedListBox.CheckOnClick = true;            
            ToolStripControlHost controlHost = new ToolStripControlHost(QuickSetCheckedListBox);
            controlHost.Padding = Padding.Empty;
            controlHost.Margin = Padding.Empty;
            controlHost.Dock = DockStyle.Fill;
            controlHost.AutoSize = true;

            QuickSetToolStripDropDown = new ToolStripDropDown();
            QuickSetToolStripDropDown.Padding = Padding.Empty;
            QuickSetToolStripDropDown.Items.Add(controlHost);
          
            Button btnApply = new Button();
            btnApply.Name = "btnApply";
            btnApply.Text = "Apply";
            btnApply.Click += new EventHandler(btnApply_Click);

            controlHost = new ToolStripControlHost(btnApply);
            controlHost.Padding = Padding.Empty;
            controlHost.Margin = new Padding(2, 2, 2, 2);
            controlHost.Dock = DockStyle.Fill;
            QuickSetToolStripDropDown.Items.Add(controlHost);            
        }
        void btnApply_Click(object sender, EventArgs e)
        {
            if (QuickSetCheckedListBox.CheckedItems.Count > 0)
            {
                QuickSetModeSetup modeSetup = this.QuickSetInputs.ModeSetup[Convert.ToInt32(this.dataGridQuickSet.SelectedRows[0].Cells[0].Value.ToString())];

                List<String> subdevices = new List<String>();
                for (int i = 0; i < QuickSetCheckedListBox.CheckedItems.Count; i++)
                {
                    subdevices.Add(QuickSetCheckedListBox.CheckedItems[i].ToString());                 
                }
                modeSetup.SubDevices = subdevices;
                QuickSetToolStripDropDown.Close();
            }
        }
        void quicksetProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
            }
            else if (e.Error != null)
            {

            }
            else
            {
                m_Progress.Close();
                _LoadDevicesFlag = false;
                ShowQuickSetContextMenu();
            }
        }
        void quicksetProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            GetDeviceTypes();
        }
        private void LoadQuickSetSubDeviceTypes()
        {           
            if (quicksetProcess != null && quicksetProcess.IsBusy == false)
                quicksetProcess.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new Forms.ProgressForm();
            m_Progress.SetMessage("Please Wait, Loading Sub Devices...");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
        }
        #endregion                                 

        #region Key Function Report Type
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked == true)
                KeyFunctionReportType = 1;
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked == true)
                KeyFunctionReportType = 2;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked == true)
                KeyFunctionReportType = 3;
        }
        #endregion

        #region Supported Ids By Platforms
        private void chkIncludeUnsupportedIds_CheckedChanged(object sender, EventArgs e)
        {
            if (treeViewPlatforms.Nodes.Count > 0)
                treeViewPlatforms.Nodes.Clear();
            if (((CheckBox)sender).Checked == true)
            {                
                grpPlatforms.Enabled = false;
            }
            else
            {
                grpPlatforms.Enabled = true;
                if (this.Platforms != null)
                {                    
                    treeViewPlatforms.Nodes.Add("All", "All Platforms");
                    treeViewPlatforms.Nodes["All"].Tag = "All";
                    foreach (Hashtable item in this.Platforms)
                    {
                        treeViewPlatforms.Nodes["All"].Nodes.Add(item["Platform_RID"].ToString(), item["Name"].ToString());
                        treeViewPlatforms.Nodes["All"].Nodes[item["Platform_RID"].ToString()].Tag = "Platform";
                    }
                    checkNodes(treeViewPlatforms.Nodes["All"], true);
                    treeViewPlatforms.Nodes["All"].Checked = true;
                    treeViewPlatforms.ExpandAll();                    
                }
            }
        }
        private void treeViewPlatforms_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    if (node.Tag.ToString() == "All")
                    {
                        checkNodes(node, node.Checked);
                    }
                    else
                    {
                        checkNodes(node, node.Checked);
                        if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                        {
                            node.Parent.Checked = true;
                        }
                        else
                        {
                            node.Parent.Checked = false;
                        }
                    }
                }
            }
        }
        private void GetSupportedPlatform()
        {
            if (chkIncludeUnsupportedIds.Checked == false)
            {
                if (_IdSetupCodeInfoParam == null)
                    _IdSetupCodeInfoParam = new IdSetupCodeInfo();
                _IdSetupCodeInfoParam.IncludeUnsupportedPlatforms = false;
                if (treeViewPlatforms.Nodes.Count > 0)
                {
                    if (_IdSetupCodeInfoParam.SupportedPlatforms == null)
                        _IdSetupCodeInfoParam.SupportedPlatforms = new List<String>();
                    else
                        _IdSetupCodeInfoParam.SupportedPlatforms.Clear();
                    foreach (TreeNode parentNode in treeViewPlatforms.Nodes)
                    {
                        if (parentNode.Nodes != null)
                        {
                            foreach (TreeNode subNode in parentNode.Nodes)
                            {
                                if (subNode.Checked == true)
                                {                                   
                                    _IdSetupCodeInfoParam.SupportedPlatforms.Add(subNode.Text);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                _IdSetupCodeInfoParam.IncludeUnsupportedPlatforms = true;
                _IdSetupCodeInfoParam.SupportedPlatforms = null;
            }
        }
        #endregion        
    }   
}