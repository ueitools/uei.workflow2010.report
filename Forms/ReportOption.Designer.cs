namespace UEI.Workflow2010.Report
{
    partial class ReportOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoWithDeviceType = new System.Windows.Forms.RadioButton();
            this.rdoWithoutDeviceType = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoWithDeviceType);
            this.groupBox1.Controls.Add(this.rdoWithoutDeviceType);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(359, 64);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // rdoWithDeviceType
            // 
            this.rdoWithDeviceType.AutoSize = true;
            this.rdoWithDeviceType.Location = new System.Drawing.Point(195, 20);
            this.rdoWithDeviceType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoWithDeviceType.Name = "rdoWithDeviceType";
            this.rdoWithDeviceType.Size = new System.Drawing.Size(140, 21);
            this.rdoWithDeviceType.TabIndex = 1;
            this.rdoWithDeviceType.Text = "With Device Type";
            this.rdoWithDeviceType.UseVisualStyleBackColor = true;
            this.rdoWithDeviceType.CheckedChanged += new System.EventHandler(this.rdoWithDeviceType_CheckedChanged);
            // 
            // rdoWithoutDeviceType
            // 
            this.rdoWithoutDeviceType.AutoSize = true;
            this.rdoWithoutDeviceType.Location = new System.Drawing.Point(16, 20);
            this.rdoWithoutDeviceType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoWithoutDeviceType.Name = "rdoWithoutDeviceType";
            this.rdoWithoutDeviceType.Size = new System.Drawing.Size(160, 21);
            this.rdoWithoutDeviceType.TabIndex = 0;
            this.rdoWithoutDeviceType.Text = "Without Device Type";
            this.rdoWithoutDeviceType.UseVisualStyleBackColor = true;
            this.rdoWithoutDeviceType.CheckedChanged += new System.EventHandler(this.rdoWithoutDeviceType_CheckedChanged);
            // 
            // ReportOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(359, 64);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ReportOption";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report Options";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoWithDeviceType;
        private System.Windows.Forms.RadioButton rdoWithoutDeviceType;
    }
}