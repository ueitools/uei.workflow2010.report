namespace UEI.Workflow2010.Report
{
    partial class EXIDTNInfoReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.baseSplitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.executorGroup = new System.Windows.Forms.GroupBox();
            this.executorMsg = new System.Windows.Forms.Label();
            this.executorHeader = new System.Windows.Forms.Label();
            this.executorList = new System.Windows.Forms.ListBox();
            this.brandGroup = new System.Windows.Forms.GroupBox();
            this.brandMsg = new System.Windows.Forms.Label();
            this.brandHeader = new System.Windows.Forms.Label();
            this.brandList = new System.Windows.Forms.ListBox();
            this.idGroup = new System.Windows.Forms.GroupBox();
            this.idMsg = new System.Windows.Forms.Label();
            this.idHeader = new System.Windows.Forms.Label();
            this.idList = new System.Windows.Forms.ListBox();
            this.tnGroup = new System.Windows.Forms.GroupBox();
            this.tnFooter = new System.Windows.Forms.Label();
            this.tnHeader = new System.Windows.Forms.Label();
            this.tnList = new System.Windows.Forms.ListBox();
            this.modelInfoHeader2 = new System.Windows.Forms.Label();
            this.modelInfoHeader = new System.Windows.Forms.Label();
            this.prefixHeader = new System.Windows.Forms.Label();
            this.dgPrefixInfo = new System.Windows.Forms.DataGridView();
            this.dgModelInfo = new System.Windows.Forms.DataGridView();
            this.baseSplitContainer.Panel1.SuspendLayout();
            this.baseSplitContainer.Panel2.SuspendLayout();
            this.baseSplitContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.executorGroup.SuspendLayout();
            this.brandGroup.SuspendLayout();
            this.idGroup.SuspendLayout();
            this.tnGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrefixInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgModelInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // baseSplitContainer
            // 
            this.baseSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.baseSplitContainer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.baseSplitContainer.Name = "baseSplitContainer";
            this.baseSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // baseSplitContainer.Panel1
            // 
            this.baseSplitContainer.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // baseSplitContainer.Panel2
            // 
            this.baseSplitContainer.Panel2.Controls.Add(this.modelInfoHeader2);
            this.baseSplitContainer.Panel2.Controls.Add(this.modelInfoHeader);
            this.baseSplitContainer.Panel2.Controls.Add(this.prefixHeader);
            this.baseSplitContainer.Panel2.Controls.Add(this.dgPrefixInfo);
            this.baseSplitContainer.Panel2.Controls.Add(this.dgModelInfo);
            this.baseSplitContainer.Size = new System.Drawing.Size(1417, 838);
            this.baseSplitContainer.SplitterDistance = 431;
            this.baseSplitContainer.SplitterWidth = 5;
            this.baseSplitContainer.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.executorGroup, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.brandGroup, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.idGroup, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tnGroup, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1417, 431);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // executorGroup
            // 
            this.executorGroup.Controls.Add(this.executorMsg);
            this.executorGroup.Controls.Add(this.executorHeader);
            this.executorGroup.Controls.Add(this.executorList);
            this.executorGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.executorGroup.Location = new System.Drawing.Point(4, 4);
            this.executorGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.executorGroup.Name = "executorGroup";
            this.executorGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.executorGroup.Size = new System.Drawing.Size(346, 423);
            this.executorGroup.TabIndex = 1;
            this.executorGroup.TabStop = false;
            // 
            // executorMsg
            // 
            this.executorMsg.AutoSize = true;
            this.executorMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.executorMsg.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.executorMsg.Location = new System.Drawing.Point(4, 405);
            this.executorMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.executorMsg.Name = "executorMsg";
            this.executorMsg.Size = new System.Drawing.Size(229, 14);
            this.executorMsg.TabIndex = 6;
            this.executorMsg.Text = "*Select an executor to view the ids";
            // 
            // executorHeader
            // 
            this.executorHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.executorHeader.Location = new System.Drawing.Point(4, 16);
            this.executorHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.executorHeader.Name = "executorHeader";
            this.executorHeader.Size = new System.Drawing.Size(131, 28);
            this.executorHeader.TabIndex = 5;
            this.executorHeader.Text = "EXECUTORS";
            this.executorHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // executorList
            // 
            this.executorList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.executorList.FormattingEnabled = true;
            this.executorList.ItemHeight = 16;
            this.executorList.Location = new System.Drawing.Point(4, 52);
            this.executorList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.executorList.Name = "executorList";
            this.executorList.Size = new System.Drawing.Size(334, 324);
            this.executorList.TabIndex = 1;
            this.executorList.SelectedIndexChanged += new System.EventHandler(this.executorList_SelectedIndexChanged);
            // 
            // brandGroup
            // 
            this.brandGroup.Controls.Add(this.brandMsg);
            this.brandGroup.Controls.Add(this.brandHeader);
            this.brandGroup.Controls.Add(this.brandList);
            this.brandGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.brandGroup.Location = new System.Drawing.Point(1066, 4);
            this.brandGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.brandGroup.Name = "brandGroup";
            this.brandGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.brandGroup.Size = new System.Drawing.Size(347, 423);
            this.brandGroup.TabIndex = 4;
            this.brandGroup.TabStop = false;
            // 
            // brandMsg
            // 
            this.brandMsg.AutoSize = true;
            this.brandMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.brandMsg.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brandMsg.Location = new System.Drawing.Point(4, 405);
            this.brandMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.brandMsg.Name = "brandMsg";
            this.brandMsg.Size = new System.Drawing.Size(298, 14);
            this.brandMsg.TabIndex = 7;
            this.brandMsg.Text = "*Select a brand to view the model information";
            // 
            // brandHeader
            // 
            this.brandHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brandHeader.Location = new System.Drawing.Point(13, 18);
            this.brandHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.brandHeader.Name = "brandHeader";
            this.brandHeader.Size = new System.Drawing.Size(252, 23);
            this.brandHeader.TabIndex = 4;
            this.brandHeader.Text = "BRANDS";
            // 
            // brandList
            // 
            this.brandList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.brandList.FormattingEnabled = true;
            this.brandList.ItemHeight = 16;
            this.brandList.Location = new System.Drawing.Point(13, 52);
            this.brandList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.brandList.Name = "brandList";
            this.brandList.Size = new System.Drawing.Size(327, 324);
            this.brandList.TabIndex = 1;
            this.brandList.SelectedIndexChanged += new System.EventHandler(this.brandList_SelectedIndexChanged);
            // 
            // idGroup
            // 
            this.idGroup.Controls.Add(this.idMsg);
            this.idGroup.Controls.Add(this.idHeader);
            this.idGroup.Controls.Add(this.idList);
            this.idGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.idGroup.Location = new System.Drawing.Point(358, 4);
            this.idGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.idGroup.Name = "idGroup";
            this.idGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.idGroup.Size = new System.Drawing.Size(346, 423);
            this.idGroup.TabIndex = 2;
            this.idGroup.TabStop = false;
            // 
            // idMsg
            // 
            this.idMsg.AutoSize = true;
            this.idMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.idMsg.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idMsg.Location = new System.Drawing.Point(4, 391);
            this.idMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.idMsg.Name = "idMsg";
            this.idMsg.Size = new System.Drawing.Size(342, 14);
            this.idMsg.TabIndex = 5;
            this.idMsg.Text = "*Select an id to view  tn,brand and prefix information";
            // 
            // idHeader
            // 
            this.idHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idHeader.Location = new System.Drawing.Point(9, 18);
            this.idHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.idHeader.Name = "idHeader";
            this.idHeader.Size = new System.Drawing.Size(325, 23);
            this.idHeader.TabIndex = 3;
            this.idHeader.Text = "IDs";
            // 
            // idList
            // 
            this.idList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.idList.FormattingEnabled = true;
            this.idList.ItemHeight = 16;
            this.idList.Location = new System.Drawing.Point(9, 52);
            this.idList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.idList.Name = "idList";
            this.idList.Size = new System.Drawing.Size(325, 324);
            this.idList.TabIndex = 2;
            this.idList.SelectedIndexChanged += new System.EventHandler(this.idList_SelectedIndexChanged);
            // 
            // tnGroup
            // 
            this.tnGroup.Controls.Add(this.tnFooter);
            this.tnGroup.Controls.Add(this.tnHeader);
            this.tnGroup.Controls.Add(this.tnList);
            this.tnGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tnGroup.Location = new System.Drawing.Point(712, 4);
            this.tnGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tnGroup.Name = "tnGroup";
            this.tnGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tnGroup.Size = new System.Drawing.Size(346, 423);
            this.tnGroup.TabIndex = 3;
            this.tnGroup.TabStop = false;
            // 
            // tnFooter
            // 
            this.tnFooter.AutoSize = true;
            this.tnFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tnFooter.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tnFooter.Location = new System.Drawing.Point(4, 405);
            this.tnFooter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tnFooter.Name = "tnFooter";
            this.tnFooter.Size = new System.Drawing.Size(274, 14);
            this.tnFooter.TabIndex = 7;
            this.tnFooter.Text = "*Select a tn to highlight supported brands";
            // 
            // tnHeader
            // 
            this.tnHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tnHeader.Location = new System.Drawing.Point(9, 18);
            this.tnHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tnHeader.Name = "tnHeader";
            this.tnHeader.Size = new System.Drawing.Size(287, 23);
            this.tnHeader.TabIndex = 4;
            this.tnHeader.Text = "TNs";
            // 
            // tnList
            // 
            this.tnList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tnList.FormattingEnabled = true;
            this.tnList.ItemHeight = 16;
            this.tnList.Location = new System.Drawing.Point(9, 52);
            this.tnList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tnList.Name = "tnList";
            this.tnList.Size = new System.Drawing.Size(325, 324);
            this.tnList.TabIndex = 1;
            this.tnList.SelectedIndexChanged += new System.EventHandler(this.tnList_SelectedIndexChanged);
            // 
            // modelInfoHeader2
            // 
            this.modelInfoHeader2.AutoSize = true;
            this.modelInfoHeader2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelInfoHeader2.Location = new System.Drawing.Point(571, 16);
            this.modelInfoHeader2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.modelInfoHeader2.Name = "modelInfoHeader2";
            this.modelInfoHeader2.Size = new System.Drawing.Size(0, 17);
            this.modelInfoHeader2.TabIndex = 4;
            // 
            // modelInfoHeader
            // 
            this.modelInfoHeader.AutoSize = true;
            this.modelInfoHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelInfoHeader.Location = new System.Drawing.Point(393, 16);
            this.modelInfoHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.modelInfoHeader.Name = "modelInfoHeader";
            this.modelInfoHeader.Size = new System.Drawing.Size(153, 17);
            this.modelInfoHeader.TabIndex = 3;
            this.modelInfoHeader.Text = "Model Information";
            // 
            // prefixHeader
            // 
            this.prefixHeader.AutoSize = true;
            this.prefixHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prefixHeader.Location = new System.Drawing.Point(4, 16);
            this.prefixHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.prefixHeader.Name = "prefixHeader";
            this.prefixHeader.Size = new System.Drawing.Size(152, 17);
            this.prefixHeader.TabIndex = 2;
            this.prefixHeader.Text = "Prefix Information";
            // 
            // dgPrefixInfo
            // 
            this.dgPrefixInfo.AllowUserToAddRows = false;
            this.dgPrefixInfo.AllowUserToDeleteRows = false;
            this.dgPrefixInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgPrefixInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgPrefixInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgPrefixInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgPrefixInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPrefixInfo.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgPrefixInfo.Location = new System.Drawing.Point(4, 39);
            this.dgPrefixInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgPrefixInfo.Name = "dgPrefixInfo";
            this.dgPrefixInfo.ReadOnly = true;
            this.dgPrefixInfo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgPrefixInfo.Size = new System.Drawing.Size(381, 359);
            this.dgPrefixInfo.TabIndex = 1;
            // 
            // dgModelInfo
            // 
            this.dgModelInfo.AllowUserToAddRows = false;
            this.dgModelInfo.AllowUserToDeleteRows = false;
            this.dgModelInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgModelInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgModelInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgModelInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgModelInfo.Location = new System.Drawing.Point(393, 39);
            this.dgModelInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgModelInfo.Name = "dgModelInfo";
            this.dgModelInfo.ReadOnly = true;
            this.dgModelInfo.Size = new System.Drawing.Size(1023, 359);
            this.dgModelInfo.TabIndex = 0;
            // 
            // EXIDTNInfoReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.baseSplitContainer);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "EXIDTNInfoReport";
            this.Size = new System.Drawing.Size(1417, 838);
            this.baseSplitContainer.Panel1.ResumeLayout(false);
            this.baseSplitContainer.Panel1.PerformLayout();
            this.baseSplitContainer.Panel2.ResumeLayout(false);
            this.baseSplitContainer.Panel2.PerformLayout();
            this.baseSplitContainer.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.executorGroup.ResumeLayout(false);
            this.executorGroup.PerformLayout();
            this.brandGroup.ResumeLayout(false);
            this.brandGroup.PerformLayout();
            this.idGroup.ResumeLayout(false);
            this.idGroup.PerformLayout();
            this.tnGroup.ResumeLayout(false);
            this.tnGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrefixInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgModelInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer baseSplitContainer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox executorGroup;
        private System.Windows.Forms.Label executorMsg;
        private System.Windows.Forms.Label executorHeader;
        private System.Windows.Forms.ListBox executorList;
        private System.Windows.Forms.GroupBox brandGroup;
        private System.Windows.Forms.Label brandMsg;
        private System.Windows.Forms.Label brandHeader;
        private System.Windows.Forms.ListBox brandList;
        private System.Windows.Forms.GroupBox idGroup;
        private System.Windows.Forms.Label idMsg;
        private System.Windows.Forms.Label idHeader;
        private System.Windows.Forms.ListBox idList;
        private System.Windows.Forms.GroupBox tnGroup;
        private System.Windows.Forms.Label tnFooter;
        private System.Windows.Forms.Label tnHeader;
        private System.Windows.Forms.ListBox tnList;
        private System.Windows.Forms.Label modelInfoHeader2;
        private System.Windows.Forms.Label modelInfoHeader;
        private System.Windows.Forms.Label prefixHeader;
        private System.Windows.Forms.DataGridView dgPrefixInfo;
        private System.Windows.Forms.DataGridView dgModelInfo;

    }
}
