﻿using UEI.Workflow2010.Report.Forms;
namespace UEI.Workflow2010.Report
{
    partial class SupportedIDByPlatformForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grpPlatforms = new System.Windows.Forms.GroupBox();
            this.treeViewPlatforms = new System.Windows.Forms.TreeView();
            this.chkIncludeUnsupportedIds = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.etchedLine1 = new UEI.Workflow2010.Report.Forms.EtchedLine();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.grpPlatforms.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grpPlatforms);
            this.splitContainer1.Panel1.Controls.Add(this.chkIncludeUnsupportedIds);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnOK);
            this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel2.Controls.Add(this.etchedLine1);
            this.splitContainer1.Size = new System.Drawing.Size(560, 342);
            this.splitContainer1.SplitterDistance = 306;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // grpPlatforms
            // 
            this.grpPlatforms.Controls.Add(this.treeViewPlatforms);
            this.grpPlatforms.Location = new System.Drawing.Point(16, 43);
            this.grpPlatforms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpPlatforms.Name = "grpPlatforms";
            this.grpPlatforms.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpPlatforms.Size = new System.Drawing.Size(359, 260);
            this.grpPlatforms.TabIndex = 2;
            this.grpPlatforms.TabStop = false;
            this.grpPlatforms.Text = "Select Platforms which you want to Include";
            // 
            // treeViewPlatforms
            // 
            this.treeViewPlatforms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewPlatforms.CheckBoxes = true;
            this.treeViewPlatforms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewPlatforms.Location = new System.Drawing.Point(4, 19);
            this.treeViewPlatforms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.treeViewPlatforms.Name = "treeViewPlatforms";
            this.treeViewPlatforms.Size = new System.Drawing.Size(351, 237);
            this.treeViewPlatforms.TabIndex = 0;
            // 
            // chkIncludeUnsupportedIds
            // 
            this.chkIncludeUnsupportedIds.AutoSize = true;
            this.chkIncludeUnsupportedIds.Checked = true;
            this.chkIncludeUnsupportedIds.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeUnsupportedIds.Location = new System.Drawing.Point(16, 15);
            this.chkIncludeUnsupportedIds.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIncludeUnsupportedIds.Name = "chkIncludeUnsupportedIds";
            this.chkIncludeUnsupportedIds.Size = new System.Drawing.Size(260, 21);
            this.chkIncludeUnsupportedIds.TabIndex = 1;
            this.chkIncludeUnsupportedIds.Text = "Include Unsupported IDs by Platform";
            this.chkIncludeUnsupportedIds.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(405, 1);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(76, 28);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(485, 1);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(71, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // etchedLine1
            // 
            this.etchedLine1.Dock = System.Windows.Forms.DockStyle.Top;
            this.etchedLine1.Edge = UEI.Workflow2010.Report.Forms.EtchEdge.Top;
            this.etchedLine1.Location = new System.Drawing.Point(0, 0);
            this.etchedLine1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.etchedLine1.Name = "etchedLine1";
            this.etchedLine1.Size = new System.Drawing.Size(560, 12);
            this.etchedLine1.TabIndex = 0;
            // 
            // SupportedIDByPlatformForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(560, 342);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SupportedIDByPlatformForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Supported Ids by Platform";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.grpPlatforms.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private EtchedLine etchedLine1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkIncludeUnsupportedIds;
        private System.Windows.Forms.GroupBox grpPlatforms;
        private System.Windows.Forms.TreeView treeViewPlatforms;
    }
}