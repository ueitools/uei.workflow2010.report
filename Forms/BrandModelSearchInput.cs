using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using UEI.Workflow2010.Report.Service;
using UEI.Workflow2010.Report.Model;

namespace UEI.Workflow2010.Report
{
    public partial class BrandModelSearchInput : Form
    {
        private OleDbConnection oledbCon = null;
        private string connectionString = String.Empty;
        private List<string> validTables = new List<string>();
        private SortedList<string, List<string>> brandModelList = new SortedList<string, List<string>>();
        
        

        private string fileName = String.Empty;
        public BrandModelSearchInput()
        {
            InitializeComponent();

            

            //string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =" + filePath + ";Extended Properties=\"Excel 8.0;HDR=YES;\";";
            //validate the excel
            //read the excel
            //get inputs from db
            //search...
        }

        #region Properties

        private IList<string> idList = new List<string>();
        public IList<string> IDs
        {
            get { return idList; }
            
        }

        private IList<Model.Region> regionsList = new List<Model.Region>();
        public IList<Model.Region> Regions
        {
            get { return regionsList; }
            
        }

        private IList<DataSources> dataSources = new List<DataSources>();
        public IList<DataSources> DataSources
        {
            get { return dataSources; }
        }

        public List<string> Brands
        {
            get {
                if (brandModelList.Count > 0)
                {
                    List<string> brands = new List<string>();
                    
                    foreach (string key in brandModelList.Keys)
                        brands.Add(key);
                   
                    return brands;
                }

                return new List<string>();
            }
        }

        public List<string> Models
        {
            get {
                if (brandModelList.Count > 0)
                {
                    List<string> models = new List<string>();
                    foreach (List<string> modelList in brandModelList.Values)
                        models.AddRange(modelList);
                    

                    return models;
                }
                return new List<string>();
                
            }
        }

        #endregion

        private string currentWork = String.Empty;

        private void GetInputFile(bool callFromFormLoad)
        {
            
            // get the excel
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.fileName = openFileDialog.FileName;

                    this.connectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source ={0};Extended Properties=\"Excel 8.0;HDR=YES;\";", fileName);
                    this.filePathLbl.Text = "File :" + fileName;

                    //UI Settings
                    this.validateStatusLbl.Image = null;
                    this.readStatusLbl.Image = null;
                    this.dbInputStatusLbl.Image = null;
                    this.searchBtn.Enabled = false;

                    //file goes to validation.call background worker
                    this.backgroundWorker.RunWorkerAsync("ValidateExcel");
                    this.backgroundWorker.ReportProgress(0, "ValidateExcel");

                }
                else
                {
                    if(callFromFormLoad)
                        this.Close();
                }
            }
        }

        private bool ValidateExcel()
        {
            try
            {
                oledbCon = new OleDbConnection(connectionString);
                oledbCon.Open();

                if (oledbCon.State == ConnectionState.Open)
                {
                    //check the schema.
                    //get tables
                    DataTable availableTables = oledbCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "Table" });
                    if (availableTables.Rows.Count > 0)
                    {
                        foreach (DataRow row in availableTables.Rows)
                        {
                            string tableName = row["TABLE_NAME"].ToString();
                            //check for brand and model column
                            DataTable columnsNeeded = oledbCon.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, tableName, "Brand" });
                            if (columnsNeeded.Rows.Count > 0)//brand column is available
                            {
                                columnsNeeded = oledbCon.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, tableName, "Model" });
                                if (columnsNeeded.Rows.Count > 0)//model column is available
                                {
                                    //table is valid
                                    validTables.Add(tableName);
                                }
                            }


                        }
                    }

                }

                if (validTables.Count > 0)
                    return true;
                else
                {
                    oledbCon.Close();
                    oledbCon.Dispose();

                    return false;
                }
            }
            catch (OleDbException oledbException)
            {
                oledbCon.Close();
                oledbCon.Dispose();

                if (oledbException.ErrorCode == -2147467259)
                {
                    MessageBox.Show(oledbException.Message);

                    return false;
                }
                else
                    throw;

            }
            catch
            {
                oledbCon.Close();
                oledbCon.Dispose();

                throw;
            }
        }

        private bool ReadExcel()
        {
            try
            {
                
                if (oledbCon.State == ConnectionState.Open)
                {
                    //create command object
                    OleDbCommand command = null;
                    foreach (string tableName in validTables)
                    {
                        string commandText = String.Format("select Brand,Model from [{0}]" , tableName);
                        command = new OleDbCommand(commandText, oledbCon);

                        OleDbDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            string brand = Convert.ToString(reader["Brand"]);
                            string model = Convert.ToString(reader["Model"]);

                            if (!String.IsNullOrEmpty(brand))
                            {
                                if (brandModelList.ContainsKey(brand))
                                {
                                    List<string> modelList = brandModelList[brand];
                                    if (!String.IsNullOrEmpty(model) && !modelList.Contains(model))
                                        modelList.Add(model);

                                }
                                else
                                {
                                    List<string> modelList = new List<string>();
                                    if (!String.IsNullOrEmpty(model))
                                        modelList.Add(model);

                                    brandModelList.Add(brand, modelList);
                                }
                            }
                        }

                        reader.Close();
                        command.Dispose();
                    }

                }

                if (brandModelList.Count > 0)
                    return true;
                else
                    return false;
                
            }
            catch
            {
                throw;
            }
            finally
            {
                oledbCon.Close();
                oledbCon.Dispose();
            }
        }

        private bool GetInputsFromDB()
        {
            try
            {
                Reports reportService = new Reports();
                //get all ids
                idList = reportService.GetAllIDs();
                //get all regions
                regionsList = reportService.GetRegions();
                //get all data sources
                dataSources = reportService.GetAllDataSources();

                if (idList.Count > 0 && regionsList.Count > 0 && dataSources.Count > 0)
                    return true;
                else
                    return false;
            }
            catch
            {
                throw;
            }

        }

        private void BrandModelSearchInput_Load(object sender, EventArgs e)
        {
            // get the excel
            GetInputFile(true);
        }
        
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
                   
            if (!worker.CancellationPending)
            {
                switch (e.Argument.ToString())
                {
                    case "ValidateExcel":
                        
                        this.currentWork = "ValidateExcel";
                        e.Result = ValidateExcel();
                        
                        break;
                    case "ReadExcel":

                        this.currentWork = "ReadExcel";
                        e.Result = ReadExcel();

                        break;
                    case "GetInputsFromDB":
                        this.currentWork = "GetInputsFromDB";
                        e.Result = GetInputsFromDB();
                        break;
                    default:
                        break;
                }

                
            }
        }
        //not called
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState.ToString() == "ValidateExcel")
                this.validateStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.loading;
            else if (e.UserState.ToString() == "ReadExcel")
                this.readStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.loading;
            else if (e.UserState.ToString() == "GetInputsFromDB")
                this.dbInputStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.loading;

        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {
                MessageBox.Show("Cancelled");

            }
            else
            {
                switch (this.currentWork)
                {
                    case "ValidateExcel":
                        if ((Boolean)e.Result)
                        {
                            //this.validateStatusLbl.Text = "valid";
                            this.validateStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.success;
                            //read the data after validation.
                            this.backgroundWorker.RunWorkerAsync("ReadExcel");
                            this.backgroundWorker.ReportProgress(0, "ReadExcel");
                        }
                        else
                            this.validateStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.fail;
                       

                        break;
                    case "ReadExcel":
                        if ((Boolean)e.Result)
                        {
                            this.readStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.success;
                            this.backgroundWorker.RunWorkerAsync("GetInputsFromDB");
                            this.backgroundWorker.ReportProgress(0, "GetInputsFromDB");


                        }
                        else
                            this.readStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.fail;
                       

                        break;
                    case "GetInputsFromDB":
                        if ((Boolean)e.Result)
                        {
                            this.searchBtn.Enabled = true;
                            this.dbInputStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.success;
                        }
                        else
                            this.dbInputStatusLbl.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.fail;

                        break;
                    
                }
            }
           
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            if (this.backgroundWorker.IsBusy)
                this.backgroundWorker.CancelAsync();

        }

        private void getFileBtn_Click(object sender, EventArgs e)
        {
            if (!this.backgroundWorker.IsBusy)
            {
               
                // get the excel
                GetInputFile(false);
                
            }
            else
            {
                MessageBox.Show("Application busy.Please try again.");
            }
            
        }
    }
}