using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class BrandFilterForm : Form
    {
        #region Properties
        private List<String> m_Brands = new List<String>();
        public List<String> Brands
        {
            get { return m_Brands; }
            set { m_Brands = value; }
        }
        #endregion

        public BrandFilterForm()
        {
            InitializeComponent();
        }       
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (lstBrands.Items.Count > 0)
            {
                foreach (String brand in lstBrands.Items)
                {
                    Brands.Add(brand);
                }
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Please enter Some Brands.");
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            String m_Brand = txtBrand.Text;
            if (!String.IsNullOrEmpty(m_Brand))
            {
                if (!lstBrands.Items.Contains(m_Brand))
                {
                    lstBrands.Items.Add(m_Brand);
                }
                //clear the text box
                txtBrand.Text = String.Empty;
            }
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            while (lstBrands.SelectedItems.Count > 0)
            {
                lstBrands.Items.Remove(lstBrands.SelectedItem);
            }  
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Brand Search List File (*.xls)|*.xls|Text File (*.txt)|*.txt";
            openFileDialog.DefaultExt = "txt";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {                
                FileInfo file = new FileInfo(openFileDialog.FileName);
                if (file.Extension.ToLower() == ".txt")
                {
                    StreamReader stream = new StreamReader(file.FullName,Encoding.Default);
                    String brand = stream.ReadLine();
                    //Get Brand List
                    while (brand != null)
                    {
                        if (!(brand.Trim().ToLower().Contains("brand")) && (brand.Trim() != String.Empty))
                        {
                            if (!lstBrands.Items.Contains(brand))
                                lstBrands.Items.Add(brand);
                        }
                        brand = stream.ReadLine();
                    }
                    stream.Close();                    
                }
                else if (file.Extension.ToLower() == ".xls")
                {
                    OleDbConnection excelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openFileDialog.FileName + ";Extended Properties=Excel 8.0;");
                    excelConnection.Open();
                    DataTable dtExcelSheets = excelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    excelConnection.Close();
                    if (dtExcelSheets.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtExcelSheets.Rows)
                        {
                            OleDbDataAdapter excelAdapter = new OleDbDataAdapter("SELECT * FROM [" + dr["TABLE_NAME"].ToString() + "]", excelConnection);
                            DataTable dtBrandList = new DataTable("BrandList");
                            excelAdapter.Fill(dtBrandList);
                            for (Int32 row = 0; row < dtBrandList.Rows.Count; row++)
                            {
                                String brand = dtBrandList.Rows[row][0].ToString();
                                if (!(brand.ToLower().Contains("brand")) && (brand.Trim() != String.Empty))
                                {
                                    if (!lstBrands.Items.Contains(brand))
                                        lstBrands.Items.Add(brand);
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Brand List is Empty");                        
                    }
                }
            }
        }

        private void txtBrand_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String m_Brand = txtBrand.Text;
                if (!String.IsNullOrEmpty(m_Brand))
                {
                    if (!lstBrands.Items.Contains(m_Brand))
                    {
                        lstBrands.Items.Add(m_Brand);
                    }
                    //clear the text box
                    txtBrand.Text = String.Empty;
                }
            }
        }           
    }
}