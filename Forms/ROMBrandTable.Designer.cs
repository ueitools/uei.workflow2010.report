namespace UEI.Workflow2010.Report
{
    partial class ROMBrandTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBrowseIDListFile = new System.Windows.Forms.Button();
            this.txtIDListInputFile = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBrowseBrandListFile = new System.Windows.Forms.Button();
            this.txtBrandListInputFile = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkRegions = new System.Windows.Forms.CheckedListBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnBrowseOutputFile = new System.Windows.Forms.Button();
            this.txtOutputFile = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBrowseIDListFile);
            this.groupBox1.Controls.Add(this.txtIDListInputFile);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(697, 79);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID List Input File";
            // 
            // btnBrowseIDListFile
            // 
            this.btnBrowseIDListFile.Location = new System.Drawing.Point(592, 25);
            this.btnBrowseIDListFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowseIDListFile.Name = "btnBrowseIDListFile";
            this.btnBrowseIDListFile.Size = new System.Drawing.Size(100, 28);
            this.btnBrowseIDListFile.TabIndex = 1;
            this.btnBrowseIDListFile.Text = "Browse";
            this.btnBrowseIDListFile.UseVisualStyleBackColor = true;
            // 
            // txtIDListInputFile
            // 
            this.txtIDListInputFile.BackColor = System.Drawing.Color.White;
            this.txtIDListInputFile.Location = new System.Drawing.Point(9, 25);
            this.txtIDListInputFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIDListInputFile.Name = "txtIDListInputFile";
            this.txtIDListInputFile.Size = new System.Drawing.Size(572, 22);
            this.txtIDListInputFile.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBrowseBrandListFile);
            this.groupBox2.Controls.Add(this.txtBrandListInputFile);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 79);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(697, 79);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Brand List Input File";
            // 
            // btnBrowseBrandListFile
            // 
            this.btnBrowseBrandListFile.Location = new System.Drawing.Point(592, 25);
            this.btnBrowseBrandListFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowseBrandListFile.Name = "btnBrowseBrandListFile";
            this.btnBrowseBrandListFile.Size = new System.Drawing.Size(100, 28);
            this.btnBrowseBrandListFile.TabIndex = 1;
            this.btnBrowseBrandListFile.Text = "Browse";
            this.btnBrowseBrandListFile.UseVisualStyleBackColor = true;
            // 
            // txtBrandListInputFile
            // 
            this.txtBrandListInputFile.BackColor = System.Drawing.Color.White;
            this.txtBrandListInputFile.Location = new System.Drawing.Point(9, 25);
            this.txtBrandListInputFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBrandListInputFile.Name = "txtBrandListInputFile";
            this.txtBrandListInputFile.Size = new System.Drawing.Size(572, 22);
            this.txtBrandListInputFile.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkRegions);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 158);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(697, 119);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Region Selection";
            // 
            // chkRegions
            // 
            this.chkRegions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chkRegions.CheckOnClick = true;
            this.chkRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkRegions.FormattingEnabled = true;
            this.chkRegions.Location = new System.Drawing.Point(4, 19);
            this.chkRegions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRegions.MultiColumn = true;
            this.chkRegions.Name = "chkRegions";
            this.chkRegions.Size = new System.Drawing.Size(689, 96);
            this.chkRegions.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(483, 363);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(592, 363);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnBrowseOutputFile);
            this.groupBox4.Controls.Add(this.txtOutputFile);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 277);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(697, 79);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Output File";
            // 
            // btnBrowseOutputFile
            // 
            this.btnBrowseOutputFile.Location = new System.Drawing.Point(592, 25);
            this.btnBrowseOutputFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowseOutputFile.Name = "btnBrowseOutputFile";
            this.btnBrowseOutputFile.Size = new System.Drawing.Size(100, 28);
            this.btnBrowseOutputFile.TabIndex = 1;
            this.btnBrowseOutputFile.Text = "Browse";
            this.btnBrowseOutputFile.UseVisualStyleBackColor = true;
            // 
            // txtOutputFile
            // 
            this.txtOutputFile.BackColor = System.Drawing.Color.White;
            this.txtOutputFile.Location = new System.Drawing.Point(9, 25);
            this.txtOutputFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOutputFile.Name = "txtOutputFile";
            this.txtOutputFile.Size = new System.Drawing.Size(572, 22);
            this.txtOutputFile.TabIndex = 0;
            // 
            // ROMBrandTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(697, 400);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ROMBrandTable";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Generate Brand Table for ROM";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBrowseIDListFile;
        private System.Windows.Forms.TextBox txtIDListInputFile;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBrowseBrandListFile;
        private System.Windows.Forms.TextBox txtBrandListInputFile;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnBrowseOutputFile;
        private System.Windows.Forms.TextBox txtOutputFile;
        private System.Windows.Forms.CheckedListBox chkRegions;
    }
}