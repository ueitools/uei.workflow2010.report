namespace UEI.Workflow2010.Report
{
    partial class DeviceCategoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgDeviceCategory = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.chkIncludeModeLetter = new System.Windows.Forms.CheckBox();
            this.rdowithSubdevices = new System.Windows.Forms.RadioButton();
            this.rdowithoutSubdevices = new System.Windows.Forms.RadioButton();
            this.dgSubDevices = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgDeviceCategory)).BeginInit();
            this.panel1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSubDevices)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(140, 8);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(50, 22);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(196, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(50, 22);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dgDeviceCategory
            // 
            this.dgDeviceCategory.AllowUserToAddRows = false;
            this.dgDeviceCategory.AllowUserToDeleteRows = false;
            this.dgDeviceCategory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgDeviceCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDeviceCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDeviceCategory.Location = new System.Drawing.Point(0, 0);
            this.dgDeviceCategory.Name = "dgDeviceCategory";
            this.dgDeviceCategory.Size = new System.Drawing.Size(440, 106);
            this.dgDeviceCategory.TabIndex = 4;
            this.dgDeviceCategory.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgDeviceCategory_CellBeginEdit);
            this.dgDeviceCategory.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgDeviceCategory_CellValidating);
            this.dgDeviceCategory.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDeviceCategory_CellValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(440, 318);
            this.panel1.TabIndex = 5;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgDeviceCategory);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(440, 318);
            this.splitContainer1.SplitterDistance = 106;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.btnOk);
            this.splitContainer2.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer2.Size = new System.Drawing.Size(440, 208);
            this.splitContainer2.SplitterDistance = 162;
            this.splitContainer2.TabIndex = 4;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Enabled = false;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.chkIncludeModeLetter);
            this.splitContainer3.Panel1.Controls.Add(this.rdowithSubdevices);
            this.splitContainer3.Panel1.Controls.Add(this.rdowithoutSubdevices);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.dgSubDevices);
            this.splitContainer3.Size = new System.Drawing.Size(440, 162);
            this.splitContainer3.SplitterDistance = 38;
            this.splitContainer3.TabIndex = 0;
            // 
            // chkIncludeModeLetter
            // 
            this.chkIncludeModeLetter.AutoSize = true;
            this.chkIncludeModeLetter.Location = new System.Drawing.Point(275, 10);
            this.chkIncludeModeLetter.Name = "chkIncludeModeLetter";
            this.chkIncludeModeLetter.Size = new System.Drawing.Size(140, 17);
            this.chkIncludeModeLetter.TabIndex = 2;
            this.chkIncludeModeLetter.Text = "Include Mode Charecter";
            this.chkIncludeModeLetter.UseVisualStyleBackColor = true;
            // 
            // rdowithSubdevices
            // 
            this.rdowithSubdevices.AutoSize = true;
            this.rdowithSubdevices.Location = new System.Drawing.Point(144, 10);
            this.rdowithSubdevices.Name = "rdowithSubdevices";
            this.rdowithSubdevices.Size = new System.Drawing.Size(111, 17);
            this.rdowithSubdevices.TabIndex = 1;
            this.rdowithSubdevices.Text = "With Device Type";
            this.rdowithSubdevices.UseVisualStyleBackColor = true;
            this.rdowithSubdevices.CheckedChanged += new System.EventHandler(this.rdowithSubdevices_CheckedChanged);
            // 
            // rdowithoutSubdevices
            // 
            this.rdowithoutSubdevices.AutoSize = true;
            this.rdowithoutSubdevices.Checked = true;
            this.rdowithoutSubdevices.Location = new System.Drawing.Point(12, 10);
            this.rdowithoutSubdevices.Name = "rdowithoutSubdevices";
            this.rdowithoutSubdevices.Size = new System.Drawing.Size(126, 17);
            this.rdowithoutSubdevices.TabIndex = 0;
            this.rdowithoutSubdevices.TabStop = true;
            this.rdowithoutSubdevices.Text = "Without Device Type";
            this.rdowithoutSubdevices.UseVisualStyleBackColor = true;
            // 
            // dgSubDevices
            // 
            this.dgSubDevices.AllowUserToAddRows = false;
            this.dgSubDevices.AllowUserToDeleteRows = false;
            this.dgSubDevices.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgSubDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSubDevices.ColumnHeadersVisible = false;
            this.dgSubDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSubDevices.Location = new System.Drawing.Point(0, 0);
            this.dgSubDevices.Name = "dgSubDevices";
            this.dgSubDevices.ReadOnly = true;
            this.dgSubDevices.Size = new System.Drawing.Size(440, 120);
            this.dgSubDevices.TabIndex = 0;
            this.dgSubDevices.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSubDevices_CellEnter);
            // 
            // DeviceCategoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(440, 318);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "DeviceCategoryForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Device Category Form";
            ((System.ComponentModel.ISupportInitialize)(this.dgDeviceCategory)).EndInit();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSubDevices)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dgDeviceCategory;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.RadioButton rdowithSubdevices;
        private System.Windows.Forms.RadioButton rdowithoutSubdevices;
        private System.Windows.Forms.DataGridView dgSubDevices;
        private System.Windows.Forms.CheckBox chkIncludeModeLetter;
    }
}