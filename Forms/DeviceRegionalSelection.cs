using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class DeviceRegionalSelection : Form
    {
        #region Variables
        #endregion

        #region Properties
        private String m_DeviceAliasRegion = String.Empty;
        public String DeviceAliasRegion
        {
            get { return m_DeviceAliasRegion; }
            set { m_DeviceAliasRegion = value; }
        }
        #endregion

        public DeviceRegionalSelection()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DeviceAliasRegion = String.Empty;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (rdoNA.Checked == true)
                DeviceAliasRegion = "North America";
            else if (rdoEU.Checked == true)
                DeviceAliasRegion = "Europe";
            else if (rdoAsia.Checked == true)
                DeviceAliasRegion = "Asia";
            if (DeviceAliasRegion != String.Empty)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Please select a Device Alias Region","Info",MessageBoxButtons.OK);
        }
    }
}