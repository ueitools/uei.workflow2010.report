using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class CalendarForm : Form
    {

        public CalendarForm()
        {
            InitializeComponent();
        }

        private DateTime selectedDate = new DateTime();
        public DateTime SelectedDate
        {
            get
            {
                return this.selectedDate;
            }
        }
       
        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            this.selectedDate = DateTime.Parse(dateTimePicker1.Text);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}