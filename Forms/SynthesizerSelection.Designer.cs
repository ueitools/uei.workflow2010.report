namespace UEI.Workflow2010.Report
{
    partial class SynthesizerSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoUEI2TempDB = new System.Windows.Forms.RadioButton();
            this.rdoUEI2DB = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkIncludeIntrons = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoUEI2TempDB);
            this.groupBox1.Controls.Add(this.rdoUEI2DB);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(423, 65);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Databases";
            // 
            // rdoUEI2TempDB
            // 
            this.rdoUEI2TempDB.AutoSize = true;
            this.rdoUEI2TempDB.Location = new System.Drawing.Point(163, 25);
            this.rdoUEI2TempDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoUEI2TempDB.Name = "rdoUEI2TempDB";
            this.rdoUEI2TempDB.Size = new System.Drawing.Size(122, 21);
            this.rdoUEI2TempDB.TabIndex = 1;
            this.rdoUEI2TempDB.Text = "UEI2 Temp DB";
            this.rdoUEI2TempDB.UseVisualStyleBackColor = true;
            // 
            // rdoUEI2DB
            // 
            this.rdoUEI2DB.AutoSize = true;
            this.rdoUEI2DB.Checked = true;
            this.rdoUEI2DB.Location = new System.Drawing.Point(29, 25);
            this.rdoUEI2DB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoUEI2DB.Name = "rdoUEI2DB";
            this.rdoUEI2DB.Size = new System.Drawing.Size(82, 21);
            this.rdoUEI2DB.TabIndex = 0;
            this.rdoUEI2DB.TabStop = true;
            this.rdoUEI2DB.Text = "UEI2 DB";
            this.rdoUEI2DB.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkIncludeIntrons);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 65);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(423, 74);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // chkIncludeIntrons
            // 
            this.chkIncludeIntrons.AutoSize = true;
            this.chkIncludeIntrons.Checked = true;
            this.chkIncludeIntrons.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeIntrons.Location = new System.Drawing.Point(29, 37);
            this.chkIncludeIntrons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIncludeIntrons.Name = "chkIncludeIntrons";
            this.chkIncludeIntrons.Size = new System.Drawing.Size(122, 21);
            this.chkIncludeIntrons.TabIndex = 0;
            this.chkIncludeIntrons.Text = "Include Introns";
            this.chkIncludeIntrons.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnCancel);
            this.groupBox3.Controls.Add(this.btnOK);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 139);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(423, 47);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(339, 12);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(68, 27);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(259, 12);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(72, 27);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // SynthesizerSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(423, 186);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SynthesizerSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Synthesizer Selection";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdoUEI2TempDB;
        private System.Windows.Forms.RadioButton rdoUEI2DB;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox chkIncludeIntrons;
    }
}