namespace UEI.Workflow2010.Report.Forms
{
    partial class ExIDMapReportOptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkIncludeLoadPercentage = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.chkIncludeBrand = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkIncludeLoadPercentage);
            this.groupBox1.Controls.Add(this.btnOK);
            this.groupBox1.Controls.Add(this.chkIncludeBrand);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(351, 158);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // chkIncludeLoadPercentage
            // 
            this.chkIncludeLoadPercentage.AutoSize = true;
            this.chkIncludeLoadPercentage.Location = new System.Drawing.Point(16, 52);
            this.chkIncludeLoadPercentage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIncludeLoadPercentage.Name = "chkIncludeLoadPercentage";
            this.chkIncludeLoadPercentage.Size = new System.Drawing.Size(219, 21);
            this.chkIncludeLoadPercentage.TabIndex = 4;
            this.chkIncludeLoadPercentage.Text = "Load Percentage for Executor";
            this.chkIncludeLoadPercentage.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(243, 124);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // chkIncludeBrand
            // 
            this.chkIncludeBrand.AutoSize = true;
            this.chkIncludeBrand.Location = new System.Drawing.Point(16, 23);
            this.chkIncludeBrand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIncludeBrand.Name = "chkIncludeBrand";
            this.chkIncludeBrand.Size = new System.Drawing.Size(117, 21);
            this.chkIncludeBrand.TabIndex = 1;
            this.chkIncludeBrand.Text = "Include Brand";
            this.chkIncludeBrand.UseVisualStyleBackColor = true;
            // 
            // ExIDMapReportOptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(351, 158);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ExIDMapReportOptionForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ex ID Map Report Options";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkIncludeBrand;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox chkIncludeLoadPercentage;
    }
}