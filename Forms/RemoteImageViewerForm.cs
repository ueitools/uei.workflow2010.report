﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class RemoteImageViewerForm : Form
    {
        #region Properties
        public List<Model.LabelIntronSearchRecord>  RemoteDetails { get; set; }
        public String                               RemoteImagePath { get; set; }
        #endregion

        public RemoteImageViewerForm()
        {
            InitializeComponent();
            this.Load += RemoteImageViewerForm_Load;
            this.remoteImage.MouseClick += remoteImage_MouseClick;
        }

        void remoteImage_MouseClick(object sender, MouseEventArgs e)
        {
            if (!String.IsNullOrEmpty(this.RemoteImagePath))
            {
                Process myProcess = new Process();
                string IMGview_path = ConfigurationSettings.AppSettings["IMGVIEWPATH"];
                try
                {
                    myProcess.StartInfo.FileName = this.RemoteImagePath;
                    myProcess.StartInfo.Verb = "";
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.Start();
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
            }
        }
        void RemoteImageViewerForm_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.RemoteImagePath))
            {
                this.remoteImage.Load(RemoteImagePath);                
            }
            if (this.RemoteDetails != null)
            {
                this.dataGridView.DataSource = RemoteDetails;
                if (this.RemoteDetails.Count > 0)
                {
                    this.dataGridView.Columns["ID"].Visible = false;
                    this.dataGridView.Columns["Synth"].Visible = false;
                    this.dataGridView.Columns["Intron"].Visible = false;
                    this.dataGridView.Columns["Priority"].Visible = false;
                }
            }
        }
    }
}
