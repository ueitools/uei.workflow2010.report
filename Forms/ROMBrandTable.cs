using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Service;
using UEI.Workflow2010.Report.Model;
using CommonForms;
using System.IO;

namespace UEI.Workflow2010.Report
{
    public partial class ROMBrandTable : Form
    {
        #region Variables       
        public IList<String> IDList             = new List<String>();
        public IList<String> FromIDList         = new List<String>();
        public IList<ModeBrandItem> BrandList   = new List<ModeBrandItem>();
        public String OutputFilePath            = String.Empty;
        public String MarketList               = String.Empty;
        #endregion

        #region Constructor
        public ROMBrandTable()
        {
            InitializeComponent();
            this.Load += new EventHandler(ROMBrandTable_Load);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
            this.btnOk.Click += new EventHandler(btnOk_Click);
            this.btnBrowseIDListFile.Click += new EventHandler(btnBrowseIDListFile_Click);
            this.btnBrowseBrandListFile.Click += new EventHandler(btnBrowseBrandListFile_Click);
            this.btnBrowseOutputFile.Click += new EventHandler(btnBrowseOutputFile_Click);
        }
        void btnBrowseOutputFile_Click(object sender, EventArgs e)
        {
            txtOutputFile.Text = LocateFile(txtOutputFile.Text, false);
        }                
        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        void btnOk_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txtIDListInputFile.Text))
            {
                if(!File.Exists(txtIDListInputFile.Text))
                {
                    MessageBox.Show("ID List Input File not provided", "Error");
                    return;
                }               
            }
            if(String.IsNullOrEmpty(txtBrandListInputFile.Text))
            {
                if(!File.Exists(txtBrandListInputFile.Text))
                {
                    MessageBox.Show("Brand List Input File not provided", "Error");
                    return;
                }               
            }
            if(String.IsNullOrEmpty(txtOutputFile.Text))
            {
                if(!File.Exists(txtOutputFile.Text))
                {
                    MessageBox.Show("Output File not provided", "Error");
                    return;
                }
            }
            if(chkRegions.CheckedIndices.Count == 0)
            {
                MessageBox.Show("Regions are not selected", "Error");
                return;
            }
            ReadIDListFile();
            ReadBrandList();
            MarketList = String.Empty;
            Model.Region m_Item = (Model.Region)chkRegions.CheckedItems[0];
            MarketList = "All " + m_Item.Name;
            for(int i = 0; i < chkRegions.CheckedItems.Count; i++)
            {
                m_Item = (Model.Region)chkRegions.CheckedItems[i];
                if(!MarketList.Contains(m_Item.Name))
                    MarketList += "|All " + m_Item.Name;
            }
            OutputFilePath = txtOutputFile.Text;
            DialogResult = DialogResult.OK;
            Close();
        }
        void ROMBrandTable_Load(object sender, EventArgs e)
        {
            LoadRegions();
        }
        #endregion

        #region Browse File
        private String LocateFile(string StartPoint, bool MustExist)
        {
            FileDialog dlg;
            if(MustExist)
            {
                dlg = new OpenFileDialog();
                dlg.Filter = "Lst files (*.lst)|*.lst|Txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dlg.DefaultExt = ".txt";
            }
            else
            {
                dlg = new SaveFileDialog();
                dlg.Filter = "Src files (*.src)|*.src|Txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dlg.DefaultExt = ".src";
            }
            if(String.IsNullOrEmpty(StartPoint))
                dlg.InitialDirectory = Configuration.GetWorkingDirectory();
            else
                dlg.InitialDirectory = StartPoint;

            dlg.CheckFileExists = MustExist;

            if(dlg.ShowDialog() == DialogResult.OK)
                return dlg.FileName;

            return StartPoint;
        }
        #endregion

        #region Load Regions
        private void LoadRegions()
        {
            Reports _Report = new Reports();
            LocationModel m_LocationCollection = _Report.GetLocations();
            List<Model.Region> m_RegionList = m_LocationCollection.GetRegions();
            if(m_RegionList.Count > 0)
            {                
                chkRegions.DataSource = m_RegionList;
                chkRegions.DisplayMember = "Name";
                chkRegions.ValueMember = "RegionID";
                for(Int32 i = 0; i < chkRegions.Items.Count; i++)
                {
                    chkRegions.SetItemCheckState(i, CheckState.Checked);
                }
            }
        }
        #endregion

        #region Read Id List File
        void btnBrowseIDListFile_Click(object sender, EventArgs e)
        {
            txtIDListInputFile.Text = LocateFile(txtIDListInputFile.Text, true);           
        }
        private void ReadIDListFile()
        {
            Char[] delim = { ' ', '-', ',', '\r', '\n', '\t' };
            String[] FileList = File.ReadAllLines(txtIDListInputFile.Text);
            String ID = String.Empty;

            foreach(String line in FileList)
            {
                if(String.IsNullOrEmpty(line))
                    continue;
                String[] item = line.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                ID = ValidateIDName(item[0]);
                if(string.IsNullOrEmpty(ID))
                    continue;
                else
                    FromIDList.Add(ID);

                if(item.Length > 1)
                    ID = ValidateIDName(item[1]);
                if(!String.IsNullOrEmpty(ID))
                    IDList.Add(ID);
            }
        }
        private string ValidateIDName(string ID)
        {
            ID = ID.Trim().ToUpper();
            if(!string.IsNullOrEmpty(ID) &&
                char.IsLetter(ID[0]) &&
                char.IsDigit(ID[1]))
            {
                int idnum = int.Parse(ID.Substring(1));
                return string.Format("{0}{1:D4}", ID[0], idnum);
            }
            return null;
        }
        #endregion

        #region Read Brand List File
        void btnBrowseBrandListFile_Click(object sender, EventArgs e)
        {
            txtBrandListInputFile.Text = LocateFile(txtBrandListInputFile.Text, true);           
        }
        private void ReadBrandList()
        {
            Char[] delim = { '-', ',', '\r', '\n', '\t', ' ' };
            String[] BrandFile = File.ReadAllLines(txtBrandListInputFile.Text);

            ModeBrandItem MI = new ModeBrandItem();
            MI.BrandInfo = new List<BrandItem>();
            BrandItem BI = new BrandItem();

            foreach(String line in BrandFile)
            {
                if(String.IsNullOrEmpty(line))
                    continue;
                if(line.Trim()[0] == ':')
                {
                    if(!String.IsNullOrEmpty(MI.Mode))
                    {
                        MI.BrandInfo.Add(BI);
                        BI = new BrandItem();
                        BrandList.Add(MI);
                        MI = new ModeBrandItem();
                        MI.BrandInfo = new List<BrandItem>();
                    }
                    MI.Mode = line.Trim().Substring(1, 1);
                    continue;
                }
                if(line.Trim()[0] == '-')
                {
                    String[] LineItems = line.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                    foreach(String item in LineItems)
                    {
                        string ID = ValidateIDName(item);
                        if(!string.IsNullOrEmpty(ID))
                        {
                            if(BI.IDPriority == null)
                                BI.IDPriority = new List<String>();
                            BI.IDPriority.Add(ID);
                        }
                    }
                }
                else
                {
                    if(!string.IsNullOrEmpty(BI.Brand))
                    {
                        MI.BrandInfo.Add(BI);
                        BI = new BrandItem();
                    }
                    BI.Brand = line.Trim();
                }
            }
            BrandList.Add(MI);
        }
        #endregion
    }
}