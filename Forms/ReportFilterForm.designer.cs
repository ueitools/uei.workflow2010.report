namespace UEI.Workflow2010.Report
{
    partial class ReportFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
              components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportFilterForm));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.tabReportFilter = new UEI.Workflow2010.Report.TabControl();
            this.tabMarket = new UEI.Workflow2010.Report.TabPage();
            this.chkUnknownLocation = new System.Windows.Forms.CheckBox();
            this.chkIncludeNonCountryRecords = new System.Windows.Forms.CheckBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.chkSubRegions = new System.Windows.Forms.TreeView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkCountries = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkRegions = new System.Windows.Forms.TreeView();
            this.tabBrand = new UEI.Workflow2010.Report.TabPage();
            this.brandFilterPanel = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnFilterBrandFile = new System.Windows.Forms.Button();
            this.btnRemoveFilterBrand = new System.Windows.Forms.Button();
            this.btnAddFilterBrand = new System.Windows.Forms.Button();
            this.lstFilterBrands = new System.Windows.Forms.ListBox();
            this.txtFilterBrand = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnMatchBrandFile = new System.Windows.Forms.Button();
            this.lstMatchBrands = new System.Windows.Forms.ListBox();
            this.btnRemoveMatchBrand = new System.Windows.Forms.Button();
            this.btnAddMatchBrand = new System.Windows.Forms.Button();
            this.txtMatchBrand = new System.Windows.Forms.TextBox();
            this.chkNoUEIBrandNumbers = new System.Windows.Forms.CheckBox();
            this.tabModel = new UEI.Workflow2010.Report.TabPage();
            this.modelFilterPanel = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnFilterModelFile = new System.Windows.Forms.Button();
            this.btnRemoveFilterModel = new System.Windows.Forms.Button();
            this.btnAddFilterModel = new System.Windows.Forms.Button();
            this.lstFilterModels = new System.Windows.Forms.ListBox();
            this.txtFilterModel = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnMatchModelFile = new System.Windows.Forms.Button();
            this.lstMatchModels = new System.Windows.Forms.ListBox();
            this.btnRemoveMatchModel = new System.Windows.Forms.Button();
            this.btnAddMatchModel = new System.Windows.Forms.Button();
            this.txtMatchModel = new System.Windows.Forms.TextBox();
            this.chkRemoveNonCodeBookModels = new System.Windows.Forms.CheckBox();
            this.chkRecordWithoutModelName = new System.Windows.Forms.CheckBox();
            this.tabDataSourceLocation = new UEI.Workflow2010.Report.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.treeDataSources = new System.Windows.Forms.TreeView();
            this.tabModelInfoSpecific = new UEI.Workflow2010.Report.TabPage();
            this.dateBasedSearchGroup = new System.Windows.Forms.GroupBox();
            this.grpMIProject = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnMIIdenticalBrowse = new System.Windows.Forms.Button();
            this.chkMIIncludeIdenticalId = new System.Windows.Forms.CheckBox();
            this.txtMIIdenticalFile = new System.Windows.Forms.TextBox();
            this.txtMIProjectName = new System.Windows.Forms.TextBox();
            this.grpMIOption = new System.Windows.Forms.GroupBox();
            this.dBSIDRd = new System.Windows.Forms.RadioButton();
            this.dbsModelsRd = new System.Windows.Forms.RadioButton();
            this.dBSTNRd = new System.Windows.Forms.RadioButton();
            this.dBSBrandsRd = new System.Windows.Forms.RadioButton();
            this.grpMIDateSelection = new System.Windows.Forms.GroupBox();
            this.dBSFromDateLbl = new System.Windows.Forms.Label();
            this.dBSFromDateTxt = new System.Windows.Forms.TextBox();
            this.dBSToDateBtn = new System.Windows.Forms.Button();
            this.dBSFromDateBtn = new System.Windows.Forms.Button();
            this.dBSToDateTxt = new System.Windows.Forms.TextBox();
            this.dBSToDateLbl = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.chkKeepDuplicateModels = new System.Windows.Forms.CheckBox();
            this.chkBxIncludeRestrictedIDs_ModelInfo = new System.Windows.Forms.CheckBox();
            this.chkTNLinks = new System.Windows.Forms.CheckBox();
            this.enableDateFilterChk = new System.Windows.Forms.CheckBox();
            this.tabDevice = new UEI.Workflow2010.Report.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.chkIncludeNonComponentLinkedRecords = new System.Windows.Forms.CheckBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.rdoCompStatusBoth = new System.Windows.Forms.RadioButton();
            this.rdoCompStatusNonBuiltIn = new System.Windows.Forms.RadioButton();
            this.rdoCompStatusBuiltIn = new System.Windows.Forms.RadioButton();
            this.chkIncludeEmptyDeviceType = new System.Windows.Forms.CheckBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.chkSubDevices = new System.Windows.Forms.TreeView();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.chkComponents = new System.Windows.Forms.TreeView();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.chkMainDevices = new System.Windows.Forms.TreeView();
            this.chkRemoveIDs = new System.Windows.Forms.CheckBox();
            this.tabIDSetupCodeInfo = new UEI.Workflow2010.Report.TabPage();
            this.chkFormatStartDigitSwitch = new System.Windows.Forms.CheckBox();
            this.chkIncludeAliasBrand = new System.Windows.Forms.CheckBox();
            this.chkBxIncludeBlankModelNames = new System.Windows.Forms.CheckBox();
            this.grpProject = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnBrowseIdenticalIdListFile = new System.Windows.Forms.Button();
            this.chkIncludeIdenticalId = new System.Windows.Forms.CheckBox();
            this.txtIdenticalIdListFile = new System.Windows.Forms.TextBox();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.chkDisplayMajorBrands = new System.Windows.Forms.CheckBox();
            this.chkDisplayBrandNumber = new System.Windows.Forms.CheckBox();
            this.grpAliasIDFile = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtAliasIDFile = new System.Windows.Forms.TextBox();
            this.txtIDOffset = new System.Windows.Forms.TextBox();
            this.lblIDOffset = new System.Windows.Forms.Label();
            this.lblSetupCodeFormat = new System.Windows.Forms.Label();
            this.gbSort = new System.Windows.Forms.GroupBox();
            this.cmbBxModelTypeISCI = new System.Windows.Forms.ComboBox();
            this.txtPriorityIDListFile = new System.Windows.Forms.TextBox();
            this.btnEndDate = new System.Windows.Forms.Button();
            this.btnStartDate = new System.Windows.Forms.Button();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.txtStartDate = new System.Windows.Forms.TextBox();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.rdoSortBySearchStatistics = new System.Windows.Forms.RadioButton();
            this.rdoSortByPOSData = new System.Windows.Forms.RadioButton();
            this.rdoSortByPriorityIDListFile = new System.Windows.Forms.RadioButton();
            this.rdoSortByIDNumber = new System.Windows.Forms.RadioButton();
            this.rdoSortByName = new System.Windows.Forms.RadioButton();
            this.rdoSortByModelCount = new System.Windows.Forms.RadioButton();
            this.cmbSetupCodeFormating = new System.Windows.Forms.ComboBox();
            this.chkIncludeRestrictedIDs = new System.Windows.Forms.CheckBox();
            this.tabBrandBasedSearch = new UEI.Workflow2010.Report.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.chkIncludeAliasBrandNames = new System.Windows.Forms.CheckBox();
            this.chkBxBlankModelNameBBS = new System.Windows.Forms.CheckBox();
            this.cmbBxModelTypeBBS = new System.Windows.Forms.ComboBox();
            this.chkMode = new System.Windows.Forms.CheckBox();
            this.chkModel = new System.Windows.Forms.CheckBox();
            this.chkDataSource = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowseBrandListFile = new System.Windows.Forms.Button();
            this.txtBrandSeachListFile = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.chkUEI2Avail = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.chkPOSAvail = new System.Windows.Forms.CheckBox();
            this.btnPOSEndDate = new System.Windows.Forms.Button();
            this.btnPOSStartDate = new System.Windows.Forms.Button();
            this.txtPOSEndDate = new System.Windows.Forms.TextBox();
            this.txtPOSStartDate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.chkOSSAvail = new System.Windows.Forms.CheckBox();
            this.btnOSSEndDate = new System.Windows.Forms.Button();
            this.btnOSSStartDate = new System.Windows.Forms.Button();
            this.txtOSSEndDate = new System.Windows.Forms.TextBox();
            this.txtOSSStartDate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabLabelIntronSearch = new UEI.Workflow2010.Report.TabPage();
            this.grpBxEnterPatterns = new System.Windows.Forms.GroupBox();
            this.btnShowDictionary = new System.Windows.Forms.Button();
            this.chkBxGetCompleteMatch = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bttnLoadLabelIntronFile = new System.Windows.Forms.Button();
            this.bttnRemoveLabelIntron = new System.Windows.Forms.Button();
            this.bttnAddLabelIntron = new System.Windows.Forms.Button();
            this.txtBxEnterLabelIntron = new System.Windows.Forms.TextBox();
            this.lstBxSearchPatterns = new System.Windows.Forms.ListBox();
            this.tabQuickSetInfo = new UEI.Workflow2010.Report.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.dataGridModeList = new System.Windows.Forms.DataGridView();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.dataGridQuickSet = new System.Windows.Forms.DataGridView();
            this.DeviceCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModeList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtModeList = new System.Windows.Forms.TextBox();
            this.txtModeName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.txtQSPriorityIdListFile = new System.Windows.Forms.TextBox();
            this.rdoQSSortByPriorityIDList = new System.Windows.Forms.RadioButton();
            this.rdoQSSortByIDNo = new System.Windows.Forms.RadioButton();
            this.rdoQSSortByModelCount = new System.Windows.Forms.RadioButton();
            this.tabKeyFunction = new UEI.Workflow2010.Report.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.tabUnsupportedId = new UEI.Workflow2010.Report.TabPage();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.grpPlatforms = new System.Windows.Forms.GroupBox();
            this.treeViewPlatforms = new System.Windows.Forms.TreeView();
            this.chkIncludeUnsupportedIds = new System.Windows.Forms.CheckBox();
            this.xlTabDrawer = new UEI.Workflow2010.Report.XlTabDrawer();
            this.tabReportFilter.SuspendLayout();
            this.tabMarket.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabBrand.SuspendLayout();
            this.brandFilterPanel.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabModel.SuspendLayout();
            this.modelFilterPanel.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabDataSourceLocation.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabModelInfoSpecific.SuspendLayout();
            this.dateBasedSearchGroup.SuspendLayout();
            this.grpMIProject.SuspendLayout();
            this.grpMIOption.SuspendLayout();
            this.grpMIDateSelection.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabDevice.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.tabIDSetupCodeInfo.SuspendLayout();
            this.grpProject.SuspendLayout();
            this.grpAliasIDFile.SuspendLayout();
            this.gbSort.SuspendLayout();
            this.tabBrandBasedSearch.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabLabelIntronSearch.SuspendLayout();
            this.grpBxEnterPatterns.SuspendLayout();
            this.tabQuickSetInfo.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridModeList)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridQuickSet)).BeginInit();
            this.groupBox20.SuspendLayout();
            this.tabKeyFunction.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.tabUnsupportedId.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.grpPlatforms.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(425, 341);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(50, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(476, 341);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(50, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Enabled = false;
            this.btnApply.Location = new System.Drawing.Point(527, 341);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(50, 23);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            // 
            // tabReportFilter
            // 
            this.tabReportFilter.ActiveColor = System.Drawing.SystemColors.Control;
            this.tabReportFilter.BackColor = System.Drawing.SystemColors.Control;
            this.tabReportFilter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.tabReportFilter.Controls.Add(this.tabMarket);
            this.tabReportFilter.Controls.Add(this.tabBrand);
            this.tabReportFilter.Controls.Add(this.tabModel);
            this.tabReportFilter.Controls.Add(this.tabDataSourceLocation);
            this.tabReportFilter.Controls.Add(this.tabModelInfoSpecific);
            this.tabReportFilter.Controls.Add(this.tabDevice);
            this.tabReportFilter.Controls.Add(this.tabIDSetupCodeInfo);
            this.tabReportFilter.Controls.Add(this.tabBrandBasedSearch);
            this.tabReportFilter.Controls.Add(this.tabLabelIntronSearch);
            this.tabReportFilter.Controls.Add(this.tabQuickSetInfo);
            this.tabReportFilter.Controls.Add(this.tabKeyFunction);
            this.tabReportFilter.Controls.Add(this.tabUnsupportedId);
            this.tabReportFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabReportFilter.ImageIndex = -1;
            this.tabReportFilter.ImageList = null;
            this.tabReportFilter.InactiveColor = System.Drawing.SystemColors.Window;
            this.tabReportFilter.Location = new System.Drawing.Point(0, 0);
            this.tabReportFilter.Name = "tabReportFilter";
            this.tabReportFilter.ScrollButtonStyle = UEI.Workflow2010.Report.ScrollButtonStyle.Never;
            this.tabReportFilter.SelectedIndex = 4;
            this.tabReportFilter.SelectedTab = this.tabModelInfoSpecific;
            this.tabReportFilter.Size = new System.Drawing.Size(581, 341);
            this.tabReportFilter.TabDock = System.Windows.Forms.DockStyle.Top;
            this.tabReportFilter.TabDrawer = this.xlTabDrawer;
            this.tabReportFilter.TabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabReportFilter.TabIndex = 0;
            this.tabReportFilter.Text = "Report Filter";
            // 
            // tabMarket
            // 
            this.tabMarket.Controls.Add(this.chkUnknownLocation);
            this.tabMarket.Controls.Add(this.chkIncludeNonCountryRecords);
            this.tabMarket.Controls.Add(this.groupBox14);
            this.tabMarket.Controls.Add(this.groupBox2);
            this.tabMarket.Controls.Add(this.groupBox1);
            this.tabMarket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMarket.ImageIndex = -1;
            this.tabMarket.Location = new System.Drawing.Point(4, 30);
            this.tabMarket.Name = "tabMarket";
            this.tabMarket.Size = new System.Drawing.Size(657, 307);
            this.tabMarket.TabIndex = 0;
            this.tabMarket.Text = "Market";
            this.tabMarket.Visible = false;
            // 
            // chkUnknownLocation
            // 
            this.chkUnknownLocation.AutoSize = true;
            this.chkUnknownLocation.Checked = true;
            this.chkUnknownLocation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUnknownLocation.Location = new System.Drawing.Point(236, 8);
            this.chkUnknownLocation.Name = "chkUnknownLocation";
            this.chkUnknownLocation.Size = new System.Drawing.Size(159, 17);
            this.chkUnknownLocation.TabIndex = 4;
            this.chkUnknownLocation.Text = "Include Unknown Locations";
            this.chkUnknownLocation.UseVisualStyleBackColor = true;
            this.chkUnknownLocation.CheckedChanged += new System.EventHandler(this.chkUnknownLocation_CheckedChanged);
            // 
            // chkIncludeNonCountryRecords
            // 
            this.chkIncludeNonCountryRecords.AutoSize = true;
            this.chkIncludeNonCountryRecords.Checked = true;
            this.chkIncludeNonCountryRecords.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeNonCountryRecords.Location = new System.Drawing.Point(11, 8);
            this.chkIncludeNonCountryRecords.Name = "chkIncludeNonCountryRecords";
            this.chkIncludeNonCountryRecords.Size = new System.Drawing.Size(201, 17);
            this.chkIncludeNonCountryRecords.TabIndex = 3;
            this.chkIncludeNonCountryRecords.Text = "Include Non Country Linked Records";
            this.chkIncludeNonCountryRecords.UseVisualStyleBackColor = true;
            this.chkIncludeNonCountryRecords.CheckedChanged += new System.EventHandler(this.chkIncludeNonCountryRecords_CheckedChanged);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.chkSubRegions);
            this.groupBox14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox14.Location = new System.Drawing.Point(172, 31);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(186, 271);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Sub Region";
            // 
            // chkSubRegions
            // 
            this.chkSubRegions.CheckBoxes = true;
            this.chkSubRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSubRegions.Location = new System.Drawing.Point(3, 16);
            this.chkSubRegions.Name = "chkSubRegions";
            this.chkSubRegions.Size = new System.Drawing.Size(180, 252);
            this.chkSubRegions.TabIndex = 1;
            this.chkSubRegions.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.chkSubRegions_AfterCheck);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkCountries);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(364, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 271);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Country";
            // 
            // chkCountries
            // 
            this.chkCountries.CheckBoxes = true;
            this.chkCountries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkCountries.Location = new System.Drawing.Point(3, 16);
            this.chkCountries.Name = "chkCountries";
            this.chkCountries.Size = new System.Drawing.Size(195, 252);
            this.chkCountries.TabIndex = 1;
            this.chkCountries.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.chkCountries_AfterCheck);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkRegions);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(8, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 271);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Region";
            // 
            // chkRegions
            // 
            this.chkRegions.CheckBoxes = true;
            this.chkRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkRegions.Location = new System.Drawing.Point(3, 16);
            this.chkRegions.Name = "chkRegions";
            this.chkRegions.Size = new System.Drawing.Size(152, 252);
            this.chkRegions.TabIndex = 0;
            this.chkRegions.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.chkRegions_AfterCheck);
            // 
            // tabBrand
            // 
            this.tabBrand.Controls.Add(this.brandFilterPanel);
            this.tabBrand.Controls.Add(this.chkNoUEIBrandNumbers);
            this.tabBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabBrand.ImageIndex = -1;
            this.tabBrand.Location = new System.Drawing.Point(4, 30);
            this.tabBrand.Name = "tabBrand";
            this.tabBrand.Size = new System.Drawing.Size(657, 307);
            this.tabBrand.TabIndex = 1;
            this.tabBrand.Text = "Brand";
            this.tabBrand.Visible = false;
            // 
            // brandFilterPanel
            // 
            this.brandFilterPanel.Controls.Add(this.groupBox4);
            this.brandFilterPanel.Controls.Add(this.groupBox3);
            this.brandFilterPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.brandFilterPanel.Location = new System.Drawing.Point(0, 28);
            this.brandFilterPanel.Name = "brandFilterPanel";
            this.brandFilterPanel.Size = new System.Drawing.Size(657, 279);
            this.brandFilterPanel.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnFilterBrandFile);
            this.groupBox4.Controls.Add(this.btnRemoveFilterBrand);
            this.groupBox4.Controls.Add(this.btnAddFilterBrand);
            this.groupBox4.Controls.Add(this.lstFilterBrands);
            this.groupBox4.Controls.Add(this.txtFilterBrand);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox4.Location = new System.Drawing.Point(286, 1);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(280, 275);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filter Out Brand Names";
            // 
            // btnFilterBrandFile
            // 
            this.btnFilterBrandFile.Image = global::UEI.Workflow2010.Report.Properties.Resources.fileload;
            this.btnFilterBrandFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilterBrandFile.Location = new System.Drawing.Point(41, 112);
            this.btnFilterBrandFile.Name = "btnFilterBrandFile";
            this.btnFilterBrandFile.Size = new System.Drawing.Size(75, 23);
            this.btnFilterBrandFile.TabIndex = 5;
            this.btnFilterBrandFile.Text = "Browse";
            this.btnFilterBrandFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFilterBrandFile.UseVisualStyleBackColor = true;
            this.btnFilterBrandFile.Click += new System.EventHandler(this.btnFilterBrandFile_Click);
            // 
            // btnRemoveFilterBrand
            // 
            this.btnRemoveFilterBrand.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveFilterBrand.Image")));
            this.btnRemoveFilterBrand.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemoveFilterBrand.Location = new System.Drawing.Point(41, 83);
            this.btnRemoveFilterBrand.Name = "btnRemoveFilterBrand";
            this.btnRemoveFilterBrand.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveFilterBrand.TabIndex = 3;
            this.btnRemoveFilterBrand.Text = "Remove";
            this.btnRemoveFilterBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemoveFilterBrand.UseVisualStyleBackColor = true;
            this.btnRemoveFilterBrand.Click += new System.EventHandler(this.btnRemoveFilterBrand_Click);
            // 
            // btnAddFilterBrand
            // 
            this.btnAddFilterBrand.Image = ((System.Drawing.Image)(resources.GetObject("btnAddFilterBrand.Image")));
            this.btnAddFilterBrand.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddFilterBrand.Location = new System.Drawing.Point(41, 54);
            this.btnAddFilterBrand.Name = "btnAddFilterBrand";
            this.btnAddFilterBrand.Size = new System.Drawing.Size(75, 23);
            this.btnAddFilterBrand.TabIndex = 2;
            this.btnAddFilterBrand.Text = "Add";
            this.btnAddFilterBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddFilterBrand.UseVisualStyleBackColor = true;
            this.btnAddFilterBrand.Click += new System.EventHandler(this.btnAddFilterBrand_Click);
            // 
            // lstFilterBrands
            // 
            this.lstFilterBrands.FormattingEnabled = true;
            this.lstFilterBrands.Location = new System.Drawing.Point(134, 18);
            this.lstFilterBrands.Name = "lstFilterBrands";
            this.lstFilterBrands.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstFilterBrands.Size = new System.Drawing.Size(140, 251);
            this.lstFilterBrands.TabIndex = 1;
            // 
            // txtFilterBrand
            // 
            this.txtFilterBrand.Location = new System.Drawing.Point(16, 21);
            this.txtFilterBrand.Name = "txtFilterBrand";
            this.txtFilterBrand.Size = new System.Drawing.Size(100, 20);
            this.txtFilterBrand.TabIndex = 0;
            this.txtFilterBrand.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilterBrand_KeyUp);
            this.txtFilterBrand.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtFilterBrand_PreviewKeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnMatchBrandFile);
            this.groupBox3.Controls.Add(this.lstMatchBrands);
            this.groupBox3.Controls.Add(this.btnRemoveMatchBrand);
            this.groupBox3.Controls.Add(this.btnAddMatchBrand);
            this.groupBox3.Controls.Add(this.txtMatchBrand);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(10, 1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 275);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Match Brand Names";
            // 
            // btnMatchBrandFile
            // 
            this.btnMatchBrandFile.Image = global::UEI.Workflow2010.Report.Properties.Resources.fileload;
            this.btnMatchBrandFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMatchBrandFile.Location = new System.Drawing.Point(53, 112);
            this.btnMatchBrandFile.Name = "btnMatchBrandFile";
            this.btnMatchBrandFile.Size = new System.Drawing.Size(75, 23);
            this.btnMatchBrandFile.TabIndex = 4;
            this.btnMatchBrandFile.Text = "Browse";
            this.btnMatchBrandFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMatchBrandFile.UseVisualStyleBackColor = true;
            this.btnMatchBrandFile.Click += new System.EventHandler(this.btnMatchBrandFile_Click);
            // 
            // lstMatchBrands
            // 
            this.lstMatchBrands.FormattingEnabled = true;
            this.lstMatchBrands.Location = new System.Drawing.Point(134, 19);
            this.lstMatchBrands.Name = "lstMatchBrands";
            this.lstMatchBrands.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstMatchBrands.Size = new System.Drawing.Size(130, 251);
            this.lstMatchBrands.TabIndex = 3;
            // 
            // btnRemoveMatchBrand
            // 
            this.btnRemoveMatchBrand.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveMatchBrand.Image")));
            this.btnRemoveMatchBrand.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemoveMatchBrand.Location = new System.Drawing.Point(53, 83);
            this.btnRemoveMatchBrand.Name = "btnRemoveMatchBrand";
            this.btnRemoveMatchBrand.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveMatchBrand.TabIndex = 2;
            this.btnRemoveMatchBrand.Text = "Remove";
            this.btnRemoveMatchBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemoveMatchBrand.UseVisualStyleBackColor = true;
            this.btnRemoveMatchBrand.Click += new System.EventHandler(this.btnRemoveMatchBrand_Click);
            // 
            // btnAddMatchBrand
            // 
            this.btnAddMatchBrand.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMatchBrand.Image")));
            this.btnAddMatchBrand.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddMatchBrand.Location = new System.Drawing.Point(53, 54);
            this.btnAddMatchBrand.Name = "btnAddMatchBrand";
            this.btnAddMatchBrand.Size = new System.Drawing.Size(75, 23);
            this.btnAddMatchBrand.TabIndex = 1;
            this.btnAddMatchBrand.Text = "Add";
            this.btnAddMatchBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddMatchBrand.UseVisualStyleBackColor = true;
            this.btnAddMatchBrand.Click += new System.EventHandler(this.btnAddMatchBrand_Click);
            // 
            // txtMatchBrand
            // 
            this.txtMatchBrand.Location = new System.Drawing.Point(28, 22);
            this.txtMatchBrand.Name = "txtMatchBrand";
            this.txtMatchBrand.Size = new System.Drawing.Size(100, 20);
            this.txtMatchBrand.TabIndex = 0;
            this.txtMatchBrand.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMatchBrand_KeyUp);
            this.txtMatchBrand.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtMatchBrand_PreviewKeyDown);
            // 
            // chkNoUEIBrandNumbers
            // 
            this.chkNoUEIBrandNumbers.AutoSize = true;
            this.chkNoUEIBrandNumbers.Enabled = false;
            this.chkNoUEIBrandNumbers.Location = new System.Drawing.Point(8, 3);
            this.chkNoUEIBrandNumbers.Name = "chkNoUEIBrandNumbers";
            this.chkNoUEIBrandNumbers.Size = new System.Drawing.Size(231, 17);
            this.chkNoUEIBrandNumbers.TabIndex = 2;
            this.chkNoUEIBrandNumbers.Text = "Include Record Without UEI Brand Number";
            this.chkNoUEIBrandNumbers.UseVisualStyleBackColor = true;
            // 
            // tabModel
            // 
            this.tabModel.Controls.Add(this.modelFilterPanel);
            this.tabModel.Controls.Add(this.chkRemoveNonCodeBookModels);
            this.tabModel.Controls.Add(this.chkRecordWithoutModelName);
            this.tabModel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabModel.ImageIndex = -1;
            this.tabModel.Location = new System.Drawing.Point(4, 30);
            this.tabModel.Name = "tabModel";
            this.tabModel.Size = new System.Drawing.Size(657, 307);
            this.tabModel.TabIndex = 2;
            this.tabModel.Text = "Model";
            this.tabModel.Visible = false;
            // 
            // modelFilterPanel
            // 
            this.modelFilterPanel.Controls.Add(this.groupBox6);
            this.modelFilterPanel.Controls.Add(this.groupBox5);
            this.modelFilterPanel.Location = new System.Drawing.Point(8, 26);
            this.modelFilterPanel.Name = "modelFilterPanel";
            this.modelFilterPanel.Size = new System.Drawing.Size(559, 253);
            this.modelFilterPanel.TabIndex = 7;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnFilterModelFile);
            this.groupBox6.Controls.Add(this.btnRemoveFilterModel);
            this.groupBox6.Controls.Add(this.btnAddFilterModel);
            this.groupBox6.Controls.Add(this.lstFilterModels);
            this.groupBox6.Controls.Add(this.txtFilterModel);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox6.Location = new System.Drawing.Point(277, -1);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(280, 251);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Filter Out Model Names";
            // 
            // btnFilterModelFile
            // 
            this.btnFilterModelFile.Image = global::UEI.Workflow2010.Report.Properties.Resources.fileload;
            this.btnFilterModelFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFilterModelFile.Location = new System.Drawing.Point(41, 114);
            this.btnFilterModelFile.Name = "btnFilterModelFile";
            this.btnFilterModelFile.Size = new System.Drawing.Size(75, 23);
            this.btnFilterModelFile.TabIndex = 6;
            this.btnFilterModelFile.Text = "Browse";
            this.btnFilterModelFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFilterModelFile.UseVisualStyleBackColor = true;
            this.btnFilterModelFile.Click += new System.EventHandler(this.btnFilterModelFile_Click);
            // 
            // btnRemoveFilterModel
            // 
            this.btnRemoveFilterModel.Image = global::UEI.Workflow2010.Report.Properties.Resources.GoRtlHS;
            this.btnRemoveFilterModel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemoveFilterModel.Location = new System.Drawing.Point(41, 84);
            this.btnRemoveFilterModel.Name = "btnRemoveFilterModel";
            this.btnRemoveFilterModel.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveFilterModel.TabIndex = 3;
            this.btnRemoveFilterModel.Text = "Remove";
            this.btnRemoveFilterModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemoveFilterModel.UseVisualStyleBackColor = true;
            this.btnRemoveFilterModel.Click += new System.EventHandler(this.btnRemoveFilterModel_Click);
            // 
            // btnAddFilterModel
            // 
            this.btnAddFilterModel.Image = global::UEI.Workflow2010.Report.Properties.Resources.GoLtrHS;
            this.btnAddFilterModel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddFilterModel.Location = new System.Drawing.Point(41, 54);
            this.btnAddFilterModel.Name = "btnAddFilterModel";
            this.btnAddFilterModel.Size = new System.Drawing.Size(75, 23);
            this.btnAddFilterModel.TabIndex = 2;
            this.btnAddFilterModel.Text = "Add";
            this.btnAddFilterModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddFilterModel.UseVisualStyleBackColor = true;
            this.btnAddFilterModel.Click += new System.EventHandler(this.btnAddFilterModel_Click);
            // 
            // lstFilterModels
            // 
            this.lstFilterModels.FormattingEnabled = true;
            this.lstFilterModels.Location = new System.Drawing.Point(134, 19);
            this.lstFilterModels.Name = "lstFilterModels";
            this.lstFilterModels.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstFilterModels.Size = new System.Drawing.Size(140, 225);
            this.lstFilterModels.TabIndex = 1;
            // 
            // txtFilterModel
            // 
            this.txtFilterModel.Location = new System.Drawing.Point(16, 21);
            this.txtFilterModel.Name = "txtFilterModel";
            this.txtFilterModel.Size = new System.Drawing.Size(100, 20);
            this.txtFilterModel.TabIndex = 0;
            this.txtFilterModel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilterModel_KeyUp);
            this.txtFilterModel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtFilterModel_PreviewKeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnMatchModelFile);
            this.groupBox5.Controls.Add(this.lstMatchModels);
            this.groupBox5.Controls.Add(this.btnRemoveMatchModel);
            this.groupBox5.Controls.Add(this.btnAddMatchModel);
            this.groupBox5.Controls.Add(this.txtMatchModel);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(1, -1);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(270, 251);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Match Model Names";
            // 
            // btnMatchModelFile
            // 
            this.btnMatchModelFile.Image = global::UEI.Workflow2010.Report.Properties.Resources.fileload;
            this.btnMatchModelFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMatchModelFile.Location = new System.Drawing.Point(53, 114);
            this.btnMatchModelFile.Name = "btnMatchModelFile";
            this.btnMatchModelFile.Size = new System.Drawing.Size(75, 23);
            this.btnMatchModelFile.TabIndex = 5;
            this.btnMatchModelFile.Text = "Browse ";
            this.btnMatchModelFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMatchModelFile.UseVisualStyleBackColor = true;
            this.btnMatchModelFile.Click += new System.EventHandler(this.btnMatchModelFile_Click);
            // 
            // lstMatchModels
            // 
            this.lstMatchModels.FormattingEnabled = true;
            this.lstMatchModels.Location = new System.Drawing.Point(134, 19);
            this.lstMatchModels.Name = "lstMatchModels";
            this.lstMatchModels.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstMatchModels.Size = new System.Drawing.Size(130, 225);
            this.lstMatchModels.TabIndex = 3;
            // 
            // btnRemoveMatchModel
            // 
            this.btnRemoveMatchModel.Image = global::UEI.Workflow2010.Report.Properties.Resources.GoRtlHS;
            this.btnRemoveMatchModel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemoveMatchModel.Location = new System.Drawing.Point(53, 84);
            this.btnRemoveMatchModel.Name = "btnRemoveMatchModel";
            this.btnRemoveMatchModel.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveMatchModel.TabIndex = 2;
            this.btnRemoveMatchModel.Text = "Remove";
            this.btnRemoveMatchModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemoveMatchModel.UseVisualStyleBackColor = true;
            this.btnRemoveMatchModel.Click += new System.EventHandler(this.btnRemoveMatchModel_Click);
            // 
            // btnAddMatchModel
            // 
            this.btnAddMatchModel.Image = global::UEI.Workflow2010.Report.Properties.Resources.GoLtrHS;
            this.btnAddMatchModel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddMatchModel.Location = new System.Drawing.Point(53, 54);
            this.btnAddMatchModel.Name = "btnAddMatchModel";
            this.btnAddMatchModel.Size = new System.Drawing.Size(75, 23);
            this.btnAddMatchModel.TabIndex = 1;
            this.btnAddMatchModel.Text = "Add";
            this.btnAddMatchModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddMatchModel.UseVisualStyleBackColor = true;
            this.btnAddMatchModel.Click += new System.EventHandler(this.btnAddMatchModel_Click);
            // 
            // txtMatchModel
            // 
            this.txtMatchModel.Location = new System.Drawing.Point(28, 22);
            this.txtMatchModel.Name = "txtMatchModel";
            this.txtMatchModel.Size = new System.Drawing.Size(100, 20);
            this.txtMatchModel.TabIndex = 0;
            this.txtMatchModel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMatchModel_KeyUp);
            this.txtMatchModel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtMatchModel_PreviewKeyDown);
            // 
            // chkRemoveNonCodeBookModels
            // 
            this.chkRemoveNonCodeBookModels.AutoSize = true;
            this.chkRemoveNonCodeBookModels.Location = new System.Drawing.Point(8, 285);
            this.chkRemoveNonCodeBookModels.Name = "chkRemoveNonCodeBookModels";
            this.chkRemoveNonCodeBookModels.Size = new System.Drawing.Size(177, 17);
            this.chkRemoveNonCodeBookModels.TabIndex = 6;
            this.chkRemoveNonCodeBookModels.Text = "Remove non code book models";
            this.chkRemoveNonCodeBookModels.UseVisualStyleBackColor = true;
            // 
            // chkRecordWithoutModelName
            // 
            this.chkRecordWithoutModelName.AutoSize = true;
            this.chkRecordWithoutModelName.Checked = true;
            this.chkRecordWithoutModelName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRecordWithoutModelName.Location = new System.Drawing.Point(8, 3);
            this.chkRecordWithoutModelName.Name = "chkRecordWithoutModelName";
            this.chkRecordWithoutModelName.Size = new System.Drawing.Size(223, 17);
            this.chkRecordWithoutModelName.TabIndex = 3;
            this.chkRecordWithoutModelName.Text = "Include Record Without UEI Model Name";
            this.chkRecordWithoutModelName.UseVisualStyleBackColor = true;
            // 
            // tabDataSourceLocation
            // 
            this.tabDataSourceLocation.Controls.Add(this.groupBox7);
            this.tabDataSourceLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDataSourceLocation.ImageIndex = -1;
            this.tabDataSourceLocation.Location = new System.Drawing.Point(4, 30);
            this.tabDataSourceLocation.Name = "tabDataSourceLocation";
            this.tabDataSourceLocation.Size = new System.Drawing.Size(657, 307);
            this.tabDataSourceLocation.TabIndex = 3;
            this.tabDataSourceLocation.Text = "Data Source Location";
            this.tabDataSourceLocation.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.treeDataSources);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(657, 307);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Select Data Sources";
            // 
            // treeDataSources
            // 
            this.treeDataSources.CheckBoxes = true;
            this.treeDataSources.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeDataSources.Location = new System.Drawing.Point(3, 16);
            this.treeDataSources.Name = "treeDataSources";
            this.treeDataSources.Size = new System.Drawing.Size(248, 288);
            this.treeDataSources.TabIndex = 1;
            this.treeDataSources.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeDataSources_AfterCheck);
            // 
            // tabModelInfoSpecific
            // 
            this.tabModelInfoSpecific.Controls.Add(this.dateBasedSearchGroup);
            this.tabModelInfoSpecific.Controls.Add(this.groupBox8);
            this.tabModelInfoSpecific.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabModelInfoSpecific.ImageIndex = -1;
            this.tabModelInfoSpecific.Location = new System.Drawing.Point(4, 30);
            this.tabModelInfoSpecific.Name = "tabModelInfoSpecific";
            this.tabModelInfoSpecific.Size = new System.Drawing.Size(573, 307);
            this.tabModelInfoSpecific.TabIndex = 4;
            this.tabModelInfoSpecific.Text = "Model Info Specific";
            this.tabModelInfoSpecific.Visible = false;
            // 
            // dateBasedSearchGroup
            // 
            this.dateBasedSearchGroup.Controls.Add(this.grpMIProject);
            this.dateBasedSearchGroup.Controls.Add(this.grpMIOption);
            this.dateBasedSearchGroup.Controls.Add(this.grpMIDateSelection);
            this.dateBasedSearchGroup.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateBasedSearchGroup.Enabled = false;
            this.dateBasedSearchGroup.Location = new System.Drawing.Point(176, 0);
            this.dateBasedSearchGroup.Name = "dateBasedSearchGroup";
            this.dateBasedSearchGroup.Size = new System.Drawing.Size(393, 307);
            this.dateBasedSearchGroup.TabIndex = 2;
            this.dateBasedSearchGroup.TabStop = false;
            // 
            // grpMIProject
            // 
            this.grpMIProject.Controls.Add(this.label5);
            this.grpMIProject.Controls.Add(this.btnMIIdenticalBrowse);
            this.grpMIProject.Controls.Add(this.chkMIIncludeIdenticalId);
            this.grpMIProject.Controls.Add(this.txtMIIdenticalFile);
            this.grpMIProject.Controls.Add(this.txtMIProjectName);
            this.grpMIProject.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpMIProject.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpMIProject.Location = new System.Drawing.Point(3, 147);
            this.grpMIProject.Name = "grpMIProject";
            this.grpMIProject.Size = new System.Drawing.Size(387, 110);
            this.grpMIProject.TabIndex = 31;
            this.grpMIProject.TabStop = false;
            this.grpMIProject.Text = "Selected Project";
            this.grpMIProject.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(62, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Select Identical Id List File";
            // 
            // btnMIIdenticalBrowse
            // 
            this.btnMIIdenticalBrowse.Location = new System.Drawing.Point(315, 81);
            this.btnMIIdenticalBrowse.Name = "btnMIIdenticalBrowse";
            this.btnMIIdenticalBrowse.Size = new System.Drawing.Size(50, 23);
            this.btnMIIdenticalBrowse.TabIndex = 13;
            this.btnMIIdenticalBrowse.Text = "Browse";
            this.btnMIIdenticalBrowse.UseVisualStyleBackColor = true;
            this.btnMIIdenticalBrowse.Click += new System.EventHandler(this.btnMIIdenticalBrowse_Click);
            // 
            // chkMIIncludeIdenticalId
            // 
            this.chkMIIncludeIdenticalId.AutoSize = true;
            this.chkMIIncludeIdenticalId.Location = new System.Drawing.Point(68, 47);
            this.chkMIIncludeIdenticalId.Name = "chkMIIncludeIdenticalId";
            this.chkMIIncludeIdenticalId.Size = new System.Drawing.Size(188, 17);
            this.chkMIIncludeIdenticalId.TabIndex = 12;
            this.chkMIIncludeIdenticalId.Text = "Include Identical Id Elimination List";
            this.chkMIIncludeIdenticalId.UseVisualStyleBackColor = true;
            // 
            // txtMIIdenticalFile
            // 
            this.txtMIIdenticalFile.BackColor = System.Drawing.Color.White;
            this.txtMIIdenticalFile.Location = new System.Drawing.Point(65, 83);
            this.txtMIIdenticalFile.Name = "txtMIIdenticalFile";
            this.txtMIIdenticalFile.ReadOnly = true;
            this.txtMIIdenticalFile.Size = new System.Drawing.Size(244, 20);
            this.txtMIIdenticalFile.TabIndex = 11;
            // 
            // txtMIProjectName
            // 
            this.txtMIProjectName.BackColor = System.Drawing.Color.White;
            this.txtMIProjectName.Location = new System.Drawing.Point(68, 19);
            this.txtMIProjectName.Name = "txtMIProjectName";
            this.txtMIProjectName.ReadOnly = true;
            this.txtMIProjectName.Size = new System.Drawing.Size(244, 20);
            this.txtMIProjectName.TabIndex = 11;
            // 
            // grpMIOption
            // 
            this.grpMIOption.Controls.Add(this.dBSIDRd);
            this.grpMIOption.Controls.Add(this.dbsModelsRd);
            this.grpMIOption.Controls.Add(this.dBSTNRd);
            this.grpMIOption.Controls.Add(this.dBSBrandsRd);
            this.grpMIOption.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpMIOption.Location = new System.Drawing.Point(3, 62);
            this.grpMIOption.Name = "grpMIOption";
            this.grpMIOption.Size = new System.Drawing.Size(387, 85);
            this.grpMIOption.TabIndex = 30;
            this.grpMIOption.TabStop = false;
            this.grpMIOption.Text = "Option";
            // 
            // dBSIDRd
            // 
            this.dBSIDRd.AutoSize = true;
            this.dBSIDRd.Location = new System.Drawing.Point(164, 47);
            this.dBSIDRd.Name = "dBSIDRd";
            this.dBSIDRd.Size = new System.Drawing.Size(69, 17);
            this.dBSIDRd.TabIndex = 25;
            this.dBSIDRd.Text = "ID Based";
            this.dBSIDRd.UseVisualStyleBackColor = true;
            this.dBSIDRd.CheckedChanged += new System.EventHandler(this.dBSOptionRd_Checked);
            // 
            // dbsModelsRd
            // 
            this.dbsModelsRd.AutoSize = true;
            this.dbsModelsRd.Checked = true;
            this.dbsModelsRd.Location = new System.Drawing.Point(68, 24);
            this.dbsModelsRd.Name = "dbsModelsRd";
            this.dbsModelsRd.Size = new System.Drawing.Size(87, 17);
            this.dbsModelsRd.TabIndex = 23;
            this.dbsModelsRd.TabStop = true;
            this.dbsModelsRd.Text = "Model Based";
            this.dbsModelsRd.UseVisualStyleBackColor = true;
            this.dbsModelsRd.CheckedChanged += new System.EventHandler(this.dBSOptionRd_Checked);
            // 
            // dBSTNRd
            // 
            this.dBSTNRd.AutoSize = true;
            this.dBSTNRd.Location = new System.Drawing.Point(164, 23);
            this.dBSTNRd.Name = "dBSTNRd";
            this.dBSTNRd.Size = new System.Drawing.Size(70, 17);
            this.dBSTNRd.TabIndex = 26;
            this.dBSTNRd.Text = "New TNs";
            this.dBSTNRd.UseVisualStyleBackColor = true;
            this.dBSTNRd.CheckedChanged += new System.EventHandler(this.dBSOptionRd_Checked);
            // 
            // dBSBrandsRd
            // 
            this.dBSBrandsRd.AutoSize = true;
            this.dBSBrandsRd.Location = new System.Drawing.Point(68, 47);
            this.dBSBrandsRd.Name = "dBSBrandsRd";
            this.dBSBrandsRd.Size = new System.Drawing.Size(86, 17);
            this.dBSBrandsRd.TabIndex = 24;
            this.dBSBrandsRd.Text = "Brand Based";
            this.dBSBrandsRd.UseVisualStyleBackColor = true;
            this.dBSBrandsRd.CheckedChanged += new System.EventHandler(this.dBSOptionRd_Checked);
            // 
            // grpMIDateSelection
            // 
            this.grpMIDateSelection.Controls.Add(this.dBSFromDateLbl);
            this.grpMIDateSelection.Controls.Add(this.dBSFromDateTxt);
            this.grpMIDateSelection.Controls.Add(this.dBSToDateBtn);
            this.grpMIDateSelection.Controls.Add(this.dBSFromDateBtn);
            this.grpMIDateSelection.Controls.Add(this.dBSToDateTxt);
            this.grpMIDateSelection.Controls.Add(this.dBSToDateLbl);
            this.grpMIDateSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpMIDateSelection.Location = new System.Drawing.Point(3, 16);
            this.grpMIDateSelection.Name = "grpMIDateSelection";
            this.grpMIDateSelection.Size = new System.Drawing.Size(387, 46);
            this.grpMIDateSelection.TabIndex = 28;
            this.grpMIDateSelection.TabStop = false;
            this.grpMIDateSelection.Text = "Select From and To Date";
            // 
            // dBSFromDateLbl
            // 
            this.dBSFromDateLbl.AllowDrop = true;
            this.dBSFromDateLbl.AutoSize = true;
            this.dBSFromDateLbl.Location = new System.Drawing.Point(6, 23);
            this.dBSFromDateLbl.Name = "dBSFromDateLbl";
            this.dBSFromDateLbl.Size = new System.Drawing.Size(56, 13);
            this.dBSFromDateLbl.TabIndex = 0;
            this.dBSFromDateLbl.Text = "From Date";
            // 
            // dBSFromDateTxt
            // 
            this.dBSFromDateTxt.Location = new System.Drawing.Point(68, 16);
            this.dBSFromDateTxt.Name = "dBSFromDateTxt";
            this.dBSFromDateTxt.Size = new System.Drawing.Size(93, 20);
            this.dBSFromDateTxt.TabIndex = 2;
            this.dBSFromDateTxt.Leave += new System.EventHandler(this.dBSFromDateTxt_Leave);
            // 
            // dBSToDateBtn
            // 
            this.dBSToDateBtn.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.dBSToDateBtn.Location = new System.Drawing.Point(343, 15);
            this.dBSToDateBtn.Name = "dBSToDateBtn";
            this.dBSToDateBtn.Size = new System.Drawing.Size(22, 21);
            this.dBSToDateBtn.TabIndex = 22;
            this.dBSToDateBtn.UseVisualStyleBackColor = true;
            this.dBSToDateBtn.Click += new System.EventHandler(this.dBSToDateBtn_Click);
            // 
            // dBSFromDateBtn
            // 
            this.dBSFromDateBtn.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.dBSFromDateBtn.Location = new System.Drawing.Point(164, 16);
            this.dBSFromDateBtn.Name = "dBSFromDateBtn";
            this.dBSFromDateBtn.Size = new System.Drawing.Size(22, 21);
            this.dBSFromDateBtn.TabIndex = 20;
            this.dBSFromDateBtn.UseVisualStyleBackColor = true;
            this.dBSFromDateBtn.Click += new System.EventHandler(this.dBSFromDateBtn_Click);
            // 
            // dBSToDateTxt
            // 
            this.dBSToDateTxt.Location = new System.Drawing.Point(244, 16);
            this.dBSToDateTxt.Name = "dBSToDateTxt";
            this.dBSToDateTxt.Size = new System.Drawing.Size(93, 20);
            this.dBSToDateTxt.TabIndex = 3;
            this.dBSToDateTxt.Leave += new System.EventHandler(this.dBSToDateTxt_Leave);
            // 
            // dBSToDateLbl
            // 
            this.dBSToDateLbl.AutoSize = true;
            this.dBSToDateLbl.Location = new System.Drawing.Point(192, 23);
            this.dBSToDateLbl.Name = "dBSToDateLbl";
            this.dBSToDateLbl.Size = new System.Drawing.Size(46, 13);
            this.dBSToDateLbl.TabIndex = 1;
            this.dBSToDateLbl.Text = "To Date";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.chkKeepDuplicateModels);
            this.groupBox8.Controls.Add(this.chkBxIncludeRestrictedIDs_ModelInfo);
            this.groupBox8.Controls.Add(this.chkTNLinks);
            this.groupBox8.Controls.Add(this.enableDateFilterChk);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox8.Location = new System.Drawing.Point(0, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(176, 307);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            // 
            // chkKeepDuplicateModels
            // 
            this.chkKeepDuplicateModels.AutoSize = true;
            this.chkKeepDuplicateModels.Enabled = false;
            this.chkKeepDuplicateModels.Location = new System.Drawing.Point(15, 19);
            this.chkKeepDuplicateModels.Name = "chkKeepDuplicateModels";
            this.chkKeepDuplicateModels.Size = new System.Drawing.Size(99, 17);
            this.chkKeepDuplicateModels.TabIndex = 0;
            this.chkKeepDuplicateModels.Text = "Keep Duplicate";
            this.chkKeepDuplicateModels.UseVisualStyleBackColor = true;
            // 
            // chkBxIncludeRestrictedIDs_ModelInfo
            // 
            this.chkBxIncludeRestrictedIDs_ModelInfo.AutoSize = true;
            this.chkBxIncludeRestrictedIDs_ModelInfo.Location = new System.Drawing.Point(15, 68);
            this.chkBxIncludeRestrictedIDs_ModelInfo.Name = "chkBxIncludeRestrictedIDs_ModelInfo";
            this.chkBxIncludeRestrictedIDs_ModelInfo.Size = new System.Drawing.Size(129, 17);
            this.chkBxIncludeRestrictedIDs_ModelInfo.TabIndex = 3;
            this.chkBxIncludeRestrictedIDs_ModelInfo.Text = "Include Restricted Ids";
            this.chkBxIncludeRestrictedIDs_ModelInfo.UseVisualStyleBackColor = true;
            // 
            // chkTNLinks
            // 
            this.chkTNLinks.AutoSize = true;
            this.chkTNLinks.Checked = true;
            this.chkTNLinks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTNLinks.Location = new System.Drawing.Point(15, 45);
            this.chkTNLinks.Name = "chkTNLinks";
            this.chkTNLinks.Size = new System.Drawing.Size(69, 17);
            this.chkTNLinks.TabIndex = 1;
            this.chkTNLinks.Text = "TN Links";
            this.chkTNLinks.UseVisualStyleBackColor = true;
            // 
            // enableDateFilterChk
            // 
            this.enableDateFilterChk.AutoSize = true;
            this.enableDateFilterChk.Location = new System.Drawing.Point(15, 91);
            this.enableDateFilterChk.Name = "enableDateFilterChk";
            this.enableDateFilterChk.Size = new System.Drawing.Size(155, 17);
            this.enableDateFilterChk.TabIndex = 0;
            this.enableDateFilterChk.Text = "Enable Date Based Search";
            this.enableDateFilterChk.UseVisualStyleBackColor = true;
            this.enableDateFilterChk.CheckedChanged += new System.EventHandler(this.enableDateFilterChk_CheckedChanged);
            // 
            // tabDevice
            // 
            this.tabDevice.Controls.Add(this.groupBox15);
            this.tabDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDevice.ImageIndex = -1;
            this.tabDevice.Location = new System.Drawing.Point(4, 30);
            this.tabDevice.Name = "tabDevice";
            this.tabDevice.Size = new System.Drawing.Size(573, 307);
            this.tabDevice.TabIndex = 3;
            this.tabDevice.Text = "Device";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.chkIncludeNonComponentLinkedRecords);
            this.groupBox15.Controls.Add(this.groupBox19);
            this.groupBox15.Controls.Add(this.chkIncludeEmptyDeviceType);
            this.groupBox15.Controls.Add(this.groupBox16);
            this.groupBox15.Controls.Add(this.groupBox17);
            this.groupBox15.Controls.Add(this.groupBox18);
            this.groupBox15.Controls.Add(this.chkRemoveIDs);
            this.groupBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox15.Location = new System.Drawing.Point(0, 0);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(573, 307);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            // 
            // chkIncludeNonComponentLinkedRecords
            // 
            this.chkIncludeNonComponentLinkedRecords.AutoSize = true;
            this.chkIncludeNonComponentLinkedRecords.Checked = true;
            this.chkIncludeNonComponentLinkedRecords.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeNonComponentLinkedRecords.Location = new System.Drawing.Point(343, 33);
            this.chkIncludeNonComponentLinkedRecords.Name = "chkIncludeNonComponentLinkedRecords";
            this.chkIncludeNonComponentLinkedRecords.Size = new System.Drawing.Size(219, 17);
            this.chkIncludeNonComponentLinkedRecords.TabIndex = 8;
            this.chkIncludeNonComponentLinkedRecords.Text = "Include Non Component Linked Records";
            this.chkIncludeNonComponentLinkedRecords.UseVisualStyleBackColor = true;
            this.chkIncludeNonComponentLinkedRecords.CheckedChanged += new System.EventHandler(this.chkIncludeNonComponentLinkedRecords_CheckedChanged);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.rdoCompStatusBoth);
            this.groupBox19.Controls.Add(this.rdoCompStatusNonBuiltIn);
            this.groupBox19.Controls.Add(this.rdoCompStatusBuiltIn);
            this.groupBox19.Location = new System.Drawing.Point(552, 13);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(10, 37);
            this.groupBox19.TabIndex = 7;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Component Status";
            this.groupBox19.Visible = false;
            // 
            // rdoCompStatusBoth
            // 
            this.rdoCompStatusBoth.AutoSize = true;
            this.rdoCompStatusBoth.Checked = true;
            this.rdoCompStatusBoth.Location = new System.Drawing.Point(220, 14);
            this.rdoCompStatusBoth.Name = "rdoCompStatusBoth";
            this.rdoCompStatusBoth.Size = new System.Drawing.Size(47, 17);
            this.rdoCompStatusBoth.TabIndex = 2;
            this.rdoCompStatusBoth.TabStop = true;
            this.rdoCompStatusBoth.Text = "Both";
            this.rdoCompStatusBoth.UseVisualStyleBackColor = true;
            this.rdoCompStatusBoth.CheckedChanged += new System.EventHandler(this.rdoCompStatusBoth_CheckedChanged);
            // 
            // rdoCompStatusNonBuiltIn
            // 
            this.rdoCompStatusNonBuiltIn.AutoSize = true;
            this.rdoCompStatusNonBuiltIn.Location = new System.Drawing.Point(134, 14);
            this.rdoCompStatusNonBuiltIn.Name = "rdoCompStatusNonBuiltIn";
            this.rdoCompStatusNonBuiltIn.Size = new System.Drawing.Size(80, 17);
            this.rdoCompStatusNonBuiltIn.TabIndex = 1;
            this.rdoCompStatusNonBuiltIn.Text = "Non Built In";
            this.rdoCompStatusNonBuiltIn.UseVisualStyleBackColor = true;
            this.rdoCompStatusNonBuiltIn.CheckedChanged += new System.EventHandler(this.rdoCompStatusNonBuiltIn_CheckedChanged);
            // 
            // rdoCompStatusBuiltIn
            // 
            this.rdoCompStatusBuiltIn.AutoSize = true;
            this.rdoCompStatusBuiltIn.Location = new System.Drawing.Point(71, 14);
            this.rdoCompStatusBuiltIn.Name = "rdoCompStatusBuiltIn";
            this.rdoCompStatusBuiltIn.Size = new System.Drawing.Size(57, 17);
            this.rdoCompStatusBuiltIn.TabIndex = 0;
            this.rdoCompStatusBuiltIn.Text = "Built In";
            this.rdoCompStatusBuiltIn.UseVisualStyleBackColor = true;
            this.rdoCompStatusBuiltIn.CheckedChanged += new System.EventHandler(this.rdoCompStatusBuiltIn_CheckedChanged);
            // 
            // chkIncludeEmptyDeviceType
            // 
            this.chkIncludeEmptyDeviceType.AutoSize = true;
            this.chkIncludeEmptyDeviceType.Checked = true;
            this.chkIncludeEmptyDeviceType.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeEmptyDeviceType.Location = new System.Drawing.Point(12, 33);
            this.chkIncludeEmptyDeviceType.Name = "chkIncludeEmptyDeviceType";
            this.chkIncludeEmptyDeviceType.Size = new System.Drawing.Size(271, 17);
            this.chkIncludeEmptyDeviceType.TabIndex = 6;
            this.chkIncludeEmptyDeviceType.Text = "Include Empty Device Type (IDBased Device Type)";
            this.chkIncludeEmptyDeviceType.UseVisualStyleBackColor = true;
            this.chkIncludeEmptyDeviceType.CheckedChanged += new System.EventHandler(this.chkIncludeEmptyDeviceType_CheckedChanged);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.chkSubDevices);
            this.groupBox16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox16.Location = new System.Drawing.Point(172, 56);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(186, 246);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Sub Devices";
            // 
            // chkSubDevices
            // 
            this.chkSubDevices.CheckBoxes = true;
            this.chkSubDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSubDevices.Location = new System.Drawing.Point(3, 16);
            this.chkSubDevices.Name = "chkSubDevices";
            this.chkSubDevices.Size = new System.Drawing.Size(180, 227);
            this.chkSubDevices.TabIndex = 1;
            this.chkSubDevices.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.chkSubDevices_AfterCheck);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.chkComponents);
            this.groupBox17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox17.Location = new System.Drawing.Point(364, 56);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(201, 246);
            this.groupBox17.TabIndex = 4;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Components";
            // 
            // chkComponents
            // 
            this.chkComponents.CheckBoxes = true;
            this.chkComponents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkComponents.Location = new System.Drawing.Point(3, 16);
            this.chkComponents.Name = "chkComponents";
            this.chkComponents.Size = new System.Drawing.Size(195, 227);
            this.chkComponents.TabIndex = 1;
            this.chkComponents.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.chkComponents_AfterCheck);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.chkMainDevices);
            this.groupBox18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox18.Location = new System.Drawing.Point(8, 56);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(158, 246);
            this.groupBox18.TabIndex = 3;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Main Devices";
            // 
            // chkMainDevices
            // 
            this.chkMainDevices.CheckBoxes = true;
            this.chkMainDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkMainDevices.Location = new System.Drawing.Point(3, 16);
            this.chkMainDevices.Name = "chkMainDevices";
            this.chkMainDevices.Size = new System.Drawing.Size(152, 227);
            this.chkMainDevices.TabIndex = 0;
            this.chkMainDevices.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.chkMainDevices_AfterCheck);
            // 
            // chkRemoveIDs
            // 
            this.chkRemoveIDs.AutoSize = true;
            this.chkRemoveIDs.Location = new System.Drawing.Point(11, 13);
            this.chkRemoveIDs.Name = "chkRemoveIDs";
            this.chkRemoveIDs.Size = new System.Drawing.Size(272, 17);
            this.chkRemoveIDs.TabIndex = 0;
            this.chkRemoveIDs.Text = "Remove IDs Linked To Selected Sub Device Types";
            this.chkRemoveIDs.UseVisualStyleBackColor = true;
            this.chkRemoveIDs.CheckedChanged += new System.EventHandler(this.chkRemoveIDs_CheckedChanged);
            // 
            // tabIDSetupCodeInfo
            // 
            this.tabIDSetupCodeInfo.Controls.Add(this.chkFormatStartDigitSwitch);
            this.tabIDSetupCodeInfo.Controls.Add(this.chkIncludeAliasBrand);
            this.tabIDSetupCodeInfo.Controls.Add(this.chkBxIncludeBlankModelNames);
            this.tabIDSetupCodeInfo.Controls.Add(this.grpProject);
            this.tabIDSetupCodeInfo.Controls.Add(this.chkDisplayMajorBrands);
            this.tabIDSetupCodeInfo.Controls.Add(this.chkDisplayBrandNumber);
            this.tabIDSetupCodeInfo.Controls.Add(this.grpAliasIDFile);
            this.tabIDSetupCodeInfo.Controls.Add(this.txtIDOffset);
            this.tabIDSetupCodeInfo.Controls.Add(this.lblIDOffset);
            this.tabIDSetupCodeInfo.Controls.Add(this.lblSetupCodeFormat);
            this.tabIDSetupCodeInfo.Controls.Add(this.gbSort);
            this.tabIDSetupCodeInfo.Controls.Add(this.cmbSetupCodeFormating);
            this.tabIDSetupCodeInfo.Controls.Add(this.chkIncludeRestrictedIDs);
            this.tabIDSetupCodeInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabIDSetupCodeInfo.ImageIndex = -1;
            this.tabIDSetupCodeInfo.Location = new System.Drawing.Point(4, 30);
            this.tabIDSetupCodeInfo.Name = "tabIDSetupCodeInfo";
            this.tabIDSetupCodeInfo.Size = new System.Drawing.Size(573, 307);
            this.tabIDSetupCodeInfo.TabIndex = 6;
            this.tabIDSetupCodeInfo.Text = "ID & Setup Code Info";
            this.tabIDSetupCodeInfo.Visible = false;
            // 
            // chkFormatStartDigitSwitch
            // 
            this.chkFormatStartDigitSwitch.AutoSize = true;
            this.chkFormatStartDigitSwitch.Checked = true;
            this.chkFormatStartDigitSwitch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFormatStartDigitSwitch.Enabled = false;
            this.chkFormatStartDigitSwitch.Location = new System.Drawing.Point(348, 63);
            this.chkFormatStartDigitSwitch.Name = "chkFormatStartDigitSwitch";
            this.chkFormatStartDigitSwitch.Size = new System.Drawing.Size(138, 17);
            this.chkFormatStartDigitSwitch.TabIndex = 20;
            this.chkFormatStartDigitSwitch.Text = "Start Base Digits With 1";
            this.chkFormatStartDigitSwitch.UseVisualStyleBackColor = true;
            this.chkFormatStartDigitSwitch.CheckedChanged += new System.EventHandler(this.chkFormatStartDigitSwitch_CheckedChanged);
            // 
            // chkIncludeAliasBrand
            // 
            this.chkIncludeAliasBrand.AutoSize = true;
            this.chkIncludeAliasBrand.Checked = true;
            this.chkIncludeAliasBrand.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeAliasBrand.Enabled = false;
            this.chkIncludeAliasBrand.Location = new System.Drawing.Point(18, 58);
            this.chkIncludeAliasBrand.Name = "chkIncludeAliasBrand";
            this.chkIncludeAliasBrand.Size = new System.Drawing.Size(117, 17);
            this.chkIncludeAliasBrand.TabIndex = 18;
            this.chkIncludeAliasBrand.Text = "Include Alias Brand";
            this.chkIncludeAliasBrand.UseVisualStyleBackColor = true;
            this.chkIncludeAliasBrand.Visible = false;
            this.chkIncludeAliasBrand.CheckedChanged += new System.EventHandler(this.chkIncludeAliasBrand_CheckedChanged);
            // 
            // chkBxIncludeBlankModelNames
            // 
            this.chkBxIncludeBlankModelNames.AutoSize = true;
            this.chkBxIncludeBlankModelNames.Checked = true;
            this.chkBxIncludeBlankModelNames.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBxIncludeBlankModelNames.Location = new System.Drawing.Point(176, 33);
            this.chkBxIncludeBlankModelNames.Name = "chkBxIncludeBlankModelNames";
            this.chkBxIncludeBlankModelNames.Size = new System.Drawing.Size(130, 17);
            this.chkBxIncludeBlankModelNames.TabIndex = 17;
            this.chkBxIncludeBlankModelNames.Text = "Include Empty Models";
            this.chkBxIncludeBlankModelNames.UseVisualStyleBackColor = true;
            this.chkBxIncludeBlankModelNames.CheckedChanged += new System.EventHandler(this.chkBxIncludeBlankModelNames_CheckedChanged);
            // 
            // grpProject
            // 
            this.grpProject.Controls.Add(this.label9);
            this.grpProject.Controls.Add(this.btnBrowseIdenticalIdListFile);
            this.grpProject.Controls.Add(this.chkIncludeIdenticalId);
            this.grpProject.Controls.Add(this.txtIdenticalIdListFile);
            this.grpProject.Controls.Add(this.txtProjectName);
            this.grpProject.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpProject.Location = new System.Drawing.Point(9, 181);
            this.grpProject.Name = "grpProject";
            this.grpProject.Size = new System.Drawing.Size(557, 62);
            this.grpProject.TabIndex = 16;
            this.grpProject.TabStop = false;
            this.grpProject.Text = "Selected Project";
            this.grpProject.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Select Identical Id List File";
            // 
            // btnBrowseIdenticalIdListFile
            // 
            this.btnBrowseIdenticalIdListFile.Location = new System.Drawing.Point(327, 33);
            this.btnBrowseIdenticalIdListFile.Name = "btnBrowseIdenticalIdListFile";
            this.btnBrowseIdenticalIdListFile.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseIdenticalIdListFile.TabIndex = 13;
            this.btnBrowseIdenticalIdListFile.Text = "Browse";
            this.btnBrowseIdenticalIdListFile.UseVisualStyleBackColor = true;
            this.btnBrowseIdenticalIdListFile.Click += new System.EventHandler(this.btnBrowseIdenticalIdListFile_Click);
            // 
            // chkIncludeIdenticalId
            // 
            this.chkIncludeIdenticalId.AutoSize = true;
            this.chkIncludeIdenticalId.Checked = true;
            this.chkIncludeIdenticalId.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeIdenticalId.Location = new System.Drawing.Point(327, 13);
            this.chkIncludeIdenticalId.Name = "chkIncludeIdenticalId";
            this.chkIncludeIdenticalId.Size = new System.Drawing.Size(188, 17);
            this.chkIncludeIdenticalId.TabIndex = 12;
            this.chkIncludeIdenticalId.Text = "Include Identical Id Elimination List";
            this.chkIncludeIdenticalId.UseVisualStyleBackColor = true;
            // 
            // txtIdenticalIdListFile
            // 
            this.txtIdenticalIdListFile.BackColor = System.Drawing.Color.White;
            this.txtIdenticalIdListFile.Location = new System.Drawing.Point(142, 36);
            this.txtIdenticalIdListFile.Name = "txtIdenticalIdListFile";
            this.txtIdenticalIdListFile.ReadOnly = true;
            this.txtIdenticalIdListFile.Size = new System.Drawing.Size(177, 20);
            this.txtIdenticalIdListFile.TabIndex = 11;
            // 
            // txtProjectName
            // 
            this.txtProjectName.BackColor = System.Drawing.Color.White;
            this.txtProjectName.Location = new System.Drawing.Point(142, 13);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.ReadOnly = true;
            this.txtProjectName.Size = new System.Drawing.Size(177, 20);
            this.txtProjectName.TabIndex = 11;
            // 
            // chkDisplayMajorBrands
            // 
            this.chkDisplayMajorBrands.AutoSize = true;
            this.chkDisplayMajorBrands.Location = new System.Drawing.Point(18, 33);
            this.chkDisplayMajorBrands.Name = "chkDisplayMajorBrands";
            this.chkDisplayMajorBrands.Size = new System.Drawing.Size(149, 17);
            this.chkDisplayMajorBrands.TabIndex = 14;
            this.chkDisplayMajorBrands.Text = "Display Major Brands Only";
            this.chkDisplayMajorBrands.UseVisualStyleBackColor = true;
            this.chkDisplayMajorBrands.CheckedChanged += new System.EventHandler(this.chkDisplayMajorBrands_CheckedChanged);
            // 
            // chkDisplayBrandNumber
            // 
            this.chkDisplayBrandNumber.AutoSize = true;
            this.chkDisplayBrandNumber.Location = new System.Drawing.Point(176, 8);
            this.chkDisplayBrandNumber.Name = "chkDisplayBrandNumber";
            this.chkDisplayBrandNumber.Size = new System.Drawing.Size(152, 17);
            this.chkDisplayBrandNumber.TabIndex = 13;
            this.chkDisplayBrandNumber.Text = "Display UEI Brand Number";
            this.chkDisplayBrandNumber.UseVisualStyleBackColor = true;
            this.chkDisplayBrandNumber.CheckedChanged += new System.EventHandler(this.chkDisplayBrandNumber_CheckedChanged);
            // 
            // grpAliasIDFile
            // 
            this.grpAliasIDFile.Controls.Add(this.btnBrowse);
            this.grpAliasIDFile.Controls.Add(this.txtAliasIDFile);
            this.grpAliasIDFile.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpAliasIDFile.Location = new System.Drawing.Point(9, 243);
            this.grpAliasIDFile.Name = "grpAliasIDFile";
            this.grpAliasIDFile.Size = new System.Drawing.Size(557, 37);
            this.grpAliasIDFile.TabIndex = 12;
            this.grpAliasIDFile.TabStop = false;
            this.grpAliasIDFile.Text = "Alias ID File";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(442, 11);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 12;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtAliasIDFile
            // 
            this.txtAliasIDFile.BackColor = System.Drawing.Color.White;
            this.txtAliasIDFile.Location = new System.Drawing.Point(136, 13);
            this.txtAliasIDFile.Name = "txtAliasIDFile";
            this.txtAliasIDFile.ReadOnly = true;
            this.txtAliasIDFile.Size = new System.Drawing.Size(265, 20);
            this.txtAliasIDFile.TabIndex = 11;
            // 
            // txtIDOffset
            // 
            this.txtIDOffset.Location = new System.Drawing.Point(440, 5);
            this.txtIDOffset.Name = "txtIDOffset";
            this.txtIDOffset.Size = new System.Drawing.Size(64, 20);
            this.txtIDOffset.TabIndex = 11;
            this.txtIDOffset.Leave += new System.EventHandler(this.txtIDOffset_Leave);
            // 
            // lblIDOffset
            // 
            this.lblIDOffset.AutoSize = true;
            this.lblIDOffset.Location = new System.Drawing.Point(334, 9);
            this.lblIDOffset.Name = "lblIDOffset";
            this.lblIDOffset.Size = new System.Drawing.Size(79, 13);
            this.lblIDOffset.TabIndex = 10;
            this.lblIDOffset.Text = "ID Offset Value";
            // 
            // lblSetupCodeFormat
            // 
            this.lblSetupCodeFormat.AutoSize = true;
            this.lblSetupCodeFormat.Location = new System.Drawing.Point(148, 62);
            this.lblSetupCodeFormat.Name = "lblSetupCodeFormat";
            this.lblSetupCodeFormat.Size = new System.Drawing.Size(98, 13);
            this.lblSetupCodeFormat.TabIndex = 8;
            this.lblSetupCodeFormat.Text = "Setup Code Format";
            // 
            // gbSort
            // 
            this.gbSort.Controls.Add(this.cmbBxModelTypeISCI);
            this.gbSort.Controls.Add(this.txtPriorityIDListFile);
            this.gbSort.Controls.Add(this.btnEndDate);
            this.gbSort.Controls.Add(this.btnStartDate);
            this.gbSort.Controls.Add(this.txtEndDate);
            this.gbSort.Controls.Add(this.txtStartDate);
            this.gbSort.Controls.Add(this.lblEndDate);
            this.gbSort.Controls.Add(this.lblStartDate);
            this.gbSort.Controls.Add(this.rdoSortBySearchStatistics);
            this.gbSort.Controls.Add(this.rdoSortByPOSData);
            this.gbSort.Controls.Add(this.rdoSortByPriorityIDListFile);
            this.gbSort.Controls.Add(this.rdoSortByIDNumber);
            this.gbSort.Controls.Add(this.rdoSortByName);
            this.gbSort.Controls.Add(this.rdoSortByModelCount);
            this.gbSort.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbSort.Location = new System.Drawing.Point(9, 84);
            this.gbSort.Name = "gbSort";
            this.gbSort.Size = new System.Drawing.Size(557, 91);
            this.gbSort.TabIndex = 5;
            this.gbSort.TabStop = false;
            this.gbSort.Text = "Sort Setup Codes By";
            // 
            // cmbBxModelTypeISCI
            // 
            this.cmbBxModelTypeISCI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBxModelTypeISCI.FormattingEnabled = true;
            this.cmbBxModelTypeISCI.Items.AddRange(new object[] {
            "All",
            "Remote Model",
            "Target Model"});
            this.cmbBxModelTypeISCI.Location = new System.Drawing.Point(228, 15);
            this.cmbBxModelTypeISCI.Name = "cmbBxModelTypeISCI";
            this.cmbBxModelTypeISCI.Size = new System.Drawing.Size(121, 21);
            this.cmbBxModelTypeISCI.TabIndex = 21;
            this.cmbBxModelTypeISCI.SelectedIndexChanged += new System.EventHandler(this.cmbBxModelTypeISCI_SelectedIndexChanged);
            // 
            // txtPriorityIDListFile
            // 
            this.txtPriorityIDListFile.BackColor = System.Drawing.Color.White;
            this.txtPriorityIDListFile.Location = new System.Drawing.Point(227, 65);
            this.txtPriorityIDListFile.Name = "txtPriorityIDListFile";
            this.txtPriorityIDListFile.ReadOnly = true;
            this.txtPriorityIDListFile.Size = new System.Drawing.Size(265, 20);
            this.txtPriorityIDListFile.TabIndex = 9;
            // 
            // btnEndDate
            // 
            this.btnEndDate.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.btnEndDate.Location = new System.Drawing.Point(533, 42);
            this.btnEndDate.Name = "btnEndDate";
            this.btnEndDate.Size = new System.Drawing.Size(22, 21);
            this.btnEndDate.TabIndex = 20;
            this.btnEndDate.UseVisualStyleBackColor = true;
            this.btnEndDate.Click += new System.EventHandler(this.btnEndDate_Click);
            // 
            // btnStartDate
            // 
            this.btnStartDate.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.btnStartDate.Location = new System.Drawing.Point(371, 41);
            this.btnStartDate.Name = "btnStartDate";
            this.btnStartDate.Size = new System.Drawing.Size(22, 21);
            this.btnStartDate.TabIndex = 19;
            this.btnStartDate.UseVisualStyleBackColor = true;
            this.btnStartDate.Click += new System.EventHandler(this.btnStartDate_Click);
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.Color.White;
            this.txtEndDate.Location = new System.Drawing.Point(449, 41);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(84, 20);
            this.txtEndDate.TabIndex = 18;
            this.txtEndDate.Leave += new System.EventHandler(this.txtEndDate_Leave);
            // 
            // txtStartDate
            // 
            this.txtStartDate.BackColor = System.Drawing.Color.White;
            this.txtStartDate.Location = new System.Drawing.Point(285, 41);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(85, 20);
            this.txtStartDate.TabIndex = 17;
            this.txtStartDate.Leave += new System.EventHandler(this.txtStartDate_Leave);
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(395, 45);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(52, 13);
            this.lblEndDate.TabIndex = 16;
            this.lblEndDate.Text = "End Date";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(224, 45);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(55, 13);
            this.lblStartDate.TabIndex = 15;
            this.lblStartDate.Text = "Start Date";
            // 
            // rdoSortBySearchStatistics
            // 
            this.rdoSortBySearchStatistics.AutoSize = true;
            this.rdoSortBySearchStatistics.Location = new System.Drawing.Point(9, 43);
            this.rdoSortBySearchStatistics.Name = "rdoSortBySearchStatistics";
            this.rdoSortBySearchStatistics.Size = new System.Drawing.Size(104, 17);
            this.rdoSortBySearchStatistics.TabIndex = 12;
            this.rdoSortBySearchStatistics.Text = "Search Statistics";
            this.rdoSortBySearchStatistics.UseVisualStyleBackColor = true;
            this.rdoSortBySearchStatistics.CheckedChanged += new System.EventHandler(this.rdoSortBySearchStatistics_CheckedChanged);
            // 
            // rdoSortByPOSData
            // 
            this.rdoSortByPOSData.AutoSize = true;
            this.rdoSortByPOSData.Location = new System.Drawing.Point(136, 43);
            this.rdoSortByPOSData.Name = "rdoSortByPOSData";
            this.rdoSortByPOSData.Size = new System.Drawing.Size(73, 17);
            this.rdoSortByPOSData.TabIndex = 11;
            this.rdoSortByPOSData.Text = "POS Data";
            this.rdoSortByPOSData.UseVisualStyleBackColor = true;
            this.rdoSortByPOSData.CheckedChanged += new System.EventHandler(this.rdoSortByPOSData_CheckedChanged);
            // 
            // rdoSortByPriorityIDListFile
            // 
            this.rdoSortByPriorityIDListFile.Location = new System.Drawing.Point(9, 60);
            this.rdoSortByPriorityIDListFile.Name = "rdoSortByPriorityIDListFile";
            this.rdoSortByPriorityIDListFile.Size = new System.Drawing.Size(163, 27);
            this.rdoSortByPriorityIDListFile.TabIndex = 10;
            this.rdoSortByPriorityIDListFile.TabStop = true;
            this.rdoSortByPriorityIDListFile.Text = "Priority ID List File (Top 25)";
            this.rdoSortByPriorityIDListFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rdoSortByPriorityIDListFile.UseVisualStyleBackColor = true;
            this.rdoSortByPriorityIDListFile.CheckedChanged += new System.EventHandler(this.rdoPriorityIDListFile_CheckedChanged);
            // 
            // rdoSortByIDNumber
            // 
            this.rdoSortByIDNumber.AutoSize = true;
            this.rdoSortByIDNumber.Location = new System.Drawing.Point(9, 16);
            this.rdoSortByIDNumber.Name = "rdoSortByIDNumber";
            this.rdoSortByIDNumber.Size = new System.Drawing.Size(76, 17);
            this.rdoSortByIDNumber.TabIndex = 7;
            this.rdoSortByIDNumber.Text = "ID Number";
            this.rdoSortByIDNumber.UseVisualStyleBackColor = true;
            this.rdoSortByIDNumber.CheckedChanged += new System.EventHandler(this.rdoIDNumber_CheckedChanged);
            // 
            // rdoSortByName
            // 
            this.rdoSortByName.AutoSize = true;
            this.rdoSortByName.Location = new System.Drawing.Point(371, 16);
            this.rdoSortByName.Name = "rdoSortByName";
            this.rdoSortByName.Size = new System.Drawing.Size(150, 17);
            this.rdoSortByName.TabIndex = 6;
            this.rdoSortByName.Text = "Alphabetical Brand Names";
            this.rdoSortByName.UseVisualStyleBackColor = true;
            this.rdoSortByName.CheckedChanged += new System.EventHandler(this.rdoSortByName_CheckedChanged);
            // 
            // rdoSortByModelCount
            // 
            this.rdoSortByModelCount.AutoSize = true;
            this.rdoSortByModelCount.Checked = true;
            this.rdoSortByModelCount.Location = new System.Drawing.Point(136, 16);
            this.rdoSortByModelCount.Name = "rdoSortByModelCount";
            this.rdoSortByModelCount.Size = new System.Drawing.Size(85, 17);
            this.rdoSortByModelCount.TabIndex = 1;
            this.rdoSortByModelCount.TabStop = true;
            this.rdoSortByModelCount.Text = "Model Count";
            this.rdoSortByModelCount.UseVisualStyleBackColor = true;
            this.rdoSortByModelCount.CheckedChanged += new System.EventHandler(this.rdoSortByModelCount_CheckedChanged);
            // 
            // cmbSetupCodeFormating
            // 
            this.cmbSetupCodeFormating.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSetupCodeFormating.FormattingEnabled = true;
            this.cmbSetupCodeFormating.Items.AddRange(new object[] {
            "Base 10",
            "Base 4 - 4 Digit",
            "Base 4 - 5 Digit",
            "Base 5 - 5 Digit",
            "Base 6 - 5 Digit",
            "Base 6 - 6 Digit"});
            this.cmbSetupCodeFormating.Location = new System.Drawing.Point(252, 61);
            this.cmbSetupCodeFormating.Name = "cmbSetupCodeFormating";
            this.cmbSetupCodeFormating.Size = new System.Drawing.Size(90, 21);
            this.cmbSetupCodeFormating.TabIndex = 4;
            this.cmbSetupCodeFormating.SelectedIndexChanged += new System.EventHandler(this.cmbSetupCodeFormating_SelectedIndexChanged);
            // 
            // chkIncludeRestrictedIDs
            // 
            this.chkIncludeRestrictedIDs.AutoSize = true;
            this.chkIncludeRestrictedIDs.Location = new System.Drawing.Point(18, 8);
            this.chkIncludeRestrictedIDs.Name = "chkIncludeRestrictedIDs";
            this.chkIncludeRestrictedIDs.Size = new System.Drawing.Size(131, 17);
            this.chkIncludeRestrictedIDs.TabIndex = 0;
            this.chkIncludeRestrictedIDs.Text = "Include Restricted IDs";
            this.chkIncludeRestrictedIDs.UseVisualStyleBackColor = true;
            this.chkIncludeRestrictedIDs.CheckedChanged += new System.EventHandler(this.chkIncludeRestrictedIDs_CheckedChanged);
            // 
            // tabBrandBasedSearch
            // 
            this.tabBrandBasedSearch.Controls.Add(this.groupBox13);
            this.tabBrandBasedSearch.Controls.Add(this.groupBox9);
            this.tabBrandBasedSearch.Controls.Add(this.groupBox10);
            this.tabBrandBasedSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabBrandBasedSearch.ImageIndex = -1;
            this.tabBrandBasedSearch.Location = new System.Drawing.Point(4, 30);
            this.tabBrandBasedSearch.Name = "tabBrandBasedSearch";
            this.tabBrandBasedSearch.Size = new System.Drawing.Size(573, 307);
            this.tabBrandBasedSearch.TabIndex = 7;
            this.tabBrandBasedSearch.Text = "Brand Based Search";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.chkIncludeAliasBrandNames);
            this.groupBox13.Controls.Add(this.chkBxBlankModelNameBBS);
            this.groupBox13.Controls.Add(this.cmbBxModelTypeBBS);
            this.groupBox13.Controls.Add(this.chkMode);
            this.groupBox13.Controls.Add(this.chkModel);
            this.groupBox13.Controls.Add(this.chkDataSource);
            this.groupBox13.Location = new System.Drawing.Point(10, 206);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(557, 96);
            this.groupBox13.TabIndex = 15;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Options";
            // 
            // chkIncludeAliasBrandNames
            // 
            this.chkIncludeAliasBrandNames.AutoSize = true;
            this.chkIncludeAliasBrandNames.Enabled = false;
            this.chkIncludeAliasBrandNames.Location = new System.Drawing.Point(167, 42);
            this.chkIncludeAliasBrandNames.Name = "chkIncludeAliasBrandNames";
            this.chkIncludeAliasBrandNames.Size = new System.Drawing.Size(117, 17);
            this.chkIncludeAliasBrandNames.TabIndex = 33;
            this.chkIncludeAliasBrandNames.Text = "Include Alias Brand";
            this.chkIncludeAliasBrandNames.UseVisualStyleBackColor = true;
            // 
            // chkBxBlankModelNameBBS
            // 
            this.chkBxBlankModelNameBBS.AutoSize = true;
            this.chkBxBlankModelNameBBS.Enabled = false;
            this.chkBxBlankModelNameBBS.Location = new System.Drawing.Point(353, 19);
            this.chkBxBlankModelNameBBS.Name = "chkBxBlankModelNameBBS";
            this.chkBxBlankModelNameBBS.Size = new System.Drawing.Size(123, 17);
            this.chkBxBlankModelNameBBS.TabIndex = 32;
            this.chkBxBlankModelNameBBS.Text = "Empty Model Names";
            this.chkBxBlankModelNameBBS.UseVisualStyleBackColor = true;
            // 
            // cmbBxModelTypeBBS
            // 
            this.cmbBxModelTypeBBS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBxModelTypeBBS.Enabled = false;
            this.cmbBxModelTypeBBS.FormattingEnabled = true;
            this.cmbBxModelTypeBBS.Items.AddRange(new object[] {
            "All",
            "Remote Model",
            "Target Model"});
            this.cmbBxModelTypeBBS.Location = new System.Drawing.Point(225, 15);
            this.cmbBxModelTypeBBS.Name = "cmbBxModelTypeBBS";
            this.cmbBxModelTypeBBS.Size = new System.Drawing.Size(121, 21);
            this.cmbBxModelTypeBBS.TabIndex = 31;
            this.cmbBxModelTypeBBS.SelectedIndexChanged += new System.EventHandler(this.cmbBxModelTypeBBS_SelectedIndexChanged);
            // 
            // chkMode
            // 
            this.chkMode.AutoSize = true;
            this.chkMode.Location = new System.Drawing.Point(18, 19);
            this.chkMode.Name = "chkMode";
            this.chkMode.Size = new System.Drawing.Size(53, 17);
            this.chkMode.TabIndex = 30;
            this.chkMode.Text = "Mode";
            this.chkMode.UseVisualStyleBackColor = true;
            // 
            // chkModel
            // 
            this.chkModel.AutoSize = true;
            this.chkModel.Location = new System.Drawing.Point(167, 19);
            this.chkModel.Name = "chkModel";
            this.chkModel.Size = new System.Drawing.Size(55, 17);
            this.chkModel.TabIndex = 29;
            this.chkModel.Text = "Model";
            this.chkModel.UseVisualStyleBackColor = true;
            this.chkModel.CheckedChanged += new System.EventHandler(this.chkModel_CheckedChanged);
            // 
            // chkDataSource
            // 
            this.chkDataSource.AutoSize = true;
            this.chkDataSource.Location = new System.Drawing.Point(18, 42);
            this.chkDataSource.Name = "chkDataSource";
            this.chkDataSource.Size = new System.Drawing.Size(86, 17);
            this.chkDataSource.TabIndex = 28;
            this.chkDataSource.Text = "Data Source";
            this.chkDataSource.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label2);
            this.groupBox9.Controls.Add(this.label1);
            this.groupBox9.Controls.Add(this.btnBrowseBrandListFile);
            this.groupBox9.Controls.Add(this.txtBrandSeachListFile);
            this.groupBox9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox9.Location = new System.Drawing.Point(10, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(557, 74);
            this.groupBox9.TabIndex = 14;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Select Brand List File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(263, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "2. In Excel file provide Brand Name at first column only";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(164, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Note: 1. Upload Microsoft Excel (*.xls) Or Text File (*.txt)";
            // 
            // btnBrowseBrandListFile
            // 
            this.btnBrowseBrandListFile.Location = new System.Drawing.Point(442, 11);
            this.btnBrowseBrandListFile.Name = "btnBrowseBrandListFile";
            this.btnBrowseBrandListFile.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseBrandListFile.TabIndex = 12;
            this.btnBrowseBrandListFile.Text = "Browse";
            this.btnBrowseBrandListFile.UseVisualStyleBackColor = true;
            this.btnBrowseBrandListFile.Click += new System.EventHandler(this.btnBrowseBrandListFile_Click);
            // 
            // txtBrandSeachListFile
            // 
            this.txtBrandSeachListFile.BackColor = System.Drawing.Color.White;
            this.txtBrandSeachListFile.Location = new System.Drawing.Point(167, 13);
            this.txtBrandSeachListFile.Name = "txtBrandSeachListFile";
            this.txtBrandSeachListFile.ReadOnly = true;
            this.txtBrandSeachListFile.Size = new System.Drawing.Size(265, 20);
            this.txtBrandSeachListFile.TabIndex = 11;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.chkUEI2Avail);
            this.groupBox10.Controls.Add(this.groupBox12);
            this.groupBox10.Controls.Add(this.groupBox11);
            this.groupBox10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox10.Location = new System.Drawing.Point(10, 83);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(557, 117);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Check Brand Availability";
            // 
            // chkUEI2Avail
            // 
            this.chkUEI2Avail.AutoCheck = false;
            this.chkUEI2Avail.AutoSize = true;
            this.chkUEI2Avail.Checked = true;
            this.chkUEI2Avail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUEI2Avail.Location = new System.Drawing.Point(18, 21);
            this.chkUEI2Avail.Name = "chkUEI2Avail";
            this.chkUEI2Avail.Size = new System.Drawing.Size(50, 17);
            this.chkUEI2Avail.TabIndex = 23;
            this.chkUEI2Avail.Text = "UEI2";
            this.chkUEI2Avail.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.chkPOSAvail);
            this.groupBox12.Controls.Add(this.btnPOSEndDate);
            this.groupBox12.Controls.Add(this.btnPOSStartDate);
            this.groupBox12.Controls.Add(this.txtPOSEndDate);
            this.groupBox12.Controls.Add(this.txtPOSStartDate);
            this.groupBox12.Controls.Add(this.label6);
            this.groupBox12.Controls.Add(this.label7);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox12.Location = new System.Drawing.Point(3, 37);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(551, 38);
            this.groupBox12.TabIndex = 22;
            this.groupBox12.TabStop = false;
            // 
            // chkPOSAvail
            // 
            this.chkPOSAvail.AutoSize = true;
            this.chkPOSAvail.Location = new System.Drawing.Point(15, 13);
            this.chkPOSAvail.Name = "chkPOSAvail";
            this.chkPOSAvail.Size = new System.Drawing.Size(48, 17);
            this.chkPOSAvail.TabIndex = 27;
            this.chkPOSAvail.Text = "POS";
            this.chkPOSAvail.UseVisualStyleBackColor = true;
            this.chkPOSAvail.CheckedChanged += new System.EventHandler(this.chkPOSAvail_CheckedChanged);
            // 
            // btnPOSEndDate
            // 
            this.btnPOSEndDate.Enabled = false;
            this.btnPOSEndDate.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.btnPOSEndDate.Location = new System.Drawing.Point(485, 9);
            this.btnPOSEndDate.Name = "btnPOSEndDate";
            this.btnPOSEndDate.Size = new System.Drawing.Size(22, 21);
            this.btnPOSEndDate.TabIndex = 26;
            this.btnPOSEndDate.UseVisualStyleBackColor = true;
            this.btnPOSEndDate.Click += new System.EventHandler(this.btnPOSEndDate_Click);
            // 
            // btnPOSStartDate
            // 
            this.btnPOSStartDate.Enabled = false;
            this.btnPOSStartDate.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.btnPOSStartDate.Location = new System.Drawing.Point(313, 9);
            this.btnPOSStartDate.Name = "btnPOSStartDate";
            this.btnPOSStartDate.Size = new System.Drawing.Size(22, 21);
            this.btnPOSStartDate.TabIndex = 25;
            this.btnPOSStartDate.UseVisualStyleBackColor = true;
            this.btnPOSStartDate.Click += new System.EventHandler(this.btnPOSStartDate_Click);
            // 
            // txtPOSEndDate
            // 
            this.txtPOSEndDate.BackColor = System.Drawing.Color.White;
            this.txtPOSEndDate.Enabled = false;
            this.txtPOSEndDate.Location = new System.Drawing.Point(401, 10);
            this.txtPOSEndDate.Name = "txtPOSEndDate";
            this.txtPOSEndDate.Size = new System.Drawing.Size(84, 20);
            this.txtPOSEndDate.TabIndex = 24;
            this.txtPOSEndDate.Leave += new System.EventHandler(this.txtPOSEndDate_Leave);
            // 
            // txtPOSStartDate
            // 
            this.txtPOSStartDate.BackColor = System.Drawing.Color.White;
            this.txtPOSStartDate.Enabled = false;
            this.txtPOSStartDate.Location = new System.Drawing.Point(222, 10);
            this.txtPOSStartDate.Name = "txtPOSStartDate";
            this.txtPOSStartDate.Size = new System.Drawing.Size(85, 20);
            this.txtPOSStartDate.TabIndex = 23;
            this.txtPOSStartDate.Leave += new System.EventHandler(this.txtPOSStartDate_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(347, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "End Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(161, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Start Date";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.chkOSSAvail);
            this.groupBox11.Controls.Add(this.btnOSSEndDate);
            this.groupBox11.Controls.Add(this.btnOSSStartDate);
            this.groupBox11.Controls.Add(this.txtOSSEndDate);
            this.groupBox11.Controls.Add(this.txtOSSStartDate);
            this.groupBox11.Controls.Add(this.label3);
            this.groupBox11.Controls.Add(this.label4);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox11.Location = new System.Drawing.Point(3, 75);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(551, 39);
            this.groupBox11.TabIndex = 21;
            this.groupBox11.TabStop = false;
            // 
            // chkOSSAvail
            // 
            this.chkOSSAvail.AutoSize = true;
            this.chkOSSAvail.Location = new System.Drawing.Point(15, 16);
            this.chkOSSAvail.Name = "chkOSSAvail";
            this.chkOSSAvail.Size = new System.Drawing.Size(138, 17);
            this.chkOSSAvail.TabIndex = 27;
            this.chkOSSAvail.Text = "Online Search Statistics";
            this.chkOSSAvail.UseVisualStyleBackColor = true;
            this.chkOSSAvail.CheckedChanged += new System.EventHandler(this.chkOSSAvail_CheckedChanged);
            // 
            // btnOSSEndDate
            // 
            this.btnOSSEndDate.Enabled = false;
            this.btnOSSEndDate.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.btnOSSEndDate.Location = new System.Drawing.Point(485, 12);
            this.btnOSSEndDate.Name = "btnOSSEndDate";
            this.btnOSSEndDate.Size = new System.Drawing.Size(22, 21);
            this.btnOSSEndDate.TabIndex = 26;
            this.btnOSSEndDate.UseVisualStyleBackColor = true;
            this.btnOSSEndDate.Click += new System.EventHandler(this.btnOSSEndDate_Click);
            // 
            // btnOSSStartDate
            // 
            this.btnOSSStartDate.Enabled = false;
            this.btnOSSStartDate.Image = global::UEI.Workflow2010.Report.Properties.Resources.calender;
            this.btnOSSStartDate.Location = new System.Drawing.Point(313, 12);
            this.btnOSSStartDate.Name = "btnOSSStartDate";
            this.btnOSSStartDate.Size = new System.Drawing.Size(22, 21);
            this.btnOSSStartDate.TabIndex = 25;
            this.btnOSSStartDate.UseVisualStyleBackColor = true;
            this.btnOSSStartDate.Click += new System.EventHandler(this.btnOSSStartDate_Click);
            // 
            // txtOSSEndDate
            // 
            this.txtOSSEndDate.BackColor = System.Drawing.Color.White;
            this.txtOSSEndDate.Enabled = false;
            this.txtOSSEndDate.Location = new System.Drawing.Point(401, 13);
            this.txtOSSEndDate.Name = "txtOSSEndDate";
            this.txtOSSEndDate.Size = new System.Drawing.Size(84, 20);
            this.txtOSSEndDate.TabIndex = 24;
            this.txtOSSEndDate.Leave += new System.EventHandler(this.txtOSSEndDate_Leave);
            // 
            // txtOSSStartDate
            // 
            this.txtOSSStartDate.BackColor = System.Drawing.Color.White;
            this.txtOSSStartDate.Enabled = false;
            this.txtOSSStartDate.Location = new System.Drawing.Point(222, 13);
            this.txtOSSStartDate.Name = "txtOSSStartDate";
            this.txtOSSStartDate.Size = new System.Drawing.Size(85, 20);
            this.txtOSSStartDate.TabIndex = 23;
            this.txtOSSStartDate.Leave += new System.EventHandler(this.txtOSSStartDate_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(347, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "End Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Start Date";
            // 
            // tabLabelIntronSearch
            // 
            this.tabLabelIntronSearch.Controls.Add(this.grpBxEnterPatterns);
            this.tabLabelIntronSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLabelIntronSearch.ImageIndex = -1;
            this.tabLabelIntronSearch.Location = new System.Drawing.Point(4, 30);
            this.tabLabelIntronSearch.Name = "tabLabelIntronSearch";
            this.tabLabelIntronSearch.Size = new System.Drawing.Size(1155, 305);
            this.tabLabelIntronSearch.TabIndex = 8;
            this.tabLabelIntronSearch.Text = "Label/Intron Search";
            // 
            // grpBxEnterPatterns
            // 
            this.grpBxEnterPatterns.Controls.Add(this.btnShowDictionary);
            this.grpBxEnterPatterns.Controls.Add(this.chkBxGetCompleteMatch);
            this.grpBxEnterPatterns.Controls.Add(this.label8);
            this.grpBxEnterPatterns.Controls.Add(this.bttnLoadLabelIntronFile);
            this.grpBxEnterPatterns.Controls.Add(this.bttnRemoveLabelIntron);
            this.grpBxEnterPatterns.Controls.Add(this.bttnAddLabelIntron);
            this.grpBxEnterPatterns.Controls.Add(this.txtBxEnterLabelIntron);
            this.grpBxEnterPatterns.Controls.Add(this.lstBxSearchPatterns);
            this.grpBxEnterPatterns.Location = new System.Drawing.Point(7, 6);
            this.grpBxEnterPatterns.Name = "grpBxEnterPatterns";
            this.grpBxEnterPatterns.Size = new System.Drawing.Size(332, 296);
            this.grpBxEnterPatterns.TabIndex = 5;
            this.grpBxEnterPatterns.TabStop = false;
            this.grpBxEnterPatterns.Text = "Label/Intron Search";
            // 
            // btnShowDictionary
            // 
            this.btnShowDictionary.CausesValidation = false;
            this.btnShowDictionary.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnShowDictionary.Image = ((System.Drawing.Image)(resources.GetObject("btnShowDictionary.Image")));
            this.btnShowDictionary.Location = new System.Drawing.Point(117, 23);
            this.btnShowDictionary.Name = "btnShowDictionary";
            this.btnShowDictionary.Size = new System.Drawing.Size(29, 20);
            this.btnShowDictionary.TabIndex = 9;
            this.btnShowDictionary.UseVisualStyleBackColor = true;
            this.btnShowDictionary.Click += new System.EventHandler(this.btnShowDictionary_Click);
            // 
            // chkBxGetCompleteMatch
            // 
            this.chkBxGetCompleteMatch.AutoSize = true;
            this.chkBxGetCompleteMatch.Location = new System.Drawing.Point(18, 228);
            this.chkBxGetCompleteMatch.Name = "chkBxGetCompleteMatch";
            this.chkBxGetCompleteMatch.Size = new System.Drawing.Size(162, 17);
            this.chkBxGetCompleteMatch.TabIndex = 8;
            this.chkBxGetCompleteMatch.Text = "Return Complete Match Only";
            this.chkBxGetCompleteMatch.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(15, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 49);
            this.label8.TabIndex = 6;
            this.label8.Text = "( For Partial Search, Use Wild Card *str*)";
            // 
            // bttnLoadLabelIntronFile
            // 
            this.bttnLoadLabelIntronFile.Image = global::UEI.Workflow2010.Report.Properties.Resources.fileload;
            this.bttnLoadLabelIntronFile.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnLoadLabelIntronFile.Location = new System.Drawing.Point(17, 182);
            this.bttnLoadLabelIntronFile.Name = "bttnLoadLabelIntronFile";
            this.bttnLoadLabelIntronFile.Size = new System.Drawing.Size(94, 23);
            this.bttnLoadLabelIntronFile.TabIndex = 6;
            this.bttnLoadLabelIntronFile.Text = "Browse";
            this.bttnLoadLabelIntronFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnLoadLabelIntronFile.UseVisualStyleBackColor = true;
            this.bttnLoadLabelIntronFile.Click += new System.EventHandler(this.bttnLoadLabelIntronFile_Click);
            // 
            // bttnRemoveLabelIntron
            // 
            this.bttnRemoveLabelIntron.Image = global::UEI.Workflow2010.Report.Properties.Resources.GoRtlHS;
            this.bttnRemoveLabelIntron.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnRemoveLabelIntron.Location = new System.Drawing.Point(17, 142);
            this.bttnRemoveLabelIntron.Name = "bttnRemoveLabelIntron";
            this.bttnRemoveLabelIntron.Size = new System.Drawing.Size(94, 23);
            this.bttnRemoveLabelIntron.TabIndex = 5;
            this.bttnRemoveLabelIntron.Text = "Remove";
            this.bttnRemoveLabelIntron.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnRemoveLabelIntron.UseVisualStyleBackColor = true;
            this.bttnRemoveLabelIntron.Click += new System.EventHandler(this.bttnRemoveLabelIntron_Click);
            // 
            // bttnAddLabelIntron
            // 
            this.bttnAddLabelIntron.Image = global::UEI.Workflow2010.Report.Properties.Resources.GoLtrHS;
            this.bttnAddLabelIntron.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnAddLabelIntron.Location = new System.Drawing.Point(17, 104);
            this.bttnAddLabelIntron.Name = "bttnAddLabelIntron";
            this.bttnAddLabelIntron.Size = new System.Drawing.Size(94, 23);
            this.bttnAddLabelIntron.TabIndex = 4;
            this.bttnAddLabelIntron.Text = "Add";
            this.bttnAddLabelIntron.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnAddLabelIntron.UseVisualStyleBackColor = true;
            this.bttnAddLabelIntron.Click += new System.EventHandler(this.bttnAddLabelIntron_Click);
            // 
            // txtBxEnterLabelIntron
            // 
            this.txtBxEnterLabelIntron.AcceptsReturn = true;
            this.txtBxEnterLabelIntron.Location = new System.Drawing.Point(17, 23);
            this.txtBxEnterLabelIntron.Name = "txtBxEnterLabelIntron";
            this.txtBxEnterLabelIntron.Size = new System.Drawing.Size(94, 20);
            this.txtBxEnterLabelIntron.TabIndex = 3;
            this.txtBxEnterLabelIntron.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBxEnterLabelIntron_KeyUp);
            this.txtBxEnterLabelIntron.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txtBxEnterLabelIntron_PreviewKeyDown);
            // 
            // lstBxSearchPatterns
            // 
            this.lstBxSearchPatterns.FormattingEnabled = true;
            this.lstBxSearchPatterns.Location = new System.Drawing.Point(152, 23);
            this.lstBxSearchPatterns.Name = "lstBxSearchPatterns";
            this.lstBxSearchPatterns.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstBxSearchPatterns.Size = new System.Drawing.Size(162, 186);
            this.lstBxSearchPatterns.TabIndex = 7;
            // 
            // tabQuickSetInfo
            // 
            this.tabQuickSetInfo.Controls.Add(this.groupBox21);
            this.tabQuickSetInfo.Controls.Add(this.groupBox20);
            this.tabQuickSetInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabQuickSetInfo.ImageIndex = -1;
            this.tabQuickSetInfo.Location = new System.Drawing.Point(4, 30);
            this.tabQuickSetInfo.Name = "tabQuickSetInfo";
            this.tabQuickSetInfo.Size = new System.Drawing.Size(657, 307);
            this.tabQuickSetInfo.TabIndex = 9;
            this.tabQuickSetInfo.Text = "QuickSet Info";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.btnDown);
            this.groupBox21.Controls.Add(this.btnUp);
            this.groupBox21.Controls.Add(this.groupBox23);
            this.groupBox21.Controls.Add(this.groupBox22);
            this.groupBox21.Controls.Add(this.btnAdd);
            this.groupBox21.Controls.Add(this.txtModeList);
            this.groupBox21.Controls.Add(this.txtModeName);
            this.groupBox21.Controls.Add(this.label11);
            this.groupBox21.Controls.Add(this.label10);
            this.groupBox21.Location = new System.Drawing.Point(0, 75);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(576, 227);
            this.groupBox21.TabIndex = 7;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Mode Setup";
            // 
            // btnDown
            // 
            this.btnDown.Image = global::UEI.Workflow2010.Report.Properties.Resources.Down;
            this.btnDown.Location = new System.Drawing.Point(337, 149);
            this.btnDown.Margin = new System.Windows.Forms.Padding(0);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(25, 25);
            this.btnDown.TabIndex = 18;
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUp
            // 
            this.btnUp.Image = global::UEI.Workflow2010.Report.Properties.Resources.Up;
            this.btnUp.Location = new System.Drawing.Point(337, 113);
            this.btnUp.Margin = new System.Windows.Forms.Padding(0);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(25, 25);
            this.btnUp.TabIndex = 17;
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.dataGridModeList);
            this.groupBox23.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox23.Location = new System.Drawing.Point(393, 16);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(180, 208);
            this.groupBox23.TabIndex = 16;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Mode List";
            // 
            // dataGridModeList
            // 
            this.dataGridModeList.AllowUserToAddRows = false;
            this.dataGridModeList.AllowUserToDeleteRows = false;
            this.dataGridModeList.AllowUserToResizeColumns = false;
            this.dataGridModeList.AllowUserToResizeRows = false;
            this.dataGridModeList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dataGridModeList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridModeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridModeList.ColumnHeadersVisible = false;
            this.dataGridModeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridModeList.Location = new System.Drawing.Point(3, 16);
            this.dataGridModeList.Name = "dataGridModeList";
            this.dataGridModeList.ReadOnly = true;
            this.dataGridModeList.RowHeadersVisible = false;
            this.dataGridModeList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridModeList.Size = new System.Drawing.Size(174, 189);
            this.dataGridModeList.TabIndex = 0;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.dataGridQuickSet);
            this.groupBox22.Location = new System.Drawing.Point(11, 55);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(320, 172);
            this.groupBox22.TabIndex = 15;
            this.groupBox22.TabStop = false;
            // 
            // dataGridQuickSet
            // 
            this.dataGridQuickSet.AllowUserToAddRows = false;
            this.dataGridQuickSet.AllowUserToResizeColumns = false;
            this.dataGridQuickSet.AllowUserToResizeRows = false;
            this.dataGridQuickSet.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridQuickSet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridQuickSet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridQuickSet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeviceCode,
            this.ModeName,
            this.ModeList});
            this.dataGridQuickSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridQuickSet.Location = new System.Drawing.Point(3, 16);
            this.dataGridQuickSet.Name = "dataGridQuickSet";
            this.dataGridQuickSet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridQuickSet.Size = new System.Drawing.Size(314, 153);
            this.dataGridQuickSet.TabIndex = 0;
            this.dataGridQuickSet.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridQuickSet_CellEndEdit);
            this.dataGridQuickSet.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridQuickSet_CellMouseClick);
            this.dataGridQuickSet.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridQuickSet_RowsRemoved);
            this.dataGridQuickSet.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridQuickSet_UserDeletingRow);
            // 
            // DeviceCode
            // 
            this.DeviceCode.HeaderText = "";
            this.DeviceCode.Name = "DeviceCode";
            this.DeviceCode.Width = 19;
            // 
            // ModeName
            // 
            this.ModeName.HeaderText = "Mode Name";
            this.ModeName.Name = "ModeName";
            this.ModeName.Width = 90;
            // 
            // ModeList
            // 
            this.ModeList.HeaderText = "Device List";
            this.ModeList.Name = "ModeList";
            this.ModeList.Width = 85;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(337, 30);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(50, 23);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtModeList
            // 
            this.txtModeList.Location = new System.Drawing.Point(207, 32);
            this.txtModeList.Name = "txtModeList";
            this.txtModeList.Size = new System.Drawing.Size(124, 20);
            this.txtModeList.TabIndex = 13;
            this.txtModeList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtModeList_KeyPress);
            // 
            // txtModeName
            // 
            this.txtModeName.Location = new System.Drawing.Point(14, 32);
            this.txtModeName.Name = "txtModeName";
            this.txtModeName.Size = new System.Drawing.Size(178, 20);
            this.txtModeName.TabIndex = 12;
            this.txtModeName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtModeName_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(204, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Device List";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Mode Name";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.txtQSPriorityIdListFile);
            this.groupBox20.Controls.Add(this.rdoQSSortByPriorityIDList);
            this.groupBox20.Controls.Add(this.rdoQSSortByIDNo);
            this.groupBox20.Controls.Add(this.rdoQSSortByModelCount);
            this.groupBox20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox20.Location = new System.Drawing.Point(0, 0);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(576, 69);
            this.groupBox20.TabIndex = 6;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Sort Setup Codes By";
            // 
            // txtQSPriorityIdListFile
            // 
            this.txtQSPriorityIdListFile.BackColor = System.Drawing.Color.White;
            this.txtQSPriorityIdListFile.Location = new System.Drawing.Point(136, 38);
            this.txtQSPriorityIdListFile.Name = "txtQSPriorityIdListFile";
            this.txtQSPriorityIdListFile.ReadOnly = true;
            this.txtQSPriorityIdListFile.Size = new System.Drawing.Size(265, 20);
            this.txtQSPriorityIdListFile.TabIndex = 9;
            // 
            // rdoQSSortByPriorityIDList
            // 
            this.rdoQSSortByPriorityIDList.AutoSize = true;
            this.rdoQSSortByPriorityIDList.Location = new System.Drawing.Point(9, 39);
            this.rdoQSSortByPriorityIDList.Name = "rdoQSSortByPriorityIDList";
            this.rdoQSSortByPriorityIDList.Size = new System.Drawing.Size(108, 17);
            this.rdoQSSortByPriorityIDList.TabIndex = 10;
            this.rdoQSSortByPriorityIDList.TabStop = true;
            this.rdoQSSortByPriorityIDList.Text = "Priority ID List File";
            this.rdoQSSortByPriorityIDList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rdoQSSortByPriorityIDList.UseVisualStyleBackColor = true;
            this.rdoQSSortByPriorityIDList.CheckedChanged += new System.EventHandler(this.rdoQSSortByPriorityIDList_CheckedChanged);
            // 
            // rdoQSSortByIDNo
            // 
            this.rdoQSSortByIDNo.AutoSize = true;
            this.rdoQSSortByIDNo.Location = new System.Drawing.Point(9, 16);
            this.rdoQSSortByIDNo.Name = "rdoQSSortByIDNo";
            this.rdoQSSortByIDNo.Size = new System.Drawing.Size(76, 17);
            this.rdoQSSortByIDNo.TabIndex = 7;
            this.rdoQSSortByIDNo.Text = "ID Number";
            this.rdoQSSortByIDNo.UseVisualStyleBackColor = true;
            this.rdoQSSortByIDNo.Visible = false;
            this.rdoQSSortByIDNo.CheckedChanged += new System.EventHandler(this.rdoQSSortByIDNo_CheckedChanged);
            // 
            // rdoQSSortByModelCount
            // 
            this.rdoQSSortByModelCount.AutoSize = true;
            this.rdoQSSortByModelCount.Checked = true;
            this.rdoQSSortByModelCount.Location = new System.Drawing.Point(136, 16);
            this.rdoQSSortByModelCount.Name = "rdoQSSortByModelCount";
            this.rdoQSSortByModelCount.Size = new System.Drawing.Size(85, 17);
            this.rdoQSSortByModelCount.TabIndex = 1;
            this.rdoQSSortByModelCount.TabStop = true;
            this.rdoQSSortByModelCount.Text = "Model Count";
            this.rdoQSSortByModelCount.UseVisualStyleBackColor = true;
            this.rdoQSSortByModelCount.CheckedChanged += new System.EventHandler(this.rdoQSSortByModelCount_CheckedChanged);
            // 
            // tabKeyFunction
            // 
            this.tabKeyFunction.Controls.Add(this.groupBox24);
            this.tabKeyFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabKeyFunction.ImageIndex = -1;
            this.tabKeyFunction.Location = new System.Drawing.Point(4, 30);
            this.tabKeyFunction.Name = "tabKeyFunction";
            this.tabKeyFunction.Size = new System.Drawing.Size(1155, 305);
            this.tabKeyFunction.TabIndex = 10;
            this.tabKeyFunction.Text = "Key Function Info";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.radioButton3);
            this.groupBox24.Controls.Add(this.radioButton2);
            this.groupBox24.Controls.Add(this.radioButton1);
            this.groupBox24.Location = new System.Drawing.Point(8, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(316, 97);
            this.groupBox24.TabIndex = 2;
            this.groupBox24.TabStop = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(23, 19);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(168, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Unique Functions within Mode";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(23, 65);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(281, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Unique Functions within Main Device and Sub Device";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(23, 42);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(232, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = "Unique Functions within Mode across all Ids";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // tabUnsupportedId
            // 
            this.tabUnsupportedId.Controls.Add(this.groupBox25);
            this.tabUnsupportedId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabUnsupportedId.ImageIndex = -1;
            this.tabUnsupportedId.Location = new System.Drawing.Point(4, 30);
            this.tabUnsupportedId.Name = "tabUnsupportedId";
            this.tabUnsupportedId.Size = new System.Drawing.Size(652, 307);
            this.tabUnsupportedId.TabIndex = 11;
            this.tabUnsupportedId.Text = "Supported Platform";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.grpPlatforms);
            this.groupBox25.Controls.Add(this.chkIncludeUnsupportedIds);
            this.groupBox25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox25.Location = new System.Drawing.Point(0, 0);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(652, 307);
            this.groupBox25.TabIndex = 0;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Unsupported Ids By Platform";
            // 
            // grpPlatforms
            // 
            this.grpPlatforms.Controls.Add(this.treeViewPlatforms);
            this.grpPlatforms.Location = new System.Drawing.Point(8, 43);
            this.grpPlatforms.Name = "grpPlatforms";
            this.grpPlatforms.Size = new System.Drawing.Size(265, 256);
            this.grpPlatforms.TabIndex = 1;
            this.grpPlatforms.TabStop = false;
            this.grpPlatforms.Text = "Select Platforms which you want to Include";
            // 
            // treeViewPlatforms
            // 
            this.treeViewPlatforms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewPlatforms.CheckBoxes = true;
            this.treeViewPlatforms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewPlatforms.Location = new System.Drawing.Point(3, 16);
            this.treeViewPlatforms.Name = "treeViewPlatforms";
            this.treeViewPlatforms.Size = new System.Drawing.Size(259, 237);
            this.treeViewPlatforms.TabIndex = 0;
            this.treeViewPlatforms.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeViewPlatforms_AfterCheck);
            // 
            // chkIncludeUnsupportedIds
            // 
            this.chkIncludeUnsupportedIds.AutoSize = true;
            this.chkIncludeUnsupportedIds.Checked = true;
            this.chkIncludeUnsupportedIds.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeUnsupportedIds.Location = new System.Drawing.Point(8, 19);
            this.chkIncludeUnsupportedIds.Name = "chkIncludeUnsupportedIds";
            this.chkIncludeUnsupportedIds.Size = new System.Drawing.Size(199, 17);
            this.chkIncludeUnsupportedIds.TabIndex = 0;
            this.chkIncludeUnsupportedIds.Text = "Include Unsupported IDs by Platform";
            this.chkIncludeUnsupportedIds.UseVisualStyleBackColor = true;
            this.chkIncludeUnsupportedIds.CheckedChanged += new System.EventHandler(this.chkIncludeUnsupportedIds_CheckedChanged);
            // 
            // ReportFilterForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(581, 364);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tabReportFilter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ReportFilterForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report Filter Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportFilterForm_FormClosing);
            this.tabReportFilter.ResumeLayout(false);
            this.tabMarket.ResumeLayout(false);
            this.tabMarket.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabBrand.ResumeLayout(false);
            this.tabBrand.PerformLayout();
            this.brandFilterPanel.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabModel.ResumeLayout(false);
            this.tabModel.PerformLayout();
            this.modelFilterPanel.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabDataSourceLocation.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.tabModelInfoSpecific.ResumeLayout(false);
            this.dateBasedSearchGroup.ResumeLayout(false);
            this.grpMIProject.ResumeLayout(false);
            this.grpMIProject.PerformLayout();
            this.grpMIOption.ResumeLayout(false);
            this.grpMIOption.PerformLayout();
            this.grpMIDateSelection.ResumeLayout(false);
            this.grpMIDateSelection.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabDevice.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.tabIDSetupCodeInfo.ResumeLayout(false);
            this.tabIDSetupCodeInfo.PerformLayout();
            this.grpProject.ResumeLayout(false);
            this.grpProject.PerformLayout();
            this.grpAliasIDFile.ResumeLayout(false);
            this.grpAliasIDFile.PerformLayout();
            this.gbSort.ResumeLayout(false);
            this.gbSort.PerformLayout();
            this.tabBrandBasedSearch.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabLabelIntronSearch.ResumeLayout(false);
            this.grpBxEnterPatterns.ResumeLayout(false);
            this.grpBxEnterPatterns.PerformLayout();
            this.tabQuickSetInfo.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridModeList)).EndInit();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridQuickSet)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.tabKeyFunction.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.tabUnsupportedId.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.grpPlatforms.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabReportFilter;
        private TabPage tabMarket;
        private TabPage tabBrand;
        private TabPage tabModel;
        private TabPage tabDataSourceLocation;
        private XlTabDrawer xlTabDrawer;
        private TabPage tabModelInfoSpecific;
        private TabPage tabDevice;
        private TabPage tabIDSetupCodeInfo;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkNoUEIBrandNumbers;
        private System.Windows.Forms.CheckBox chkRecordWithoutModelName;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox chkTNLinks;
        private System.Windows.Forms.CheckBox chkKeepDuplicateModels;
        private System.Windows.Forms.CheckBox chkRemoveIDs;
        private System.Windows.Forms.GroupBox gbSort;
        private System.Windows.Forms.RadioButton rdoSortByModelCount;
        private System.Windows.Forms.ComboBox cmbSetupCodeFormating;
        private System.Windows.Forms.CheckBox chkIncludeRestrictedIDs;
        private System.Windows.Forms.RadioButton rdoSortByName;
        private System.Windows.Forms.Label lblSetupCodeFormat;
        private System.Windows.Forms.TextBox txtIDOffset;
        private System.Windows.Forms.Label lblIDOffset;
        private System.Windows.Forms.GroupBox grpAliasIDFile;
        private System.Windows.Forms.RadioButton rdoSortByIDNumber;
        private System.Windows.Forms.TextBox txtAliasIDFile;
        private System.Windows.Forms.TextBox txtPriorityIDListFile;
        private System.Windows.Forms.CheckBox chkDisplayBrandNumber;
        private System.Windows.Forms.CheckBox chkDisplayMajorBrands;
        private System.Windows.Forms.TextBox txtIdenticalIdListFile;
        private System.Windows.Forms.RadioButton rdoSortByPriorityIDListFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.RadioButton rdoSortBySearchStatistics;
        private System.Windows.Forms.RadioButton rdoSortByPOSData;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.TextBox txtEndDate;
        private System.Windows.Forms.TextBox txtStartDate;
        private System.Windows.Forms.Button btnStartDate;
        private System.Windows.Forms.Button btnEndDate;
        private System.Windows.Forms.CheckBox chkRemoveNonCodeBookModels;
        private System.Windows.Forms.Button btnBrowseIdenticalIdListFile;
        private System.Windows.Forms.CheckBox enableDateFilterChk;
        private System.Windows.Forms.GroupBox dateBasedSearchGroup;
        private System.Windows.Forms.TextBox dBSToDateTxt;
        private System.Windows.Forms.TextBox dBSFromDateTxt;
        private System.Windows.Forms.Label dBSToDateLbl;
        private System.Windows.Forms.Label dBSFromDateLbl;
        private System.Windows.Forms.Button dBSFromDateBtn;
        private System.Windows.Forms.Button dBSToDateBtn;
        private System.Windows.Forms.RadioButton dbsModelsRd;
        private System.Windows.Forms.RadioButton dBSBrandsRd;
        private System.Windows.Forms.RadioButton dBSIDRd;
        private System.Windows.Forms.RadioButton dBSTNRd;
        private System.Windows.Forms.Panel brandFilterPanel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnFilterBrandFile;
        private System.Windows.Forms.Button btnRemoveFilterBrand;
        private System.Windows.Forms.Button btnAddFilterBrand;
        private System.Windows.Forms.ListBox lstFilterBrands;
        private System.Windows.Forms.TextBox txtFilterBrand;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnMatchBrandFile;
        private System.Windows.Forms.ListBox lstMatchBrands;
        private System.Windows.Forms.Button btnRemoveMatchBrand;
        private System.Windows.Forms.Button btnAddMatchBrand;
        private System.Windows.Forms.TextBox txtMatchBrand;
        private System.Windows.Forms.Panel modelFilterPanel;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnFilterModelFile;
        private System.Windows.Forms.Button btnRemoveFilterModel;
        private System.Windows.Forms.Button btnAddFilterModel;
        private System.Windows.Forms.ListBox lstFilterModels;
        private System.Windows.Forms.TextBox txtFilterModel;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnMatchModelFile;
        private System.Windows.Forms.ListBox lstMatchModels;
        private System.Windows.Forms.Button btnRemoveMatchModel;
        private System.Windows.Forms.Button btnAddMatchModel;
        private System.Windows.Forms.TextBox txtMatchModel;
        private TabPage tabBrandBasedSearch;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnBrowseBrandListFile;
        private System.Windows.Forms.TextBox txtBrandSeachListFile;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button btnPOSEndDate;
        private System.Windows.Forms.Button btnPOSStartDate;
        private System.Windows.Forms.TextBox txtPOSEndDate;
        private System.Windows.Forms.TextBox txtPOSStartDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnOSSEndDate;
        private System.Windows.Forms.Button btnOSSStartDate;
        private System.Windows.Forms.TextBox txtOSSEndDate;
        private System.Windows.Forms.TextBox txtOSSStartDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkUEI2Avail;
        private System.Windows.Forms.CheckBox chkPOSAvail;
        private System.Windows.Forms.CheckBox chkOSSAvail;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox chkModel;
        private System.Windows.Forms.CheckBox chkDataSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkMode;
        private System.Windows.Forms.Label label2;
        private TabPage tabLabelIntronSearch;
        private System.Windows.Forms.GroupBox grpBxEnterPatterns;
        private System.Windows.Forms.CheckBox chkBxGetCompleteMatch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bttnLoadLabelIntronFile;
        private System.Windows.Forms.Button bttnRemoveLabelIntron;
        private System.Windows.Forms.Button bttnAddLabelIntron;
        private System.Windows.Forms.TextBox txtBxEnterLabelIntron;
        private System.Windows.Forms.ListBox lstBxSearchPatterns;
        private System.Windows.Forms.GroupBox grpProject;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.CheckBox chkIncludeIdenticalId;
        private System.Windows.Forms.ComboBox cmbBxModelTypeISCI;
        private System.Windows.Forms.ComboBox cmbBxModelTypeBBS;
        private System.Windows.Forms.CheckBox chkBxIncludeBlankModelNames;
        private System.Windows.Forms.CheckBox chkBxBlankModelNameBBS;
        private System.Windows.Forms.Button btnShowDictionary;
        private System.Windows.Forms.CheckBox chkIncludeAliasBrand;
        private System.Windows.Forms.CheckBox chkIncludeAliasBrandNames;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TreeView chkSubRegions;
        private System.Windows.Forms.TreeView chkCountries;
        private System.Windows.Forms.TreeView chkRegions;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TreeView chkSubDevices;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TreeView chkComponents;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TreeView chkMainDevices;
        private System.Windows.Forms.CheckBox chkIncludeNonCountryRecords;
        private System.Windows.Forms.CheckBox chkIncludeEmptyDeviceType;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.RadioButton rdoCompStatusBoth;
        private System.Windows.Forms.RadioButton rdoCompStatusNonBuiltIn;
        private System.Windows.Forms.RadioButton rdoCompStatusBuiltIn;
        private System.Windows.Forms.CheckBox chkBxIncludeRestrictedIDs_ModelInfo;
        private System.Windows.Forms.GroupBox grpMIDateSelection;
        private System.Windows.Forms.GroupBox grpMIOption;
        private System.Windows.Forms.GroupBox grpMIProject;
        private System.Windows.Forms.CheckBox chkMIIncludeIdenticalId;
        private System.Windows.Forms.TextBox txtMIProjectName;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TreeView treeDataSources;
        private System.Windows.Forms.Button btnMIIdenticalBrowse;
        private System.Windows.Forms.TextBox txtMIIdenticalFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkUnknownLocation;
        private System.Windows.Forms.CheckBox chkFormatStartDigitSwitch;
        private System.Windows.Forms.CheckBox chkIncludeNonComponentLinkedRecords;
        private TabPage tabQuickSetInfo;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.TextBox txtQSPriorityIdListFile;
        private System.Windows.Forms.RadioButton rdoQSSortByPriorityIDList;
        private System.Windows.Forms.RadioButton rdoQSSortByIDNo;
        private System.Windows.Forms.RadioButton rdoQSSortByModelCount;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtModeList;
        private System.Windows.Forms.TextBox txtModeName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView dataGridQuickSet;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.DataGridView dataGridModeList;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeviceCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModeList;
        private TabPage tabKeyFunction;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private TabPage tabUnsupportedId;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.CheckBox chkIncludeUnsupportedIds;
        private System.Windows.Forms.GroupBox grpPlatforms;
        private System.Windows.Forms.TreeView treeViewPlatforms;
        //private DBM.Forms.CheckBoxComboBox chkDeviceTypes;
    }
}