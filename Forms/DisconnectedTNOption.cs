using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class DisconnectedTNOption : Form
    {
        public DisconnectedTNOption()
        {
            InitializeComponent();
        }
        
        #region Properties
        private Int32 m_FirstRange = 0;
        public Int32 FirstRange
        {
            get { return m_FirstRange; }
            set { m_FirstRange = value; }
        }
        private Int32 m_LastRange = -1;
        public Int32 LastRange
        {
            get { return m_LastRange; }
            set { m_LastRange = value; }
        }
        #endregion
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtFirst.Text.Trim() != String.Empty)
                FirstRange = Int32.Parse(txtFirst.Text);
            if (txtLast.Text.Trim() != String.Empty)
                LastRange = Int32.Parse(txtLast.Text);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }      
    }
}