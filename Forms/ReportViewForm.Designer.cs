namespace UEI.Workflow2010.Report.Forms
{
    partial class ReportViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportContainer = new System.Windows.Forms.SplitContainer();
            this.modeTree = new System.Windows.Forms.TreeView();
            this.reportGrid = new System.Windows.Forms.DataGridView();
            this.reportContainer.Panel1.SuspendLayout();
            this.reportContainer.Panel2.SuspendLayout();
            this.reportContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // reportContainer
            // 
            this.reportContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportContainer.Location = new System.Drawing.Point(0, 0);
            this.reportContainer.Name = "reportContainer";
            // 
            // reportContainer.Panel1
            // 
            this.reportContainer.Panel1.Controls.Add(this.modeTree);
            // 
            // reportContainer.Panel2
            // 
            this.reportContainer.Panel2.Controls.Add(this.reportGrid);
            this.reportContainer.Size = new System.Drawing.Size(821, 266);
            this.reportContainer.SplitterDistance = 273;
            this.reportContainer.TabIndex = 0;
            // 
            // modeTree
            // 
            this.modeTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modeTree.Location = new System.Drawing.Point(0, 0);
            this.modeTree.Name = "modeTree";
            this.modeTree.Size = new System.Drawing.Size(273, 266);
            this.modeTree.TabIndex = 0;
            // 
            // reportGrid
            // 
            this.reportGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reportGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportGrid.Location = new System.Drawing.Point(0, 0);
            this.reportGrid.Name = "reportGrid";
            this.reportGrid.Size = new System.Drawing.Size(544, 266);
            this.reportGrid.TabIndex = 0;
            // 
            // ReportViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 266);
            this.Controls.Add(this.reportContainer);
            this.Name = "ReportViewForm";
            this.Text = "ReportViewForm";
            this.reportContainer.Panel1.ResumeLayout(false);
            this.reportContainer.Panel2.ResumeLayout(false);
            this.reportContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reportGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer reportContainer;
        private System.Windows.Forms.TreeView modeTree;
        private System.Windows.Forms.DataGridView reportGrid;
    }
}