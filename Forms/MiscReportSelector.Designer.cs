namespace UEI.Workflow2010.Report
{
    partial class MiscReportSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkdLstReport = new System.Windows.Forms.CheckedListBox();
            this.btnGetReport = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkdLstReport
            // 
            this.chkdLstReport.CheckOnClick = true;
            this.chkdLstReport.FormattingEnabled = true;
            this.chkdLstReport.Items.AddRange(new object[] {
            "MainDevices",
            "SubDevices",
            "StandardBrands",
            "UniqueAliasBrands",
            "UniqueBrandVariations",
            "UniqueDeviceModels",
            "UniqueRemoteModels",
            "UniqueKeyFunctions",
            "Keys",
            "TotalUsedExecutors",
            "TotalExecutors"});
            this.chkdLstReport.Location = new System.Drawing.Point(8, 21);
            this.chkdLstReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkdLstReport.Name = "chkdLstReport";
            this.chkdLstReport.Size = new System.Drawing.Size(241, 225);
            this.chkdLstReport.TabIndex = 0;
            // 
            // btnGetReport
            // 
            this.btnGetReport.Location = new System.Drawing.Point(137, 250);
            this.btnGetReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetReport.Name = "btnGetReport";
            this.btnGetReport.Size = new System.Drawing.Size(113, 28);
            this.btnGetReport.TabIndex = 1;
            this.btnGetReport.Text = "Get Report";
            this.btnGetReport.UseVisualStyleBackColor = true;
            this.btnGetReport.Click += new System.EventHandler(this.btnGetReport_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkdLstReport);
            this.groupBox1.Controls.Add(this.btnGetReport);
            this.groupBox1.Location = new System.Drawing.Point(16, 57);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(264, 283);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select from the list";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 42);
            this.label1.TabIndex = 4;
            this.label1.Text = "Get the total count for the items given below.";
            // 
            // MiscReportSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(291, 354);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MiscReportSelector";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Consolidated Report";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox chkdLstReport;
        private System.Windows.Forms.Button btnGetReport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;

    }
}