namespace UEI.Workflow2010.Report
{
    partial class DeviceRegionalSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.rdoAsia = new System.Windows.Forms.RadioButton();
            this.rdoEU = new System.Windows.Forms.RadioButton();
            this.rdoNA = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.btnOK);
            this.groupBox1.Controls.Add(this.rdoAsia);
            this.groupBox1.Controls.Add(this.rdoEU);
            this.groupBox1.Controls.Add(this.rdoNA);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(355, 176);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Regional Name for Device Type";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(172, 96);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(62, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(106, 96);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(56, 28);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // rdoAsia
            // 
            this.rdoAsia.AutoSize = true;
            this.rdoAsia.Location = new System.Drawing.Point(248, 38);
            this.rdoAsia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoAsia.Name = "rdoAsia";
            this.rdoAsia.Size = new System.Drawing.Size(56, 21);
            this.rdoAsia.TabIndex = 2;
            this.rdoAsia.TabStop = true;
            this.rdoAsia.Text = "Asia";
            this.rdoAsia.UseVisualStyleBackColor = true;
            // 
            // rdoEU
            // 
            this.rdoEU.AutoSize = true;
            this.rdoEU.Location = new System.Drawing.Point(161, 38);
            this.rdoEU.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoEU.Name = "rdoEU";
            this.rdoEU.Size = new System.Drawing.Size(75, 21);
            this.rdoEU.TabIndex = 1;
            this.rdoEU.TabStop = true;
            this.rdoEU.Text = "Europe";
            this.rdoEU.UseVisualStyleBackColor = true;
            // 
            // rdoNA
            // 
            this.rdoNA.AutoSize = true;
            this.rdoNA.Location = new System.Drawing.Point(31, 38);
            this.rdoNA.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoNA.Name = "rdoNA";
            this.rdoNA.Size = new System.Drawing.Size(119, 21);
            this.rdoNA.TabIndex = 0;
            this.rdoNA.TabStop = true;
            this.rdoNA.Text = "North America";
            this.rdoNA.UseVisualStyleBackColor = true;
            // 
            // DeviceRegionalSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(355, 176);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DeviceRegionalSelection";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Regional Name Form";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.RadioButton rdoAsia;
        private System.Windows.Forms.RadioButton rdoEU;
        private System.Windows.Forms.RadioButton rdoNA;
    }
}