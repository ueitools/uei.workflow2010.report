﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.Workflow2010.Report
{
	public class TabChangingEventArgs : EventArgs
	{
		/// <summary>
		/// Creates an instance of the <see cref="TabChangingEventArgs"/>
		/// class with the specified indices.
		/// </summary>
		/// <param name="currentIndex">The current selected index of the <see cref="TabControl"/>.</param>
		/// <param name="newIndex">The value to which the selected index changes.</param>
		public TabChangingEventArgs(int currentIndex, int newIndex)
		{
			this.cIndex = currentIndex;
			this.nIndex = newIndex;
		}

		/// <summary>
		/// Gets the value of the <see cref="TabControl.SelectedIndex"/> for the
		/// <see cref="TJTabControl"/> from which this event got generated.
		/// </summary>
		public int CurrentIndex
		{
			get
			{
				return cIndex;
			}
		}

		/// <summary>
		/// Gets the value to which the <see cref="TabControl.SelectedIndex"/>
		/// will change.
		/// </summary>
		public int NewIndex
		{
			get
			{
				return nIndex;
			}
		}

		/// <summary>
		/// Gets and sets a value indicating whether the <see cref="TabControl"/>'s
		/// <see cref="TabControl.SelectedIndex"/> should change.
		/// </summary>
		public bool Cancel
		{
			get
			{
				return cancelEvent;
			}
			set
			{
				cancelEvent = value;
			}
		}

		/// <summary>
		/// A boolean value to indicate if the event should get cancelled.
		/// </summary>
		private bool cancelEvent;

		/// <summary>
		/// The current index to report in the event.
		/// </summary>
		private int cIndex;

		/// <summary>
		/// The index to which the tab control changes.
		/// </summary>
		private int nIndex;
	}
}
