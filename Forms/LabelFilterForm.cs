using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class LabelFilterForm : Form
    {
        public LabelFilterForm()
        {
            InitializeComponent();
        }


        private string _LabelPattern;

        public string LabelPattern
        {
            get { return _LabelPattern; }
            set { _LabelPattern = value; }
        }

	
	

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            string labelSearchPattern = String.Empty;

            //string labelPattern = txtLabel1.Text.Trim();

            //if (!String.IsNullOrEmpty(labelPattern))
            //    labelSearchPattern += GetSearchPattern(labelPattern) + ",";

            //labelPattern = txtLabel2.Text.Trim();
            //if (!String.IsNullOrEmpty(labelPattern))
            //    labelSearchPattern += GetSearchPattern(labelPattern) + ",";

            //labelPattern = txtLabel3.Text.Trim();
            //if (!String.IsNullOrEmpty(labelPattern))
            //    labelSearchPattern += GetSearchPattern(labelPattern);


            for (int i = 1; i <= 3; i++)
            {
                TextBox txtBox = (TextBox) this.groupBox1.Controls["txtLabel" + i];
                string labelPattern = txtBox.Text.Trim();
                if (!String.IsNullOrEmpty(labelPattern))
                    labelSearchPattern += GetSearchPattern(labelPattern) + ",";
            }

            if(!String.IsNullOrEmpty(labelSearchPattern))
                this._LabelPattern = labelSearchPattern.Remove(labelSearchPattern.LastIndexOf(','), 1);

            this.Close();
        }

        private string GetSearchPattern(string searchString)
        {
            
            string modifiedString = searchString;

            if (modifiedString.StartsWith("*"))
            {
                modifiedString = modifiedString.Remove(0, 1).Insert(0, "%");
            }

            if (modifiedString.EndsWith("*"))
            {
                int index = modifiedString.LastIndexOf('*');
                modifiedString = modifiedString.Remove(index, 1) + "%";
            }

            return modifiedString;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
    }
}