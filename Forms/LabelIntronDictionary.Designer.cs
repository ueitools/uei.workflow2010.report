namespace UEI.Workflow2010.Report
{
    partial class LabelIntronDictionary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.txtBxEnterText = new System.Windows.Forms.TextBox();
            this.cmbBxSelectCategory = new System.Windows.Forms.ComboBox();
            this.grdVwLabelIntronDictionary = new System.Windows.Forms.DataGridView();
            this.SelectItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnAddItems = new System.Windows.Forms.Button();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdVwLabelIntronDictionary)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.txtBxEnterText);
            this.groupBox14.Controls.Add(this.cmbBxSelectCategory);
            this.groupBox14.Controls.Add(this.grdVwLabelIntronDictionary);
            this.groupBox14.Location = new System.Drawing.Point(11, 6);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox14.Size = new System.Drawing.Size(407, 320);
            this.groupBox14.TabIndex = 7;
            this.groupBox14.TabStop = false;
            // 
            // txtBxEnterText
            // 
            this.txtBxEnterText.Location = new System.Drawing.Point(12, 18);
            this.txtBxEnterText.Margin = new System.Windows.Forms.Padding(4);
            this.txtBxEnterText.Name = "txtBxEnterText";
            this.txtBxEnterText.Size = new System.Drawing.Size(160, 22);
            this.txtBxEnterText.TabIndex = 2;
            this.txtBxEnterText.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBxEnterText_KeyUp);
            // 
            // cmbBxSelectCategory
            // 
            this.cmbBxSelectCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBxSelectCategory.FormattingEnabled = true;
            this.cmbBxSelectCategory.Location = new System.Drawing.Point(181, 17);
            this.cmbBxSelectCategory.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBxSelectCategory.Name = "cmbBxSelectCategory";
            this.cmbBxSelectCategory.Size = new System.Drawing.Size(211, 24);
            this.cmbBxSelectCategory.TabIndex = 1;
            this.cmbBxSelectCategory.SelectedIndexChanged += new System.EventHandler(this.cmbBxSelectCategory_SelectedIndexChanged);
            // 
            // grdVwLabelIntronDictionary
            // 
            this.grdVwLabelIntronDictionary.AllowUserToAddRows = false;
            this.grdVwLabelIntronDictionary.AllowUserToDeleteRows = false;
            this.grdVwLabelIntronDictionary.AllowUserToResizeRows = false;
            this.grdVwLabelIntronDictionary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdVwLabelIntronDictionary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdVwLabelIntronDictionary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SelectItem});
            this.grdVwLabelIntronDictionary.Location = new System.Drawing.Point(12, 49);
            this.grdVwLabelIntronDictionary.Margin = new System.Windows.Forms.Padding(4);
            this.grdVwLabelIntronDictionary.Name = "grdVwLabelIntronDictionary";
            this.grdVwLabelIntronDictionary.RowHeadersWidth = 4;
            this.grdVwLabelIntronDictionary.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdVwLabelIntronDictionary.Size = new System.Drawing.Size(381, 255);
            this.grdVwLabelIntronDictionary.TabIndex = 0;
            this.grdVwLabelIntronDictionary.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdVwLabelIntronDictionary_CellValueChanged);
            this.grdVwLabelIntronDictionary.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdVwLabelIntronDictionary_CurrentCellDirtyStateChanged);
            // 
            // SelectItem
            // 
            this.SelectItem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.SelectItem.HeaderText = "";
            this.SelectItem.Name = "SelectItem";
            this.SelectItem.Width = 5;
            // 
            // btnAddItems
            // 
            this.btnAddItems.Location = new System.Drawing.Point(316, 334);
            this.btnAddItems.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddItems.Name = "btnAddItems";
            this.btnAddItems.Size = new System.Drawing.Size(100, 28);
            this.btnAddItems.TabIndex = 3;
            this.btnAddItems.Text = "Add";
            this.btnAddItems.UseVisualStyleBackColor = true;
            this.btnAddItems.Click += new System.EventHandler(this.btnAddItems_Click);
            // 
            // LabelIntronDictionary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(427, 367);
            this.Controls.Add(this.btnAddItems);
            this.Controls.Add(this.groupBox14);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LabelIntronDictionary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Label\\Intron Dictionary";
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdVwLabelIntronDictionary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox cmbBxSelectCategory;
        private System.Windows.Forms.DataGridView grdVwLabelIntronDictionary;
        private System.Windows.Forms.TextBox txtBxEnterText;
        private System.Windows.Forms.Button btnAddItems;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectItem;
    }
}