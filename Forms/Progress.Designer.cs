using System.Drawing;
namespace UEI.Workflow2010.Report.Forms
{    
    partial class Progress
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerAutoProgress = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Progress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Progress";
            this.Size = new System.Drawing.Size(301, 37);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerAutoProgress;
    }
}
