namespace UEI.Workflow2010.Report.Forms
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel = new System.Windows.Forms.Button();
            this.Process = new System.Windows.Forms.Label();
            this.lblMsg = new System.Windows.Forms.Label();
            this.progress1 = new UEI.Workflow2010.Report.Forms.Progress();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.AutoSize = true;
            this.Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel.Location = new System.Drawing.Point(108, 160);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(67, 28);
            this.Cancel.TabIndex = 11;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Process
            // 
            this.Process.AutoSize = true;
            this.Process.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Process.Location = new System.Drawing.Point(37, 118);
            this.Process.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Process.Name = "Process";
            this.Process.Size = new System.Drawing.Size(63, 17);
            this.Process.TabIndex = 9;
            this.Process.Text = "Process:";
            // 
            // lblMsg
            // 
            this.lblMsg.Location = new System.Drawing.Point(108, 117);
            this.lblMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(256, 17);
            this.lblMsg.TabIndex = 13;
            this.lblMsg.Text = "label1";
            // 
            // progress1
            // 
            this.progress1.AutoProgress = true;
            this.progress1.IndicatorColor = System.Drawing.Color.Green;
            this.progress1.Location = new System.Drawing.Point(31, 44);
            this.progress1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.progress1.Name = "progress1";
            this.progress1.Position = 6;
            this.progress1.ProgressBoxStyle = UEI.Workflow2010.Report.Forms.Progress.ProgressBoxStyleConstants.SOLIDSMALLER;
            this.progress1.ShowBorder = false;
            this.progress1.Size = new System.Drawing.Size(333, 21);
            this.progress1.TabIndex = 12;
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(383, 199);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.progress1);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Process);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ProgressForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Progress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label Process;
        private Progress progress1;
        private System.Windows.Forms.Label lblMsg;
    }
}