namespace UEI.Workflow2010.Report
{
    partial class LabelIntronDistinctView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.trVwDistinctView = new System.Windows.Forms.TreeView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dtGrdVwDetails = new System.Windows.Forms.DataGridView();
            this.treeViewTN = new System.Windows.Forms.TreeView();
            this.distinctViewBackWorker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGrdVwDetails)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.trVwDistinctView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer.Size = new System.Drawing.Size(690, 422);
            this.splitContainer.SplitterDistance = 131;
            this.splitContainer.TabIndex = 0;
            // 
            // trVwDistinctView
            // 
            this.trVwDistinctView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trVwDistinctView.HideSelection = false;
            this.trVwDistinctView.Location = new System.Drawing.Point(0, 0);
            this.trVwDistinctView.Name = "trVwDistinctView";
            this.trVwDistinctView.Size = new System.Drawing.Size(131, 422);
            this.trVwDistinctView.TabIndex = 0;
            this.trVwDistinctView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.trVwDistinctView_BeforeSelect);
            this.trVwDistinctView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trVwDistinctView_AfterSelect);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dtGrdVwDetails);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.treeViewTN);
            this.splitContainer1.Size = new System.Drawing.Size(555, 422);
            this.splitContainer1.SplitterDistance = 405;
            this.splitContainer1.TabIndex = 1;
            // 
            // dtGrdVwDetails
            // 
            this.dtGrdVwDetails.AllowUserToAddRows = false;
            this.dtGrdVwDetails.AllowUserToDeleteRows = false;
            this.dtGrdVwDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGrdVwDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGrdVwDetails.Location = new System.Drawing.Point(0, 0);
            this.dtGrdVwDetails.Name = "dtGrdVwDetails";
            this.dtGrdVwDetails.ReadOnly = true;
            this.dtGrdVwDetails.Size = new System.Drawing.Size(405, 422);
            this.dtGrdVwDetails.TabIndex = 0;
            // 
            // treeViewTN
            // 
            this.treeViewTN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewTN.Location = new System.Drawing.Point(0, 0);
            this.treeViewTN.Name = "treeViewTN";
            this.treeViewTN.Size = new System.Drawing.Size(146, 422);
            this.treeViewTN.TabIndex = 1;
            // 
            // distinctViewBackWorker
            // 
            this.distinctViewBackWorker.WorkerReportsProgress = true;
            this.distinctViewBackWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.distinctViewBackWorker_DoWork);
            this.distinctViewBackWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.distinctViewBackWorker_ProgressChanged);
            this.distinctViewBackWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.distinctViewBackWorker_RunWorkerCompleted);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 425);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(690, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // LabelIntronDistinctView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.splitContainer);
            this.Name = "LabelIntronDistinctView";
            this.Size = new System.Drawing.Size(690, 447);
            this.Load += new System.EventHandler(this.LabelIntronDistinctView_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtGrdVwDetails)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TreeView trVwDistinctView;
        private System.Windows.Forms.DataGridView dtGrdVwDetails;
        private System.ComponentModel.BackgroundWorker distinctViewBackWorker;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeViewTN;
    }
}
