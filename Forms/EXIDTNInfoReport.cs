using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Model;
using UEI.Workflow2010.Report.Service;

namespace UEI.Workflow2010.Report
{
    public partial class EXIDTNInfoReport : UserControl
    {
        public EXIDTNInfoReport()
        {
            InitializeComponent();
            FillExecutorList();            
        }
        private IDInfo selectedID = null;
        private enum ResetFlag
        {
            EXECUTOR = 0,
            ID = 1,
            BRAND = 2
        }
        private void FillExecutorList()
        {
            List<Int32> executors = GetExecutorList();            
            if (executors != null)
            {
                executorList.Items.Clear();
                foreach (int executor in executors)
                {
                    executorList.Items.Add(String.Format("E{0:D3}", executor));
                }
            }
        }
        private void executorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (executorList.SelectedItem != null)
            {
                String a = System.Text.RegularExpressions.Regex.Match(executorList.SelectedItem.ToString(), "\\d+").Value;
                int executorCode = Convert.ToInt32(a);
                this.Cursor = Cursors.WaitCursor;
                ResetAll(ResetFlag.EXECUTOR);
                List<IDInfo> idInfoList = GetIDsForExecutor(executorCode);
                if (idInfoList != null)
                {
                    idList.ValueMember = "ID";
                    foreach (IDInfo id in idInfoList)
                    {
                        idList.Items.Add(id);
                    }
                    //set the header
                    idHeader.Text = String.Format("IDs FOR EXECUTOR : {0}", executorList.SelectedItem.ToString());
                    this.Cursor = Cursors.Default;
                }
            }
        }
        private void idList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (idList.SelectedItem != null)
            {                
                selectedID = idList.SelectedItem as IDInfo;
                System.Diagnostics.Debug.WriteLine("Selected ID :" + selectedID.ID);                
                this.Cursor = Cursors.WaitCursor;
                ResetAll(ResetFlag.ID);
                Reports reportService = new Reports();
                reportService.GetModelRecordForID(selectedID);
                //TimeLog.Stop("");
                DataSet prefixSet = selectedID.Prefixes.GetDataSet();
                //display data and prefix
                dgPrefixInfo.DataSource = (prefixSet != null) ? prefixSet.Tables[0] : null;
                List<string> listOfTn = null;
                List<string> listOfBrands = null;
                selectedID.ModelRecords.GetTNAndBrandList(out listOfTn, out listOfBrands);
                //display tn list
                tnList.Items.Clear();
                // listOfTn.Sort();
                foreach (string tn in listOfTn)
                {
                    //tnList.Items.Add(String.Format("TN{0:D5}",tn));
                    //string formattedTN = (String.IsNullOrEmpty(tn)) ? "-" : String.Format("TN{0:D5}", Convert.ToInt32(tn));
                    //if (!String.IsNullOrEmpty(tn))
                    //    formattedTN = String.Format("TN{0:D5}", tn);
                    //else
                    //    formattedTN = "-";
                    if (!String.IsNullOrEmpty(tn))
                        tnList.Items.Add(String.Format("TN{0:D5}", Convert.ToInt32(tn)));
                }
                tnList.Sorted = true;
                //add to brand list
                brandList.Items.Clear();
                listOfBrands.Sort();
                foreach (string brand in listOfBrands)
                {
                    brandList.Items.Add(brand);
                }
                //BusinessObject.PrefixCollection p = reportService.GetPrefixTest(id);
                // dgOutput.DataSource = p;
                tnHeader.Text = String.Format("TNs FOR ID : {0}", selectedID.ID);
                brandHeader.Text = String.Format("BRANDS FOR ID : {0}", selectedID.ID);
                this.Cursor = Cursors.Default;
                System.Diagnostics.Debug.WriteLine("Done");                
            }
        }
        private void brandList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (brandList.SelectedItem != null)
            {
                brandList.Refresh();
                String selectedBrand = brandList.SelectedItem.ToString();
                DataSet modelSet = selectedID.ModelRecords.GetModelsByBrand(selectedBrand);
                dgModelInfo.DataSource = modelSet.Tables[0];
                modelInfoHeader2.Text = String.Format("- {0}", selectedBrand);
            }
        }
        private void tnList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tnList.SelectedItem != null)
            {
                //remove the formatting from selected TN
                string tn = System.Text.RegularExpressions.Regex.Match(tnList.SelectedItem.ToString(), "\\d+").Value;

                //remove the leading zeroes if any
                tn = tn.TrimStart('0');

                ////get the brands under the tn
                List<String> supportedBrands = selectedID.ModelRecords.GetSupportedBrands(tn);

                if (brandList.Items.Count > 0)
                {
                    brandList.Refresh();

                    foreach (string supportedBrand in supportedBrands)
                    {
                        HighlightBrand(supportedBrand);
                    }
                }
            }
        }
        private void HighlightBrand(string supportedBrand)
        {
            int index = brandList.FindString(supportedBrand);
            Rectangle itemRect = brandList.GetItemRectangle(index);
            Graphics listBoxGraphicsObj = Graphics.FromHwnd(brandList.Handle);
            listBoxGraphicsObj.FillRectangle(new SolidBrush(Color.Gray), itemRect);
            listBoxGraphicsObj.DrawString(supportedBrand, brandList.Font, new SolidBrush(Color.White), itemRect);
            this.Invalidate(itemRect);
        }

        private void ResetAll(ResetFlag flag)
        {
            switch (flag)
            {
                case ResetFlag.EXECUTOR:
                    idHeader.Text = "IDs";
                    idList.Items.Clear();

                    tnHeader.Text = "TNs";
                    tnList.Items.Clear();

                    brandHeader.Text = "BRANDS";
                    brandList.Items.Clear();

                    modelInfoHeader2.Text = String.Empty;
                    dgModelInfo.DataSource = null;
                    dgPrefixInfo.DataSource = null;

                    break;
                case ResetFlag.ID:
                    tnHeader.Text = "TNs";
                    tnList.Items.Clear();

                    brandHeader.Text = "BRANDS";
                    brandList.Items.Clear();
                    modelInfoHeader2.Text = String.Empty;
                    dgModelInfo.DataSource = null;
                    dgPrefixInfo.DataSource = null;
                    break;
                case ResetFlag.BRAND:
                    modelInfoHeader2.Text = String.Empty;
                    dgModelInfo.DataSource = null;
                    break;
            }
        }
        private List<Int32> GetExecutorList()
        {
            try
            {
                Reports reportService = new Reports();
                return reportService.GetExecutorList();
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Could not retrieve executors. Error is logged.");
                return null;
            }
        }
        private List<IDInfo> GetIDsForExecutor(int executorCode)
        {
            try
            {
                Reports reportService = new Reports();
                return reportService.GetIDForExecutor(executorCode);
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Could not retrieve ids. Error is logged.");
                return null;
            }
        }        
    }
}