using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class ModelInfoReportPopup : Form
    {
        public ModelInfoReportPopup()
        {
            InitializeComponent();
        }

        private DataSet dataToDisplay;

        public DataSet DisplayData
        {
            set { dataToDisplay = value;
            this.dgSearchResult.DataSource = dataToDisplay.Tables[0];
            this.dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            }
        }

    }
}