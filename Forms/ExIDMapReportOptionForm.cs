using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class ExIDMapReportOptionForm : Form
    {
        #region Proerties       
        private Boolean m_IncludeBrand = false;
        public Boolean IncludeBrand
        {
            get { return m_IncludeBrand; }
            set { m_IncludeBrand = value; }
        }
        private Boolean m_IncludeLoadPercentage = false;
        public Boolean IncludeLoadPercentage
        {
            get { return m_IncludeLoadPercentage; }
            set { m_IncludeLoadPercentage = value; }
        }
        #endregion

        public ExIDMapReportOptionForm()
        {
            InitializeComponent();
            this.btnOK.Click += new EventHandler(btnOK_Click);            
        }        
        void btnOK_Click(object sender, EventArgs e)
        {           
            if(this.chkIncludeBrand.Checked == true)
                this.IncludeBrand = true;
            else
                this.IncludeBrand = false;

            if(this.chkIncludeLoadPercentage.Checked == true)
                this.IncludeLoadPercentage = true;
            else
                this.IncludeLoadPercentage = false;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }        
    }
}