using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace UEI.Workflow2010.Report.Forms
{    
    public partial class Progress : UserControl
    {
        private Byte m_SpeedMultiPlier = 2;
        private Boolean m_RequireClear = false;
        private Graphics m_Graphics;
        private Boolean m_Increasing = true;

        #region Enumerations
        public enum ProgressStyleConstants
        {
            LEFTTORIGHT,
            LEFTANDRIGHT
        }
        public enum ProgressTypeConstants
        {
            BOXTYPE,
            GRAPHICTYPE
        }
        public enum ProgressBoxStyleConstants
        {
            SOLIDSAMESIZE,
            BOXAROUND,
            SOLIDBIGGER,
            SOLIDSMALLER,
        }
        #endregion

        #region Properties
        private ProgressTypeConstants m_ProgressType = ProgressTypeConstants.BOXTYPE;
        private Boolean ShouldSerializeProgressType()
        {
            return m_ProgressType != ProgressTypeConstants.BOXTYPE;
        }
        [Description("Determines the type of progress bar")]
        public ProgressTypeConstants ProgressType
        {
            get
            {
                return m_ProgressType;
            }
            set
            {
                m_ProgressType = value;
                this.Invalidate();
            }
        }

        private Image m_NormalImage;
        private Boolean ShouldSerializeNormalImage()
        {
            return m_NormalImage != null;
        }
        [Description("Gets/sets the background graphic")]
        public Image NormalImage
        {
            get
            {
                return m_NormalImage;
            }
            set
            {
                m_NormalImage = value;
                this.Invalidate();
            }

        }
        private Image m_PointImage;
        private Boolean ShouldSerializePointImage()
        {
            return m_PointImage != null;
        }
        [Description("Gets/sets the point graphic")]
        public Image PointImage
        {
            get
            {
                return m_PointImage;
            }
            set
            {
                m_PointImage = value;
                this.Invalidate();
            }

        }
        private bool m_ShowBorder = true;
        [Description("Determines if the border is shown"), DefaultValue(true)]
        public Boolean ShowBorder
        {
            get
            {
                return m_ShowBorder;
            }
            set
            {
                m_ShowBorder = value;
                this.Invalidate();
            }
        }
        private int m_NumPoints;
        [Description("Number of points in the progressbar"), Browsable(false)]
        public int NumPoints
        {
            get
            {
                return m_NumPoints;
            }
        }
        private int m_Position;
        [Description("Position of the progress indicator"), Browsable(false)]
        public int Position
        {
            get
            {
                return m_Position;
            }
            set
            {
                m_Position = value;
                this.Invalidate();
            }
        }
        private Color m_IndicatorColor = Color.Red;
        private bool ShouldSerializeIndicatorColor()
        {
            return m_IndicatorColor != Color.Red;
        }
        [Description("Color of the indicator")]
        public Color IndicatorColor
        {
            get
            {
                return m_IndicatorColor;
            }
            set
            {
                m_IndicatorColor = value;
                this.Invalidate();
            }
        }
        private ProgressStyleConstants m_ProgressStyle = ProgressStyleConstants.LEFTTORIGHT;
        private bool ShouldSerializeProgressStyle()
        {
            return m_ProgressStyle != ProgressStyleConstants.LEFTTORIGHT;
        }
        [Description("Indicates the progress indicator rotation style")]
        public ProgressStyleConstants ProgressStyle
        {
            get
            {
                return m_ProgressStyle;
            }
            set
            {
                m_ProgressStyle = value;
                this.Invalidate();
            }
        }
        private bool m_AutoProgress = false;
        [Description("Indicates whether auto-progress is enabled"), DefaultValue(false)]
        public bool AutoProgress
        {
            get
            {
                return m_AutoProgress;
            }
            set
            {
                this.timerAutoProgress.Interval = (255 - this.m_AutoProgressSpeed) * this.m_SpeedMultiPlier;
                if (value)
                {
                    this.timerAutoProgress.Start();
                }
                else
                {
                    this.timerAutoProgress.Stop();
                }
                m_AutoProgress = value;
            }
        }
        private int m_AutoProgressSpeed = 100;
        private bool ShouldSerializeAutoProgressSpeed()
        {
            return m_AutoProgressSpeed != 100;
        }
        [Description("Indicates the speed of the progress indicator (1 [slower] to 254 [faster]")]
        public int AutoProgressSpeed
        {
            get
            {
                return m_AutoProgressSpeed;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }
                else if (value > 254)
                {
                    value = 254;
                }
                timerAutoProgress.Stop();
                timerAutoProgress.Interval = (255 - value) * m_SpeedMultiPlier;
                timerAutoProgress.Enabled = this.m_AutoProgress;
                m_AutoProgressSpeed = value;
            }
        }
        private ProgressBoxStyleConstants m_ProgressBoxStyle = ProgressBoxStyleConstants.SOLIDSAMESIZE;
        private bool ShouldSerializeProgressBoxStyle()
        {
            return m_ProgressBoxStyle != ProgressBoxStyleConstants.SOLIDSAMESIZE;
        }
        public ProgressBoxStyleConstants ProgressBoxStyle
        {
            get
            {
                return m_ProgressBoxStyle;
            }
            set
            {
                m_ProgressBoxStyle = value;
                this.Invalidate();
            }
        }
        #endregion

        #region Methods
        private void ResizeHandler(object sender, System.EventArgs e)
        {
            this.m_RequireClear = true;
            this.m_Position = 0;
            this.Invalidate();
        }
        private void TimerHandler(object sender, System.EventArgs e)
        {
            if (this.m_Position == this.m_NumPoints - 1)
            {
                if (this.m_ProgressStyle == ProgressStyleConstants.LEFTTORIGHT)
                {
                    this.m_Position = 0;
                }
                else
                {
                    this.m_Position -= 1;
                    this.m_Increasing = false;
                }
            }
            else if ((this.m_Position == 0) && (!this.m_Increasing))
            {
                this.m_Position += 1;
                this.m_Increasing = true;
            }
            else
            {
                if (this.m_Increasing)
                {
                    this.m_Position += 1;
                }
                else
                {
                    this.m_Position -= 1;
                }
            }
            this.m_RequireClear = false;
            this.Invalidate();
        }
        private void PaintHandler(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            this.m_Graphics = e.Graphics;
            this.m_Graphics.SmoothingMode = SmoothingMode.HighSpeed;
            if (this.m_RequireClear)
            {
                this.m_Graphics.Clear(this.BackColor);
            }
            DrawBackground();
        }
        private void DrawBackground()
        {
            this.m_NumPoints = 0;
            if (this.Width > 0 && this.Height > 0)
            {
                if (this.m_ShowBorder)
                {
                    this.m_Graphics.DrawRectangle(new Pen(SystemColors.ActiveBorder), new Rectangle(0, 0, this.Width - 1, this.Height - 1));
                }
                int iBoxSize = checked((int)(this.Height * 0.75));
                int iBoxLeft = iBoxSize / 2;
                if (iBoxSize > 3)
                {
                    do
                    {
                        Rectangle r = new Rectangle(iBoxLeft, 0, this.Height - 1, this.Height - 1);
                        if (r.Left + r.Width > this.Width)
                        {
                            break;
                        }
                        if (this.m_NumPoints == this.m_Position)
                        {
                            PositionIndicator(r);
                        }
                        else
                        {
                            Rectangle r2 = new Rectangle(r.Left + 3, r.Top + 3, r.Width - 6, r.Height - 6);
                            if ((this.m_NormalImage != null) && (this.m_ProgressType == ProgressTypeConstants.GRAPHICTYPE))
                            {
                                this.m_Graphics.DrawImage(this.m_NormalImage, r2);
                            }
                            else
                            {
                                this.m_Graphics.FillRectangle(new SolidBrush(this.ForeColor), r2);
                            }
                        }
                        iBoxLeft += checked((int)(iBoxSize * 1.5));
                        this.m_NumPoints += 1;
                    }
                    while (true);
                }
            }
        }
        private void PositionIndicator(Rectangle Rect)
        {
            if ((this.m_PointImage != null) && (this.m_ProgressType == ProgressTypeConstants.GRAPHICTYPE))
            {
                this.m_Graphics.DrawImage(this.m_PointImage, Rect);
            }
            else
            {
                Rectangle R2;
                if (this.ProgressBoxStyle == ProgressBoxStyleConstants.SOLIDSAMESIZE)
                {
                    R2 = new Rectangle(Rect.Left + 3, Rect.Top + 3, Rect.Width - 5, Rect.Height - 5);
                    this.m_Graphics.FillRectangle(new SolidBrush(m_IndicatorColor), R2);
                }
                else if (this.ProgressBoxStyle == ProgressBoxStyleConstants.BOXAROUND)
                {
                    this.m_Graphics.DrawRectangle(new Pen(m_IndicatorColor), Rect);
                    R2 = new Rectangle(Rect.Left + 3, Rect.Top + 3, Rect.Width - 5, Rect.Height - 5);
                    this.m_Graphics.FillRectangle(new SolidBrush(m_IndicatorColor), R2);
                }
                else if (this.ProgressBoxStyle == ProgressBoxStyleConstants.SOLIDBIGGER)
                {
                    this.m_Graphics.FillRectangle(new SolidBrush(m_IndicatorColor), Rect);
                }
                else if (this.ProgressBoxStyle == ProgressBoxStyleConstants.SOLIDSMALLER)
                {
                    R2 = new Rectangle(Rect.Left + 5, Rect.Top + 5, Rect.Width - 9, Rect.Height - 9);
                    this.m_Graphics.FillRectangle(new SolidBrush(m_IndicatorColor), R2);
                }
            }
        }
        #endregion

        public Progress()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(this.PaintHandler);
            this.Resize += new EventHandler(this.ResizeHandler);
            this.timerAutoProgress.Tick += new EventHandler(this.TimerHandler);
        }
    }
}
