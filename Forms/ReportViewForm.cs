using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report.Forms
{
    public partial class ReportViewForm : Form
    {
        private ReportMain currentReport = null;
        private IList<string> listOfModes = null;
       
        public ReportViewForm(IList<string> modeList, ReportMain reportMain)
        {
            InitializeComponent();

            this.currentReport = reportMain;
            this.listOfModes = modeList;


        }

        private void SetModeTree()
        {
            if (this.listOfModes != null)
            {
                //create a root node.
                TreeNode rootNode = new TreeNode("AllModes");
                foreach (string mode in this.listOfModes)
                {
                    //Create nodes for each mode
                    TreeNode modeNode = new TreeNode(mode);
                    //add to root node
                    rootNode.Nodes.Add(modeNode);
                }
            }
        }

       
    }
}