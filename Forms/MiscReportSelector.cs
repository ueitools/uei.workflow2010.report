using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class MiscReportSelector : Form
    {
        private List<string> selectedItems = new List<string>();

        private Dictionary<string, bool> reportItems = null;

        public MiscReportSelector()
        {
            InitializeComponent();
        }


        public Dictionary<string, bool> ReportItemSelections
        {
            get {
                return this.reportItems;
            }
        }
       
        private Dictionary<string, bool> CreateReportItemDictionary()
        {
            Dictionary<string, bool> reportItemSelections = new Dictionary<string, bool>();

            foreach (object item in this.chkdLstReport.Items)
            {
                reportItemSelections.Add(item.ToString(), false);
            }

            return reportItemSelections;
        }

        private void btnGetReport_Click(object sender, EventArgs e)
        {
           CheckedListBox.CheckedItemCollection checkedItems =  this.chkdLstReport.CheckedItems;

           Dictionary<string, bool> reportItemSelections = CreateReportItemDictionary();

          
           foreach (object checkedItem in checkedItems)
           {
               reportItemSelections[checkedItem.ToString()] = true;
           }

           this.reportItems = reportItemSelections;


           this.DialogResult = DialogResult.OK;
           this.Close();

        }
    }
}