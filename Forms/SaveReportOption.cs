using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    [Flags]
    public enum SaveReportButtons
    {
        Text = 0x0000,
        Excel = 0x0001,
        Pdf = 0x0002
    }
    public partial class SaveReportOption : Form
    {
        public SaveReportOption()
        {
            InitializeComponent();
        }

        private SaveOption _SaveOption = SaveOption.Excel;
        /// <summary>
        /// Gets the file type selected by the user.Default value is excel.
        /// </summary>
        public SaveOption SaveTo
        {
            get { return this._SaveOption; }
        }
        private void btnSaveToText_Click(object sender, EventArgs e)
        {
            this._SaveOption = SaveOption.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        public void SetWizardButtons(SaveReportButtons m_SaveReportButtons)
        {
            this.btnSaveToText.Enabled = ((m_SaveReportButtons & SaveReportButtons.Text) != 0);
            this.btnSaveToExcel.Enabled = ((m_SaveReportButtons & SaveReportButtons.Excel) != 0);
            this.btnSaveToPdf.Enabled = ((m_SaveReportButtons & SaveReportButtons.Pdf) != 0);
        }
        private void btnSaveToExcel_Click(object sender, EventArgs e)
        {
            this._SaveOption = SaveOption.Excel;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnSaveToPdf_Click(object sender, EventArgs e)
        {
            this._SaveOption = SaveOption.Pdf;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }       
    }
}