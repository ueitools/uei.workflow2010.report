namespace UEI.Workflow2010.Report
{
    partial class BrandModelSearchInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.filePathLbl = new System.Windows.Forms.Label();
            this.readFileLbl = new System.Windows.Forms.Label();
            this.fileValidateLbl = new System.Windows.Forms.Label();
            this.validateStatusLbl = new System.Windows.Forms.Label();
            this.searchBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.readStatusLbl = new System.Windows.Forms.Label();
            this.dbInputStatusLbl = new System.Windows.Forms.Label();
            this.getFileBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.44444F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.55556F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 236F));
            this.tableLayoutPanel.Controls.Add(this.filePathLbl, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.readFileLbl, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.fileValidateLbl, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.validateStatusLbl, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.searchBtn, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.cancelBtn, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.readStatusLbl, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.dbInputStatusLbl, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.getFileBtn, 2, 1);
            this.tableLayoutPanel.Location = new System.Drawing.Point(35, 15);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(511, 197);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // filePathLbl
            // 
            this.filePathLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.SetColumnSpan(this.filePathLbl, 3);
            this.filePathLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filePathLbl.Location = new System.Drawing.Point(5, 5);
            this.filePathLbl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.filePathLbl.Name = "filePathLbl";
            this.filePathLbl.Size = new System.Drawing.Size(501, 43);
            this.filePathLbl.TabIndex = 2;
            this.filePathLbl.Text = "File Path";
            this.filePathLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // readFileLbl
            // 
            this.readFileLbl.Location = new System.Drawing.Point(5, 109);
            this.readFileLbl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.readFileLbl.Name = "readFileLbl";
            this.readFileLbl.Size = new System.Drawing.Size(180, 33);
            this.readFileLbl.TabIndex = 1;
            this.readFileLbl.Text = "Reading File...";
            this.readFileLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fileValidateLbl
            // 
            this.fileValidateLbl.Location = new System.Drawing.Point(5, 57);
            this.fileValidateLbl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fileValidateLbl.Name = "fileValidateLbl";
            this.fileValidateLbl.Size = new System.Drawing.Size(180, 43);
            this.fileValidateLbl.TabIndex = 0;
            this.fileValidateLbl.Text = "Validating File";
            this.fileValidateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // validateStatusLbl
            // 
            this.validateStatusLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.validateStatusLbl.AutoSize = true;
            this.validateStatusLbl.Location = new System.Drawing.Point(194, 53);
            this.validateStatusLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.validateStatusLbl.Name = "validateStatusLbl";
            this.validateStatusLbl.Size = new System.Drawing.Size(74, 51);
            this.validateStatusLbl.TabIndex = 3;
            this.validateStatusLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // searchBtn
            // 
            this.searchBtn.Enabled = false;
            this.searchBtn.Location = new System.Drawing.Point(277, 109);
            this.searchBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(100, 28);
            this.searchBtn.TabIndex = 4;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(277, 151);
            this.cancelBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 28);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 151);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 33);
            this.label1.TabIndex = 6;
            this.label1.Text = "Getting Inputs From DB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // readStatusLbl
            // 
            this.readStatusLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.readStatusLbl.AutoSize = true;
            this.readStatusLbl.Location = new System.Drawing.Point(194, 105);
            this.readStatusLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.readStatusLbl.Name = "readStatusLbl";
            this.readStatusLbl.Size = new System.Drawing.Size(74, 41);
            this.readStatusLbl.TabIndex = 7;
            // 
            // dbInputStatusLbl
            // 
            this.dbInputStatusLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dbInputStatusLbl.AutoSize = true;
            this.dbInputStatusLbl.Location = new System.Drawing.Point(194, 147);
            this.dbInputStatusLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dbInputStatusLbl.Name = "dbInputStatusLbl";
            this.dbInputStatusLbl.Size = new System.Drawing.Size(74, 49);
            this.dbInputStatusLbl.TabIndex = 8;
            // 
            // getFileBtn
            // 
            this.getFileBtn.Location = new System.Drawing.Point(277, 57);
            this.getFileBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.getFileBtn.Name = "getFileBtn";
            this.getFileBtn.Size = new System.Drawing.Size(100, 28);
            this.getFileBtn.TabIndex = 9;
            this.getFileBtn.Text = "Get File";
            this.getFileBtn.UseVisualStyleBackColor = true;
            this.getFileBtn.Click += new System.EventHandler(this.getFileBtn_Click);
            // 
            // BrandModelSearchInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(584, 238);
            this.Controls.Add(this.tableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "BrandModelSearchInput";
            this.Text = "Brand - Model Search ";
            this.Load += new System.EventHandler(this.BrandModelSearchInput_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label fileValidateLbl;
        private System.Windows.Forms.Label readFileLbl;
        private System.Windows.Forms.Label filePathLbl;
        private System.Windows.Forms.Label validateStatusLbl;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label readStatusLbl;
        private System.Windows.Forms.Label dbInputStatusLbl;
        private System.Windows.Forms.Button getFileBtn;
    }
}