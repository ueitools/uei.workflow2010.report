using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Service;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Diagnostics;

namespace UEI.Workflow2010.Report
{
   
    
    public partial class LabelIntronDistinctView : UserControl
    {
        #region Variables
        private Model.IDistinctView viewObject;
        private SortedList<string, List<string>> distinctData = null;
        private SortedList<string, DataTable> detailsData = null;
        private UEI.Workflow2010.Report.Forms.ProgressForm progressForm = null;
        private Reports reportService = null;
        private Forms.RemoteImageViewerForm remoteimageViewer = null;
        #endregion

        #region Properties
        public Dictionary<String, List<Model.LabelIntronSearchRecord>> TNList { get; set; }
        #endregion

        public LabelIntronDistinctView()
        {
            InitializeComponent();            
        }       
        public LabelIntronDistinctView(Model.IDistinctView dataObject)
        {
            InitializeComponent();                        
            this.viewObject = dataObject;
        }        
        public Model.IDistinctView DataObject
        {
            get { return viewObject; }
            set { viewObject = value; }
        }

        private void LabelIntronDistinctView_Load(object sender, EventArgs e)
        {
            if (this.viewObject != null)
            {
                this.treeViewTN.BeforeSelect += treeViewTN_BeforeSelect;
                this.treeViewTN.AfterSelect += treeViewTN_AfterSelect;
                reportService = new Reports();
                //call the background worker
                if(!this.distinctViewBackWorker.IsBusy)
                    this.distinctViewBackWorker.RunWorkerAsync("GetDistinctViewData");
                //distinctData = this.viewObject.DistinctViewData;

                ////display in the treeview
                //foreach (string key  in distinctData.Keys)
                //{
                //    TreeNode rootNode = new TreeNode(key);
                //    List<string> valueList =  distinctData[key];
                //    if (valueList.Count > 0)
                //    {
                //        TreeNode[] valueNodes = new TreeNode[valueList.Count];

                //        for (int i = 0; i < valueList.Count; i++)
                //        {
                //            valueNodes[i] = new TreeNode(valueList[i]);
                //        }

                //        rootNode.Nodes.AddRange(valueNodes);
                //    }

                //    this.trVwDistinctView.Nodes.Add(rootNode);
                //}

            }
        }
        private void trVwDistinctView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Tag.ToString() == "ID")
                {
                    string id = e.Node.Text;
                    //get the details for the id
                    if (detailsData == null)
                    {
                        //detailsData = this.viewObject.DetailsViewData;

                        if (!this.distinctViewBackWorker.IsBusy)
                            this.distinctViewBackWorker.RunWorkerAsync("GetDetailsViewData");
                    }
                    else
                    {
                        DataTable dtDetails = detailsData[id];
                        this.dtGrdVwDetails.DataSource = dtDetails;
                        LoadTNForSelectedID(e.Node.Text, dtDetails);
                    }
                }
            }
        }
        private void trVwDistinctView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }
        private void distinctViewBackWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker backWorker = (BackgroundWorker)sender;
            backWorker.ReportProgress(0);

            if (e.Argument.ToString() == "GetDistinctViewData")
            {
                distinctData = this.viewObject.DistinctViewData;
                e.Result = "GetDistinctViewData";
            }
            else if (e.Argument.ToString() == "GetDetailsViewData")
            {
                detailsData = this.viewObject.DetailsViewData;
                e.Result = "GetDetailsViewData";
            }            
        }
        private void distinctViewBackWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UEI.Workflow2010.Report.Service.ErrorLogging.WriteToErrorLogFile(e.Error);

                MessageBox.Show("Error occurred in displaying report.Please check error log.","Exception");
                
            }
            else if (!e.Cancelled)
            {
                if (e.Result.ToString() == "GetDistinctViewData")
                {
                    if (distinctData != null && distinctData.Count > 0)
                    {
                        //display in the treeview
                        foreach (string key in distinctData.Keys)
                        {
                            TreeNode rootNode = new TreeNode(key);
                            rootNode.Tag = "Mode";
                            List<string> valueList = distinctData[key];
                            if (valueList.Count > 0)
                            {
                                TreeNode[] valueNodes = new TreeNode[valueList.Count];

                                for (int i = 0; i < valueList.Count; i++)
                                {
                                    valueNodes[i] = new TreeNode(valueList[i]);
                                    valueNodes[i].Tag = "ID";
                                }                                
                                rootNode.Nodes.AddRange(valueNodes);
                            }
                            this.trVwDistinctView.Nodes.Add(rootNode);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Could not find any data.");

                    }
                }
                else if (e.Result.ToString() == "GetDetailsViewData")
                {
                    //get the selected id
                    string id =  this.trVwDistinctView.SelectedNode.Text;
                    if (!String.IsNullOrEmpty(id))
                    {
                        DataTable dtDetails = detailsData[id];
                        this.dtGrdVwDetails.DataSource = dtDetails;
                        LoadTNForSelectedID(id, dtDetails);
                    }
                }

                //if (progressForm != null)
                //    progressForm.Close();

                lblStatus.Text = "Ready.";
                lblStatus.Image = null;
                //enable controls in parent form.
                ReportMainForm parentForm = (ReportMainForm)this.Parent;
                parentForm.ToggleMenus(true);
            }

            //BackgroundWorker backWorker = (BackgroundWorker)sender;
            //backWorker.ReportProgress(100);


        }

        private void distinctViewBackWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)
            {
                lblStatus.Image = (Image)UEI.Workflow2010.Report.Properties.Resources.loading;
                lblStatus.Text = "Loading data..";

                //disable controls in parent form.
                ReportMainForm parentForm = (ReportMainForm)this.Parent;
                parentForm.ToggleMenus(false);
                //progressForm = new UEI.Workflow2010.Report.Forms.ProgressForm();
                //progressForm.SetMessage("Please wait");
                //progressForm.HideCancelButton();
                //progressForm.Show();
            }
           

        }       
        private void LoadTNForSelectedID(String SelectedID, DataTable dtDetails)
        {
            this.treeViewTN.Nodes.Clear();
            //Dictionary<String, List<Model.LabelIntronSearchRecord>> m_TNList = reportService.GetTNforSelectedID(SelectedID);
            this.TNList = reportService.GetTNforSelectedID(SelectedID);
            if (this.TNList != null)
            {
                foreach (String item in this.TNList.Keys)
                {
                    if (dtDetails != null)
                    {
                        List<Model.LabelIntronSearchRecord> searchRelatedLabels = this.TNList[item];
                        if (searchRelatedLabels != null)
                        {
                            foreach (Model.LabelIntronSearchRecord findItem in searchRelatedLabels)
                            {
                                try
                                {
                                    if ((dtDetails.Select("Label='" + findItem.Label + "'").Length > 0) && (!treeViewTN.Nodes.ContainsKey(item)))
                                        treeViewTN.Nodes.Add(item, item);
                                }
                                catch { }
                            }                           
                        }                        
                    }                    
                }
            }
        }
        void treeViewTN_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {               
                TreeNode node = ((TreeNode)e.Node);
                if (node != null && !String.IsNullOrEmpty(node.Text))
                {
                    remoteimageViewer = new Forms.RemoteImageViewerForm();                                      
                    remoteimageViewer.RemoteImagePath = ViewRemotePicture(Int32.Parse(node.Text.ToString().Substring(2)));
                    if (this.TNList.ContainsKey(node.Text))
                        remoteimageViewer.RemoteDetails = this.TNList[node.Text];
                    else
                        remoteimageViewer.RemoteDetails = null;
                    remoteimageViewer.Show(this);
                }
            }
        }
        private void trVwDistinctView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
            {
                if (e.Node.Parent == null)
                    e.Cancel = true;
            }
        }
        void treeViewTN_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
            {
                if (e.Node.Parent == null)
                    e.Cancel = true;
            }
        }
        private string RemotePicturePath(int _tn)
        {
            /// use windows picture and fax viewer 
            /// to open the picture locate in
            /// G:\Lib_Capture\Img\PICTURES 
            string TNpic_path = ConfigurationSettings.AppSettings["TNPICPATH"];
            string strTN = "";

            if (_tn >= 90000)
            {
                strTN = "Tn" + (_tn).ToString();
                //TNpic_path += "TN90000-\\" + strTN + ".jpg";
                TNpic_path += "TN90000-\\";
            }
            else if (_tn >= 10000)
            {
                strTN = "Tn" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN" +
                    (_tn - tmp).ToString()
                    + "-TN" + (_tn - tmp + 999).ToString();
                //TNpic_path +=  tmp_str+ "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }
            else if (_tn >= 1000)
            {
                strTN = "Tn0" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN0" + (_tn - tmp).ToString()
                    + "-TN0" + (_tn - tmp + 999).ToString();
                //TNpic_path += tmp_str + "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }
            else
            {
                strTN = "Tn00" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN00000"
                    + "-TN00" + (_tn - tmp + 999).ToString();
                //TNpic_path += tmp_str + "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }

            List<string> file_list = FindFiles(TNpic_path, strTN);
            if (file_list.Count != 0)
                return file_list[0];
            else
                return string.Empty;
        }
        private List<string> FindFiles(string dir, string strTN)
        {
            List<string> file_list = new List<string>();
            DirectoryInfo tn_dir = new DirectoryInfo(dir);
            foreach (FileInfo fi in tn_dir.GetFiles(strTN + "*.jpg"))
            {
                file_list.Add(fi.FullName);
            }
            return file_list;
        }
        private String ViewRemotePicture(int _tn)
        {
            /// use windows picture and fax viewer 
            /// to open the picture locate in
            /// G:\Lib_Capture\Img\PICTURES 
            string TNpic_path = ConfigurationSettings.AppSettings["TNPICPATH"];
            string strTN = "";

            if (_tn >= 90000)
            {
                strTN = "Tn" + (_tn).ToString();
                TNpic_path += "TN90000-\\";
            }
            else if (_tn >= 10000)
            {
                strTN = "Tn" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN" +
                    (_tn - tmp).ToString()
                    + "-TN" + (_tn - tmp + 999).ToString();
                TNpic_path += tmp_str + "\\";
            }
            else if (_tn >= 1000)
            {
                strTN = "Tn0" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN0" + (_tn - tmp).ToString()
                    + "-TN0" + (_tn - tmp + 999).ToString();
                TNpic_path += tmp_str + "\\";
            }
            else
            {
                strTN = "Tn00" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN00000"
                    + "-TN00" + (_tn - tmp + 999).ToString();
                TNpic_path += tmp_str + "\\";
            }

            //Process myProcess = new Process();
            string IMGview_path = ConfigurationSettings.AppSettings["IMGVIEWPATH"];
            String result = String.Empty;
            try
            {
                //                List<string> file_list = FindFiles(TNpic_path, strTN);
                string[] images = Directory.GetFiles(TNpic_path, strTN + ".*",
                                                SearchOption.AllDirectories);
                if (images.Length > 0)
                {
                    result = images[0];
                    //myProcess.StartInfo.FileName = images[0];
                    //myProcess.StartInfo.Verb = "";
                    //myProcess.StartInfo.CreateNoWindow = true;
                    //myProcess.Start();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return result;
        }

        

       
    }
}
