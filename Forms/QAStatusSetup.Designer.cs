namespace UEI.Workflow2010.Report.Forms
{
    partial class QAStatusSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkProjectHold = new System.Windows.Forms.CheckBox();
            this.chkExecHold = new System.Windows.Forms.CheckBox();
            this.chkModified = new System.Windows.Forms.CheckBox();
            this.chkPassedQA = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkModifiedTNs = new System.Windows.Forms.CheckBox();
            this.rdoVerboseReport = new System.Windows.Forms.RadioButton();
            this.rdoIDListOnly = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Location = new System.Drawing.Point(5, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(407, 169);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select The Status Flags You Want To Track";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(4, 19);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(399, 146);
            this.splitContainer1.SplitterDistance = 196;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkProjectHold);
            this.groupBox2.Controls.Add(this.chkExecHold);
            this.groupBox2.Controls.Add(this.chkModified);
            this.groupBox2.Controls.Add(this.chkPassedQA);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(196, 146);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ID Status";
            // 
            // chkProjectHold
            // 
            this.chkProjectHold.AutoSize = true;
            this.chkProjectHold.Location = new System.Drawing.Point(36, 108);
            this.chkProjectHold.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkProjectHold.Name = "chkProjectHold";
            this.chkProjectHold.Size = new System.Drawing.Size(107, 21);
            this.chkProjectHold.TabIndex = 3;
            this.chkProjectHold.Text = "Project Hold";
            this.chkProjectHold.UseVisualStyleBackColor = true;
            // 
            // chkExecHold
            // 
            this.chkExecHold.AutoSize = true;
            this.chkExecHold.Location = new System.Drawing.Point(36, 80);
            this.chkExecHold.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkExecHold.Name = "chkExecHold";
            this.chkExecHold.Size = new System.Drawing.Size(93, 21);
            this.chkExecHold.TabIndex = 2;
            this.chkExecHold.Text = "Exec Hold";
            this.chkExecHold.UseVisualStyleBackColor = true;
            // 
            // chkModified
            // 
            this.chkModified.AutoSize = true;
            this.chkModified.Location = new System.Drawing.Point(36, 52);
            this.chkModified.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkModified.Name = "chkModified";
            this.chkModified.Size = new System.Drawing.Size(83, 21);
            this.chkModified.TabIndex = 1;
            this.chkModified.Text = "Modified";
            this.chkModified.UseVisualStyleBackColor = true;
            // 
            // chkPassedQA
            // 
            this.chkPassedQA.AutoSize = true;
            this.chkPassedQA.Checked = true;
            this.chkPassedQA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPassedQA.Location = new System.Drawing.Point(36, 23);
            this.chkPassedQA.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPassedQA.Name = "chkPassedQA";
            this.chkPassedQA.Size = new System.Drawing.Size(101, 21);
            this.chkPassedQA.TabIndex = 0;
            this.chkPassedQA.Text = "Passed QA";
            this.chkPassedQA.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkModifiedTNs);
            this.groupBox3.Controls.Add(this.rdoVerboseReport);
            this.groupBox3.Controls.Add(this.rdoIDListOnly);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(198, 146);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Report Options";
            // 
            // chkModifiedTNs
            // 
            this.chkModifiedTNs.AutoSize = true;
            this.chkModifiedTNs.Enabled = false;
            this.chkModifiedTNs.Location = new System.Drawing.Point(63, 80);
            this.chkModifiedTNs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkModifiedTNs.Name = "chkModifiedTNs";
            this.chkModifiedTNs.Size = new System.Drawing.Size(113, 21);
            this.chkModifiedTNs.TabIndex = 2;
            this.chkModifiedTNs.Text = "Modified TNs";
            this.chkModifiedTNs.UseVisualStyleBackColor = true;
            // 
            // rdoVerboseReport
            // 
            this.rdoVerboseReport.AutoSize = true;
            this.rdoVerboseReport.Location = new System.Drawing.Point(35, 50);
            this.rdoVerboseReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoVerboseReport.Name = "rdoVerboseReport";
            this.rdoVerboseReport.Size = new System.Drawing.Size(129, 21);
            this.rdoVerboseReport.TabIndex = 1;
            this.rdoVerboseReport.TabStop = true;
            this.rdoVerboseReport.Text = "Verbose Report";
            this.rdoVerboseReport.UseVisualStyleBackColor = true;
            this.rdoVerboseReport.CheckedChanged += new System.EventHandler(this.rdoVerboseReport_CheckedChanged);
            // 
            // rdoIDListOnly
            // 
            this.rdoIDListOnly.AutoSize = true;
            this.rdoIDListOnly.Checked = true;
            this.rdoIDListOnly.Location = new System.Drawing.Point(35, 22);
            this.rdoIDListOnly.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdoIDListOnly.Name = "rdoIDListOnly";
            this.rdoIDListOnly.Size = new System.Drawing.Size(101, 21);
            this.rdoIDListOnly.TabIndex = 0;
            this.rdoIDListOnly.TabStop = true;
            this.rdoIDListOnly.Text = "ID List Only";
            this.rdoIDListOnly.UseVisualStyleBackColor = true;
            this.rdoIDListOnly.CheckedChanged += new System.EventHandler(this.rdoIDListOnly_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(331, 182);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(253, 182);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(69, 28);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // QAStatusSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(416, 217);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QAStatusSetup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "QA Status Report Setup";
            this.groupBox1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkProjectHold;
        private System.Windows.Forms.CheckBox chkExecHold;
        private System.Windows.Forms.CheckBox chkModified;
        private System.Windows.Forms.CheckBox chkPassedQA;
        private System.Windows.Forms.CheckBox chkModifiedTNs;
        private System.Windows.Forms.RadioButton rdoVerboseReport;
        private System.Windows.Forms.RadioButton rdoIDListOnly;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
    }
}