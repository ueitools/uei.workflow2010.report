using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using UEI.Workflow2010.Report.Model;

namespace UEI.Workflow2010.Report
{
    public partial class DeviceCategoryForm : Form
    {
        private ReportMain m_dbmReport = null;
        public ReportMain DBMReport
        {
            get { return this.m_dbmReport; }
            set { this.m_dbmReport = value; }
        }
        private DeviceCategoryRecords deviceCategories = new DeviceCategoryRecords();
        public DeviceCategoryRecords Category
        {
            get { return deviceCategories; }
            set { deviceCategories = value; }
        }
        private Boolean withSubdevices = false;
        public Boolean WithSubdevices
        {
            get { return withSubdevices; }
            set { withSubdevices = value; }
        }
        private Boolean m_IncludeModeChar = false;
        public Boolean IncludeModeChar
        {
            get { return m_IncludeModeChar; }
            set { m_IncludeModeChar = value; }
        }
        public DeviceCategoryForm()
        {
            InitializeComponent();
            this.chkIncludeModeLetter.CheckedChanged += chkIncludeModeLetter_CheckedChanged;
            this.Load += new EventHandler(DeviceCategoryForm_Load);
        }

        void chkIncludeModeLetter_CheckedChanged(object sender, EventArgs e)
        {
            this.IncludeModeChar = chkIncludeModeLetter.Checked;
        }

        void DeviceCategoryForm_Load(object sender, EventArgs e)
        {
            if (DBMReport != null)
            {
                if (DBMReport.TypeOfReport == ReportType.BSC || DBMReport.TypeOfReport == ReportType.BSCPIC || DBMReport.TypeOfReport == ReportType.IDBrandList)
                    splitContainer3.Enabled = true;                
            }
            if (Category.Count > 0)
            {   
                dgDeviceCategory.DataSource = Category;
                dgDeviceCategory.Columns[3].Visible = false;
                btnOk.Enabled = true;
            }            
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.dgSubDevices.SelectedRows.Count > 0)
            {
                for (int i = 0; i < this.dgSubDevices.SelectedRows.Count; i++)
                {
                    DeviceCategory deviceCategory = Category.GetModeRecord(this.dgSubDevices.SelectedRows[i].Cells[0].Value.ToString());
                    deviceCategory.SubDevices.Add(this.dgSubDevices.SelectedRows[i].Cells[1].Value.ToString());
                }
            }            
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        Int32 intWorkseet = 1;
        private void dgDeviceCategory_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value) != String.Empty)
            {
                if (Category.Validate(dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value.ToString(), dgDeviceCategory.Rows[e.RowIndex].Cells[2].Value.ToString()))
                {
                    MessageBox.Show("Enter Alternate Worksheet Name.");
                    dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value = "";
                }
            }
            if (e.ColumnIndex == 2)
            {                
                if (Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == String.Empty || Convert.ToInt32(dgDeviceCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) <= 0)
                {
                    MessageBox.Show("Worksheet value must be a non-negative integer.");
                    dgDeviceCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = intWorkseet;
                }
                if (Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value) != String.Empty && Category.CheckMappingName(Convert.ToInt32(dgDeviceCategory.Rows[e.RowIndex].Cells[2].Value)))
                {
                    dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value = "";
                }                
            }
            
            Category = Category.SetMappingandWorkSheet(Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[0].Value),
                Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value), Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[2].Value));
        }
       
        private void dgDeviceCategory_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 0) 
                e.Cancel = true;
            if (e.ColumnIndex == 1 && Convert.ToString(dgDeviceCategory.Rows[e.RowIndex].Cells[1].Value) == String.Empty)
            {
                if(Category.CheckMappingName(Convert.ToInt32(dgDeviceCategory.Rows[e.RowIndex].Cells[2].Value)))
                    e.Cancel = true;
            }
            if (e.ColumnIndex == 2)
            {
                intWorkseet = Convert.ToInt32(dgDeviceCategory.Rows[e.RowIndex].Cells[2].Value);
            }
        }
        private void dgDeviceCategory_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {            
            if (dgDeviceCategory.Rows[e.RowIndex].IsNewRow) { return; }
            if (e.ColumnIndex == 1)
            {
                // Allows only Alpha-Numericís
                Char[] newAlphaNumeric = dgDeviceCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToCharArray();
                for (Int32 i = 0; i < newAlphaNumeric.Length; i++)
                {
                    if (!(Char.IsLetter(newAlphaNumeric[i]) || Char.IsDigit(newAlphaNumeric[i])))
                    {
                        e.Cancel = true;
                        dgDeviceCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
                        MessageBox.Show("Worksheet name must be Alpha Numeric.");
                        break;
                    }
                }                
            }
            if (e.ColumnIndex == 2)
            {
                // Allows only Numericís
                Int32 newInteger;
                if (!Int32.TryParse(e.FormattedValue.ToString(), out newInteger) || newInteger < 0)
                {
                    e.Cancel = true;
                    MessageBox.Show("Worksheet value must be a Non-Negative Numeric");
                }
            }            
        }  
        void BindSubDeviceFilter()
        {
            if (m_dbmReport != null)
            {
                DataTable dtsubdeviceclassification = new DataTable("SubDevices");
                DataRow dr = null;                
                dtsubdeviceclassification.Columns.Add("Mode");
                dtsubdeviceclassification.Columns.Add("Sub Device Type");
                switch (DBMReport.TypeOfReport)
                {
                    case ReportType.BSC:
                        IDBSCReportModel reportModel = (IDBSCReportModel)this.DBMReport.ReportObject;
                        IList<String> modeList = reportModel.GetDistinctModes();
                        foreach (String mode in modeList)
                        {
                            IList<String> subdeviceList = reportModel.GetDistinctSubDeviceTypes(mode);
                            dr = dtsubdeviceclassification.NewRow();
                           
                            foreach (String devices in subdeviceList)
                            {
                                if (dr == null)
                                    dr = dtsubdeviceclassification.NewRow();
                                dr[0] = mode;
                                dr[1] = devices;
                                dtsubdeviceclassification.Rows.Add(dr);
                                dr = null;
                            }
                        }
                        DataView dataView = new DataView(dtsubdeviceclassification);
                        dataView.Sort = "[Mode],[Sub Device Type]";
                        dtsubdeviceclassification = dataView.ToTable();
                        dtsubdeviceclassification.AcceptChanges();
                        break;
                    case ReportType.BSCPIC:
                        reportModel = (IDBSCReportModel)this.DBMReport.ReportObject;
                        modeList = reportModel.GetDistinctModes();
                        foreach (String mode in modeList)
                        {
                            IList<String> subdeviceList = reportModel.GetDistinctSubDeviceTypes(mode);
                            dr = dtsubdeviceclassification.NewRow();

                            foreach (String devices in subdeviceList)
                            {
                                if (dr == null)
                                    dr = dtsubdeviceclassification.NewRow();
                                dr[0] = mode;
                                dr[1] = devices;
                                dtsubdeviceclassification.Rows.Add(dr);
                                dr = null;
                            }
                        }
                        dataView = new DataView(dtsubdeviceclassification);
                        dataView.Sort = "[Mode],[Sub Device Type]";
                        dtsubdeviceclassification = dataView.ToTable();
                        dtsubdeviceclassification.AcceptChanges();
                        break;
                    case ReportType.IDBrandList:
                        IdBrandReportModel idBrandModel = (IdBrandReportModel)this.DBMReport.ReportObject;
                        //modeList = idBrandModel.GetDistinctModes();
                        modeList = idBrandModel.Modes;
                        foreach (string mode in modeList)
                        {
                           List<string> subDeviceList = idBrandModel.GetDistinctSubDeviceList(mode);
                           dr = dtsubdeviceclassification.NewRow();

                           foreach (String devices in subDeviceList)
                           {
                               if (dr == null)
                                   dr = dtsubdeviceclassification.NewRow();
                               dr[0] = mode;
                               dr[1] = devices;
                               dtsubdeviceclassification.Rows.Add(dr);
                               dr = null;
                           }
                        }
                        dataView = new DataView(dtsubdeviceclassification);
                        dataView.Sort = "[Mode],[Sub Device Type]";
                        dtsubdeviceclassification = dataView.ToTable();
                        dtsubdeviceclassification.AcceptChanges();
                        break;
                }
                this.dgSubDevices.DataSource = dtsubdeviceclassification;
                if (dtsubdeviceclassification.Rows.Count > 0)
                {
                    this.dgSubDevices.SelectAll();
                    this.dgSubDevices.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void rdowithSubdevices_CheckedChanged(object sender, EventArgs e)
        {
            if (rdowithSubdevices.Checked == true)
            {
                this.dgSubDevices.Enabled = true;
                BindSubDeviceFilter();
                withSubdevices = true;
            }
            else
            {
                if (this.dgSubDevices.Rows.Count > 0)
                {
                    this.dgSubDevices.DataSource = null;
                    this.dgSubDevices.Rows.Clear();
                    this.dgSubDevices.Enabled = false;
                }
                withSubdevices = false;
            }
        }       
        private void dgSubDevices_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                this.dgSubDevices.Rows[e.RowIndex].Selected = true;
            }
        }             
    }   
}