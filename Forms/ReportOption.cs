using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.Workflow2010.Report
{
    public partial class ReportOption : Form
    {
        public ReportOption()
        {
            InitializeComponent();            
        }
        //Report Option
        //False : Without Device Type (Default)
        //True  : With Device Type
        private Boolean m_ReportOption = false;
        public Boolean ReportOptions
        {
            get { return m_ReportOption; }
            set { m_ReportOption = value; }
        }
        private void rdoWithoutDeviceType_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoWithoutDeviceType.Checked)
            {
                this.ReportOptions = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.ReportOptions = true;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void rdoWithDeviceType_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoWithDeviceType.Checked)
            {
                this.ReportOptions = true;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.ReportOptions = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }	
    }
}