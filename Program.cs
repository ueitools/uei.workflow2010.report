using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
namespace UEI.Workflow2010.Report
{
    static class Program
    {
        #region Single Instance of Application
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        public static bool HasPriorInstance()
        {
            Process currentProcess = Process.GetCurrentProcess();
            string fileName = currentProcess.StartInfo.FileName;
            foreach (Process process in Process.GetProcessesByName(currentProcess.ProcessName))
            {
                if (process.Id == currentProcess.Id) { continue; }
                if (process.StartInfo.FileName != fileName) { continue; }
                SetForegroundWindow(process.MainWindowHandle);
                return true;
            }
            return false;
        }
        #endregion

        #region The main entry point for the application.
        [STAThread]
        static void Main()
        {
            //if (!HasPriorInstance())
            //{
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new ReportMainForm());
            //}
        }
        #endregion
    }
}