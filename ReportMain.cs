using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Model;
using CommonForms;
using System.ComponentModel;
using System.Data;
using UEI.Workflow2010.Report.Service;
using UEI.Workflow2010.Report.ExportToText;
using UEI.Workflow2010.Report.ExportToExcel;
using UEI.Workflow2010.Report.ExportToPDF;
using System.IO;

namespace UEI.Workflow2010.Report
{
    public class ReportMain
    {
        #region Variables
        private Boolean m_IncludeBand = false;
        private Boolean m_IncludeLoadPercentage = false;
        private Boolean m_UseDataBase = true;
        private Boolean m_IncludeIntron = true;
        private IList<String> m_IdListforReport = null;
        private IList<String> m_KeepIdList = new List<String>();
        private IList<Id> m_ids = new List<Id>();
        private IList<Region> regions = new List<Region>();
        private IList<Country> countries = new List<Country>();
        private IList<Region> subregions = new List<Region>();
        private IList<DeviceType> deviceTypes = new List<DeviceType>();
        public List<String> selectedModes = new List<String>();
        private IList<DeviceType> subdevicetypes = new List<DeviceType>();
        private IList<Model.Component> components = new List<Model.Component>();
        private IList<DataSources> dataSources = new List<DataSources>();
        public String DeviceAliasRegion = String.Empty;
        public String DeviceTypeFlag = "N";
        private IList<String> selectedIDs = null;
        private IdSetupCodeInfo _IDSetupCodeInfoParams = null;
        private QuickSetInfo _QuickSetInfoParams = null;
        private BrandBasedSeach m_brandSearchOptions = null;
        private QAStatus m_QAStatus = null;
        private BrandFilter _brandFilter = null;
        private ModelFilter _modelFilter = null;
        private ModelInfoSpecific _modelInfoSpecificFilters = null;        
        private Int32 msgExportToExcel = 0;
        private Boolean RemoveIDSForSelectedSubDevices = false;
        private CommonForms.IDSelectionForm frmIDSelection = null;
        private System.ComponentModel.BackgroundWorker backGroundWorker = new System.ComponentModel.BackgroundWorker();
        private Forms.ProgressForm progress = null;
        private ReportMainForm _MdiParent = null;
        //private Form frmSearchResult = null;
        private bool isDataSaved = false;
        private Dictionary<string, bool> _ConsolidateReportItems = null;
        private bool exceptionOccuredFlag = false;
        private SelType SelectedType = SelType.MODE;
        #endregion

        #region labelintronreport
        private LabelIntronDistinctView distinctView = null;
        private string _LabelIntronPattern = String.Empty;
        private LabelIntronFilters _LabelIntronFilters = null;
        
        #endregion

        #region BrandModelSearch
        private List<string> brands = null;
        private List<string> models = null;
        private ModelInfoSpecific.IsTargetFilter isTargetFilter= ModelInfoSpecific.IsTargetFilter.ShowTargetAndRemote;
        #endregion                

        #region Properties
        private string _saveFailMessage = String.Empty;
        public string SaveFailMessage
        {
            get { return _saveFailMessage; }
        }
        private ReportType reportType;
        public ReportType TypeOfReport
        {
            get { return this.reportType; }
        }
        private Object _ReportObject = null;
        public Object ReportObject
        {
            get { return this._ReportObject; }
        }
        private IDModeRecordModel allModeNames = new IDModeRecordModel();
        public IDModeRecordModel ModeNames
        {
            get { return allModeNames; }
            set { allModeNames = value; }
        }
        private Int32 percentIncrement = 0;
        public int PercentIncrement
        {
            get
            {
                return percentIncrement;
            }
            set
            {
                percentIncrement = value;
            }
        }
        private bool _ReportCloseStatus = true;
        public bool ReportCloseStatus
        {
            get { return this._ReportCloseStatus; }
        }
        private Int32 m_FirstRange = 0;
        public Int32 FirstRange
        {
            get { return m_FirstRange; }
            set { m_FirstRange = value; }
        }
        private Int32 m_LastRange = -1;
        public Int32 LastRange
        {
            get { return m_LastRange; }
            set { m_LastRange = value; }
        }
        public int KeyFunctionReportType { get; set; }
        #endregion

        #region Constructor
        public ReportMain() { }
        #endregion

        #region To be used in Pick Manager
        public ReportMain(ReportType _ReportType,Form _parent, IList<String> idlList, String ProjectName)
        {
            ErrorLogging.WriteToLogFile("Report Name:" + Convert.ToString(_ReportType), false);
            _MdiParent = (ReportMainForm)_parent;
            reportType = _ReportType;
            backGroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(backGroundWorker_DoWork);
            backGroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backGroundWorker_RunWorkerCompleted);
            switch (reportType)
            {
                case ReportType.IDList:
                    IDList();
                    break;
                case ReportType.BSCPIC:
                    BSCIDList(idlList, ProjectName);
                    break;
                case ReportType.IDBrandList:
                    ShowIDBrandReportInputForms();
                    break;
                case ReportType.MODELINFOPIC:
                    GetModelInformationInputs(idlList, ProjectName);
                    break;
            }
        }
        #endregion

        #region Used in Work Flow 2010
        public ReportMain(ReportType _ReportType, Form _parent)
        {
            ErrorLogging.WriteToLogFile("--------------------------------------", false);
            ErrorLogging.WriteToLogFile("Report Name:" + Convert.ToString(_ReportType), false);
            ErrorLogging.WriteToLogFile("--------------------------------------", false);
            
            //if (frmIDSelection == null)
            //    frmIDSelection = new IDSelectionForm();

            if (_IDSetupCodeInfoParams == null)
                _IDSetupCodeInfoParams = new IdSetupCodeInfo();
            
            _MdiParent = (ReportMainForm)_parent;
                       
            reportType = _ReportType;
            backGroundWorker.DoWork +=new System.ComponentModel.DoWorkEventHandler(backGroundWorker_DoWork);
            backGroundWorker.RunWorkerCompleted +=new System.ComponentModel.RunWorkerCompletedEventHandler(backGroundWorker_RunWorkerCompleted);
            //get the child form instance
            //frmSearchResult = GetChildInstance();
            //frmSearchResult.MdiParent = _MdiParent;            
            switch (reportType)
            {               
                case ReportType.IDList:
                    IDList();
                    break;             
                case ReportType.BSC:
                    BSCIDList();
                    break;
                case ReportType.ModelInfoWithArdKey:
                    GetModelInformationInputs_ArdKey();
                    break;
                case ReportType.QuickSet:
                    GetQuickSetInputs();
                    break;
                case ReportType.KeyFunction:
                    GetKeyFunctionInput();
                    break;
                case ReportType.IDBrandList:
                    ShowIDBrandReportInputForms();
                    break;
                case ReportType.ModelInformation:
                    GetModelInformationInputs();
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    GetModelInformationInputs_EmptyDevice();
                    break;
                case ReportType.EXIDTNInfo:
                    ShowEXIDTNInfoReportForm();
                    break;
                case ReportType.RestrictedIDList:
                    GenerateRestrictedIDReport();
                    break;
                case ReportType.QASTATUS:
                    GetQAStatus();
                    break;
                case ReportType.EMPTY_ID:
                    GetEmptyNonEmptyIDReportInput(true);
                    break;
                case ReportType.NONEMPTY_ID:
                    GetEmptyNonEmptyIDReportInput(false);
                    break;
                case ReportType.SupportedIDByPlatform:
                    GetSupportedIdsByPlatform();
                    break;
                case ReportType.UNIQUE_FUNC_IR:
                    GetUniqueFunctionsIR();
                    break;
                case ReportType.DISCONNECTED_TNS:
                    GetDisconnectedTNs();
                    break;
                case ReportType.LabelSearch:
                    GetLabelSearchInputs();
                    break;
                case ReportType.IntronSearch:
                    GetIntronSearchInputs();
                    break;
                case ReportType.ExcludeLabelSearch:
                    GetExcludeLabelSearchInputs();
                    break;
                case ReportType.ExcludeIntronSearch:
                    GetExcludeIntronSearchInputs();
                    break;
                case ReportType.IntronSearchWithBrand:
                    GetIntronLabelSearchWithBrandInputs();
                    break;
                case ReportType.LabelSearchWithBrand:
                    GetIntronLabelSearchWithBrandInputs();
                    break;
                case ReportType.BrandBasedSearch:
                    GetBrandBasedSearchResult();
                    break;
                case ReportType.BrandModelBasedSearch:
                    GetBrandModelSearchInputs();
                    break;
                case ReportType.BrandSearch:
                    GetBrandSearchInput();
                    break;
                case ReportType.MiscReport:
                    GetConsolidatedDataInputs();
                    break;
                case ReportType.ExecIdMap:
                    GetExecIdMapInputs();
                    break;
                case ReportType.ExecIdPrefixSame:
                    GetExecIdPrefixSameInputs();
                    break;
                case ReportType.SYNTHESIZER_TABLE:
                    GetSynthesizerTableReportInput();
                    break;
                case ReportType.DUPLICATE_INTRONS:
                    GetDuplicateIntronReportInput();
                    break;
                case ReportType.BRAND_TABLE_ROM:
                    GetBrandTableRomReportInput();
                    break;
                case ReportType.LocationSearch:
                    GetLocationByLoad();
                    break;
            }
        }
        #endregion
        
        #region Backgroundworker
        private void backGroundWorker_DoWork(Object sender, DoWorkEventArgs e)
        {
            switch (reportType)
            {
                case ReportType.IDList:
                    this._ReportObject = GetIDByRegionReport();
                    break;
                case ReportType.BSC:
                    this._ReportObject = GetBSCReport();
                    break;
                case ReportType.BSCPIC:
                    this._ReportObject = GetPICBSCReport();
                    break;
                case ReportType.ModelInfoWithArdKey:
                    this._ReportObject = GetModelInformationReport_ArdKey();
                    break;
                case ReportType.QuickSet:
                    this._ReportObject = GetQuickSetReport();
                    break;
                case ReportType.KeyFunction:
                    this._ReportObject = GetKeyFunctionReport();
                    break;
                case ReportType.IDBrandList:
                    this._ReportObject = GetIDBrandReport();
                    break;
                case ReportType.ModelInformation:
                    this._ReportObject = GetModelInformationReport();
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    this._ReportObject = GetModelInformationReport_EmptyDevice();
                    break;
                case ReportType.MODELINFOPIC:
                    this._ReportObject = GetModelInformationReport();
                    break;
                case ReportType.RestrictedIDList:
                    this._ReportObject = GetRestrictedIDs();
                    break;                    
                case ReportType.QASTATUS:
                    this._ReportObject = GetQAStatusReport();
                    break;
                case ReportType.EMPTY_ID:
                    this._ReportObject = GetEmptyNonEmptyIDReport(true);
                    break;
                case ReportType.NONEMPTY_ID:
                    this._ReportObject = GetEmptyNonEmptyIDReport(false);
                    break;                    
                case ReportType.UNIQUE_FUNC_IR:
                    this._ReportObject = GetUniqueFunctionsIRReport();
                    break;
                case ReportType.SupportedIDByPlatform:
                    this._ReportObject = GetSupportedIdByPlatformReport();
                    break;
                case ReportType.DISCONNECTED_TNS:
                    this._ReportObject = GetDisconnectedTNReport();
                    break;
                case ReportType.LabelSearch:
                    this._ReportObject = GetLabelSearchReport();
                    break;
                case ReportType.IntronSearch:
                    this._ReportObject = GetIntronSearchReport();
                    break;
                case ReportType.ExcludeLabelSearch:
                    this._ReportObject = GetExcludeLabelSearchReport();
                    break;
                case ReportType.ExcludeIntronSearch:
                    this._ReportObject = GetExcludeIntronSearchReport();
                    break;
                case ReportType.IntronSearchWithBrand:
                    this._ReportObject = GetLabelIntronSearchWithBrandReport();
                    break;
                case ReportType.LabelSearchWithBrand:
                    this._ReportObject = GetLabelIntronSearchWithBrandReport();
                    break;
                case ReportType.BrandBasedSearch:
                    this._ReportObject = GetBrandBasedSearchReport();
                    break;
                case ReportType.BrandModelBasedSearch:
                    this._ReportObject = GetBrandModelSearchResults();
                    break;
                case ReportType.BrandSearch:
                    this._ReportObject = GetBrandSearchResult();
                    break;
                case ReportType.MiscReport:
                    this._ReportObject = GenerateConsolidatedReport();
                    break;
                case ReportType.ExecIdMap:
                    this._ReportObject = GetExecIdMap();
                    break;
                case ReportType.ExecIdPrefixSame:
                    this._ReportObject = GetExecIdWithSamePrefix();
                    break;
                case ReportType.SYNTHESIZER_TABLE:
                    this._ReportObject = GetSynthesizerTableReport();
                    break;
                case ReportType.DUPLICATE_INTRONS:
                    this._ReportObject = GetDuplicateIntronReport();
                    break;
                case ReportType.BRAND_TABLE_ROM:
                    GetBrandTableRomReport();
                    break;
                case ReportType.LocationSearch:
                    this._ReportObject = GetLocationsByLoadReport();
                    break;
            }
        }        
        private void backGroundWorker_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {            
            progress.DialogResult = DialogResult.OK;
            progress.Close();           
        }
        private bool userCancellationFlag = false;
        private void ResetTimer()
        {
            if (backGroundWorker != null || backGroundWorker.IsBusy == false)
                backGroundWorker.RunWorkerAsync();

            if (progress == null)
                progress = new Forms.ProgressForm();
            progress.SetMessage("Please Wait...");
            progress.ShowInTaskbar = false;            
            progress.ShowDialog();
            if (progress.DialogResult == DialogResult.Cancel)
            {
                userCancellationFlag = true;
            }
        }       
        #endregion        

        #region Brand Based Search Report
        private void GetBrandBasedSearchResult()
        {
            try
            {
                IList<String> idlList = GetIDs();
                if (idlList != null && idlList.Count > 0)
                {
                    ReportFilterForm filterWizard = new ReportFilterForm(ReportType.BrandBasedSearch, idlList);
                    filterWizard.SelectedType = SelectedType;
                    DialogResult result = filterWizard.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        m_ids                   = filterWizard.Ids;
                        regions                 = filterWizard.Regions;
                        deviceTypes             = filterWizard.DeviceTypes;
                        selectedModes           = filterWizard._selectedModes;
                        dataSources             = filterWizard.DataSources;
                        m_brandSearchOptions    = filterWizard.brandbasedSeach;
                        _IDSetupCodeInfoParams  = filterWizard.IdSetupCodeInfoParam;
                        filterWizard.Close();
                        ResetTimer();
                        ShowReport("Brand Based Search");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }
        }
        private BrandBasedSearchModel GetBrandBasedSearchReport()
        {
            Reports report = new Reports();
            BrandBasedSearchModel brandbasedSearchmodel = report.GetBrandBasedSearchResult(m_ids, regions, selectedModes, deviceTypes, dataSources,
                m_brandSearchOptions,_IDSetupCodeInfoParams);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            return brandbasedSearchmodel;            
        }
        #endregion

        #region Brand Table Rom Report
        public IList<String> BrandTableRomIDList                = new List<String>();
        public IList<String> BrandTableRomFromIDList            = new List<String>();
        public IList<ModeBrandItem> BrandTableRomBrandList      = new List<ModeBrandItem>();
        private String OutputFilePath                           = String.Empty;
        private String MarketList                               = String.Empty;
        private void GetBrandTableRomReportInput()
        {
            try
            {
                ROMBrandTable m_ROMBrandTableForm = new ROMBrandTable();
                DialogResult result = m_ROMBrandTableForm.ShowDialog();
                if(result == DialogResult.OK)
                {
                    BrandTableRomIDList = m_ROMBrandTableForm.IDList;
                    BrandTableRomFromIDList = m_ROMBrandTableForm.FromIDList;
                    BrandTableRomBrandList = m_ROMBrandTableForm.BrandList;
                    OutputFilePath  = m_ROMBrandTableForm.OutputFilePath;
                    MarketList      = m_ROMBrandTableForm.MarketList;
                    m_ROMBrandTableForm.Close();
                    ResetTimer();
                    //ShowReport("Band ROM Table Report");
                    if(File.Exists(OutputFilePath))
                        File.Delete(OutputFilePath);
                    StreamWriter rpt = File.AppendText(OutputFilePath);
                    WriteTable1(rpt);
                    WriteTable2(rpt);
                    WriteTable3(rpt);
                    WriteTable4(rpt);
                    rpt.Close();
                    System.Diagnostics.Process.Start(OutputFilePath);
                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }
        }
        private void GetBrandTableRomReport()
        {
            //Lookup Brand Numbers
            Reports m_BrandTableRomReport = new Reports();
            BrandTableRomBrandList = m_BrandTableRomReport.GetLookupBrandNumbers(BrandTableRomBrandList);
            GetBrandIds();
            this._ReportObject = BrandTableRomBrandList;
        }
        private void GetBrandIds()
        {
            elcinsql_Model.ModelWS WebBMS = new elcinsql_Model.ModelWS();
            WebBMS.Url = CommonForms.WorkflowConfig.GetCfg("ModelWSv2");
            elcinsql_Model.UEIDeviceCodeListEx result;
            foreach(ModeBrandItem list in BrandTableRomBrandList)
            {
                foreach(BrandItem item in list.BrandInfo)
                {
                    String m_Branch = "CA|CY|CF:BSkyB Web Tool|CF:Captured|CF:CCF Capture|CF:Classic|CF:CME|CF:CME 1999|CF:Combined Code|CF:Complemented ID|CF:Customer|CF:Distributor|CF:Event|CF:Field Capture|CF:Field Info|CF:GFK|CF:JP1|CF:Konig|CF:Manual|CF:Manufacturer|CF:New Konig|CF:Not defined in CF|CF:OEM Product|CF:OFA Web Tool|CF:PCF Capture|CF:Picture|CF:Pilot|CF:POS Data AS|CF:POS Data EU|CF:Small Cap|CF:Spec Capture|CF:Supplier Info|CF:UEIC|CF:UK|CF:US Capture|CF:USB Cap|CF:Visa|CF:Web info|CF:Zbase Cap|CF:ZBase Xref";
                    //result = WebBMS.GetBSCList(item.Brand, list.Mode, MarketList, "CA|CY|EU", 5000, 0);
                    result = WebBMS.GetBSCList(item.Brand,
                                                list.Mode,
                                                String.Empty,
                                                String.Empty,
                                                String.Empty,
                                                String.Empty,
                                                MarketList,
                                                String.Empty,
                                                String.Empty,
                                                m_Branch,
                                                5000,
                                                0);
                    IList<String> List = new List<string>();
                    foreach(elcinsql_Model.UEIDeviceCode id in result.codeList)
                    {
                        if(!BrandTableRomFromIDList.Contains(id.code))
                            continue;
                        if(!List.Contains(id.code))
                            List.Add(id.code);
                    }
                    // prioritise the ID list if given
                    if(item.IDPriority != null && item.IDPriority.Count > 0)
                        for(int idx = item.IDPriority.Count - 1; idx >= 0; idx--)
                            if(List.Contains(item.IDPriority[idx]))
                            {
                                List.Remove(item.IDPriority[idx]);
                                List.Insert(0, item.IDPriority[idx]);
                            }
                    list.BrandInfo[list.BrandInfo.IndexOf(item)].IDList = List;
                }
            }
        }
        private void WriteTable1(StreamWriter rpt)
        {
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(";         Manufacturer code look up table                               ;");
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");
            rpt.WriteLine("          public DEVBrandTable          ;Device Brand Table");
            rpt.WriteLine("DEVBrandTable:                          ;");
            rpt.WriteLine(string.Format("          db     {0,-2}                     ;", BrandTableRomBrandList.Count));
            foreach(ModeBrandItem mode in BrandTableRomBrandList)
            {
                rpt.WriteLine(string.Format("          dw     {0}_Mode                 ;", mode.Mode));
                rpt.WriteLine(string.Format("          dw     {0}_Brand                ;", mode.Mode));
            }
            rpt.WriteLine(" ");
        }
        private void WriteTable2(StreamWriter rpt)
        {
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");

            foreach(ModeBrandItem mode in BrandTableRomBrandList)
            {
                int brandcount = 0;
                foreach(BrandItem item in mode.BrandInfo)
                    if(item.IDList.Count > 0) brandcount++;

                rpt.WriteLine(string.Format("{0}_Brand:                                ;", mode.Mode));
                rpt.WriteLine(string.Format("          db     {0,-2}                     ;", brandcount));
                foreach(BrandItem item in mode.BrandInfo)
                {
                    if(item.IDList.Count == 0)
                        continue;
                    rpt.WriteLine(
                        string.Format("          dw     {0:D4}                   ;{1}",
                                                        item.BrandNum, item.Brand));
                }
                rpt.WriteLine(" ");
                foreach(BrandItem item in mode.BrandInfo)
                {
                    if(item.IDList.Count == 0)
                        continue;
                    rpt.WriteLine(
                        string.Format("          dw     B{0}{1:D4}                 ;{2}",
                                                        mode.Mode, item.BrandNum, item.Brand));
                }
            }
            rpt.WriteLine(" ");
        }
        private void WriteTable3(StreamWriter rpt)
        {
            IList<int> BNum = new List<int>();
            IList<string> BName = new List<string>();

            foreach(ModeBrandItem mode in BrandTableRomBrandList)
            {
                foreach(BrandItem brand in mode.BrandInfo)
                {
                    if(brand.IDList.Count == 0)
                        continue;
                    if(!BNum.Contains(brand.BrandNum))
                    {
                        BNum.Add(brand.BrandNum);
                        BName.Add(brand.Brand);
                    }
                }
            }

            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");
            rpt.WriteLine("          public BrandTable             ;Manufacture table");
            rpt.WriteLine("BrandTable:");
            rpt.WriteLine(string.Format("          dw     {0}", BNum.Count));
            for(int idx = 0; idx < BNum.Count; idx++)
            {
                rpt.WriteLine(
                    string.Format("          dw     {0:D4}                   ;{1}", BNum[idx], BName[idx]));
            }
            rpt.WriteLine(" ");
            for(int idx = 0; idx < BNum.Count; idx++)
            {
                rpt.WriteLine(
                    string.Format("          dw     BName{0:D4}              ;{1}", BNum[idx], BName[idx]));
            }
            rpt.WriteLine(" ");
            rpt.WriteLine("BrandTableEnd::");
            rpt.WriteLine(" ");
            for(int idx = 0; idx < BNum.Count; idx++)
            {
                rpt.WriteLine(
                    string.Format("BName{0:D4}:       .asciz            \"{1}\"", BNum[idx], BName[idx]));
            }
            rpt.WriteLine(" ");
        }
        private void WriteTable4(StreamWriter rpt)
        {
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");
            foreach(ModeBrandItem mode in BrandTableRomBrandList)
            {
                foreach(BrandItem item in mode.BrandInfo)
                {
                    if(item.IDList.Count == 0)
                        continue;

                    rpt.WriteLine(
                        string.Format("B{0}{1:D4}:                                 ;    {2}",
                        mode.Mode, item.BrandNum, item.Brand));
                    rpt.WriteLine(string.Format("          db     {0}", item.IDList.Count));
                    foreach(string id in item.IDList)
                    {
                        rpt.WriteLine(
                            string.Format("          dw     {0}+{1}_Mode            ;{2}",
                            id.Substring(1),
                            id[0],
                            id));
                    }
                    rpt.WriteLine(" ");
                }
            }
        }
        #endregion

        #region Synthesizer Table Report
        private void GetSynthesizerTableReportInput()
        {
            try
            {
                IDSelectionForm m_IdSelectionForm = new IDSelectionForm();
                DialogResult result = m_IdSelectionForm.ShowDialog();
                if(result == DialogResult.OK)
                {
                    SynthesizerSelection m_SynthesizerSelection = new SynthesizerSelection();
                    result = m_SynthesizerSelection.ShowDialog();
                    if(result == DialogResult.OK)
                    {
                        m_UseDataBase = m_SynthesizerSelection.UseDataBase;
                        m_IncludeIntron = m_SynthesizerSelection.IncludeIntrons;
                        m_SynthesizerSelection.Close();
                        IList<String> idLIst = GetIdListforSynthesizerTableReport();
                        if(idLIst != null)
                        {
                            m_IdListforReport = m_IdSelectionForm.GetSelectedIDList(idLIst);
                            foreach(String id in m_IdListforReport)
                            {
                                if(!selectedModes.Contains(id.Substring(0, 1).ToUpper()))
                                {
                                    selectedModes.Add(id.Substring(0, 1).ToUpper());
                                }
                            }
                            m_IdSelectionForm.Close();                            
                            ResetTimer();
                            ShowReport("Synthesizer Table Report");
                        }
                        else
                        {
                            MessageBox.Show("No Ids are Found");
                        }
                    }
                }              
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }
        }
        private IList<String> GetIdListforSynthesizerTableReport()
        {
            try
            {
                Reports reports = new Reports();
                IList<String> idList = reports.GetIdListforSynthesizerTableReport(m_UseDataBase);                
                return idList;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        private SynthesizerTableModel GetSynthesizerTableReport()
        {
            try
            {
                Reports reports = new Reports();
                Model.SynthesizerTableModel SynthesizerTableReport = reports.GetSynthesizerTableReport(m_IdListforReport, m_IncludeIntron, selectedModes);
                //Get All Mode Names
                this.allModeNames = reports.GetAllModeNames();
                return SynthesizerTableReport;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        #endregion

        #region Duplicate Intron Report
        private void GetDuplicateIntronReportInput()
        {
            try
            {
                IDSelectionForm m_IdSelectionForm = new IDSelectionForm();
                DialogResult result = m_IdSelectionForm.ShowDialog();
                if(result == DialogResult.OK)
                {
                    IList<String> idList = GetIdListforDuplicateIntronReport();
                    if(idList != null)
                    {
                        m_IdListforReport = m_IdSelectionForm.GetSelectedIDList(idList);
                        foreach(String id in m_IdListforReport)
                        {
                            if(!selectedModes.Contains(id.Substring(0, 1).ToUpper()))
                            {
                                selectedModes.Add(id.Substring(0, 1).ToUpper());
                            }
                        }
                        m_IdSelectionForm.Close();
                        ResetTimer();
                        ShowReport("Duplicate Intron Report");
                    }
                    else
                    {
                        MessageBox.Show("No Ids are Found");
                    }

                }
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }
        }
        private IList<String> GetIdListforDuplicateIntronReport()
        {
            try
            {
                Reports reports = new Reports();
                IList<String> idList = reports.GetIdListforDuplcaiteIntronReport();
                return idList;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        private LabelIntronSearchReport GetDuplicateIntronReport()
        {
            try
            {
                Reports reports = new Reports();
                Model.LabelIntronSearchReport DuplicateIntronReport = reports.GetDuplicateIntronReport(m_IdListforReport);
                //Get All Mode Names
                this.allModeNames = reports.GetAllModeNames();
                return DuplicateIntronReport;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        #endregion

        #region Disconnected TN
        private void GetDisconnectedTNs()
        {
            Report.Forms.DisconnectedTNOption disconnectedtn = new Report.Forms.DisconnectedTNOption();
            if (disconnectedtn.ShowDialog() == DialogResult.OK)
            {
                FirstRange = disconnectedtn.FirstRange;
                LastRange = disconnectedtn.LastRange;
                ResetTimer();
                ShowReport("Disconnected TN Report");
            }
        }
        private IList<Model.TNInfo> GetDisconnectedTNReport()
        {
            Reports disconnectedtns = new Reports();
            return(disconnectedtns.GetDisconnectedTNReport(FirstRange,LastRange));
        }
        #endregion

        #region Unique Functions IR
        private void GetUniqueFunctionsIR()
        {
            ResetTimer();
            ShowReport("Unique Function/IR Report");
        }
        private Model.UniqueFuncIRCountModel GetUniqueFunctionsIRReport()
        {
            Reports uniqueFuncsIR = new Reports();
            return(uniqueFuncsIR.GetUniqueFunctionsIR());
        }
        #endregion

        #region Get QA Status Report
        private QAStatusReportModel GetQAStatusReport()
        {
            Reports report = new Reports();
            QAStatusReportModel qastatusreport = report.GetQAStatusReport(m_QAStatus);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            return qastatusreport;
        }
        private void GetQAStatus()
        {
            Report.Forms.QAStatusSetup QASetup = new Report.Forms.QAStatusSetup();
            
            if (QASetup.ShowDialog() == DialogResult.OK)
            {
                m_QAStatus = new QAStatus();
                m_QAStatus.FlagQ = QASetup.FlagQ;
                m_QAStatus.FlagM = QASetup.FlagM;
                m_QAStatus.FlagP = QASetup.FlagP;
                m_QAStatus.FlagE = QASetup.FlagE;
                m_QAStatus.Verbose = QASetup.Verbose;
                m_QAStatus.TNStatus = QASetup.TNStatus;
                ResetTimer();
                ShowReport("QA Status Report");
            }
        }
        #endregion

        #region ID Selection
        public void IDList()
        {           
            try
            {
                IList<string> idlList = GetIDs();
                if (idlList != null && idlList.Count > 0)
                {
                    m_KeepIdList = new List<String>();
                    m_KeepIdList = idlList;

                    ReportFilterForm _filterWizard = new ReportFilterForm(ReportType.IDList, idlList);
                    _filterWizard.SelectedType = SelectedType;
                    DialogResult result = _filterWizard.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        m_ids                   = _filterWizard.Ids;
                        regions                 = _filterWizard.Regions;                        
                        deviceTypes             = _filterWizard.DeviceTypes;
                        selectedModes           = _filterWizard._selectedModes;                       
                        dataSources             = _filterWizard.DataSources;
                        _IDSetupCodeInfoParams  = _filterWizard.IdSetupCodeInfoParam;
                        _filterWizard.Close();
                        if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            SaveCurrentSelections();
                        }
                        ResetTimer();
                        ShowReport("ID Selection");
                    }
                }
                
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }            
        }
        private IDByRegionReportModel GetIDByRegionReport()
        {
            IncludeUnsupportedIdByPlatform();
            Reports report = new Reports();
            IDByRegionReportModel idByRegions = report.IDByRegionandSubDevice(m_ids, regions, selectedModes,deviceTypes, 
                dataSources, _IDSetupCodeInfoParams);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            //ErrorLogging.WriteToLogFile("******************************", false);
            return idByRegions;
        }
        #endregion

        #region Include Unsupported Id by Platform
        private DataSet GetSupportedIdByPlatformReport()
        {
            IncludeUnsupportedIdByPlatform();
            DataSet dsIDList = new DataSet("IDs");
            dsIDList.Tables.Add("SupportedIDByPlatform");
            dsIDList.Tables[0].Columns.Add("ID");
            dsIDList.AcceptChanges();
            if (this.m_ids.Count > 0)
            {
                DataRow dr = null;
                foreach (Id item in this.m_ids)
                {
                    dr = dsIDList.Tables[0].NewRow();
                    dr[0] = item.ID;
                    dsIDList.Tables[0].Rows.Add(dr);
                    dr = null;
                    if (m_KeepIdList != null)
                    {
                        if (m_KeepIdList.Contains(item.ID))
                        {
                            m_KeepIdList.Remove(item.ID);
                        }
                    }
                }
                dsIDList.AcceptChanges();
            }
            return dsIDList;
        }
        private void GetSupportedIdsByPlatform()
        {
            try
            {
                IList<String> idList = GetIDs();
                if (idList != null && idList.Count > 0)
                {
                    m_KeepIdList = new List<String>();
                    m_KeepIdList = idList;
                    SupportedIDByPlatformForm _SupportedIDByPlatformForm = new SupportedIDByPlatformForm();
                    DialogResult result = _SupportedIDByPlatformForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        this.m_ids = new List<Id>();
                        foreach (String item in idList)
                        {
                            this.m_ids.Add(new Id(item));
                        }
                        _IDSetupCodeInfoParams = _SupportedIDByPlatformForm.Parameter;
                        _SupportedIDByPlatformForm.Close();                       
                        ResetTimer();
                        ShowReport("Supported Ids By Platform");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            } 
        }
        private void IncludeUnsupportedIdByPlatform()
        {
            if (m_ids.Count>0)
            {
                if (_IDSetupCodeInfoParams != null)
                {
                    if (_IDSetupCodeInfoParams.IncludeUnsupportedPlatforms == false && _IDSetupCodeInfoParams.SupportedPlatforms != null)
                    {
                        Reports report = new Reports();
                        IList<String> unsupportedIdList = report.GetAllSupportedIdsForPlatforms(_IDSetupCodeInfoParams.SupportedPlatforms);
                        if (unsupportedIdList != null)
                        {
                            IList<Id> newIdList = new List<Id>();
                            foreach (Id item in m_ids)
                            {
                                if (unsupportedIdList.Contains(item.ID))
                                    newIdList.Add(item);
                            }
                            m_ids = newIdList;
                        }
                        else
                        {
                            this.m_ids = new List<Id>();
                        }
                    }
                }
            }
        }
        #endregion

        #region Key Function Report

        private void GetKeyFunctionInput()
        {
            try
            {
                IList<String> idList = GetIDs();
                if (idList != null && idList.Count > 0)
                {
                    m_KeepIdList = new List<String>();
                    m_KeepIdList = idList;
                    ReportFilterForm _filterWizard = new ReportFilterForm(ReportType.KeyFunction, idList);
                    _filterWizard.SelectedType = SelectedType;
                    DialogResult result = _filterWizard.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        m_ids = _filterWizard.Ids;
                        regions = _filterWizard.Regions;
                        deviceTypes = _filterWizard.DeviceTypes;
                        selectedModes = _filterWizard._selectedModes;
                        dataSources = _filterWizard.DataSources;
                        RemoveIDSForSelectedSubDevices = _filterWizard.RemoveIDSForSelectedSubDevices;
                        _IDSetupCodeInfoParams = _filterWizard.IdSetupCodeInfoParam;
                        KeyFunctionReportType = _filterWizard.KeyFunctionReportType;
                        _filterWizard.Close();
                        ResetTimer();
                        ShowReport("Key Function Count");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }
        }
        private KeyFunctionModel GetKeyFunctionReport()
        {
            Reports report = new Reports();
            KeyFunctionModel model = report.GetKeyFunctionReport(m_ids, regions, selectedModes, deviceTypes, dataSources, _IDSetupCodeInfoParams, KeyFunctionReportType);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            return model;            
        }
        #endregion

        #region Save Missing Id List
        private void SaveMissingIdList(String missingreportFileName)
        {
            try
            {
                String m_savemsg = String.Empty;
                DataSet reportSetMissingIdList = new DataSet();
                reportSetMissingIdList.Tables.Add("MissingIdList");
                reportSetMissingIdList.Tables["MissingIdList"].Columns.Add("ID");
                DataRow dr = null;
                if(m_KeepIdList.Count > 0)
                {
                    foreach(String m_Item in m_KeepIdList)
                    {
                        dr = reportSetMissingIdList.Tables["MissingIdList"].NewRow();
                        dr[0] = m_Item;
                        reportSetMissingIdList.Tables["MissingIdList"].Rows.Add(dr);
                        dr = null;
                    }
                    reportSetMissingIdList.Tables["MissingIdList"].AcceptChanges();
                    reportSetMissingIdList.AcceptChanges();
                    String filePath = Configuration.GetWorkingDirectory();
                    TextReportGenerator.GenerateNormalReport(reportSetMissingIdList, filePath, missingreportFileName, out m_savemsg);
                }
            }
            catch
            {
            }
        }
        #endregion

        #region BSC
        public void BSCIDList()
        {
           try
            {
                IList<String> idlList = GetIDs();
                if (idlList != null && idlList.Count > 0)
                {
                    m_KeepIdList = new List<String>();
                    m_KeepIdList = idlList;
                    ReportFilterForm _filterWizard = new ReportFilterForm(ReportType.BSC, idlList);
                    _filterWizard.SelectedType = SelectedType;
                    DialogResult result = _filterWizard.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        m_ids                           = _filterWizard.Ids;
                        regions                         = _filterWizard.Regions;                                                
                        deviceTypes                     = _filterWizard.DeviceTypes;
                        selectedModes                   = _filterWizard._selectedModes;
                        dataSources                     = _filterWizard.DataSources;
                        RemoveIDSForSelectedSubDevices  = _filterWizard.RemoveIDSForSelectedSubDevices;
                        _IDSetupCodeInfoParams          = _filterWizard.IdSetupCodeInfoParam;
                        _filterWizard.Close();
                        if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            SaveCurrentSelections();
                        }
                        ResetTimer();
                        ShowReport("Brand Setup Code");
                    }

                }
               
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }           
        }
        //Added to support BSC in Pick Manager
        public void BSCIDList(IList<String> idlList, String ProjectName)
        {
            if (idlList != null && idlList.Count > 0)
            {
                m_KeepIdList = new List<String>();
                m_KeepIdList = idlList;
                ReportFilterForm _filterWizard = new ReportFilterForm(ReportType.BSCPIC, idlList,ProjectName);
                //Set to file or range type to pass ids for loading device types else set mode
                _filterWizard.SelectedType =  SelType.FILE;
                //_filterWizard.ProjectName = ProjectName;
                if (_filterWizard.ShowDialog() == DialogResult.OK)
                {
                    m_ids                           = _filterWizard.Ids;
                    regions                         = _filterWizard.Regions;
                    deviceTypes                     = _filterWizard.DeviceTypes;
                    selectedModes                   = _filterWizard._selectedModes;
                    dataSources                     = _filterWizard.DataSources;
                    RemoveIDSForSelectedSubDevices  = _filterWizard.RemoveIDSForSelectedSubDevices;
                    _IDSetupCodeInfoParams          = _filterWizard.IdSetupCodeInfoParam;                    
                    _filterWizard.Close();
                    reportType = ReportType.BSCPIC;
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("Brand Setup Code");
                }
            }
            else
                MessageBox.Show("No ids retrieved for the selection.");
        }
        private IDBSCReportModel GetBSCReport()
        {
            IncludeUnsupportedIdByPlatform();
            Reports report = new Reports();
            IDBSCReportModel model = report.GetBSCIDList(m_ids, regions, DeviceAliasRegion, selectedModes, deviceTypes, dataSources,
                 _IDSetupCodeInfoParams);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            return model;
        }
        private IDBSCReportModel GetPICBSCReport()
        {
            IncludeUnsupportedIdByPlatform();
            Reports report = new Reports();
            IDBSCReportModel model = report.GetBSCIDList(m_ids, regions, DeviceAliasRegion, selectedModes, deviceTypes, dataSources,
                 _IDSetupCodeInfoParams);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            return model;
        }
        #endregion  

        #region ID Brand
        private void ShowIDBrandReportInputForms()
        {
            try
            {
                IList<string> idlList = GetIDs();
                if (idlList != null && idlList.Count > 0)
                {
                    ReportFilterForm filterWizard = new ReportFilterForm(ReportType.IDBrandList, idlList);
                    filterWizard.SelectedType = SelectedType;
                    DialogResult result = filterWizard.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        m_ids           = filterWizard.Ids;
                        regions         = filterWizard.Regions;
                        deviceTypes     = filterWizard.DeviceTypes;
                        selectedModes   = filterWizard._selectedModes;
                        dataSources     = filterWizard.DataSources;
                        _IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                        filterWizard.Close();
                        if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            SaveCurrentSelections();
                        }
                        ResetTimer();
                        ShowReport("ID By Brand");
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }                                       
        }
        private IdBrandReportModel GetIDBrandReport()
        {
            IncludeUnsupportedIdByPlatform();
            Reports report = new Reports();
            IdBrandReportModel model = report.GetIDBrandReport(m_ids, regions, DeviceAliasRegion, selectedModes, deviceTypes, dataSources, _IDSetupCodeInfoParams);
            //IdBrandReportModel model = report.GetIDBrandReport(m_ids,regions,subregions, countries, deviceTypes,subdevicetypes,components, dataSources, _IDSetupCodeInfoParams);
            //Get All Mode Names
            this.allModeNames = report.GetAllModeNames();
            //ErrorLogging.WriteToLogFile("******************************", false);
            return model ;
        }           
        #endregion    
   
        #region Get ID List
        private IList<String> GetIDs()
        {
            IList<string> idList = null;
            IDSelectionForm idSelectionForm = null;
            
            try
            {
                //Use id selection form object
                idSelectionForm = new IDSelectionForm();
                DialogResult result = idSelectionForm.ShowDialog();

                if (result == DialogResult.OK)
                {
                    SelectedType = idSelectionForm.SelectedType;
                    idList = idSelectionForm.SelectedIDList;                    
                    if (idList == null || idList.Count == 0)
                        MessageBox.Show("No ids retrieved for the selection.");
                }
                return idList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idSelectionForm != null)
                {
                    idSelectionForm.Dispose();
                }
            }
       }
        #endregion

        #region QuickSet Model Info Report
        private void GetQuickSetInputRegenerate()
        {
            //get the ids
            if (m_KeepIdList != null && m_KeepIdList.Count > 0)
            {                
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.QuickSetRegenerate, m_KeepIdList);                
                filterWizard.SelectedType = SelectedType;                
                DialogResult result = filterWizard.ShowDialog();
                if (result == DialogResult.OK)
                {                    
                    this.m_ids = filterWizard.Ids;
                    this.regions = filterWizard.Regions;
                    this.dataSources = filterWizard.DataSources;
                    this.deviceTypes = filterWizard.DeviceTypes;                    
                    filterWizard.Close();
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("QuickSet Model Info Report");
                }
            }
        }
        private void GetQuickSetInputs()
        {
            //get the ids
            IList<string> idList = GetIDs();
            if (idList != null && idList.Count > 0)
            {
                m_KeepIdList = new List<String>();
                m_KeepIdList = idList;
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.QuickSet, idList);
                filterWizard.SelectedType = SelectedType;
                DialogResult result = filterWizard.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.m_ids = filterWizard.Ids;
                    this.regions = filterWizard.Regions;
                    this.dataSources = filterWizard.DataSources;
                    this.deviceTypes = filterWizard.DeviceTypes;
                    this.selectedModes = filterWizard._selectedModes;
                    this._brandFilter = filterWizard.BrandSpecificSel;
                    this._modelFilter = filterWizard.ModelSpecificSel;
                    this._modelInfoSpecificFilters = filterWizard.ModelInfoSpecificSel;
                    this._IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                    this._QuickSetInfoParams = filterWizard.QuickSetInputs;
                    filterWizard.Close();
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("QuickSet Model Info Report");

                }
            }
        }
        private Model.ModelInfoRecordCollection GetQuickSetReport()
        {
            try
            {
                IncludeUnsupportedIdByPlatform();
                Model.ModelInfoRecordCollection oldmodelInfoReport = null;
                Reports reports = new Reports();
                if (this._ReportObject != null)
                {
                    try
                    {
                        oldmodelInfoReport = (Model.ModelInfoRecordCollection)this._ReportObject;
                    }
                    catch { }
                }
                Model.ModelInfoRecordCollection modelInfoReport = reports.GeQuickSetReport(this.m_ids, this.regions, this.dataSources, this.selectedModes, this.deviceTypes, this._brandFilter, this._modelFilter, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams);
                if (oldmodelInfoReport != null)
                {
                    modelInfoReport = reports.GeQuickSetMergeReport(oldmodelInfoReport, modelInfoReport);
                }
                return modelInfoReport;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        #endregion

        #region Model Info Report with ARD Key
        private void GetModelInformationInputs_ArdKey_Regenerate()
        {
            //get the ids
            if (m_KeepIdList != null && m_KeepIdList.Count > 0)
            {
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.ModelInfoWithArdKeyRegenerate, m_KeepIdList);
                filterWizard.SelectedType = SelectedType;
                filterWizard.QuickSetInputs = _QuickSetInfoParams;
                DialogResult result = filterWizard.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.m_ids = filterWizard.Ids;
                    this.regions = filterWizard.Regions;
                    this.dataSources = filterWizard.DataSources;
                    this.deviceTypes = filterWizard.DeviceTypes;
                    //this.selectedModes = filterWizard._selectedModes;
                    //this._brandFilter = filterWizard.BrandSpecificSel;
                    //this._modelFilter = filterWizard.ModelSpecificSel;
                    //this._modelInfoSpecificFilters = filterWizard.ModelInfoSpecificSel;
                    //this._IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                    //this._QuickSetInfoParams = filterWizard.QuickSetInputs;
                    filterWizard.Close();
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("Model Info Report with Ard Key");
                }
            }
        }
        private void GetModelInformationInputs_ArdKey()
        {
            //get the ids
            IList<string> idList = GetIDs();
            if (idList != null && idList.Count > 0)
            {
                m_KeepIdList = new List<String>();
                m_KeepIdList = idList;
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.ModelInfoWithArdKey, idList);
                filterWizard.SelectedType = SelectedType;
                DialogResult result = filterWizard.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.m_ids = filterWizard.Ids;
                    this.regions = filterWizard.Regions;
                    this.dataSources = filterWizard.DataSources;
                    this.deviceTypes = filterWizard.DeviceTypes;
                    this.selectedModes = filterWizard._selectedModes;
                    this._brandFilter = filterWizard.BrandSpecificSel;
                    this._modelFilter = filterWizard.ModelSpecificSel;
                    this._modelInfoSpecificFilters = filterWizard.ModelInfoSpecificSel;
                    this._IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                    filterWizard.Close();
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("Model Info Report with Ard Key");

                }
            }
        }
        private DataSet GetModelInformationReport_ArdKey()
        {
            try
            {
                IncludeUnsupportedIdByPlatform();
                DataSet oldmodelInfoReport = null;
                Reports reports = new Reports();
                if (this._ReportObject != null)
                {
                    try
                    {
                        oldmodelInfoReport = (DataSet)this._ReportObject;
                    }
                    catch { }
                }
                DataSet modelInfoReport = reports.GetModelInfoReport_ArdKey(this.m_ids, this.regions, this.dataSources, this.selectedModes, this.deviceTypes, this._brandFilter, this._modelFilter, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams);
                if (oldmodelInfoReport != null)
                {
                    //Add missing id to model info report to last received model info report
                    if (modelInfoReport != null)
                    {
                        if (modelInfoReport.Tables[0].Rows.Count > 0)
                        {
                            if (modelInfoReport.Tables[0].Columns.Contains("TRModel"))
                            {
                                modelInfoReport.Tables[0].Columns["TRModel"].ColumnName = "T/R";
                            }
                            for (int i = 0; i < modelInfoReport.Tables[0].Rows.Count; i++)
                            {
                                oldmodelInfoReport.Tables[0].ImportRow(modelInfoReport.Tables[0].Rows[i]);
                            }
                        }
                        if (modelInfoReport.Tables[1].Rows.Count > 1)
                        {
                            for (int i = 0; i < modelInfoReport.Tables[1].Rows.Count; i++)
                            {
                                oldmodelInfoReport.Tables[1].ImportRow(modelInfoReport.Tables[1].Rows[i]);
                            }
                        }
                    }
                    return oldmodelInfoReport;                  
                }
                return modelInfoReport;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        DataSet ModelInfoReportOutputWithArdKey()
        {
            DataSet dsReport = null;
            if (this._ReportObject != null)
            {
                DataSet modelInfoArdKeyReport = (DataSet)this._ReportObject;
                if (modelInfoArdKeyReport != null)
                {
                    IList<String> m_tempIdList = new List<String>();
                    DataTable dtId = modelInfoArdKeyReport.Tables[0].DefaultView.ToTable(true, new String[] { "ID" });
                    if (m_KeepIdList != null && dtId.Rows.Count > 0)
                    {
                        for (int i = 0; i < m_KeepIdList.Count; i++)
                        {
                            if (dtId.Select("ID='"+ m_KeepIdList[i] +"'").Length == 0)
                            {
                                m_tempIdList.Add(m_KeepIdList[i]);
                            }
                        }
                    }
                    m_KeepIdList.Clear();
                    m_KeepIdList = m_tempIdList;
                    dsReport = new DataSet();
                    if (modelInfoArdKeyReport.Tables[0].Rows.Count > 0)
                    {                        
                        if (modelInfoArdKeyReport.Tables[0].Columns.Contains("TRModel"))
                        {
                            modelInfoArdKeyReport.Tables[0].Columns["TRModel"].ColumnName = "T/R";                            
                        }
                        DataTable dtReport = null;
                        //Apply Column Selector                        
                        if (this._modelInfoSpecificFilters.ColumnSelector != null)
                        {
                            List<String> strColumns = new List<String>();
                            strColumns.Add("Model");
                            strColumns.Add("Brand");
                            strColumns.Add("T/R");                            
                            strColumns.Add("ID");
                            foreach (String item in this._modelInfoSpecificFilters.ColumnSelector.Keys)
                            {
                                if (this._modelInfoSpecificFilters.ColumnSelector[item] == true)
                                {
                                    strColumns.Add(item);
                                }
                            }
                            String[] strSelectedColumns = new String[strColumns.Count];
                            for (int i = 0; i < strColumns.Count; i++)
                            {
                                strSelectedColumns[i] = strColumns[i];
                            }
                            dtReport = modelInfoArdKeyReport.Tables[0].DefaultView.ToTable(true, strSelectedColumns);
                        }
                        else
                        {
                            //Brand	T/R	Model	ID	MainDevice	SubDevice	ARDKey	ARDTrustLevel
                            //dtReport = modelInfoArdKeyReport.Tables[0].DefaultView.ToTable();
                            //dtReport = modelInfoArdKeyReport.Tables[0].DefaultView.ToTable(true, new String[] { "Model", "Brand", "T/R", "ID", "MainDevice", "SubDevice", "ARDKey", "ARDTrustLevel" });
                            dtReport = modelInfoArdKeyReport.Tables[0].DefaultView.ToTable(true, new String[] { "Model", "Brand", "T/R", "ID", "SubDevice", "ARDKey", "ARDTrustLevel" });
                        }

                        //Applay Sub Device
                        if (this._modelInfoSpecificFilters.RowSelector != null && dtReport != null)
                        {                            
                            foreach (String item in this._modelInfoSpecificFilters.RowSelector.Keys)
                            {
                                if (this._modelInfoSpecificFilters.RowSelector[item] == false)
                                {
                                    if (this._modelInfoSpecificFilters.RowSelectorName == "MainDevice")
                                    {
                                        foreach (DataRow row in dtReport.Select("MainDevice ='" + item + "'"))
                                        {
                                            row["MainDevice"] = String.Empty;
                                        }
                                    }
                                    if (this._modelInfoSpecificFilters.RowSelectorName == "SubDevice")
                                    {
                                        foreach (DataRow row in dtReport.Select("SubDevice ='" + item + "'"))
                                        {
                                            row["SubDevice"] = String.Empty;
                                        }
                                    }                                    
                                }
                            }
                            dtReport.AcceptChanges();
                        }
                        dtReport.TableName = "ModelInfoReport_ARD Key";                        
                        dsReport.Tables.Add(dtReport);
                    }
                    if (modelInfoArdKeyReport.Tables[1].Rows.Count > 1)
                    {
                        DataTable dtReport = modelInfoArdKeyReport.Tables[1].DefaultView.ToTable();
                        dtReport.TableName = "ModelInfoReport_Brand_ARD Key";                       
                        dsReport.Tables.Add(dtReport);
                    }
                    //modelInfoArdKeyReport.Tables[0].TableName = "ModelInfoReport";
                    //dsReport = modelInfoArdKeyReport;
                }
            }
            return dsReport;
        }
        #endregion

        #region Model Information
        //Called from PIC Manager
        private void GetModelInformationInputs(IList<String> idList, String ProjectName)
        {
            if (idList != null && idList.Count > 0)
            {
                m_KeepIdList = new List<String>();
                m_KeepIdList = idList;
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.MODELINFOPIC, idList,ProjectName);
                filterWizard.SelectedType = SelType.FILE;
                DialogResult result = filterWizard.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.m_ids                  = filterWizard.Ids;
                    this.regions                = filterWizard.Regions;
                    this.dataSources            = filterWizard.DataSources;
                    this.deviceTypes            = filterWizard.DeviceTypes;
                    this.selectedModes          = filterWizard._selectedModes;
                    this._brandFilter           = filterWizard.BrandSpecificSel;
                    this._modelFilter           = filterWizard.ModelSpecificSel;
                    this._modelInfoSpecificFilters = filterWizard.ModelInfoSpecificSel;
                    this._IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                    filterWizard.Close();
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("Model Information");
                }
            }
            else
                MessageBox.Show("No ids retrieved for the selection.");
        }
        private void GetModelInformationInputs_EmptyDevice()
        {
            //get the ids
            IList<string> idList = GetIDs();
            if(idList != null && idList.Count > 0)
            {
                m_KeepIdList = new List<String>();
                m_KeepIdList = idList;
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.ModelInformation_EmptyDevice, idList);
                filterWizard.SelectedType = SelectedType;
                DialogResult result = filterWizard.ShowDialog();
                if(result == DialogResult.OK)
                {
                    this.m_ids = filterWizard.Ids;
                    this.regions = filterWizard.Regions;
                    this.dataSources = filterWizard.DataSources;
                    this.deviceTypes = null;
                    this.selectedModes = filterWizard._selectedModes;
                    this._brandFilter = filterWizard.BrandSpecificSel;
                    this._modelFilter = filterWizard.ModelSpecificSel;
                    this._modelInfoSpecificFilters = filterWizard.ModelInfoSpecificSel;
                    this._IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                    filterWizard.Close();
                    ResetTimer();
                    ShowReport("Model Information");

                }
            }
        }
        private void GetModelInformationInputs()
        {
            //get the ids
            IList<string> idList = GetIDs();
            if (idList != null && idList.Count > 0)
            {
                m_KeepIdList = new List<String>();
                m_KeepIdList = idList;
                //show the report filter form.
                ReportFilterForm filterWizard = new ReportFilterForm(ReportType.ModelInformation, idList);
                filterWizard.SelectedType = SelectedType;
                DialogResult result = filterWizard.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.m_ids = filterWizard.Ids;
                    this.regions = filterWizard.Regions;
                    this.dataSources = filterWizard.DataSources;
                    this.deviceTypes = filterWizard.DeviceTypes;
                    this.selectedModes = filterWizard._selectedModes;
                    this._brandFilter = filterWizard.BrandSpecificSel;
                    this._modelFilter = filterWizard.ModelSpecificSel;
                    this._modelInfoSpecificFilters = filterWizard.ModelInfoSpecificSel;
                    this._IDSetupCodeInfoParams = filterWizard.IdSetupCodeInfoParam;
                    filterWizard.Close();
                    if (MessageBox.Show("Want to Save Current Selection to Log File?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveCurrentSelections();
                    }
                    ResetTimer();
                    ShowReport("Model Information");
                    
                }
            }
        }       
        private Model.ModelInfoRecordCollection GetModelInformationReport_EmptyDevice()
        {
            try
            {
                IncludeUnsupportedIdByPlatform();
                Reports reports = new Reports();
                Model.ModelInfoRecordCollection modelInfoReport = reports.GetModelInfoReport(this.m_ids, this.regions, this.dataSources, this.selectedModes, this.deviceTypes, this._brandFilter, this._modelFilter, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams);
                return modelInfoReport;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        private Model.ModelInfoRecordCollection GetModelInformationReport()
        {
            try
            {
                IncludeUnsupportedIdByPlatform();
                Reports reports = new Reports();
                Model.ModelInfoRecordCollection modelInfoReport = reports.GetModelInfoReport(this.m_ids, this.regions, this.dataSources, this.selectedModes, this.deviceTypes, this._brandFilter, this._modelFilter, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams);
                return modelInfoReport;
            }
            catch
            {
                this.exceptionOccuredFlag = true;
                return null;
            }
        }
        public void FilterModelInfoReport(ModelInfoSpecific.IsTargetFilter reportFilter)
        {
            this._modelInfoSpecificFilters.TargetModelFilter = reportFilter;
            ShowReport("Model Information");
        }
        public void FilterModelInfoReport(DisplayFilter displayMode)
        {
            this._modelInfoSpecificFilters.DisplayMode = displayMode;
            ShowReport("Model Information");
        }
        #endregion

        #region Save To Report Log File
        private void SaveCurrentSelections()
        {
            try
            {
                if (File.Exists(ErrorLogging.reportLogFile))
                {
                    if (MessageBox.Show("Report Log File in your Working Directory \r\n" + ErrorLogging.reportLogFile + " \r\n already Exists, Want to Over Write it?", this.reportType.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        ErrorLogging.WriteToReportLogFile(this.reportType.ToString(), this.m_ids, this.dataSources, this.selectedModes, this.regions, this.deviceTypes, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams,this._QuickSetInfoParams, true);
                        MessageBox.Show("Report Log File Over Write Successfully in your Working Directory \r\n" + ErrorLogging.reportLogFile, this.reportType.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        ErrorLogging.WriteToReportLogFile(this.reportType.ToString(), this.m_ids, this.dataSources, this.selectedModes, this.regions, this.deviceTypes, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams, this._QuickSetInfoParams, false);
                        MessageBox.Show("Report Log File Created Successfully in your Working Directory \r\n" + ErrorLogging.reportLogFile, this.reportType.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    ErrorLogging.WriteToReportLogFile(this.reportType.ToString(), this.m_ids, this.dataSources, this.selectedModes, this.regions, this.deviceTypes, this._modelInfoSpecificFilters, this._IDSetupCodeInfoParams, this._QuickSetInfoParams, false);
                    MessageBox.Show("Report Log File Created Successfully in your Working Directory \r\n" + ErrorLogging.reportLogFile, this.reportType.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {                
            }
        }
        #endregion

        #region EX/ID/TNInfo Report
        public void ShowEXIDTNInfoReportForm()
        {
                       
            _MdiParent.WindowState = FormWindowState.Maximized;
            _MdiParent.Text = "EX/ID/TN Info";
            _MdiParent.EnableSaveMenus(false);
            _MdiParent.Show();

            _MdiParent.Cursor = Cursors.WaitCursor;

            //display the user control
            EXIDTNInfoReport reportForm = new EXIDTNInfoReport();
            reportForm.Parent = _MdiParent;
            reportForm.Dock = DockStyle.Fill;
            reportForm.BringToFront();
            _MdiParent.Cursor = Cursors.Default;            
        }
        #endregion

        #region LabelSearch
        private void GetLabelSearchInputs()
        {
           this.selectedIDs = GetIDs();

           if (this.selectedIDs != null && this.selectedIDs.Count > 0)
           {
               //show the label filter form
               //LabelFilterForm filterForm = new LabelFilterForm();
               ReportFilterForm filterForm = new ReportFilterForm(this.reportType, this.selectedIDs);               
               if (filterForm.ShowDialog() == DialogResult.OK)
               {
                   //this._LabelIntronPattern = filterForm.LabelPattern;
                   this._LabelIntronFilters = filterForm.LabelIntronSearchInputs;
                   ResetTimer();
                   ShowReport("Label Search");
               }
           }            
        }
        private void GetExcludeLabelSearchInputs()
        { 
            this.selectedIDs = GetIDs();

            if (this.selectedIDs != null && this.selectedIDs.Count > 0)
            {
                //show the label filter form
                //LabelFilterForm filterForm = new LabelFilterForm();
                ReportFilterForm filterForm = new ReportFilterForm(this.reportType, this.selectedIDs);
                if (filterForm.ShowDialog() == DialogResult.OK)
                {
                    //this._LabelIntronPattern = filterForm.LabelPattern;
                    this._LabelIntronFilters = filterForm.LabelIntronSearchInputs;
                    ResetTimer();
                    ShowReport("Exclude Label Search");
                }
            }
        }
        private LabelIntronSearchReport GetLabelSearchReport()
        {
            try
            {
                if (this.selectedIDs != null && this.selectedIDs.Count > 0)
                {
                    Reports reportService = new Reports();
                    this.allModeNames = reportService.GetAllModeNames();
                    //return reportService.GetLabelIntronSearchReport(false, this.selectedIDs, this._LabelIntronPattern);
                    return reportService.GetLabelIntronSearchReport(false, this.selectedIDs, this._LabelIntronFilters.SearchPattern,this._LabelIntronFilters.IsCompleteMatch);

                }

                return null;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Error occurred in generating report.");
                return null;
            }
        }
        private LabelIntronSearchReport GetExcludeLabelSearchReport()
        { 
            try
            {
                if (this.selectedIDs != null && this.selectedIDs.Count > 0)
                {
                    Reports reportService = new Reports();
                    this.allModeNames = reportService.GetAllModeNames();
                    //return reportService.GetLabelIntronSearchReport(false, this.selectedIDs, this._LabelIntronPattern);
                    return reportService.GetExcludeLabelIntronSearchReport(false, this.selectedIDs, this._LabelIntronFilters.SearchPattern, this._LabelIntronFilters.IsCompleteMatch);

                }

                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Error occurred in generating report.");
                return null;
            }
        }    
        public void FilterLabelSearchReport(DisplayFilter displayMode)
        {
            this._LabelIntronFilters.DisplayMode = displayMode;

            if (displayMode == DisplayFilter.Distinct)
            {
                LabelIntronSearchReport labelReport = (LabelIntronSearchReport)this._ReportObject;
                if (distinctView == null)//first time
                {
                    distinctView = new LabelIntronDistinctView(labelReport);

                    //distinctView.DataObject = labelReport;
                    distinctView.Parent = this._MdiParent;
                    distinctView.Dock = DockStyle.Fill;

                    //this._MdiParent.Show();
                    distinctView.BringToFront();
                }
                else
                {
                    distinctView.Show();
                }

            }
            else
            {
                //remove the control if it is available.
                if (distinctView != null && this._MdiParent.Controls.Contains(distinctView))
                {
                    this.distinctView.Hide();
                    //this._MdiParent.Controls.Remove(distinctView);
                }

               // ShowReport("Label Search");
            }

        }
        public void FilterLabelIntronSearchReport(String searchMode, LabelIntronFilters secondrySearch, String displayMode)
        {
            LabelIntronSearchReport labelReport = null;
            LabelIntronSearchReport secondarylabelReport = null;
            this.distinctView = null;            
            Reports reportService = new Reports();
            if (this._ReportObject != null)
            {
                labelReport = (LabelIntronSearchReport)this._ReportObject;
                switch (searchMode)
                {
                    case "LabelSearch":
                        secondarylabelReport = reportService.GetLabelIntronSearchReport(false, this.selectedIDs, secondrySearch.SearchPattern, secondrySearch.IsCompleteMatch);
                        if (secondarylabelReport != null)
                        {
                            labelReport = labelReport.SearchSecondryKeyLabels(secondarylabelReport, searchMode);
                        }
                        this._ReportObject = labelReport;
                        ShowReport("Label Search");
                        break;
                    case "IntronSearch":
                        secondarylabelReport = reportService.GetLabelIntronSearchReport(true, this.selectedIDs, secondrySearch.SearchPattern, secondrySearch.IsCompleteMatch);
                        if (secondarylabelReport != null)
                        {
                            labelReport = labelReport.SearchSecondryKeyLabels(secondarylabelReport, searchMode);
                        }
                        this._ReportObject = labelReport;
                        ShowReport("Intron Search");
                        break;
                }
                
                switch (displayMode)
                {
                    case "DistinctMode":
                        FilterLabelSearchReport(DisplayFilter.Distinct);
                        break;
                    case "ShowAllMode":
                        FilterLabelSearchReport(DisplayFilter.ShowAll);
                        break;
                }
            }            
        }
        #endregion

        #region IntronSearch
        private void GetIntronSearchInputs()
        {
            this.selectedIDs = GetIDs();

            if (this.selectedIDs != null && this.selectedIDs.Count > 0)
            {
                //show the label filter form
               // IntronFilterForm filterForm = new IntronFilterForm();
                ReportFilterForm filterForm = new ReportFilterForm(this.reportType, this.selectedIDs);

                if (filterForm.ShowDialog() == DialogResult.OK)
                {
                   // this._LabelIntronPattern = filterForm.IntronPattern;
                    this._LabelIntronFilters = filterForm.LabelIntronSearchInputs;
                    ResetTimer();
                    ShowReport("Intron Search");
                }
            }
        }
        private void GetExcludeIntronSearchInputs()
        {
            this.selectedIDs = GetIDs();

            if (this.selectedIDs != null && this.selectedIDs.Count > 0)
            {
                //show the label filter form
                // IntronFilterForm filterForm = new IntronFilterForm();
                ReportFilterForm filterForm = new ReportFilterForm(this.reportType, this.selectedIDs);

                if (filterForm.ShowDialog() == DialogResult.OK)
                {
                    // this._LabelIntronPattern = filterForm.IntronPattern;
                    this._LabelIntronFilters = filterForm.LabelIntronSearchInputs;
                    ResetTimer();
                    ShowReport("Exclude Intron Search");
                }
            }
        }
        private LabelIntronSearchReport GetIntronSearchReport()
        {
            try
            {
                if (this.selectedIDs != null && this.selectedIDs.Count > 0)
                {
                    Reports reportService = new Reports();
                    this.allModeNames = reportService.GetAllModeNames();
                    return reportService.GetLabelIntronSearchReport(true, this.selectedIDs, this._LabelIntronFilters.SearchPattern,this._LabelIntronFilters.IsCompleteMatch);
                }
                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Error occurred in generating report.");
                return null;
            }
        }
        private LabelIntronSearchReport GetExcludeIntronSearchReport()
        {
            try
            {
                if (this.selectedIDs != null && this.selectedIDs.Count > 0)
                {
                    Reports reportService = new Reports();
                    this.allModeNames = reportService.GetAllModeNames();
                    return reportService.GetExcludeLabelIntronSearchReport(true, this.selectedIDs, this._LabelIntronFilters.SearchPattern, this._LabelIntronFilters.IsCompleteMatch);
                }
                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Error occurred in generating report.");
                return null;
            }
        }
        public void FilterIntronSearchReport(DisplayFilter displayMode)
        {
            this._LabelIntronFilters.DisplayMode = displayMode;

            if (displayMode == DisplayFilter.Distinct)
            {
                LabelIntronSearchReport labelReport = (LabelIntronSearchReport)this._ReportObject;
                distinctView = new LabelIntronDistinctView(labelReport);
                //distinctView.DataObject = labelReport;
                distinctView.Parent = this._MdiParent;
                distinctView.Dock = DockStyle.Fill;

                this._MdiParent.Show();
                distinctView.BringToFront();

            }
            else
            {
                //remove the control if it is available.
                if (distinctView != null && this._MdiParent.Controls.Contains(distinctView))
                {
                    this._MdiParent.Controls.Remove(distinctView);
                }

                ShowReport("Intron Search");
            }

            //ShowReport("Label Search");

        }
        #endregion

        #region Intron Label Search With Brands
        private void GetIntronLabelSearchWithBrandInputs()
        {
            //MessageBox.Show("TBD,\nPlease select without Brand Option for Intron Search","Intron Search With Brand",MessageBoxButtons.OK,MessageBoxIcon.Information);
            this.selectedIDs = GetIDs();
            if(this.selectedIDs != null && this.selectedIDs.Count > 0)
            {
                ReportFilterForm filterForm = new ReportFilterForm(this.reportType, this.selectedIDs);
                filterForm.SelectedType = SelectedType;
                if(filterForm.ShowDialog() == DialogResult.OK)
                {
                    m_ids = filterForm.Ids;
                    regions = filterForm.Regions;
                    deviceTypes = filterForm.DeviceTypes;
                    selectedModes = filterForm._selectedModes;
                    dataSources = filterForm.DataSources;
                    _IDSetupCodeInfoParams = filterForm.IdSetupCodeInfoParam;
                    this._LabelIntronFilters = filterForm.LabelIntronSearchInputs;

                    filterForm.Close();                                        
                    ResetTimer();
                    if(this._LabelIntronFilters.IsIntronSearch == true)
                        ShowReport("Intron Search");
                    else
                        ShowReport("Label Search");
                }
            }
        }
        private DataSet GetLabelIntronSearchWithBrandReport()
        {
            DataSet dataSetReport = null;
            try
            {
                if(this.selectedIDs != null && this.selectedIDs.Count > 0)
                {
                    Reports reportService = new Reports();
                    this.allModeNames = reportService.GetAllModeNames();
                    //Get Label Intron Search
                    LabelIntronSearchReport m_LabelIntronSearchReport = m_LabelIntronSearchReport = reportService.GetLabelIntronSearchReport(this._LabelIntronFilters.IsIntronSearch, this.selectedIDs, this._LabelIntronFilters.SearchPattern, this._LabelIntronFilters.IsCompleteMatch);
                    if(m_LabelIntronSearchReport != null)
                    {
                        DataSet reportSetLabelIntron = null;
                        if(this._LabelIntronFilters.IsIntronSearch)
                          reportSetLabelIntron = m_LabelIntronSearchReport.GetReportForDisplay(this.allModeNames, "IntronSearch");
                        else
                          reportSetLabelIntron = m_LabelIntronSearchReport.GetReportForDisplay(this.allModeNames, "LabelSearch");
                        //Get ID Brand Report
                        IdBrandReportModel m_IdBrandReportModel = reportService.GetIDBrandReport(m_ids, regions, DeviceAliasRegion, selectedModes, deviceTypes, dataSources, _IDSetupCodeInfoParams);
                        if(m_IdBrandReportModel != null)
                        {
                            DataSet reportSetIdBrand = m_IdBrandReportModel.GetIDBrandReportDataSet(this._IDSetupCodeInfoParams, this.ModeNames);
                            if(reportSetIdBrand != null && reportSetIdBrand.Tables["ID-Brand"] != null && reportSetIdBrand.Tables["ID-Brand"].Rows.Count > 0)
                            {
                                DataTable dataTableReport = null;
                                if(this._LabelIntronFilters.IsIntronSearch)
                                {
                                    dataSetReport = new DataSet("IntronSearchReport");
                                    dataTableReport = new DataTable("IntronSearch");
                                }
                                else
                                {
                                    dataSetReport = new DataSet("LabelSearchReport");
                                    dataTableReport = new DataTable("LabelSearch");
                                }                                

                                DataTable dataTableIDBrand = reportSetIdBrand.Tables["ID-Brand"];
                                if(reportSetLabelIntron != null && reportSetLabelIntron != null)
                                {
                                    DataTable dataTableIntronSearch = null;
                                     if(this._LabelIntronFilters.IsIntronSearch)
                                         dataTableIntronSearch = reportSetLabelIntron.Tables["IntronSearch"];
                                     else
                                         dataTableIntronSearch = reportSetLabelIntron.Tables["LabelSearch"];
                                    String[] Columns = { "Label" };
                                    DataTable dtLabels = GetDistinctRecords(dataTableIntronSearch, Columns);
                                    dataTableReport.Columns.Add("ID");
                                    dataTableReport.Columns.Add("Brands");
                                    foreach(DataRow dr in dtLabels.Rows)
                                    {
                                        if(dr[0].ToString() != String.Empty)
                                        {
                                            dataTableReport.Columns.Add(dr[0].ToString());
                                        }
                                    }
                                    dataTableReport.AcceptChanges();
                                    DataRow dataRow = null;

                                    foreach(DataRow dr in dataTableIDBrand.Rows)
                                    {
                                        dataRow = dataTableReport.NewRow();
                                        dataRow[0] = dr[0];
                                        dataRow[1] = dr[1];
                                        if(dr[1].ToString() != String.Empty)
                                            foreach(DataRow dr1 in dtLabels.Rows)
                                            {
                                                if(dr1[0].ToString() != String.Empty)
                                                {
                                                    try
                                                    {
                                                        String m_Label = dr1[0].ToString();
                                                        if(m_Label.Contains("'") == true)
                                                        {
                                                            m_Label = m_Label.Replace("'","''");
                                                        }
                                                        DataRow[] drCheckIntron = dataTableIntronSearch.Select("ID = '" + dr["ID"].ToString() + "' and Label = '" + m_Label + "'");
                                                        if(drCheckIntron.Length > 0)
                                                        {
                                                            if(drCheckIntron[0]["Intron"].ToString() != String.Empty)
                                                            {
                                                                dataRow[dr1[0].ToString()] = "Y";
                                                            }
                                                            else
                                                            {
                                                                dataRow[dr1[0].ToString()] = "N";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            dataRow[dr1[0].ToString()] = "N";
                                                        }
                                                    }
                                                    catch
                                                    {
                                                        dataRow[dr1[0].ToString()] = "N";
                                                    }
                                                }
                                            }
                                        dataTableReport.Rows.Add(dataRow);
                                        dataRow = null;
                                    }
                                }
                                dataTableReport.AcceptChanges();
                                dataSetReport.Tables.Add(dataTableReport);
                            }
                        }
                    }
                }
                return dataSetReport;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Error occurred in generating report.");
                return null;
            }
        }
        private DataTable GetDistinctRecords(DataTable dt, String[] Columns)
        {
            DataTable dtUniqRecords = new DataTable();
            dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
            return dtUniqRecords;
        }
        private void DGV_Creation(ref DataGridView dgv,DataTable dt)
        {           
            for(int i = 0; i <= dt.Columns.Count; i++)
                dgv.Columns.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
            for(int j = 0; j < dt.Rows.Count; j++)
                dgv.Rows.Add(dt.Rows[j]);
        }
        #endregion

        #region RestrictedIDReport
        private void GenerateRestrictedIDReport()
        {
            ResetTimer();
            ShowReport("Restricted IDs");
        }
        private RestrictedIdModel GetRestrictedIDs()
        {
            Reports report = new Reports();
            this.allModeNames = report.GetAllModeNames();

            return report.GetRestrictedIDModel();
        }
        #endregion

        #region Empty/NonEmpty ID Report
        private void GetEmptyNonEmptyIDReportInput(bool emptyIDReport)
        {
            selectedIDs =  GetIDs();
            
            if (selectedIDs != null && selectedIDs.Count>0)
            {
                ResetTimer();
                string caption = "Non Empty ID Report";
                if (emptyIDReport)
                    caption = "Empty ID Report";

                ShowReport(caption);
                    
            }
        }
        private Empty_NonEmptyIdModel GetEmptyNonEmptyIDReport(bool emptyIDReport)
        {
            try
            {
                if (selectedIDs != null)
                {
                    Reports report = new Reports();
                    this.allModeNames = report.GetAllModeNames();

                    return report.GetEmptyNonEmptyIdReport((List<String>)selectedIDs, emptyIDReport);

                    //if (emptyIDReport)
                    //   return report.GetEmptyIDsFromList((List<String>)selectedIDs);
                    //else
                    //  return  report.GetNonEmptyIDsFromList((List<String>)selectedIDs);
                }
                return null;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                return null;
            }
        }
        #endregion        

        #region Get Brand Seach
        private void GetBrandSearchInput()
        {
            try
            {
                using (Forms.BrandFilterForm brandInput = new Forms.BrandFilterForm())
                {
                    if (brandInput.ShowDialog() == DialogResult.OK)
                    {
                        this.brands = brandInput.Brands;
                        brandInput.Close();
                        ResetTimer();
                        ShowReport("Brand Search");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"An error has occurred while getting inputs. Please check the error log for details");
                ErrorLogging.WriteToErrorLogFile(ex);
            }
        }
        private BrandSearchModel GetBrandSearchResult()
        {
            try
            {
                Reports reportService = new Reports();
                BrandSearchModel results = reportService.GetBrandSearchReport(brands);
                return results;
            }
            catch
            {
                MessageBox.Show(@"An error has occurred while generating results. Please check the error log for details");
                return null;
            }
        }
        #endregion

        #region BrandModelSearch
        private void GetBrandModelSearchInputs()
        {
            try
            {
                using (BrandModelSearchInput brandModelInput = new BrandModelSearchInput())
                {
                    if (brandModelInput.ShowDialog() == DialogResult.OK)
                    {
                        //get the inputs.
                        this.selectedIDs = brandModelInput.IDs;
                        this.regions = brandModelInput.Regions;
                        this.dataSources = brandModelInput.DataSources;
                        this.brands = brandModelInput.Brands;
                        this.models = brandModelInput.Models;

                        brandModelInput.Close();

                        ResetTimer();
                        ShowReport("Brand/Model Search");
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(@"An error has occurred while getting inputs. Please check the error log for details");
                
                ErrorLogging.WriteToErrorLogFile(ex);

             }
        }

        private ModelInfoRecordCollection GetBrandModelSearchResults()
        {
            try
            {
                Reports reportService = new Reports();
                ModelInfoRecordCollection results = reportService.GetBrandModelReport(this.selectedIDs, this.regions, this.dataSources, 
                    this.brands,this.models);

                return results;

            }
            catch
            {
                MessageBox.Show(@"An error has occurred while generating results.
                Please check the error log for details");

                return null;
            }
        }
        #endregion

        #region Executor Id with Prefix Same Inputs
        private void GetExecIdPrefixSameInputs()
        {
            try
            {
                this.selectedIDs = GetIDs();
                if(this.selectedIDs != null)
                {
                    if(this.selectedIDs != null && this.selectedIDs.Count > 0)
                    {
                        ResetTimer();
                        ShowReport("ExecIdWithSamePrefix");
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(@"An error has occurred while getting inputs. Please check the error log for details");
                ErrorLogging.WriteToErrorLogFile(ex);
            }
        }       
        private DataSet GetExecIdWithSamePrefix()
        {
            DataSet dsResults = null;
            try
            {
                Reports reportService = new Reports();
                dsResults = reportService.GetExecutorIdWithSamePrefix(this.selectedIDs);
            }
            catch(Exception ex)
            {
                MessageBox.Show(@"An error has occurred while getting inputs. Please check the error log for details");
                ErrorLogging.WriteToErrorLogFile(ex);
            }
            return dsResults;
        }
        #endregion

        #region ExecIdMap
        private void GetExecIdMapInputs()
        {
            try
            {
                this.selectedIDs = GetIDs();
                if(this.selectedIDs != null)
                {
                    Forms.ExIDMapReportOptionForm m_Option = new Forms.ExIDMapReportOptionForm();
                    if(m_Option.ShowDialog() == DialogResult.OK)
                    {
                        m_IncludeBand = m_Option.IncludeBrand;
                        m_IncludeLoadPercentage = m_Option.IncludeLoadPercentage;
                        if(this.selectedIDs != null && this.selectedIDs.Count > 0)
                        {
                            ResetTimer();
                            ShowReport("ExecIdMap");
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(@"An error has occurred while getting inputs. Please check the error log for details");
                ErrorLogging.WriteToErrorLogFile(ex);
            }
        }
        private DataSet GetExecIdMap()
        {
            DataSet dsResults = null;
            try
            {
                
                Reports reportService = new Reports();                
                Dictionary<Int32, ExIdMap> results = reportService.GetExecIdMap(this.selectedIDs);

                if (results.Count > 0)
                {
                    dsResults = new DataSet("ExecIdMapSet");
                    DataTable execIdMap = dsResults.Tables.Add("ExecIdMap");
                    if (_modelInfoSpecificFilters != null)
                    {
                        #region For Custom Format
                        execIdMap.Columns.Add("Executor");                        
                        if (_modelInfoSpecificFilters.ColumnSelector.ContainsKey("Count") == true)
                            if (_modelInfoSpecificFilters.ColumnSelector["Count"] == true)
                                execIdMap.Columns.Add("Count");
                        if (_modelInfoSpecificFilters.ColumnSelector.ContainsKey("Percentage") == true)
                            if (_modelInfoSpecificFilters.ColumnSelector["Percentage"] == true)
                                execIdMap.Columns.Add("Percentage");
                        execIdMap.Columns.Add("ID");
                        if (m_IncludeBand == true)
                        {
                            execIdMap.Columns.Add("Brand");
                        }
                        foreach (Int32 key in results.Keys)
                        {                            
                            ExIdMap m_IdBrand = results[key];                           
                            foreach (String id in m_IdBrand.Ids.Keys)
                            {
                                DataRow newRow = execIdMap.NewRow();
                                if (newRow.Table.Columns.Contains("Executor"))
                                    newRow["Executor"] = key;
                                
                                if (newRow.Table.Columns.Contains("Count"))
                                    newRow["Count"] = m_IdBrand.Ids.Count;

                                if ((m_IncludeBand == true)&&(newRow.Table.Columns.Contains("Brand")))
                                {
                                    StringBuilder brandbuilder = new StringBuilder();
                                    foreach (String brand in m_IdBrand.Ids[id])
                                    {
                                        if (!String.IsNullOrEmpty(brand))
                                            brandbuilder.Append(brand + ", ");
                                    }
                                    if (brandbuilder.Length > 0)
                                        brandbuilder.Remove(brandbuilder.Length - 2, 2);
                                    newRow["Brand"] = brandbuilder;
                                }

                                newRow["ID"] = id;
                                execIdMap.Rows.Add(newRow);
                                newRow = null;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Without Custom Format
                        execIdMap.Columns.Add("Executor");
                        execIdMap.Columns.Add("Count");
                        execIdMap.Columns.Add("ID");

                        if (m_IncludeBand == true)
                            execIdMap.Columns.Add("Brand");

                        if (m_IncludeLoadPercentage == true)
                            execIdMap.Columns.Add("Percentage");

                        foreach (Int32 key in results.Keys)
                        {                            
                            DataRow newRow = execIdMap.NewRow();
                            
                            ExIdMap m_IdBrand = results[key];
                            Int32 idCount = 0;                                                                                   
                            StringBuilder idbuilder = new StringBuilder();
                            StringBuilder brandbuilder = new StringBuilder();
                            foreach (String id in m_IdBrand.Ids.Keys)
                            {
                                idCount = idCount + 1;
                                idbuilder.Append(id + ", ");
                                if (m_IncludeBand == true)
                                {
                                    brandbuilder = new StringBuilder();
                                    foreach (String brand in m_IdBrand.Ids[id])
                                    {
                                        if (!String.IsNullOrEmpty(brand))
                                            brandbuilder.Append(brand + ", ");
                                    }
                                    if (brandbuilder.Length > 0)
                                        brandbuilder.Remove(brandbuilder.Length - 2, 2);                                    
                                }

                                if (idbuilder.Length > 32753)
                                {
                                    if (idbuilder.Length > 0)
                                        idbuilder.Remove(idbuilder.Length - 2, 2);
                                    newRow["ID"] = idbuilder.ToString();

                                    if (m_IncludeBand == true)
                                    {
                                        newRow["Brand"] = brandbuilder.ToString();
                                    }                                   
                                    if (m_IncludeLoadPercentage == true)
                                    {
                                        float m_Percentage = 0;
                                        m_Percentage = ((float)m_IdBrand.Ids.Count * 100f) / ((float)this.selectedIDs.Count);
                                        newRow["Percentage"] = String.Format("{0:0.0000}", m_Percentage);
                                    }
                                    newRow["Executor"] = key;
                                    newRow["Count"] = idCount;
                                    execIdMap.Rows.Add(newRow);
                                    newRow = null;
                                    idCount = 0;
                                    idbuilder = new StringBuilder();
                                    newRow = execIdMap.NewRow();                                    
                                }
                            }

                            newRow["Executor"] = key;
                            newRow["Count"] = idCount;
                            if (idbuilder.Length > 0)
                                idbuilder.Remove(idbuilder.Length - 2, 2);
                            newRow["ID"] = idbuilder.ToString();
                            if (m_IncludeBand == true)
                            {
                                newRow["Brand"] = brandbuilder.ToString();
                            }                            
                            if (m_IncludeLoadPercentage == true)
                            {
                                float m_Percentage = 0;
                                m_Percentage = ((float)m_IdBrand.Ids.Count * 100f) / ((float)this.selectedIDs.Count);
                                newRow["Percentage"] = String.Format("{0:0.0000}", m_Percentage);
                            }
                            execIdMap.Rows.Add(newRow);
                        }
                        #endregion
                    }                                        
                    execIdMap.AcceptChanges();
                }                
            }
            catch(Exception ex)
            {
                MessageBox.Show(@"An error has occurred while getting inputs. Please check the error log for details");
                ErrorLogging.WriteToErrorLogFile(ex);
            }
            return dsResults;
        }       
        #endregion

        #region MiscReports

        private void GetConsolidatedDataInputs()
        {
            using (MiscReportSelector selector = new MiscReportSelector())
            {
                if (selector.ShowDialog() == DialogResult.OK)
                {
                    this._ConsolidateReportItems = selector.ReportItemSelections;
                    ResetTimer();
                    ShowReport("Consolidate Report");
                }
            }
        }

        private DataTable GenerateConsolidatedReport()
        {
            try
            {
                if (this._ConsolidateReportItems!=null)
                {
                    Reports reportService = new Reports();

                    return reportService.GenerateConsolidatedReport(this._ConsolidateReportItems["TotalExecutors"], this._ConsolidateReportItems["TotalUsedExecutors"], this._ConsolidateReportItems["MainDevices"], this._ConsolidateReportItems["SubDevices"], false,
                        this._ConsolidateReportItems["StandardBrands"], this._ConsolidateReportItems["UniqueAliasBrands"], this._ConsolidateReportItems["UniqueBrandVariations"], this._ConsolidateReportItems["UniqueDeviceModels"], this._ConsolidateReportItems["UniqueRemoteModels"], this._ConsolidateReportItems["UniqueKeyFunctions"], this._ConsolidateReportItems["Keys"]);
                }

                return null;
            }
            catch
            {
                MessageBox.Show(@"An error has occurred while generating results.
                Please check the error log for details");

                return null;
            }
        }

        #endregion

        #region Locations By Load
        void GetLocationByLoad()
        {
            try
            {
                this.selectedIDs = GetIDs();
                if (this.selectedIDs != null && this.selectedIDs.Count > 0)
                {
                    ResetTimer();
                    ShowReport("Location Search");
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }   
        }
        DataSet GetLocationsByLoadReport()
        {
            Reports reportService = new Reports();
            DataSet dsResult = reportService.GetLocationsByLoad(this.selectedIDs);
            return dsResult;
        }
        #endregion

        #region Report Result
        private void ShowReport(String _Caption)
        {
            bool showReport = false;
            DataGridView dgSearchResult = _MdiParent.ReportGridView;                                    
            if (this._ReportObject != null)
            {
                switch (this.reportType)
                {                    
                    case ReportType.IDList:
                        DataSet reportSet = Reports.GetIDByRegionDataSet((IDByRegionReportModel)this._ReportObject, this._IDSetupCodeInfoParams, this.ModeNames, ref m_KeepIdList);
                        if (reportSet != null && reportSet.Tables["IDLIST"] != null && reportSet.Tables["IDLIST"].Rows.Count > 0)
                        {                            
                            dgSearchResult.DataSource = reportSet.Tables["IDList"];
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            showReport = true;                            
                        }
                        SaveMissingIdList("MissingIdList_IDSelection_Report");
                        break;
                    case ReportType.LocationSearch:
                        reportSet = (DataSet)this._ReportObject;
                        if (reportSet != null)
                        {                            
                            dgSearchResult.DataSource = reportSet.Tables[0];
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            showReport = true;
                        }
                        break;
                    case ReportType.BSC:
                        reportSet = Reports.GetBSCReportDataSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, this.ModeNames, ref m_KeepIdList);
                        if (reportSet != null && reportSet.Tables["BSCList"] != null && reportSet.Tables["BSCList"].Rows.Count > 0)
                        {
                            dgSearchResult.DataSource = reportSet.Tables["BSCList"];
                            dgSearchResult.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            showReport = true;
                        }
                        SaveMissingIdList("MissingIdList_BSC_Report");
                        break;
                    case ReportType.BSCPIC:
                        reportSet = Reports.GetBSCReportDataSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, this.ModeNames, ref m_KeepIdList);
                        if (reportSet != null && reportSet.Tables["BSCList"] != null && reportSet.Tables["BSCList"].Rows.Count > 0)
                        {
                            dgSearchResult.DataSource = reportSet.Tables["BSCList"];
                            dgSearchResult.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            showReport = true;
                        }
                        SaveMissingIdList("MissingIdList_BSC_Report");
                        break;
                    case ReportType.SupportedIDByPlatform:
                        {
                            reportSet = (DataSet)this._ReportObject;
                            if (reportSet != null && reportSet.Tables[0] != null && reportSet.Tables[0].Rows.Count > 0)
                            {
                                dgSearchResult.DataSource = reportSet.Tables[0];
                                dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                                showReport = true;
                            }
                            SaveMissingIdList("MissingIdList_SupportedIdsByPlatform_Report");
                        }
                        break;
                    case ReportType.KeyFunction:
                        {
                            KeyFunctionModel keyFunctionReport = (KeyFunctionModel)this._ReportObject;
                            reportSet = keyFunctionReport.GetKeyFunctionReportDataSet(KeyFunctionReportType, this.ModeNames);
                            if (reportSet != null && reportSet.Tables["KeyFunctionCount"] != null && reportSet.Tables["KeyFunctionCount"].Rows.Count > 0)
                            {
                                dgSearchResult.DataSource = reportSet.Tables["KeyFunctionCount"];                               
                                dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                                showReport = true;
                            }
                        }
                        break;
                    case ReportType.IDBrandList:
                        IdBrandReportModel idBrandReport = (IdBrandReportModel)this._ReportObject;
                        reportSet = idBrandReport.GetIDBrandReportDataSet(this._IDSetupCodeInfoParams, this.ModeNames);
                        if (reportSet != null && reportSet.Tables["ID-Brand"] != null && reportSet.Tables["ID-Brand"].Rows.Count > 0)
                        {
                            dgSearchResult.DataSource = reportSet.Tables["ID-Brand"];
                            dgSearchResult.Columns["Brands"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            showReport = true;
                        }
                        break;
                    case ReportType.QuickSet:
                        {
                            ModelInfoRecordCollection quicksetInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                            TimeSpan totalExecTime = TimeSpan.Zero;

                            ErrorLogging.WriteToLogFile("--Function Name: GetQuickSetInfoDataSet--", false);
                            ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                            DateTime spStart = DateTime.Now;
                            ErrorLogging.WriteToLogFile("QuickSet ModelInfo Report execution time", false);
                            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                            reportSet = quicksetInfoReport.GetQuickSetInfoDataSet(this._modelInfoSpecificFilters, _QuickSetInfoParams, ref m_KeepIdList);
                            DateTime spEnd = DateTime.Now;
                            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
                            totalExecTime = spEnd - spStart;
                            ErrorLogging.WriteToLogFile("Total excution Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                            ErrorLogging.WriteToLogFile("##########################################################", false);

                            if (reportSet != null && reportSet.Tables["ModelInfoReport"] != null && reportSet.Tables["ModelInfoReport"].Rows.Count > 0)
                            {
                                //remove if any bound column is present in datagrid.
                                if (dgSearchResult.Columns.Count > 0)
                                    dgSearchResult.Columns.Clear();

                                dgSearchResult.DataSource = reportSet.Tables["ModelInfoReport"];                                
                                dgSearchResult.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;                               
                                dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                                if (_modelInfoSpecificFilters.ModelInfoOptions == ModelInfoSpecific.ModelReportOptions.ModelBased)
                                {
                                    //Show Report Column Selector
                                    ShowReportColumnSelector(dgSearchResult);
                                }

                                showReport = true;
                            }
                            if (m_KeepIdList.Count > 0)
                            {
                                if (MessageBox.Show(m_KeepIdList.Count.ToString() + " Ids are missing from Report\r\nWant to regenerate?", "QuickSet Model Info Report", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    GetQuickSetInputRegenerate();
                                }
                                else
                                {
                                    SaveMissingIdList("MissingIdList_QuickSet_ModelInfo_Report");
                                }
                            }                            
                        }
                        break;
                    case ReportType.ModelInfoWithArdKey:
                        {                            
                            TimeSpan totalExecTime = TimeSpan.Zero;
                            ErrorLogging.WriteToLogFile("--Function Name: ModelInfoReportOutputWithArdKey --", false);
                            ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                            DateTime spStart = DateTime.Now;
                            ErrorLogging.WriteToLogFile("Model Info Report with Ard Key execution time", false);
                            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                                                       
                            //reportSet = quicksetInfoReport.GetQuickSetInfoDataSet(this._modelInfoSpecificFilters, _QuickSetInfoParams, ref m_KeepIdList);
                            reportSet = ModelInfoReportOutputWithArdKey();

                            DateTime spEnd = DateTime.Now;
                            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
                            totalExecTime = spEnd - spStart;
                            ErrorLogging.WriteToLogFile("Total excution Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                            ErrorLogging.WriteToLogFile("##########################################################", false);

                            //if (reportSet != null && reportSet.Tables["BSCInfoReport"] != null && reportSet.Tables["BSCInfoReport"].Rows.Count > 0)
                            if (reportSet != null)
                            {
                                //remove if any bound column is present in datagrid.
                                if (dgSearchResult.Columns.Count > 0)
                                    dgSearchResult.Columns.Clear();

                                dgSearchResult.DataSource = reportSet.Tables[0];
                                if (_modelInfoSpecificFilters.ModelInfoOptions == ModelInfoSpecific.ModelReportOptions.ModelBased)
                                {
                                    //Show Report Column Selector
                                    ShowReportColumnSelector(dgSearchResult);
                                }                                
                                showReport = true;
                            }
                            if (m_KeepIdList.Count > 0)
                            {
                                if (MessageBox.Show(m_KeepIdList.Count.ToString() + " Ids are missing from Report\r\nWant to regenerate?", "Model Info with Ard Key Report", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    GetModelInformationInputs_ArdKey_Regenerate();                                    
                                }
                                else
                                {
                                    SaveMissingIdList("MissingIdList_ModelInfo_ArdKey_Report");
                                }
                            }
                        }
                        break;
                    case ReportType.ModelInformation:
                        ModelInfoRecordCollection modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                        reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                        if (reportSet != null && reportSet.Tables["ModelInfoReport"] != null && reportSet.Tables["ModelInfoReport"].Rows.Count > 0)
                        {
                            bool isDistinctMode = this._modelInfoSpecificFilters.DisplayMode.Equals(DisplayFilter.Distinct);
                            //show the filters  
                            ToolStripLabel modelCountLbl = (ToolStripLabel)this._MdiParent.ReportToolStrip.Items["modelCountLbl"];
                            modelCountLbl.Visible = isDistinctMode;
                            
                            //enable or disable istarget filter in the report form.
                            if (this._modelInfoSpecificFilters.ModelInfoOptions != ModelInfoSpecific.ModelReportOptions.ModelBased)
                            {
                                this._MdiParent.ReportToolStrip.Items["IsTargetModelFilter"].Visible = false;
                            }
                            
                            //remove if any bound column is present in datagrid.
                            if (dgSearchResult.Columns.Count > 0)
                                dgSearchResult.Columns.Clear();

                            dgSearchResult.CellContentClick -= new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);

                            if (isDistinctMode)
                            {
                                DataGridViewLinkColumn link = new DataGridViewLinkColumn();
                                link.HeaderText = reportSet.Tables["ModelInfoReport"].Columns[0].ColumnName;
                                link.DataPropertyName = reportSet.Tables["ModelInfoReport"].Columns[0].ColumnName;
                                link.LinkBehavior = LinkBehavior.NeverUnderline;
                                link.SortMode = DataGridViewColumnSortMode.Automatic;

                                dgSearchResult.Columns.Add(link);
                                //dgSearchResult.CellContentClick -= new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);
                                dgSearchResult.CellContentClick += new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);
                                modelCountLbl.Text = "Total Count : " + reportSet.Tables["ModelInfoReport"].Rows.Count.ToString();                                
                            }                            
                            dgSearchResult.AutoGenerateColumns = !isDistinctMode;
                            //dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            //dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["ModelInfoReport"];
                            if (_modelInfoSpecificFilters.ModelInfoOptions == ModelInfoSpecific.ModelReportOptions.ModelBased)
                            {
                                //Show Report Column Selector
                                ShowReportColumnSelector(dgSearchResult);
                            }
                            showReport = true;                           
                        }
                        SaveMissingIdList("MissingIdList_ModelInfo_Report");
                        break;
                    case ReportType.ModelInformation_EmptyDevice:
                        modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                        reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                        if(reportSet != null && reportSet.Tables["ModelInfoReport"] != null && reportSet.Tables["ModelInfoReport"].Rows.Count > 0)
                        {
                            bool isDistinctMode = this._modelInfoSpecificFilters.DisplayMode.Equals(DisplayFilter.Distinct);
                            //show the filters  
                            ToolStripLabel modelCountLbl = (ToolStripLabel)this._MdiParent.ReportToolStrip.Items["modelCountLbl"];
                            modelCountLbl.Visible = isDistinctMode;

                            //enable or disable istarget filter in the report form.
                            if(this._modelInfoSpecificFilters.ModelInfoOptions != ModelInfoSpecific.ModelReportOptions.ModelBased)
                            {
                                this._MdiParent.ReportToolStrip.Items["IsTargetModelFilter"].Visible = false;
                            }

                            //remove if any bound column is present in datagrid.
                            if(dgSearchResult.Columns.Count > 0)
                                dgSearchResult.Columns.Clear();

                            dgSearchResult.CellContentClick -= new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);

                            if(isDistinctMode)
                            {
                                DataGridViewLinkColumn link = new DataGridViewLinkColumn();
                                link.HeaderText = reportSet.Tables["ModelInfoReport"].Columns[0].ColumnName;
                                link.DataPropertyName = reportSet.Tables["ModelInfoReport"].Columns[0].ColumnName;
                                link.LinkBehavior = LinkBehavior.NeverUnderline;
                                link.SortMode = DataGridViewColumnSortMode.Automatic;

                                dgSearchResult.Columns.Add(link);
                                //dgSearchResult.CellContentClick -= new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);
                                dgSearchResult.CellContentClick += new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);
                                modelCountLbl.Text = "Total Count : " + reportSet.Tables["ModelInfoReport"].Rows.Count.ToString();
                            }
                            dgSearchResult.AutoGenerateColumns = !isDistinctMode;
                            //dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            //dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["ModelInfoReport"];
                            if(_modelInfoSpecificFilters.ModelInfoOptions == ModelInfoSpecific.ModelReportOptions.ModelBased)
                            {
                                //Show Report Column Selector
                                ShowReportColumnSelector(dgSearchResult);
                            }
                            showReport = true;
                        }
                        SaveMissingIdList("MissingIdList_ModelInfo_EmptyDevice_Report");
                        break;
                    case ReportType.MODELINFOPIC:
                        ModelInfoRecordCollection modelInfoPICReport = (ModelInfoRecordCollection)this._ReportObject;
                        reportSet = modelInfoPICReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                        if (reportSet != null && reportSet.Tables["ModelInfoReport"] != null && reportSet.Tables["ModelInfoReport"].Rows.Count > 0)
                        {
                            bool isDistinctMode = this._modelInfoSpecificFilters.DisplayMode.Equals(DisplayFilter.Distinct);                            
                            //show the filters  
                            ToolStripLabel modelCountLbl = (ToolStripLabel)this._MdiParent.ReportToolStrip.Items["modelCountLbl"];
                            modelCountLbl.Visible = isDistinctMode;

                            //enable or disable istarget filter in the report form.
                            if (this._modelInfoSpecificFilters.ModelInfoOptions != ModelInfoSpecific.ModelReportOptions.ModelBased)
                            {
                                this._MdiParent.ReportToolStrip.Items["IsTargetModelFilter"].Visible = false;
                            }

                            //remove if any bound column is present in datagrid.
                            if (dgSearchResult.Columns.Count > 0)
                                dgSearchResult.Columns.Clear();

                            dgSearchResult.CellContentClick -= new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);

                            if (isDistinctMode)
                            {
                                DataGridViewLinkColumn link = new DataGridViewLinkColumn();
                                link.HeaderText = reportSet.Tables["ModelInfoReport"].Columns[0].ColumnName;
                                link.DataPropertyName = reportSet.Tables["ModelInfoReport"].Columns[0].ColumnName;
                                link.LinkBehavior = LinkBehavior.NeverUnderline;
                                link.SortMode = DataGridViewColumnSortMode.Automatic;

                                dgSearchResult.Columns.Add(link);
                                //dgSearchResult.CellContentClick -= new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);
                                dgSearchResult.CellContentClick += new DataGridViewCellEventHandler(dgSearchResult_CellContentClick);
                                modelCountLbl.Text = "Total Count : " + reportSet.Tables["ModelInfoReport"].Rows.Count.ToString();
                            }
                            dgSearchResult.AutoGenerateColumns = !isDistinctMode;
                            dgSearchResult.DataSource = reportSet.Tables["ModelInfoReport"];
                            if (_modelInfoSpecificFilters.ModelInfoOptions == ModelInfoSpecific.ModelReportOptions.ModelBased)
                            {
                                //Show Report Column Selector
                                ShowReportColumnSelector(dgSearchResult);
                            }
                            showReport = true;
                        }
                        SaveMissingIdList("MissingIdList_ModelInfo_Report");
                        break;
                    case ReportType.RestrictedIDList:
                        reportSet = Reports.GetRestrictedIDDSForDisplay((RestrictedIdModel)this._ReportObject, this.allModeNames);

                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["RestrictedIDs"];
                            showReport = true;
                        }
                        break;
                    case ReportType.QASTATUS:
                        reportSet = Reports.GetQAStatusReportDataSet((QAStatusReportModel)this._ReportObject, this.allModeNames);
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["QAStatus"];
                            showReport = true;
                        }                       
                        break;
                    case ReportType.EMPTY_ID:
                        reportSet = Reports.GetEmptyNonEmptyIDDSForDisplay((Empty_NonEmptyIdModel)this._ReportObject, this.allModeNames);
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["Empty IDs"];
                            showReport = true;
                        }     
                        break;
                    case ReportType.NONEMPTY_ID:
                        reportSet = Reports.GetEmptyNonEmptyIDDSForDisplay((Empty_NonEmptyIdModel)this._ReportObject, this.allModeNames);
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["Non Empty IDs"];
                            showReport = true;
                        }     
                        break;
                    case ReportType.UNIQUE_FUNC_IR:
                        reportSet = Reports.GetUniqueFunctionsIRDataSet((UniqueFuncIRCountModel)this._ReportObject);
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["UniqueFuncIRCountReport"];
                            showReport = true;                           
                        }
                        break;
                    case ReportType.DISCONNECTED_TNS:
                        reportSet = Reports.GetDisconnectedTNReportDataSet((IList<Model.TNInfo>)this._ReportObject);
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["DisconnectedTNReport"];
                            showReport = true;
                        }
                        break;
                    case ReportType.LabelSearch:
                        LabelIntronSearchReport labelReport = (LabelIntronSearchReport)this._ReportObject;
                        reportSet = labelReport.GetReportForDisplay(this.allModeNames, "LabelSearch");
                       
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            dgSearchResult.DataSource = reportSet.Tables["LabelSearch"];
                            showReport = true;

                        }
                        break;
                    case ReportType.IntronSearch:
                        LabelIntronSearchReport intronReport = (LabelIntronSearchReport)this._ReportObject;
                        reportSet = intronReport.GetReportForDisplay(this.allModeNames, "IntronSearch");
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            dgSearchResult.DataSource = reportSet.Tables["IntronSearch"];
                            showReport = true;

                        }
                        break;

                    case ReportType.ExcludeLabelSearch:
                         LabelIntronSearchReport ExcludelabelReport = (LabelIntronSearchReport)this._ReportObject;
                         reportSet = ExcludelabelReport.GetReportForDisplay(this.allModeNames, "ExcludeLabelSearch");
                       
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            dgSearchResult.DataSource = reportSet.Tables["ExcludeLabelSearch"];
                            showReport = true;

                        }
                        break;

                    case ReportType.ExcludeIntronSearch:
                         LabelIntronSearchReport ExcludeIntronReport = (LabelIntronSearchReport)this._ReportObject;
                         reportSet = ExcludeIntronReport.GetReportForDisplay(this.allModeNames, "ExcludeIntronSearch");
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            dgSearchResult.DataSource = reportSet.Tables["ExcludeIntronSearch"];
                            showReport = true;

                        }
                        break;
                    case ReportType.IntronSearchWithBrand:
                        reportSet = (DataSet)this._ReportObject;
                        if(reportSet != null)
                        {                                                                                  
                            try
                            {
                                dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                                dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                                dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                                dgSearchResult.DataSource = reportSet.Tables["IntronSearch"];
                                dgSearchResult.Columns["Brands"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            }
                            catch(Exception ex)
                            {
                                dgSearchResult.DataSource = null;
                                MessageBox.Show("You can export Intron Search Report to Excel file, \n Not able to display in grid due to following error:\n" + ex.Message, "Intron Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }                                                        
                            showReport = true;
                        }
                        break;
                    case ReportType.LabelSearchWithBrand:
                        reportSet = (DataSet)this._ReportObject;
                        if(reportSet != null)
                        {
                            try
                            {
                                dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                                dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                                dgSearchResult.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                                dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                                dgSearchResult.DataSource = reportSet.Tables["LabelSearch"];
                                dgSearchResult.Columns["Brands"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            }
                            catch(Exception ex)
                            {
                                dgSearchResult.DataSource = null;
                                MessageBox.Show("You can export Intron Search Report to Excel file, \n Not able to display in grid due to following error:\n" + ex.Message, "Intron Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            showReport = true;
                        }
                        break;
                    case ReportType.BrandBasedSearch:
                        reportSet = Reports.GetBrandBasedSearchReport((BrandBasedSearchModel)this._ReportObject,this.m_brandSearchOptions, this.allModeNames,"BrandBasedSearch");
                        if (reportSet != null)
                        {                            
                            String fileName = String.Empty;
                            String filePath = String.Empty;
                            filePath = Configuration.GetWorkingDirectory();
                            fileName = this.reportType.ToString();
                            _MdiParent._ReportSaveStatus = ExcelReportGenerator.GenerateReport(reportSet, filePath, fileName, out _saveFailMessage);
                            showReport = true;
                        }
                        break;
                    case ReportType.BrandModelBasedSearch:
                        ModelInfoRecordCollection brandModelSearch = (ModelInfoRecordCollection)this._ReportObject;
                        reportSet = brandModelSearch.GetBMSearchResultset(this.isTargetFilter);
                        if (reportSet != null)
                        {
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            dgSearchResult.DataSource = reportSet.Tables["ModelInfoReport"];
                            showReport = true;
                        }

                        break;
                    case ReportType.BrandSearch:
                        BrandSearchModel brandSearch = (BrandSearchModel)this._ReportObject;
                        reportSet = brandSearch.GetBrandSearchResult();
                        if (reportSet != null)
                        {
                            dgSearchResult.DataSource = reportSet.Tables["Brands"];
                            dgSearchResult.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;                             
                            showReport = true;
                        }
                        break; 
                    case ReportType.MiscReport:
                        DataTable consolidateReport = (DataTable)this._ReportObject;
                        if (consolidateReport != null)
                        {
                            dgSearchResult.DataSource = consolidateReport;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
                            showReport = true;
                        }
                        break;
                    case ReportType.ExecIdMap:
                        reportSet = (DataSet)this._ReportObject;
                        if (reportSet != null)
                        {                            
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;                                                        
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;                            
                            dgSearchResult.DataSource = reportSet.Tables["ExecIdMap"];
                            if (reportSet.Tables["ExecIdMap"].Columns.Contains("ID"))
                            {
                                dgSearchResult.Columns["ID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                                dgSearchResult.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                                if ((m_IncludeBand == true)&&(reportSet.Tables["ExecIdMap"].Columns.Contains("Brand")))
                                {
                                    dgSearchResult.Columns["Brand"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                                    dgSearchResult.Columns["Brand"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                                }
                            }
                            ShowReportColumnSelector(dgSearchResult);
                            showReport = true;
                        }
                        break;                 
                    case ReportType.ExecIdPrefixSame:
                        reportSet = (DataSet)this._ReportObject;
                        if(reportSet != null)
                        {
                            dgSearchResult.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
                            dgSearchResult.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                            dgSearchResult.DataSource = reportSet.Tables["ExecIdSamePrefix"];
                            dgSearchResult.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                            dgSearchResult.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;                                                       
                            showReport = true;
                        }
                        break;
                    case ReportType.SYNTHESIZER_TABLE:
                        SynthesizerTableModel synthesizerTable = (SynthesizerTableModel)this._ReportObject;
                        reportSet = Reports.GetSynthesizerTableReportDataSet(synthesizerTable, m_IncludeIntron, this.ModeNames);
                        dgSearchResult.DataSource = reportSet.Tables["SynthesizerTable"];                        
                        showReport = true;
                        break;
                    case ReportType.DUPLICATE_INTRONS:
                        LabelIntronSearchReport duplicateIntrons = (LabelIntronSearchReport)this._ReportObject;
                        reportSet = duplicateIntrons.GetReportForDisplay(this.ModeNames, "DuplicateIntrons");
                        dgSearchResult.DataSource = reportSet.Tables["DuplicateIntrons"];
                        showReport = true;
                        break;                      
                }
            }
            if (showReport)
            {
                  _MdiParent.Show();
                   _MdiParent.Text = _Caption;
                                
                if (this._IDSetupCodeInfoParams.SetupCodeFormatErrorFlag == true)
                {
                    string filePath = Configuration.GetWorkingDirectory();
                    MessageBox.Show("This Setup Code report is incomplete. The Invalid Setup Codes are not displayed in this report.\n Invalid setup codes are logged to " + filePath + "\\FormatErrorCodes.txt", "Report");
                    string message = String.Empty;
                    TextReportGenerator.GenerateReport(this._IDSetupCodeInfoParams.GetSetupCodesWithFormatError(), filePath , "FormatErrorCodes", out message);
                }
            }
            else
            {
                if (this.exceptionOccuredFlag)
                {
                    MessageBox.Show("Could not retrieve data.Please check error log for details.","Exception",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else if(!userCancellationFlag)
                    MessageBox.Show("No data found for the selection.","Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }
        //used in model info report.
        void dgSearchResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)//avoiding header cell.
            {
                DataGridView gridView = (DataGridView)sender;
                string cellValue = Convert.ToString(gridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                ModelInfoRecordCollection modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                DataSet results = modelInfoReport.GetSubReport(cellValue, this._modelInfoSpecificFilters);

                if (results != null)
                {
                    Forms.ModelInfoReportPopup dataPopup = new UEI.Workflow2010.Report.Forms.ModelInfoReportPopup();
                    dataPopup.DisplayData = results;
                    dataPopup.ShowDialog();
                }
            }                        
        }
        public bool SaveReport(bool isDefaultSave)
        {            
            string filePath = String.Empty;
            string fileName = String.Empty;
            SaveReportOption saveOption = new SaveReportOption();
            if (this._ReportObject != null)
            {
                if (reportType == ReportType.QuickSet)
                    saveOption.SetWizardButtons(SaveReportButtons.Excel);
                if (reportType == ReportType.ModelInfoWithArdKey)
                    saveOption.SetWizardButtons(SaveReportButtons.Excel);
                //show the save option form                
                if (saveOption.ShowDialog() == DialogResult.OK)
                {                   
                    fileName = GetFileNameAndPath(isDefaultSave, saveOption.SaveTo, out filePath);
                    if (!String.IsNullOrEmpty(fileName))
                    {
                        if (saveOption.SaveTo == SaveOption.Excel)
                        {
                            isDataSaved = SaveToExcel(filePath, fileName);
                            //add ext to filename for msgbox
                            if (this.reportType == ReportType.ModelInfoWithArdKey)
                                fileName = fileName + ".xlsx";
                            else
                                fileName = fileName + ".xls";                            
                        }
                        else if (saveOption.SaveTo == SaveOption.Pdf)
                        {
                            isDataSaved = SaveToPdf(filePath, fileName);
                            fileName = fileName + ".pdf";
                        }
                        else if (saveOption.SaveTo == SaveOption.Text)
                        {
                            isDataSaved = SaveToText(filePath, fileName);
                            fileName = fileName + ".txt";
                        }
                    }
                    else
                        isDataSaved = false;                    
                }
                else
                    isDataSaved = false;
            }

            if (isDataSaved)
            {
                if (msgExportToExcel == 1 && saveOption.SaveTo == SaveOption.Excel)
                {
                   // MessageBox.Show("Report saved to " + filePath + fileName+"\n Note: Report is exported to multiple Sheets since it contains more than 65536 records.", "Report Save");
                    MessageBox.Show("Note: Report is exported to multiple Sheets since it contains more than 65536 records.", "Report Save");
                }
               // else
                   // MessageBox.Show("Report saved to " + filePath + fileName, "Report Save");

            }
            else
                MessageBox.Show("Report not saved." + this._saveFailMessage, "Report Save");


            return isDataSaved;
        }
        private string GetFileNameAndPath(bool isDefaultSave,SaveOption option,out string filePath)
        {
            string fileName = String.Empty;
            filePath = String.Empty;

            if (isDefaultSave)
            {
                //string ext = (option == SaveOption.Excel) ? ".xls" : ".txt";
                filePath = Configuration.GetWorkingDirectory();
                fileName = this.reportType.ToString();
            }
            else
            {
                //show savefiledialog.
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.InitialDirectory = CommonForms.Configuration.GetWorkingDirectory();
                if (option == SaveOption.Excel)
                {
                    if (this.reportType == ReportType.ModelInfoWithArdKey)
                    {
                        dialog.Filter = "Excel file (*.xlsx)|";
                        dialog.DefaultExt = ".xlsx";
                    }
                    else
                    {
                        dialog.Filter = "Excel file (*.xls)|";
                        dialog.DefaultExt = ".xls";
                    }                    
                }
                else if (option == SaveOption.Text)
                {
                    dialog.Filter = "Text file (*.txt)|";
                    dialog.DefaultExt = ".txt";
                }
                else if (option == SaveOption.Pdf)
                {
                    dialog.Filter = "Pdf file (*.pdf)|";
                    dialog.DefaultExt = ".pdf";
                }
                dialog.AddExtension = true;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = dialog.FileName.Substring(dialog.FileName.LastIndexOf("\\") + 1);
                    filePath = dialog.FileName.Remove(dialog.FileName.LastIndexOf("\\") + 1);
                    fileName = fileName.Remove(fileName.LastIndexOf('.'));//removes extension.
                }
            }

            return fileName;

        }
        private bool SaveToText(string path, string reportName)
        {
            DeviceRegionalSelection m_DeviceAliasSelection = null;
            DataSet reportSet = null;
            DeviceCategoryRecords deviceCategories = null;
            switch (this.reportType)
            {
                case ReportType.LocationSearch:
                     reportSet = (DataSet)this._ReportObject;                       
                    break;
                case ReportType.IDList:
                    reportSet = Reports.GetIDByRegionModeBasedSet((IDByRegionReportModel)this._ReportObject, this._IDSetupCodeInfoParams);
                    break;
                case ReportType.KeyFunction:
                     KeyFunctionModel keyFunctionReport = (KeyFunctionModel)this._ReportObject;
                     reportSet = keyFunctionReport.GetKeyFunctionReportDataSet(KeyFunctionReportType, this.ModeNames);                            
                    break;
                case ReportType.SupportedIDByPlatform:
                    {
                        reportSet = (DataSet)this._ReportObject;                       
                    }
                    break;
                case ReportType.BSC:
                     m_DeviceAliasSelection = new DeviceRegionalSelection();
                     if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                     {
                         if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                         {
                             DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                             ResetTimer();
                         }
                         if (ShowReportOption())
                         {
                             deviceCategories = GetDeviceCategorization();
                             if (deviceCategories != null)
                                 reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, true,ref m_KeepIdList);
                         }
                         else
                         {
                             deviceCategories = GetDeviceCategorization();
                             if (deviceCategories != null)
                                 reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, false,ref m_KeepIdList);
                         }
                     }
                    break;
                case ReportType.BSCPIC:
                     m_DeviceAliasSelection = new DeviceRegionalSelection();
                     if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                     {
                         if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                         {
                             DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                             ResetTimer();
                         }
                         if (ShowReportOption())
                         {
                             deviceCategories = GetDeviceCategorization();
                             if (deviceCategories != null)
                                 reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, true,ref m_KeepIdList);
                         }
                         else
                         {
                             deviceCategories = GetDeviceCategorization();
                             if (deviceCategories != null)
                                 reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, false,ref m_KeepIdList);
                         }
                     }
                    break;
                case ReportType.IDBrandList:
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }
                        
                        if (ShowReportOption())
                        {
                            deviceCategories = GetDeviceCategorization();
                            if (deviceCategories != null)
                            {
                                IdBrandReportModel idBrandReport = (IdBrandReportModel)this._ReportObject;
                                reportSet = idBrandReport.GetIDBrandModeBasedSetForText(this._IDSetupCodeInfoParams, deviceCategories, true);
                            }
                        }
                        else
                        {
                            deviceCategories = GetDeviceCategorization();
                            if (deviceCategories != null)
                            {
                                IdBrandReportModel idBrandReport = (IdBrandReportModel)this._ReportObject;
                                reportSet = idBrandReport.GetIDBrandModeBasedSetForText(this._IDSetupCodeInfoParams, deviceCategories, false);
                            }
                        }
                    }
                    break;
                case ReportType.MODELINFOPIC:
                    ModelInfoRecordCollection modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                    break;
                case ReportType.ModelInformation:
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                    break;
                case ReportType.RestrictedIDList:
                    reportSet = Reports.GetRestrictedIDDSForSaving((RestrictedIdModel)this._ReportObject, this.allModeNames);
                    break;
                case ReportType.QASTATUS:
                    reportSet = Reports.GetQAStatusTextReportDataSet((QAStatusReportModel)this._ReportObject, this.allModeNames);
                    break;
                case ReportType.EMPTY_ID:
                    reportSet = Reports.GetEmptyNonEmptyIDDSForSaving((Empty_NonEmptyIdModel)this._ReportObject, this.allModeNames);
                    break;
                case ReportType.NONEMPTY_ID:
                    reportSet = Reports.GetEmptyNonEmptyIDDSForSaving((Empty_NonEmptyIdModel)this._ReportObject, this.allModeNames);
                    break;
                case ReportType.UNIQUE_FUNC_IR:
                    reportSet = Reports.GetUniqueFunctionsIRDataSet((UniqueFuncIRCountModel)this._ReportObject);
                    break;
                case ReportType.DISCONNECTED_TNS:
                    reportSet = Reports.GetDisconnectedTNReportDataSet((IList<Model.TNInfo>)this._ReportObject);
                    break;
                case ReportType.LabelSearch:
                    LabelIntronSearchReport labelSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = labelSearch.GetReportForTextFile(this.allModeNames,this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.IntronSearch:
                    LabelIntronSearchReport intronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = intronSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.ExcludeLabelSearch:
                    LabelIntronSearchReport ExcludelabelSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = ExcludelabelSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.ExcludeIntronSearch:
                    LabelIntronSearchReport ExcludeIntronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = ExcludeIntronSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.IntronSearchWithBrand:
                    reportSet = (DataSet)this._ReportObject;                    
                    break;
                case ReportType.LabelSearchWithBrand:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.BrandBasedSearch:
                    reportSet = Reports.GetBrandBasedSearchReport((BrandBasedSearchModel)this._ReportObject, this.m_brandSearchOptions, this.allModeNames, "BrandBasedSearch");
                    break;
                case ReportType.BrandModelBasedSearch:
                     modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                     reportSet =  modelInfoReport.GetBMSearchResultset(this.isTargetFilter);
                    break;
                case ReportType.BrandSearch:
                    BrandSearchModel brandSearch = (BrandSearchModel)this._ReportObject;
                    reportSet = brandSearch.GetBrandSearchResult();
                    break;
                case ReportType.MiscReport:
                    DataTable consolidateReport = (DataTable)this._ReportObject;
                    reportSet = new DataSet();
                    //Copy() used to avoid an exception occuring due to using same datatable in different datasets.
                    reportSet.Tables.Add(consolidateReport.Copy());
                    break;
                case ReportType.ExecIdMap:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.ExecIdPrefixSame:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.SYNTHESIZER_TABLE:
                    SynthesizerTableModel synthesizerTable = (SynthesizerTableModel)this._ReportObject;
                    reportSet = Reports.GetSynthesizerTableReportDataSet(synthesizerTable, m_IncludeIntron, this.ModeNames);
                    break;
                case ReportType.DUPLICATE_INTRONS:
                    LabelIntronSearchReport duplicateIntronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = duplicateIntronSearch.GetReportForTextFile(this.ModeNames,DisplayFilter.ShowAll);
                    break;
            }
            //|| ReportType.EMPTY_ID == this.reportType || ReportType.NONEMPTY_ID == this.reportType
            if (ReportType.UNIQUE_FUNC_IR == this.reportType || ReportType.DISCONNECTED_TNS == this.reportType || this.reportType == ReportType.MiscReport)
                return TextReportGenerator.GenerateNormalReport(reportSet, path, reportName, out this._saveFailMessage);
            if (reportSet != null)
               return TextReportGenerator.GenerateReport(reportSet, path, reportName, out this._saveFailMessage,this.ModeNames);

           return false;
        }
        private bool SaveToExcel(string path, string reportName)
        {
            DeviceRegionalSelection m_DeviceAliasSelection = null;
            DataSet reportSet = null;
            msgExportToExcel = 0;
            DeviceCategoryRecords deviceCategories = null;
            Boolean subdeviceflag = false;
            switch (this.reportType)
            {
                case ReportType.LocationSearch:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.IDList:
                    reportSet = Reports.GetIDByRegionModeBasedSet((IDByRegionReportModel)this._ReportObject, this._IDSetupCodeInfoParams);
                    break;
                case ReportType.SupportedIDByPlatform:
                    {
                        reportSet = (DataSet)this._ReportObject;
                    }
                    break;
                case ReportType.KeyFunction:
                    KeyFunctionModel keyFunctionReport = (KeyFunctionModel)this._ReportObject;
                    reportSet = keyFunctionReport.GetKeyFunctionReportDataSetForExcel(KeyFunctionReportType, this.ModeNames);
                    break;
                case ReportType.BSC:   
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }

                        deviceCategories = ShowDeviceCategorization(out subdeviceflag);
                        if (deviceCategories != null)
                            reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, subdeviceflag,ref m_KeepIdList);
                    }
                    break;
                case ReportType.BSCPIC:
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }
                        deviceCategories = ShowDeviceCategorization(out subdeviceflag);
                        if (deviceCategories != null)
                            reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, subdeviceflag,ref m_KeepIdList);
                    }
                    break;
                case ReportType.IDBrandList:
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }
                        deviceCategories = ShowDeviceCategorization(out subdeviceflag);
                        if (deviceCategories != null)
                        {
                            IdBrandReportModel idBrandReport = (IdBrandReportModel)this._ReportObject;
                            reportSet = idBrandReport.GetIDBrandModeBasedSet(this._IDSetupCodeInfoParams, deviceCategories, subdeviceflag);
                        }
                    }
                    break;
                case ReportType.MODELINFOPIC:
                    ModelInfoRecordCollection modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.ExportModelInfo(this._modelInfoSpecificFilters);
                    if (reportSet.Tables.Count > 1)
                    {
                        if (this._modelInfoSpecificFilters.QuickSetFormat == false)
                            msgExportToExcel = 1;
                    }
                    break;
                case ReportType.ModelInfoWithArdKey:
                    {
                        reportSet = ModelInfoReportOutputWithArdKey();                        
                    }
                    break;
                case ReportType.QuickSet:
                    {
                        modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;                        
                        reportSet = modelInfoReport.ExportQuickSetReport(this._modelInfoSpecificFilters,this._QuickSetInfoParams);                        
                    }
                    break;
                case ReportType.ModelInformation:
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.ExportModelInfo(this._modelInfoSpecificFilters);
                    if (reportSet.Tables.Count > 1)
                    {
                        if (this._modelInfoSpecificFilters.QuickSetFormat == false)
                            msgExportToExcel = 1;
                    }
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.ExportModelInfo(this._modelInfoSpecificFilters);
                    if(reportSet.Tables.Count > 1)
                    {
                        if (this._modelInfoSpecificFilters.QuickSetFormat == false)
                            msgExportToExcel = 1;
                    }
                    break;
                case ReportType.RestrictedIDList:
                    reportSet = Reports.GetRestrictedIDDSForSaving((RestrictedIdModel)this._ReportObject);
                    break;
                case ReportType.QASTATUS:
                    reportSet = Reports.GetQAStatusExcelReportDataSet((QAStatusReportModel)this._ReportObject);
                    break;
                case ReportType.EMPTY_ID:
                    reportSet = Reports.GetEmptyNonEmptyIDDSForSaving((Empty_NonEmptyIdModel)this._ReportObject);
                    break;
                case ReportType.NONEMPTY_ID:
                    reportSet = Reports.GetEmptyNonEmptyIDDSForSaving((Empty_NonEmptyIdModel)this._ReportObject);
                    break;
                case ReportType.UNIQUE_FUNC_IR:
                    reportSet = Reports.GetUniqueFunctionsIRDataSet((UniqueFuncIRCountModel)this._ReportObject);
                    break;
                case ReportType.DISCONNECTED_TNS:
                    reportSet = Reports.GetDisconnectedTNReportDataSet((IList<Model.TNInfo>)this._ReportObject);
                    break;
                case ReportType.LabelSearch:
                    LabelIntronSearchReport labelSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = labelSearch.GetReportForExcel(this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.IntronSearch:
                    LabelIntronSearchReport intronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = intronSearch.GetReportForExcel(this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.ExcludeLabelSearch:
                     LabelIntronSearchReport ExcludelabelSearch = (LabelIntronSearchReport)this._ReportObject;
                     reportSet = ExcludelabelSearch.GetReportForExcel(this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.ExcludeIntronSearch:
                     LabelIntronSearchReport ExcludeIntronSearch = (LabelIntronSearchReport)this._ReportObject;
                     reportSet = ExcludeIntronSearch.GetReportForExcel(this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.IntronSearchWithBrand:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.LabelSearchWithBrand:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.BrandBasedSearch:
                    reportSet = Reports.GetBrandBasedSearchReport((BrandBasedSearchModel)this._ReportObject, this.m_brandSearchOptions, this.allModeNames, "BrandBasedSearch");
                    break;
                case ReportType.BrandModelBasedSearch:
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.ExportBMSearchResultSet(this.isTargetFilter);
                    if (reportSet.Tables.Count > 1)
                    {
                        msgExportToExcel = 1;
                    }
                    break;
                case ReportType.BrandSearch:
                    BrandSearchModel brandSearch = (BrandSearchModel)this._ReportObject;
                    reportSet = brandSearch.GetBrandSearchResult();
                    break;
                case ReportType.MiscReport:
                    DataTable consolidateReport = (DataTable)this._ReportObject;
                    reportSet = new DataSet();
                    reportSet.Tables.Add(consolidateReport.Copy());//used to avoid a particular exception.
                    break;
                case ReportType.ExecIdMap:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.ExecIdPrefixSame:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.SYNTHESIZER_TABLE:
                    SynthesizerTableModel synthesizerTable = (SynthesizerTableModel)this._ReportObject;
                    reportSet = Reports.GetSynthesizerTableReportExcelPDFDataSet(synthesizerTable, m_IncludeIntron, this.ModeNames);
                    break;
                case ReportType.DUPLICATE_INTRONS:
                    LabelIntronSearchReport duplicateIntronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = duplicateIntronSearch.GetReportForExcel(DisplayFilter.ShowAll);                   
                    break;
            }

            if (reportSet != null)
            {                
                if ((this.reportType == ReportType.ModelInfoWithArdKey)||(this.reportType == ReportType.QuickSet))
                    return ExcelReportGenerator.GenerateReport(reportSet, path, reportName, out _saveFailMessage, "xlsx");
                else if ((this._modelInfoSpecificFilters  != null)&&((this.reportType == ReportType.ModelInformation)||(this.reportType == ReportType.MODELINFOPIC)||(this.reportType == ReportType.ModelInformation_EmptyDevice)||(this.reportType == ReportType.ExecIdMap)))
                {
                    if (this._modelInfoSpecificFilters.QuickSetFormat == true)
                        return ExcelReportGenerator.GenerateReport(reportSet, path, reportName, out _saveFailMessage, "xlsx");
                    else
                        return ExcelReportGenerator.GenerateReport(reportSet, path, reportName, out _saveFailMessage);
                }
                else
                    return ExcelReportGenerator.GenerateReport(reportSet, path, reportName, out _saveFailMessage);
            }

            return false;
        }
        private bool SaveToPdf(string path, string reportName)
        {
            DeviceRegionalSelection m_DeviceAliasSelection = null;
            DataSet reportSet = null;
            DeviceCategoryRecords deviceCategories = null;
            Boolean subdeviceflag = false;
            switch (this.reportType)
            {
                case ReportType.LocationSearch:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.IDList:
                    reportSet = Reports.GetIDByRegionModeBasedSet((IDByRegionReportModel)this._ReportObject, this._IDSetupCodeInfoParams);
                    break;
                case ReportType.SupportedIDByPlatform:
                    {
                        reportSet = (DataSet)this._ReportObject;
                    }
                    break;
                case ReportType.KeyFunction:
                    KeyFunctionModel keyFunctionReport = (KeyFunctionModel)this._ReportObject;
                    reportSet = keyFunctionReport.GetKeyFunctionReportDataSet(KeyFunctionReportType, this.ModeNames);
                    break;
                case ReportType.BSC:
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }

                        deviceCategories = ShowDeviceCategorization(out subdeviceflag);
                        if (deviceCategories != null)
                            reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, subdeviceflag,ref m_KeepIdList);
                    }
                    break;
                case ReportType.BSCPIC:
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }

                        deviceCategories = ShowDeviceCategorization(out subdeviceflag);
                        if (deviceCategories != null)
                            reportSet = Reports.GetBSCReportModeBasedSet((IDBSCReportModel)this._ReportObject, this._IDSetupCodeInfoParams, deviceCategories, subdeviceflag,ref m_KeepIdList);
                    }
                    break;
                case ReportType.IDBrandList:
                    m_DeviceAliasSelection = new DeviceRegionalSelection();
                    if (m_DeviceAliasSelection.ShowDialog() == DialogResult.OK)
                    {
                        if (DeviceAliasRegion == String.Empty || DeviceAliasRegion != m_DeviceAliasSelection.DeviceAliasRegion)
                        {
                            DeviceAliasRegion = m_DeviceAliasSelection.DeviceAliasRegion;
                            ResetTimer();
                        }
                        deviceCategories = ShowDeviceCategorization(out subdeviceflag);
                        if (deviceCategories != null)
                        {
                            IdBrandReportModel idBrandReport = (IdBrandReportModel)this._ReportObject;
                            reportSet = idBrandReport.GetIDBrandModeBasedSet(this._IDSetupCodeInfoParams, deviceCategories, subdeviceflag);
                        }
                    }
                    break;
                case ReportType.MODELINFOPIC:
                    //reportSet = Reports.GetModelInfoDataset((Model.ModelInfoRecordCollection)this._ReportObject, this._modelInfoSpecificFilters);
                    ModelInfoRecordCollection modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                    break;
                case ReportType.ModelInformation:
                    //reportSet = Reports.GetModelInfoDataset((Model.ModelInfoRecordCollection)this._ReportObject, this._modelInfoSpecificFilters);
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    //reportSet = Reports.GetModelInfoDataset((Model.ModelInfoRecordCollection)this._ReportObject, this._modelInfoSpecificFilters);
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                    break;
                case ReportType.RestrictedIDList:
                    reportSet = Reports.GetRestrictedIDDSForSaving((RestrictedIdModel)this._ReportObject);
                    break;
                case ReportType.QASTATUS:
                    reportSet = Reports.GetQAStatusExcelReportDataSet((QAStatusReportModel)this._ReportObject);
                    break;
                case ReportType.EMPTY_ID:
                    reportSet = Reports.GetEmptyNonEmptyIDDSForSaving((Empty_NonEmptyIdModel)this._ReportObject);
                    break;
                case ReportType.NONEMPTY_ID:
                    reportSet = Reports.GetEmptyNonEmptyIDDSForSaving((Empty_NonEmptyIdModel)this._ReportObject);
                    break;
                case ReportType.UNIQUE_FUNC_IR:
                    reportSet = Reports.GetUniqueFunctionsIRDataSet((UniqueFuncIRCountModel)this._ReportObject);
                    break;
                case ReportType.DISCONNECTED_TNS:
                    reportSet = Reports.GetDisconnectedTNReportDataSet((IList<Model.TNInfo>)this._ReportObject);
                    break;
                case ReportType.LabelSearch:
                    LabelIntronSearchReport labelSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = labelSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.IntronSearch:
                    LabelIntronSearchReport intronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = intronSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.ExcludeLabelSearch:
                    LabelIntronSearchReport ExcludelabelSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = ExcludelabelSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.ExcludeIntronSearch:
                    LabelIntronSearchReport ExcludeIntronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = ExcludeIntronSearch.GetReportForTextFile(this.allModeNames, this._LabelIntronFilters.DisplayMode);
                    break;
                case ReportType.IntronSearchWithBrand:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.LabelSearchWithBrand:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.BrandBasedSearch:
                    reportSet = Reports.GetBrandBasedSearchReport((BrandBasedSearchModel)this._ReportObject, this.m_brandSearchOptions, this.allModeNames, "BrandBasedSearch");
                    break;
                case ReportType.BrandModelBasedSearch:
                    modelInfoReport = (ModelInfoRecordCollection)this._ReportObject;
                    reportSet = modelInfoReport.GetBMSearchResultset(this.isTargetFilter);
                    break;
                case ReportType.BrandSearch:
                    BrandSearchModel brandSearch = (BrandSearchModel)this._ReportObject;
                    reportSet = brandSearch.GetBrandSearchResult();
                    break;
                case ReportType.MiscReport:
                    DataTable consolidateReport = (DataTable)this._ReportObject;
                    reportSet = new DataSet();
                    reportSet.Tables.Add(consolidateReport.Copy());
                    break;
                case ReportType.ExecIdMap:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.ExecIdPrefixSame:
                    reportSet = (DataSet)this._ReportObject;
                    break;
                case ReportType.SYNTHESIZER_TABLE:
                    SynthesizerTableModel synthesizerTable = (SynthesizerTableModel)this._ReportObject;
                    reportSet = Reports.GetSynthesizerTableReportExcelPDFDataSet(synthesizerTable, m_IncludeIntron, this.ModeNames);
                    break;
                case ReportType.DUPLICATE_INTRONS:
                    LabelIntronSearchReport duplicateIntronSearch = (LabelIntronSearchReport)this._ReportObject;
                    reportSet = duplicateIntronSearch.GetReportForTextFile(this.ModeNames,DisplayFilter.ShowAll);
                    break;
            }

            if (reportSet != null)
                return PdflReportGenerator.GenerateReport(reportSet, path, reportName, out _saveFailMessage);

            return false;
        }
        private DeviceCategoryRecords GetDeviceCategorization()
        {
            DeviceCategory category = null;
            DeviceCategoryRecords records = new DeviceCategoryRecords();
            Int32 wsNo = 1;
            //To Collect all distinct device types
            DataTable dtsubdeviceclassification = new DataTable("SubDevices");
            DataRow dr = null;
            dtsubdeviceclassification.Columns.Add("Mode");
            dtsubdeviceclassification.Columns.Add("Sub Device Type");
            IList<String> modes = new List<String>();
            switch (this.reportType)
            {
                case ReportType.BSC:                    
                    IDBSCReportModel idbsc = (IDBSCReportModel)this._ReportObject;
                    modes = idbsc.GetDistinctModes();                    
                    foreach (String mode in modes)
                    {
                        category = new DeviceCategory();
                        category.Modes = mode;
                        category.WorkSheet = wsNo++;
                        //Get Sub Device Information for each Mode
                        IList<String> subdeviceList = idbsc.GetDistinctSubDeviceTypes(mode);
                        foreach (String devices in subdeviceList)
                        {
                            if (dr == null)
                                dr = dtsubdeviceclassification.NewRow();
                            dr[0] = mode;
                            dr[1] = devices;
                            dtsubdeviceclassification.Rows.Add(dr);
                            dr = null;
                        }
                        records.Add(category);
                        category = null;
                    }                    
                    break;
                case ReportType.BSCPIC:
                    IDBSCReportModel idpicbsc = (IDBSCReportModel)this._ReportObject;
                    modes = idpicbsc.GetDistinctModes();
                    foreach (String mode in modes)
                    {
                        category = new DeviceCategory();
                        category.Modes = mode;
                        category.WorkSheet = wsNo++;
                        //Get Sub Device Information for each Mode
                        IList<String> subdeviceList = idpicbsc.GetDistinctSubDeviceTypes(mode);
                        foreach (String devices in subdeviceList)
                        {
                            if (dr == null)
                                dr = dtsubdeviceclassification.NewRow();
                            dr[0] = mode;
                            dr[1] = devices;
                            dtsubdeviceclassification.Rows.Add(dr);
                            dr = null;
                        }
                        records.Add(category);
                        category = null;
                    }
                    break;
                case ReportType.IDBrandList:                    
                    IdBrandReportModel idbrands = (IdBrandReportModel)this._ReportObject;
                    //modes = idbrands.GetDistinctModes();
                    modes = idbrands.Modes;                
                    foreach (String mode in modes)
                    {
                        category = new DeviceCategory();
                        category.Modes = mode;
                        category.WorkSheet = wsNo++;
                        //Get Sub Device Information for each Mode
                        IList<String> subdeviceList = idbrands.GetDistinctSubDeviceList(mode);
                        foreach (String devices in subdeviceList)
                        {
                            if (dr == null)
                                dr = dtsubdeviceclassification.NewRow();
                            dr[0] = mode;
                            dr[1] = devices;
                            dtsubdeviceclassification.Rows.Add(dr);
                            dr = null;
                        }
                        records.Add(category);
                        category = null;
                    }            
                    break;                
            }
            //Sort Device Types
            DataView dataView = new DataView(dtsubdeviceclassification);
            dataView.Sort = "[Mode],[Sub Device Type]";
            dtsubdeviceclassification = dataView.ToTable();
            dtsubdeviceclassification.AcceptChanges();
            foreach (DataRow items in dtsubdeviceclassification.Rows)
            {
                category = records.GetModeRecord(items[0].ToString());
                //Add Sub device types to the category
                category.SubDevices.Add(items[1].ToString());
            }                 
            return records;
        }
        private Boolean ShowReportOption()
        {
            ReportOption m_reportOption = new ReportOption();
            m_reportOption.ShowDialog();
            return m_reportOption.ReportOptions;

        }
        private DeviceCategoryRecords ShowDeviceCategorization(out Boolean subdeviceflag)
        {
            subdeviceflag = false;
            IList<String> modes = new List<String>();
            switch (this.reportType)
            {
                case ReportType.BSC:
                    IDBSCReportModel idbsc = (IDBSCReportModel)this._ReportObject;
                    modes = idbsc.GetDistinctModes();
                    break;
                case ReportType.BSCPIC:
                    IDBSCReportModel idpicbsc = (IDBSCReportModel)this._ReportObject;
                    modes = idpicbsc.GetDistinctModes();
                    break;
                case ReportType.IDBrandList:
                    IdBrandReportModel idbrands = (IdBrandReportModel)this._ReportObject;
                    //modes = idbrands.GetDistinctModes();
                    modes = idbrands.Modes;
                    break;
                case ReportType.IDList:
                    break;
            }         
            DeviceCategoryRecords records = new DeviceCategoryRecords();
            DeviceCategory category = null;
            Int32 wsNo = 1;
            
            foreach (String mode in modes)
            {
                category = new DeviceCategory();
                category.Modes = mode;            
                category.WorkSheet = wsNo++;
                records.Add(category);
            }

            DeviceCategoryForm deviceCategory = new DeviceCategoryForm();
            deviceCategory.DBMReport = this;
            deviceCategory.Category = records;
            DialogResult result = deviceCategory.ShowDialog();            
            if (result == DialogResult.OK)
            {
                this._IDSetupCodeInfoParams.IncludeModeChar = deviceCategory.IncludeModeChar;
                subdeviceflag = deviceCategory.WithSubdevices;
                return deviceCategory.Category;
            }

            return null;
        }
        #endregion   
     
        #region Report Column Selector

        #region Variables
        private DataGridView dataGridView = null;
        private CheckedListBox checkedListBox = null;
        private ToolStripDropDown toolStripDropDown = null;
        private Int32 intMaxHeight = 300;
        private Int32 intWidth = 200;
        #endregion

        #region Properties
        public Int32 MaxHeight
        {
            get { return intMaxHeight; }
            set { intMaxHeight = value; }
        }
        public Int32 Width
        {
            get { return intWidth; }
            set { intWidth = value; }
        }
        private CheckedListBox ReportCheckedListBox
        {
            get { return checkedListBox; }
            set { checkedListBox = value; }
        }
        private ToolStripDropDown ReportToolStripDropDown
        {
            get { return toolStripDropDown; }
            set { toolStripDropDown = value; }
        }
        public DataGridView ReportDataGridView
        {
            get { return dataGridView; }
            set
            {
                dataGridView = value;
                // Attach CellMouseClick handler to DataGridView
                if (dataGridView != null)
                {
                    dataGridView.CellMouseClick += new DataGridViewCellMouseEventHandler(dataGridView_CellMouseClick);
                }
            }
        }
        #endregion        

        private void ShowReportColumnSelector(DataGridView m_DataGridView)
        {            
            ReportCheckedListBox = new CheckedListBox();
            ReportCheckedListBox.CheckOnClick = true;
            //ReportCheckedListBox.ItemCheck += new ItemCheckEventHandler(ReportCheckedListBox_ItemCheck);
            ToolStripControlHost controlHost = new ToolStripControlHost(ReportCheckedListBox);
            controlHost.Padding = Padding.Empty;
            controlHost.Margin = Padding.Empty;
            controlHost.Dock = DockStyle.Fill;
            controlHost.AutoSize = true;

            ReportToolStripDropDown = new ToolStripDropDown();
            ReportToolStripDropDown.Padding = Padding.Empty;
            ReportToolStripDropDown.Items.Add(controlHost);

            if (this.reportType == ReportType.ModelInformation || this.reportType == ReportType.MODELINFOPIC || this.reportType == ReportType.ModelInformation_EmptyDevice )
            {
                CheckBox chkQuickSetFormat = new CheckBox();
                chkQuickSetFormat.Text = "Quickset Format";

                chkQuickSetFormat.Checked = _modelInfoSpecificFilters.QuickSetFormat;
                chkQuickSetFormat.BackColor = controlHost.BackColor;
                chkQuickSetFormat.CheckedChanged += chkQuickSetFormat_CheckedChanged;
                controlHost = new ToolStripControlHost(chkQuickSetFormat);
                controlHost.Padding = Padding.Empty;
                controlHost.Margin = Padding.Empty;
                controlHost.AutoSize = true;
                controlHost.Margin = new Padding(2, 2, 2, 2);
                controlHost.Dock = DockStyle.Fill;
                ReportToolStripDropDown.Items.Add(controlHost);
            }
            if (this.reportType == ReportType.ExecIdMap)
            {
                if (_modelInfoSpecificFilters == null)
                    _modelInfoSpecificFilters = new ModelInfoSpecific();

                CheckBox chkQuickSetFormat = new CheckBox();
                chkQuickSetFormat.Text = "Custom Format";

                chkQuickSetFormat.Checked = _modelInfoSpecificFilters.QuickSetFormat;
                chkQuickSetFormat.BackColor = controlHost.BackColor;
                chkQuickSetFormat.CheckedChanged += chkQuickSetFormat_CheckedChanged;
                controlHost = new ToolStripControlHost(chkQuickSetFormat);
                controlHost.Padding = Padding.Empty;
                controlHost.Margin = Padding.Empty;
                controlHost.AutoSize = true;
                controlHost.Margin = new Padding(2, 2, 2, 2);
                controlHost.Dock = DockStyle.Fill;
                ReportToolStripDropDown.Items.Add(controlHost);
            }
            Button btnApply = new Button();
            btnApply.Name = "btnApply";
            btnApply.Text = "Apply";
            btnApply.Click += new EventHandler(btnApply_Click);
            controlHost = new ToolStripControlHost(btnApply);
            controlHost.Padding = Padding.Empty;
            controlHost.Margin = new Padding(2,2,2,2);
            controlHost.Dock = DockStyle.Fill;
            ReportToolStripDropDown.Items.Add(controlHost);

            this.ReportDataGridView = m_DataGridView;
        }
        void chkQuickSetFormat_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            if (chk.Checked)
            {
                _modelInfoSpecificFilters.QuickSetFormat = true;
                MessageBox.Show("Supports Office Excel 2010 and Above", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                _modelInfoSpecificFilters.QuickSetFormat = false;
            }
        }
        void btnApply_Click(object sender, EventArgs e)
        {
            if (ReportCheckedListBox != null)
            {
                for (int i = 0; i < ReportCheckedListBox.Items.Count; i++)
                {
                    if (_modelInfoSpecificFilters.RowSelectorName == String.Empty)
                        _modelInfoSpecificFilters.ColumnSelector[ReportCheckedListBox.Items[i].ToString()] = ReportCheckedListBox.GetItemChecked(i);
                    else
                    {
                        _modelInfoSpecificFilters.RowSelector[ReportCheckedListBox.Items[i].ToString()] = ReportCheckedListBox.GetItemChecked(i);
                    }
                }

                ReportToolStripDropDown.Close();
                if (this.reportType == ReportType.ModelInformation || this.reportType == ReportType.MODELINFOPIC || this.reportType == ReportType.ModelInformation_EmptyDevice)
                {
                    ShowReport("Model Information");
                }
                else if (this.reportType == ReportType.ModelInfoWithArdKey)
                {
                    ShowReport("Model Info Report with Ard Key");
                }
                else if (this.reportType == ReportType.ExecIdMap)
                {
                    ResetTimer();
                    ShowReport("ExecIdMap");
                }
                else if (this.reportType == ReportType.QuickSet)
                {                    
                    ShowReport("QuickSet Model Info Report");
                }
            }
        }
        void ReportCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (ReportCheckedListBox.SelectedItem != null)
            {
                if (_modelInfoSpecificFilters.RowSelectorName == String.Empty)
                    _modelInfoSpecificFilters.ColumnSelector[ReportCheckedListBox.SelectedItem.ToString()] = (e.NewValue == CheckState.Checked);
                else
                {
                    _modelInfoSpecificFilters.RowSelector[ReportCheckedListBox.SelectedItem.ToString()] = (e.NewValue == CheckState.Checked);                   
                }
                ShowReport("Model Information");
            }
        }       
        void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex == -1 && e.ColumnIndex != -1)
            {
                switch (_modelInfoSpecificFilters.ModelInfoOptions)
                {
                    case ModelInfoSpecific.ModelReportOptions.ModelBased:
                        {
                            if (this._ReportObject != null)
                            {
                                if (this.ReportDataGridView.Columns[e.ColumnIndex].HeaderText == "MainDevice" || this.ReportDataGridView.Columns[e.ColumnIndex].HeaderText == "SubDevice")
                                {
                                    DataSet reportSet = null;
                                    if (this.reportType == ReportType.ModelInfoWithArdKey)
                                    {
                                        reportSet = (DataSet)this._ReportObject;
                                        if (reportSet.Tables[0].Rows.Count > 0)
                                            reportSet.Tables[0].TableName = "ModelInfoReport";
                                    }
                                    else
                                    {
                                        ModelInfoRecordCollection modelInfoPICReport = (ModelInfoRecordCollection)this._ReportObject;
                                        reportSet = modelInfoPICReport.GetModelInfoDataset(this._modelInfoSpecificFilters, ref m_KeepIdList);
                                    }
                                    if (this.ReportDataGridView.Columns[e.ColumnIndex].HeaderText == "MainDevice")
                                    {
                                        ReportCheckedListBox.Items.Clear();
                                        _modelInfoSpecificFilters.RowSelectorName = "MainDevice";
                                        //_modelInfoSpecificFilters.ColumnSelector = null;
                                        if (_modelInfoSpecificFilters.RowSelector != null)
                                            _modelInfoSpecificFilters.RowSelector.Clear();
                                        if (_modelInfoSpecificFilters.RowSelector == null)
                                            _modelInfoSpecificFilters.RowSelector = new Dictionary<String, Boolean>();
                                        DataTable dtMainDevices = GetDistinctRecords(reportSet.Tables["ModelInfoReport"], new String[] { "MainDevice" });
                                        if (dtMainDevices.Rows.Count > 0)
                                        {
                                            foreach (DataRow item in dtMainDevices.Rows)
                                            {
                                                if (!String.IsNullOrEmpty(item[0].ToString()))
                                                {
                                                    _modelInfoSpecificFilters.RowSelector.Add(item[0].ToString(), true);
                                                    ReportCheckedListBox.Items.Add(item[0].ToString(), true);
                                                }
                                            }
                                        }
                                    }
                                    if (this.ReportDataGridView.Columns[e.ColumnIndex].HeaderText == "SubDevice")
                                    {
                                        _modelInfoSpecificFilters.RowSelectorName = "SubDevice";
                                        ReportCheckedListBox.Items.Clear();
                                        if (_modelInfoSpecificFilters.RowSelector != null)
                                            _modelInfoSpecificFilters.RowSelector.Clear();
                                        if (_modelInfoSpecificFilters.RowSelector == null)
                                            _modelInfoSpecificFilters.RowSelector = new Dictionary<String, Boolean>();
                                        DataTable dtSubDevices = GetDistinctRecords(reportSet.Tables["ModelInfoReport"], new String[] { "SubDevice" });
                                        if (dtSubDevices.Rows.Count > 0)
                                        {
                                            foreach (DataRow item in dtSubDevices.Rows)
                                            {
                                                if (!String.IsNullOrEmpty(item[0].ToString()))
                                                {
                                                    _modelInfoSpecificFilters.RowSelector.Add(item[0].ToString(), true);
                                                    ReportCheckedListBox.Items.Add(item[0].ToString(), true);
                                                }
                                            }
                                        }
                                    }
                                    if (ReportCheckedListBox.Items.Count > 0)
                                    {
                                        Int32 PreferredHeight = (ReportCheckedListBox.Items.Count * 16) + 7;
                                        ReportCheckedListBox.Height = (PreferredHeight < MaxHeight) ? PreferredHeight : MaxHeight;
                                        ReportCheckedListBox.Width = this.Width;     
                                        
                                        ReportToolStripDropDown.Show(ReportDataGridView,ReportDataGridView.PointToClient(System.Windows.Forms.Cursor.Position));
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            if (e.Button == MouseButtons.Right && e.RowIndex == -1 && e.ColumnIndex == -1)
            {
                if (this.reportType != ReportType.QuickSet)
                {
                    _modelInfoSpecificFilters.RowSelectorName = String.Empty;
                    ReportCheckedListBox.Items.Clear();
                    _modelInfoSpecificFilters.RowSelector = null;
                    if (_modelInfoSpecificFilters.ColumnSelector != null)
                        _modelInfoSpecificFilters.ColumnSelector.Clear();
                    if (_modelInfoSpecificFilters.ColumnSelector == null)
                        _modelInfoSpecificFilters.ColumnSelector = new Dictionary<String, Boolean>();
                    switch (_modelInfoSpecificFilters.ModelInfoOptions)
                    {
                        case ModelInfoSpecific.ModelReportOptions.ModelBased:
                            foreach (DataGridViewColumn dgvCol in ReportDataGridView.Columns)
                            {
                                if (dgvCol.HeaderText != "Model" && dgvCol.HeaderText != "Brand" && dgvCol.HeaderText != "T/R" && dgvCol.HeaderText != "ID" && dgvCol.HeaderText != "Executor")
                                {
                                    if (dgvCol.HeaderText == "TN Link")
                                    {
                                        if (_modelInfoSpecificFilters.SelectTNLink == false)
                                        {
                                            ReportCheckedListBox.Items.Add(dgvCol.HeaderText, dgvCol.Visible);
                                            _modelInfoSpecificFilters.ColumnSelector.Add(dgvCol.HeaderText, dgvCol.Visible);
                                        }
                                    }
                                    else
                                    {
                                        ReportCheckedListBox.Items.Add(dgvCol.HeaderText, dgvCol.Visible);
                                        _modelInfoSpecificFilters.ColumnSelector.Add(dgvCol.HeaderText, dgvCol.Visible);
                                    }
                                }
                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.TNBased:
                            foreach (DataGridViewColumn dgvCol in ReportDataGridView.Columns)
                            {
                                if (dgvCol.HeaderText != "TN" && dgvCol.HeaderText != "Mode")
                                {
                                    ReportCheckedListBox.Items.Add(dgvCol.HeaderText, dgvCol.Visible);
                                    _modelInfoSpecificFilters.ColumnSelector.Add(dgvCol.HeaderText, dgvCol.Visible);
                                }
                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.BrandBased:
                            foreach (DataGridViewColumn dgvCol in ReportDataGridView.Columns)
                            {
                                if (dgvCol.HeaderText != "Mode" && dgvCol.HeaderText != "Brand")
                                {
                                    ReportCheckedListBox.Items.Add(dgvCol.HeaderText, dgvCol.Visible);
                                    _modelInfoSpecificFilters.ColumnSelector.Add(dgvCol.HeaderText, dgvCol.Visible);
                                }
                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.IDBased:
                            foreach (DataGridViewColumn dgvCol in ReportDataGridView.Columns)
                            {
                                if (dgvCol.HeaderText != "ID")
                                {
                                    ReportCheckedListBox.Items.Add(dgvCol.HeaderText, dgvCol.Visible);
                                    _modelInfoSpecificFilters.ColumnSelector.Add(dgvCol.HeaderText, dgvCol.Visible);
                                }
                            }
                            break;
                    }

                    //if (ReportCheckedListBox.Items.Count > 0)
                    //if is disabled to display Quickset Format CheckBox for User
                    {
                        Int32 PreferredHeight = (ReportCheckedListBox.Items.Count * 16) + 7;
                        ReportCheckedListBox.Height = (PreferredHeight < MaxHeight) ? PreferredHeight : MaxHeight;
                        ReportCheckedListBox.Width = this.Width;
                        ReportToolStripDropDown.Show(ReportDataGridView.PointToScreen(new System.Drawing.Point(e.X, e.Y)));
                    }
                }
            }
        }
        #endregion
    }

    #region Type enums
    public enum ReportType
    {
        IDList,
        RestrictedIDList,
        BSC,
        BSCPIC,
        QASTATUS,
        DISCONNECTED_TNS,
        NONEMPTY_ID,
        EMPTY_ID,     
        SupportedIDByPlatform,
        UNIQUE_FUNC_IR,
        IDBrandList,
        ModelInformation,
        MODELINFOPIC,
        ModelInfoWithArdKey,
        ModelInfoWithArdKeyRegenerate,
        EXIDTNInfo,
        LabelSearch,
        IntronSearch,
        ExcludeLabelSearch,
        ExcludeIntronSearch,
        IntronSearchWithBrand,
        LabelSearchWithBrand,
        BrandBasedSearch,
        BrandModelBasedSearch,
        BrandSearch,
        MiscReport,
        ExecIdMap,
        ExecIdPrefixSame,
        SYNTHESIZER_TABLE,
        DUPLICATE_INTRONS,
        BRAND_TABLE_ROM,
        ModelInformation_EmptyDevice,
        QuickSet,
        QuickSetRegenerate,
        KeyFunction,
        EdId,
        LocationSearch
    }

    public enum SaveOption
    {
        Excel,
        Text,
        Pdf
    }
    #endregion
        
    #region Config
    public static class Config
    {
        private static String _DB = String.Empty;
        public static String DB
        {
            get
            {
                _DB = CommonForms.Configuration.GetConfigItem("DataBase");
                return _DB;
            }
        }
        public static SelectType IDFilters
        {
            get
            {
                //Get the Saved Selection Type
                SelType _type = SelType.MODE;
                String _strIdFilterSel = CommonForms.Configuration.GetConfigItem("IdFilterSel");
                switch (_strIdFilterSel)
                {
                    case "RANGE":
                        _type = SelType.RANGE;
                        break;
                    case "MODE":
                        _type = SelType.MODE;
                        break;
                    case "FILE":
                        _type = SelType.FILE;
                        break;
                }

                //Get the Saved Selection Values
                SelectType _selectType = new SelectType();
                String _strIdFilterRes = CommonForms.Configuration.GetConfigItem("IdFilterRes");
                switch (_type)
                {
                    case SelType.RANGE:
                        _selectType.SelectionType = SelType.RANGE;
                        _selectType.Result = _strIdFilterRes;
                        break;
                    case SelType.MODE:
                        _selectType.SelectionType = SelType.MODE;
                        _selectType.Result = _strIdFilterRes;
                        break;
                    case SelType.FILE:
                        _selectType.SelectionType = SelType.FILE;
                        _selectType.Result = _strIdFilterRes;
                        break;
                }
                return _selectType;
            }
        }
    }
    #endregion
}