using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.Service;
namespace UEI.Workflow2010.Report
{
    public partial class ReportMainForm : Form
    {
        #region Variables
        private ReportMain _CurrentReportDbm = null;        
        private ReportType _ReportType = ReportType.IDList;
        public bool _ReportSaveStatus = false;
        #endregion
        #region Constructor
        public ReportMainForm()
        {
            InitializeComponent();                       
        }
        //public ReportMainForm(ReportType reportType,IList<String> idList)
        //{
        //    InitializeComponent();
        //    this._ReportType = reportType;
        //    _CurrentReportDbm = new ReportMain(reportType,this, idList,"");
        //}
        public ReportMainForm(ReportType reportType)
        {
            InitializeComponent();
           
            this._ReportType = reportType;
           _CurrentReportDbm = new ReportMain(this._ReportType, this);            
        }
        public ReportMainForm(ReportType reportType,Form mdiParent,IList<String> idList,String ProjectName)
        {
            InitializeComponent();
            this.MdiParent = mdiParent;
            this._ReportType = reportType;
            switch (this._ReportType)
            {
                case ReportType.MODELINFOPIC:
                    //Add the is target filter
                    ToolStripComboBox isTargetCombo = new ToolStripComboBox();
                    isTargetCombo.Items.Add("TargetAndRemoteModel");
                    isTargetCombo.Items.Add("TargetModelOnly");
                    isTargetCombo.Items.Add("RemoteModelOnly");

                    isTargetCombo.Name = "IsTargetModelFilter";
                    isTargetCombo.SelectedIndexChanged += new EventHandler(isTargetCombo_SelectedIndexChanged);
                    isTargetCombo.DropDownStyle = ComboBoxStyle.DropDownList;
                    isTargetCombo.AutoSize = true;
                    isTargetCombo.SelectedIndex = 0;
                    ShowDisplayFilter();
                    ReportToolStrip.Items.Add(new ToolStripSeparator());
                    ToolStripLabel modelCountLbl = new ToolStripLabel();
                    modelCountLbl.Name = "modelCountLbl";

                    ReportToolStrip.Items.Add(isTargetCombo);
                    ReportToolStrip.Items.Add(new ToolStripSeparator());
                    //ReportToolStrip.Items.Add(displayModeButtons);

                    ReportToolStrip.Items.Add(modelCountLbl);
                    //ReportToolStrip.Items.Add(new ToolStripSeparator());
                    ReportToolStrip.Visible = true;
                    break;
            }
            _CurrentReportDbm = new ReportMain(this._ReportType,this,idList,ProjectName);
        }                
        public ReportMainForm(ReportType reportType,Form mdiParent)
        {
            InitializeComponent();
            this._ReportType = reportType;
            this.MdiParent = mdiParent;
            InitializeCustomSettings();
            _CurrentReportDbm = new ReportMain(this._ReportType, this);
        }
        #endregion
        public DataGridView ReportGridView
        {
            get { return this.dgSearchResult; }
        }
        public ToolStrip ReportToolStrip
        {
            get { return this.rMFToolStrip; }
        }
        #region About
        private void dbmAboutHelpMenu_Click(object sender, EventArgs e)
        {
            AboutBox frmAbout = new AboutBox();
            frmAbout.ShowDialog();
        }
        #endregion
        private void InitializeCustomSettings()
        {
            switch (this._ReportType)
            {                
                case ReportType.ModelInformation:
                        //Add the is target filter
                        ToolStripComboBox isTargetCombo = new ToolStripComboBox();
                        isTargetCombo.Items.Add("TargetAndRemoteModel");
                        isTargetCombo.Items.Add("TargetModelOnly");
                        isTargetCombo.Items.Add("RemoteModelOnly");

                        isTargetCombo.Name = "IsTargetModelFilter";
                        isTargetCombo.SelectedIndexChanged += new EventHandler(isTargetCombo_SelectedIndexChanged);
                        isTargetCombo.DropDownStyle = ComboBoxStyle.DropDownList;
                        isTargetCombo.AutoSize = true;
                        isTargetCombo.SelectedIndex = 0;
                        ShowDisplayFilter();
                        ReportToolStrip.Items.Add(new ToolStripSeparator());
                        ToolStripLabel modelCountLbl = new ToolStripLabel();
                        modelCountLbl.Name = "modelCountLbl";
                        
                        ReportToolStrip.Items.Add(isTargetCombo);
                        ReportToolStrip.Items.Add(new ToolStripSeparator());    
                        //ReportToolStrip.Items.Add(displayModeButtons);
                        
                        ReportToolStrip.Items.Add(modelCountLbl);
                        //ReportToolStrip.Items.Add(new ToolStripSeparator());
                        ReportToolStrip.Visible = true;                    
                    break;
                case ReportType.ModelInformation_EmptyDevice:
                    //Add the is target filter
                    isTargetCombo = new ToolStripComboBox();
                    isTargetCombo.Items.Add("TargetAndRemoteModel");
                    isTargetCombo.Items.Add("TargetModelOnly");
                    isTargetCombo.Items.Add("RemoteModelOnly");

                    isTargetCombo.Name = "IsTargetModelFilter";
                    isTargetCombo.SelectedIndexChanged += new EventHandler(isTargetCombo_SelectedIndexChanged);
                    isTargetCombo.DropDownStyle = ComboBoxStyle.DropDownList;
                    isTargetCombo.AutoSize = true;
                    isTargetCombo.SelectedIndex = 0;
                    ShowDisplayFilter();
                    ReportToolStrip.Items.Add(new ToolStripSeparator());
                    modelCountLbl = new ToolStripLabel();
                    modelCountLbl.Name = "modelCountLbl";

                    ReportToolStrip.Items.Add(isTargetCombo);
                    ReportToolStrip.Items.Add(new ToolStripSeparator());
                    //ReportToolStrip.Items.Add(displayModeButtons);

                    ReportToolStrip.Items.Add(modelCountLbl);
                    //ReportToolStrip.Items.Add(new ToolStripSeparator());
                    ReportToolStrip.Visible = true;
                    break;             
                case ReportType.LabelSearch:                    
                    ShowDisplayFilter();
                    break;
                case ReportType.IntronSearch:
                    ShowDisplayFilter();
                    break;               
            }
        }
        void displayModeClick(object sender, EventArgs e)
        {
            ToolStripButton selectedDisplayMode = (ToolStripButton)sender;
            ToolStripDropDownButton dropDown = (ToolStripDropDownButton)ReportToolStrip.Items["DisplayModes"];

            if (selectedDisplayMode.CheckState == CheckState.Checked)
            {
                Model.DisplayFilter selectedMode = UEI.Workflow2010.Report.Model.DisplayFilter.ShowAll;

                if (selectedDisplayMode.Name == "DistinctMode")
                {
                    ToolStripButton showAllModeBtn = (ToolStripButton)dropDown.DropDownItems["ShowAllMode"];
                    showAllModeBtn.CheckState = CheckState.Unchecked;
                    showAllModeBtn.Enabled = true;

                    selectedMode = Model.DisplayFilter.Distinct;

                    //this._CurrentReportDbm.FilterModelInfoReport(Model.DisplayFilter.Distinct);
                    
                }
                else if (selectedDisplayMode.Name == "ShowAllMode")
                {
                    ToolStripButton distinctModeBtn = (ToolStripButton)dropDown.DropDownItems["DistinctMode"];
                    distinctModeBtn.CheckState = CheckState.Unchecked;
                    distinctModeBtn.Enabled = true;

                    selectedMode = Model.DisplayFilter.ShowAll;

                    //this._CurrentReportDbm.FilterModelInfoReport(Model.DisplayFilter.ShowAll);
                }

                selectedDisplayMode.Enabled = false;//to avoid repeated clicking on checked button.

                switch(this._ReportType)
                {
                    case ReportType.ModelInformation:
                        this._CurrentReportDbm.FilterModelInfoReport(selectedMode);
                        break;
                    case ReportType.ModelInformation_EmptyDevice:
                        this._CurrentReportDbm.FilterModelInfoReport(selectedMode);
                        break;
                    case ReportType.MODELINFOPIC:
                        this._CurrentReportDbm.FilterModelInfoReport(selectedMode);
                        break;
                    case ReportType.LabelSearch:
                        this._CurrentReportDbm.FilterLabelSearchReport(selectedMode);
                        break;
                    case ReportType.IntronSearch:
                        this._CurrentReportDbm.FilterLabelSearchReport(selectedMode);
                        break;                   
                }                
            }
        }
        void isTargetCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._CurrentReportDbm != null)
            {
                ToolStripComboBox isTargetCombo = (ToolStripComboBox)sender;
                
                switch (isTargetCombo.Text)
                {
                    case "TargetModelOnly":
                        this._CurrentReportDbm.FilterModelInfoReport(Model.ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly);
                        break;
                    case "RemoteModelOnly":
                        this._CurrentReportDbm.FilterModelInfoReport(Model.ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly);
                        break;
                    default:
                        this._CurrentReportDbm.FilterModelInfoReport(Model.ModelInfoSpecific.IsTargetFilter.ShowTargetAndRemote);
                        break;
                }
            }
        }
        private void ShowDisplayFilter()
        {
            //Display filters
            ToolStripDropDownButton displayModeButtons = new ToolStripDropDownButton("Display Modes");
            displayModeButtons.Name = "DisplayModes";

            ToolStripButton distinctBtn = new ToolStripButton("Distinct", null, new EventHandler(displayModeClick), "DistinctMode");
            ToolStripButton showAllBtn = new ToolStripButton("Show All", null, new EventHandler(displayModeClick), "ShowAllMode");
            distinctBtn.CheckOnClick = true;
            showAllBtn.CheckOnClick = true;
            showAllBtn.CheckState = CheckState.Checked;
            showAllBtn.Enabled = false;
            displayModeButtons.DropDownItems.AddRange(new ToolStripItem[] { distinctBtn, showAllBtn });
            ReportToolStrip.Items.Add(displayModeButtons);

            //Add Search Drop Down for Secondary Key Label and Intron Search
            if (this._ReportType == ReportType.LabelSearch || this._ReportType == ReportType.IntronSearch)
            {
                ToolStripDropDownButton searchButtons = new ToolStripDropDownButton("Search");
                searchButtons.Name = "Search";

                ToolStripButton labelSearchButton = new ToolStripButton("Label Search", null, new EventHandler(searchModeClick), "LabelSearch");
                ToolStripButton intronSearchButton = new ToolStripButton("Intron Search", null, new EventHandler(searchModeClick), "IntronSearch");

                labelSearchButton.CheckOnClick = true;
                intronSearchButton.CheckOnClick = true;

                searchButtons.DropDownItems.AddRange(new ToolStripItem[] { labelSearchButton, intronSearchButton });
                ReportToolStrip.Items.Add(searchButtons);
            }

        }
        void searchModeClick(Object sender, EventArgs e)
        {
            ToolStripDropDownButton dropDowndisplaymode = (ToolStripDropDownButton)ReportToolStrip.Items["DisplayModes"];
            ToolStripButton selectedDisplayMode = null;
            foreach (ToolStripButton item in dropDowndisplaymode.DropDownItems)
            {                             
                if (item.CheckState == CheckState.Checked)
                {
                    selectedDisplayMode = item;                    
                }               
            }

            ToolStripButton selectedSearchMode = (ToolStripButton)sender;
            ToolStripDropDownButton dropDown = (ToolStripDropDownButton)ReportToolStrip.Items["Search"];
            if (selectedSearchMode.Name == "LabelSearch")
            {
                ToolStripButton searchModebutton = (ToolStripButton)dropDown.DropDownItems["IntronSearch"];
                searchModebutton.CheckState = CheckState.Unchecked;
                ReportFilterForm filterForm = new ReportFilterForm(ReportType.LabelSearch, new List<String>());
                if (filterForm.ShowDialog() == DialogResult.OK)
                {
                    Model.LabelIntronFilters labelIntronFilters = filterForm.LabelIntronSearchInputs;
                    this._CurrentReportDbm.FilterLabelIntronSearchReport("LabelSearch", labelIntronFilters, selectedDisplayMode.Name);
                }
            }
            else if (selectedSearchMode.Name == "IntronSearch")
            {
                ToolStripButton searchModebutton = (ToolStripButton)dropDown.DropDownItems["LabelSearch"];
                searchModebutton.CheckState = CheckState.Unchecked;
                ReportFilterForm filterForm = new ReportFilterForm(ReportType.IntronSearch, new List<String>());
                if (filterForm.ShowDialog() == DialogResult.OK)
                {
                    Model.LabelIntronFilters labelIntronFilters = filterForm.LabelIntronSearchInputs;
                    this._CurrentReportDbm.FilterLabelIntronSearchReport("IntronSearch", labelIntronFilters, selectedDisplayMode.Name);
                }
            }            
        }
        public void EnableSaveMenus(bool flag)
        {
            this.saveToolStripButton.Enabled = flag;
            this.saveAsToolStrip.Enabled = flag;
        }
        internal void ToggleMenus(bool flag)
        {
            this.rMFToolStrip.Enabled = flag;
        }
        #region Exist
        private void dbmExitFileMenu_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }
        #endregion
        #region Report Main Form Loading
        private void DBMMainForm_Load(object sender, EventArgs e)
        {
            // create DBMMain object
           // _CurrentReportDbm = new ReportMain(this._ReportType, this);
        }
        #endregion
        #region Prompt on Report Form Closing
        protected override void OnClosing(CancelEventArgs e)
        {
            //Form[] childForms = this.MdiChildren;
            //if (childForms != null)
            //{
            //    foreach (Form frmReport in childForms)
            //    {
            //        frmReport.Close();
            //    }
            //}

            //if (_CurrentReportDbm != null)
            //{
            //    bool closeStatus = _CurrentReportDbm.ReportCloseStatus;

            //    e.Cancel = !closeStatus;
            //}  
            //base.OnClosing(e);
            if (this._CurrentReportDbm != null && this._ReportType != ReportType.EXIDTNInfo)
            {
                if (!this._ReportSaveStatus)
                {
                    DialogResult result = MessageBox.Show("Do you want to save the report before closing?", "Reports", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        //save it and close.
                        if (this._CurrentReportDbm != null)
                        {
                            this._ReportSaveStatus = this._CurrentReportDbm.SaveReport(true);
                            this._CurrentReportDbm = null;
                        }
                       
                    }
                    else if (result == DialogResult.No)
                    {
                        //do nothing

                        //do not save.close
                        this._CurrentReportDbm = null;
                        //_MdiParent.Close();
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        //do nothing.
                        e.Cancel = true;
                        //this._ReportCloseStatus = false;
                    }
                }
            }
        }
        #endregion
        #region Save Report
        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            if (_CurrentReportDbm != null)
            {
                this._ReportSaveStatus = _CurrentReportDbm.SaveReport(true);
            }
        }
        #endregion
        #region Save As
        private void saveAsToolStrip_Click(object sender, EventArgs e)
        {
            if (_CurrentReportDbm != null)
            {
                this._ReportSaveStatus = _CurrentReportDbm.SaveReport(false);
            }
        }
        #endregion
        #region Sort Report Grid Command
        private void dgSearchResult_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            if(this._ReportType != ReportType.ModelInformation)
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            if(this._ReportType != ReportType.ModelInformation_EmptyDevice)
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            if (this._ReportType != ReportType.ModelInfoWithArdKey)
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }
        #endregion
    }    
}