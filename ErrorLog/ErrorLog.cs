using System;
using System.Collections.Generic;
using System.Text;
using CommonForms;
using System.IO;
namespace UEI.Workflow2010.Report.ErrorLog
{
    public class ErrorLog
    {
        private static String errorlogFile = String.Empty;
        static ErrorLog()
        {
            errorlogFile = GetSettings("ErrorLog");
        }
        private static String GetSettings(String val)
        {
            String path = String.Empty;
            String file = String.Empty;
            switch (val)
            {
                case "ErrorLog":
                    path = Configuration.GetWorkingDirectory();
                    if (path == String.Empty)
                    {                        
                        path = "c:\\Working\\";
                    }
                    file = path +"ErrorLog.txt";
                    break;
            }
            return file;
        }
        public static void WriteToErrorLogFile(String errorMessage)
        {
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                fs = new FileStream(errorlogFile, FileMode.Append, FileAccess.Write);
                sw = new StreamWriter(fs);
                sw.WriteLine("Invalid Setup Codes for :" + errorlogFile);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }
    }
}