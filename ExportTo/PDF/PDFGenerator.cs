using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
namespace UEI.Workflow2010.Report.ExportToPDF
{
    public class PDFGenerator : IDisposable
    {
        private Document pdfDocument = null;
        private Table pdfContent = null;
        private String strFilePath = String.Empty;
        public String FilePath
        {
            get
            {
                return strFilePath;

            }
            set
            {
                strFilePath = value;
            }
        }
        private String strFileName = String.Empty;
        public String FileName
        {
            get
            {
                return strFileName;
            }
            set
            {
                strFileName = value + ".pdf";
            }
        }

        public PDFGenerator()
        {
            pdfDocument = new Document();
        }

        public void GeneratePdf(DataSet dsDataSet)
        {
            if (!IsFileOpen())
            {
                //Remove  the pdf File if Exists
                RemoveFiles();
                FileStream stream = new FileStream(FilePath + "\\" + FileName, FileMode.Create);
                PdfWriter writer = PdfWriter.GetInstance(pdfDocument, stream);
                pdfDocument.Open();
                pdfDocument.AddCreationDate();
                pdfDocument.AddTitle(FileName + " Report");
                pdfDocument.AddAuthor("UEI");

                for (int i = 0; i < dsDataSet.Tables.Count; i++)
                {
                    DataTable dtDataTable = new DataTable();
                    dtDataTable = dsDataSet.Tables[i];
                    if (dtDataTable.Rows.Count > 0)
                    {
                        pdfContent = new Table(dtDataTable.Columns.Count);
                        pdfContent.BorderWidth = 1;
                        pdfContent.BorderColor = new Color(0, 0, 0);
                        pdfContent.Padding = 1;
                        pdfContent.Spacing = 1;

                        Cell cell = new Cell(new Phrase(dtDataTable.TableName, FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));
                        cell.Header = true;
                        cell.GrayFill = 1.9f;
                        cell.Colspan = dtDataTable.Columns.Count;
                        pdfContent.AddCell(cell);

                        for (int intCol = 0; intCol < dtDataTable.Columns.Count; intCol++)
                        {
                            cell = new Cell(new Phrase(dtDataTable.Columns[intCol].ColumnName, FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD)));
                            if (intCol == 0)
                                cell.Width = 2;
                            else
                                cell.Width = 24;

                            cell.HorizontalAlignment = 1;
                            pdfContent.AddCell(cell);
                        }

                        for (int intRows = 0; intRows < dtDataTable.Rows.Count; intRows++)
                        {
                            Boolean deviceCheck = false;
                            for (int intCol = 0; intCol < dtDataTable.Columns.Count; intCol++)
                            {
                                if(intCol < dtDataTable.Columns.Count - 1 && dtDataTable.Rows[intRows][intCol + 1].ToString().Trim() == String.Empty && dtDataTable.TableName.Contains("ModelInfoReport") == false && dsDataSet.DataSetName.Contains("LabelIntron") == false && dsDataSet.DataSetName.Contains("BrandBasedSearch") == false && dsDataSet.DataSetName.Contains("BrandSearch") == false && dsDataSet.DataSetName.Contains("ExecIdMapSet") == false && dsDataSet.DataSetName.Contains("SynthesizerTable") == false && dsDataSet.DataSetName.Contains("NewDataSet") == false)
                                    deviceCheck = true;
                                if (deviceCheck == true)
                                {
                                    cell = new Cell(new Phrase(dtDataTable.Rows[intRows][intCol].ToString().Trim(), FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL)));
                                    cell.GrayFill = 0.9f;
                                }
                                else
                                {
                                    cell = new Cell(new Phrase(dtDataTable.Rows[intRows][intCol].ToString().Trim(), FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL)));
                                }
                                if (intCol == 0)
                                    cell.Width = 2;
                                else
                                    cell.Width = 24;

                                pdfContent.AddCell(cell);
                            }
                        }

                        //Add Content           
                        pdfDocument.Add(pdfContent);
                    }
                }

                pdfDocument.Close();
                writer.Close();
                stream.Close();
            }
        }
        /// <summary>
        /// Check whether the file is already opened.
        /// </summary>
        /// <returns></returns>
        private bool IsFileOpen()
        {
            try
            {
                FileInfo fileInfo = new FileInfo(FilePath + "\\" + FileName);

                if (fileInfo.Exists)
                {
                    FileStream stream = fileInfo.Open(FileMode.Open);

                    stream.Close();
                    stream.Dispose();
                }

                return false;
            }
            catch (IOException)
            {
                throw;
            }
        }

        #region Remove File
        private void RemoveFiles()
        {
            DirectoryInfo dir = new DirectoryInfo(FilePath);
            FileInfo[] fiArr = dir.GetFiles(FileName);
            foreach (FileInfo fi in fiArr)
            {
                if (fi.Extension.ToString() == ".pdf")
                {
                    fi.Delete();
                }
            }
        }
        #endregion
        #region IDisposable Members
        public void Dispose()
        {
            pdfDocument = null;
        }
        #endregion
    }
}