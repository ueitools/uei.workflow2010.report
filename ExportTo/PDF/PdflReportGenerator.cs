using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace UEI.Workflow2010.Report.ExportToPDF
{
    public static class PdflReportGenerator
    {
        public static bool GenerateReport(DataSet inputData, String outputFilePath, String reportName, out String exceptionMsg)
        {
            exceptionMsg = String.Empty;
            try
            {
                if (!String.IsNullOrEmpty(outputFilePath) && inputData != null && !String.IsNullOrEmpty(reportName))
                {

                    PDFGenerator gen = new PDFGenerator();
                    gen.FilePath = outputFilePath;
                    gen.FileName = reportName;
                    gen.GeneratePdf(inputData);                    
                    System.Diagnostics.Process.Start(outputFilePath + reportName + ".pdf");
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                return false;
            }
        }
    }
}
