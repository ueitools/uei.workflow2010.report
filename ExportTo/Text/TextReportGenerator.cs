using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;

namespace UEI.Workflow2010.Report.ExportToText
{
    public static class TextReportGenerator
    {
        /// <summary>
        /// Generates a text file from the input data inside the dataset.
        /// </summary>
        /// <remarks>Created on 10/22/2010 by binil.</remarks>
        /// <param name="inputData">Data to write as a dataset</param>
        /// <param name="outputFilePath">File path</param>
        /// <param name="reportName">Name of the report.</param>
        /// <param name="exceptionMsg">holds exception message if anything occurs.</param>
        /// <returns>True or False</returns>
        public static bool GenerateReport(DataSet inputData, string outputFilePath, string reportName, out string exceptionMsg,Model.IDModeRecordModel allModeNames)
        {
            exceptionMsg = String.Empty;

            try
            {
                if (inputData != null && !String.IsNullOrEmpty(outputFilePath) && !String.IsNullOrEmpty(reportName))
                {
                    //get the file
                    StreamWriter textWriter = File.CreateText(outputFilePath + reportName + ".txt");
                    //iterate through tables in the dataset
                    foreach (DataTable table in inputData.Tables)
                    {
                        if (table.TableName == "FormatErrorCodes")
                            textWriter.WriteLine("###### Setup Codes with format errors ######");
                        else if (table.TableName == "Brands")
                        {
                            textWriter.WriteLine("###### Search Result ######");
                        }
                        else
                        {
                            String modeName = allModeNames.getModeName(table.TableName);
                            //dr[0] = mode;
                            modeName = modeName + " (" + table.TableName + ")";
                            textWriter.WriteLine("###### Setup Codes For {0} ######", modeName);
                        }

                        textWriter.WriteLine();
                        //write header
                        //object[] columns = new object[table.Columns.Count];
                        //for (int j = 0; j < table.Columns.Count; j++)
                        //{
                        //    columns[j] = table.Columns[j].ColumnName;
                        //}

                        //StringBuilder format = new StringBuilder();
                        //for (int i = 0; i < columns.Length; i++)
                        //{
                        //    format.Append("{" + i.ToString() + ",-35}");
                        //}
                        //textWriter.WriteLine(format.ToString(), columns);

                        //put header lining
                        //for (int j = 0; j < table.Columns.Count; j++)
                        //{
                        //    columns[j] = "-------------------------";
                        //}
                        //textWriter.WriteLine(format.ToString(), columns);

                        //write the content
                        foreach (DataRow row in table.Rows)
                        {
                            object[] items = row.ItemArray;
                            Boolean isRowWrappable = false;
                            Boolean flagfornewLine = false;
                            StringBuilder formatString = new StringBuilder();

                            //if (Convert.ToString(items[1]) == String.Empty && Convert.ToString(items[0]) != String.Empty)
                            //    flagfornewLine = true;
                            
                            for (int i = 0; i < items.Length; i++)
                            {
                                if(items.Length > 1)
                                    if (Convert.ToString(items[1]) == String.Empty && Convert.ToString(items[0]) != String.Empty)
                                        flagfornewLine = true;
                                
                                formatString.Append("{" + i.ToString() + ",-40}");

                                //check whether any item needs to be wrapped.
                                string item = Convert.ToString(items[i]);
                                if (!String.IsNullOrEmpty(item))
                                    if (item.LastIndexOf(',') != -1 && item.Split(',').Length > 5)
                                        isRowWrappable = true;
                            }

                            if (flagfornewLine)//for subdevice headers
                            {
                                textWriter.WriteLine();
                                textWriter.WriteLine(formatString.ToString(), items);
                                //textWriter.WriteLine("------------------------------");
                                //textWriter.WriteLine();
                            }
                            else//for the contents
                            {
                                if (isRowWrappable)
                                {
                                    System.Collections.ArrayList subRows = SplitRows(items);

                                    foreach (object[] subRow in subRows)
                                        textWriter.WriteLine(formatString.ToString(), subRow);

                                }
                                else
                                    textWriter.WriteLine(formatString.ToString(), items);
                            }
                            //textWriter.WriteLine(formatString.ToString(), items);
                            //if (flagfornewLine)
                            //{
                            //    for (int j = 1; j < table.Columns.Count; j++)
                            //    {
                            //        columns[j] = "";
                            //    }
                            //    textWriter.WriteLine(format.ToString(), columns);
                            //}
                        }
                        textWriter.WriteLine();
                    }
                    textWriter.Flush();
                    textWriter.Close();
                    textWriter.Dispose();

                    if (inputData.Tables[0].TableName != "FormatErrorCodes")
                        System.Diagnostics.Process.Start(outputFilePath + reportName + ".txt");
                }
                return true;
            }
            catch(Exception ex)
            {
                exceptionMsg = ex.Message;
                return false;
            }
        }
        private static System.Collections.ArrayList SplitRows(object[] items)
        {
            
            try
            {
                System.Collections.ArrayList rowList = new System.Collections.ArrayList();
               // al.Add(new object[items.Length]);//add the default main row.

                int totalRowsNeeded = 0;

                for (int i = 0; i < items.Length; i++)
                {
                    string item = Convert.ToString(items[i]);
                    if (!String.IsNullOrEmpty(item))
                    {
                        string[] itemArr = item.Split(',');
                        //get the number of rows needed to accomodate the items.
                        int rowsNeeded = ((itemArr.Length % 5) == 0) ? (itemArr.Length / 5) : (itemArr.Length / 5) + 1;
                        //get the total num of rows needed.
                        totalRowsNeeded = Math.Max(totalRowsNeeded, rowsNeeded);
                    }
                }


                for (int i = 0; i < totalRowsNeeded; i++)
                {
                    object[] subRow = new object[items.Length];
                    rowList.Add(subRow);
                }

                for (int i = 0; i < items.Length; i++)
                {
                                     
                    string data = Convert.ToString(items[i]);

                    if (data != String.Empty)
                    {
                        if (data.LastIndexOf(',') != -1)//check whether item is a comma separated string.
                        {
                            string[] dataSplitted = data.Split(',');
                            StringBuilder dataBuilder = new StringBuilder();
                            int rowCount = 0;
                            for (int j = 0, k = 0; j < dataSplitted.Length; j++, k++)
                            {
                                dataBuilder.Append(dataSplitted[j] + ",");

                                if (k == 4)
                                {

                                    string appendedData = dataBuilder.ToString().Trim();//remove white space in front of the text.
                                    
                                    object[] subRow = rowList[rowCount] as object[];
                                    subRow[i] = appendedData;

                                    dataBuilder = new StringBuilder();

                                    k = -1;//reset the counters
                                    rowCount++;
                                }
                            }

                            if (dataBuilder.Length != 0)//get the remaining if any
                            {
                                //remove last occurrence of ',' & remove white space in front of the text.
                                string appendedData = dataBuilder.Remove(dataBuilder.Length - 1, 1).ToString().Trim();
                                

                                object[] subRow = rowList[rowCount] as object[];
                                subRow[i] = appendedData;

                            }
                        }
                        else
                        {
                            //add to the 1st array in array list
                            object[] subRow = rowList[0] as object[];
                            subRow[i] = data;
                        }
                    }
                }

                return rowList;
            }
            catch(Exception ex)
            {
                ex.Data.Add("SplitRows",items);
                throw ex;
                
               
            }
        }
        public static bool GenerateReport(DataSet inputData, string outputFilePath, string reportName, out string exceptionMsg)
        {
            exceptionMsg = String.Empty;

            try
            {
                if (inputData != null && !String.IsNullOrEmpty(outputFilePath) && !String.IsNullOrEmpty(reportName))
                {
                    //get the file
                    StreamWriter textWriter = File.CreateText(outputFilePath + reportName + ".txt");
                    //iterate through tables in the dataset
                    foreach (DataTable table in inputData.Tables)
                    {
                        if (table.TableName == "FormatErrorCodes")
                            textWriter.WriteLine("###### Setup Codes with format errors ######");
                        else
                        {
                            textWriter.WriteLine("###### Setup Codes For {0} ######", table.TableName);
                        }

                        textWriter.WriteLine();
                        //write header
                        object[] columns = new object[table.Columns.Count];
                        for (int j = 0; j < table.Columns.Count; j++)
                        {
                            columns[j] = table.Columns[j].ColumnName;
                        }

                        StringBuilder format = new StringBuilder();
                        for (int i = 0; i < columns.Length; i++)
                        {
                            format.Append("{" + i.ToString() + ",-35}");
                        }
                        textWriter.WriteLine(format.ToString(), columns);

                        //put header lining
                        //for (int j = 0; j < table.Columns.Count; j++)
                        //{
                        //    columns[j] = "-------------------------";
                        //}
                        //textWriter.WriteLine(format.ToString(), columns);

                        //write the content
                        foreach (DataRow row in table.Rows)
                        {
                            object[] items = row.ItemArray;
                            Boolean flagfornewLine = false;
                            StringBuilder formatString = new StringBuilder();
                            for (int i = 0; i < items.Length; i++)
                            {
                                if (Convert.ToString(items[1]) == String.Empty && Convert.ToString(items[0]) != String.Empty)
                                    flagfornewLine = true;
                                formatString.Append("{" + i.ToString() + ",-35}");
                            }
                            if (flagfornewLine)
                                textWriter.WriteLine();
                            textWriter.WriteLine(formatString.ToString(), items);
                            //if (flagfornewLine)
                            //{
                            //    for (int j = 1; j < table.Columns.Count; j++)
                            //    {
                            //        columns[j] = "";
                            //    }
                            //    textWriter.WriteLine(format.ToString(), columns);
                            //}
                        }
                        textWriter.WriteLine();
                    }
                    textWriter.Flush();
                    textWriter.Close();
                    textWriter.Dispose();

                    if (inputData.Tables[0].TableName != "FormatErrorCodes")
                        System.Diagnostics.Process.Start(outputFilePath + reportName + ".txt");
                }
                return true;
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                return false;
            }
        }
        public static bool GenerateNormalReport(DataSet inputData, string outputFilePath, string reportName, out string exceptionMsg)
        {
            exceptionMsg = String.Empty;

            try
            {
                if (inputData != null && !String.IsNullOrEmpty(outputFilePath) && !String.IsNullOrEmpty(reportName))
                {
                    //get the file
                    StreamWriter textWriter = File.CreateText(outputFilePath + reportName + ".txt");
                    //iterate through tables in the dataset
                    foreach (DataTable table in inputData.Tables)
                    {
                        //write header
                        object[] columns = new object[table.Columns.Count];
                        for (int j = 0; j < table.Columns.Count; j++)
                        {
                            columns[j] = table.Columns[j].ColumnName;
                        }
                        
                        StringBuilder format = new StringBuilder();
                        for (int i = 0; i < columns.Length; i++)
                        {
                            format.Append("{" + i.ToString() + ",-20}");
                        }
                        textWriter.WriteLine(format.ToString(), columns);
                        //put header lining
                        for (int j = 0; j < table.Columns.Count; j++)
                        {
                            columns[j] = "---------------";
                        }
                        textWriter.WriteLine(format.ToString(), columns);

                        //write the content
                        foreach (DataRow row in table.Rows)
                        {
                            object[] items = row.ItemArray;
                            StringBuilder formatString = new StringBuilder();
                            for (int i = 0; i < items.Length; i++)
                            {
                                formatString.Append("{" + i.ToString() + ",-20}");
                            }
                            textWriter.WriteLine(formatString.ToString(), items);
                        }
                    }
                    textWriter.Flush();
                    textWriter.Close();
                    textWriter.Dispose();
                    System.Diagnostics.Process.Start(outputFilePath + reportName + ".txt");
                }
                return true;
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                return false;
            }
        }
    }
}
