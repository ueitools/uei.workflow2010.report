using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.Xsl;


namespace UEI.Workflow2010.Report.ExportToExcel
{
    public static class ExcelReportGenerator
    {
        /// <summary>
        /// Generates excel report based on the input dataset.
        /// </summary>
        /// <remarks>Modified on 10/22/2010 by binil.</remarks>
        /// <param name="inputData"></param>
        /// <param name="reportType"></param>
        /// <param name="outputFilePath"></param>
        /// <param name="reportName"></param>
        /// <param name="exceptionMsg"></param>
        /// <returns></returns>
        public static bool GenerateReport(DataSet inputData, string outputFilePath, string reportName, out string exceptionMsg)
        {
            exceptionMsg = String.Empty;
            bool returnVal = false;
            try
            {
                if (!String.IsNullOrEmpty(outputFilePath) && inputData != null && !String.IsNullOrEmpty(reportName))
                {

                    ExcelGenerator gen = new ExcelGenerator();
                    gen.FilePath = outputFilePath;
                    gen.FileName = reportName;
                    gen.AddDataSet(inputData);
                    returnVal = gen.GenerateExcel();
                    if (returnVal)
                        System.Diagnostics.Process.Start(outputFilePath + reportName + ".xls");
                    return returnVal;
                }
                return returnVal;
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                return false;
            }
        }
        public static bool GenerateReport(DataSet inputData, string outputFilePath, string reportName, out string exceptionMsg, String excelExtension)
        {
            exceptionMsg = String.Empty;
            bool returnVal = false;
            try
            {
                if (!String.IsNullOrEmpty(outputFilePath) && inputData != null && !String.IsNullOrEmpty(reportName))
                {
                    switch (excelExtension)
                    {
                        case "xls":
                            ExcelGenerator gen = new ExcelGenerator();
                            gen.FilePath = outputFilePath;
                            gen.FileName = reportName;
                            gen.AddDataSet(inputData);
                            returnVal = gen.GenerateExcel();
                            if (returnVal)
                                System.Diagnostics.Process.Start(outputFilePath + reportName + ".xls");
                            break;
                        case "xlsx":
                            ExcelOperation excelop = new ExcelOperation();
                            excelop.FilePath = outputFilePath;
                            excelop.FileName = reportName;
                            excelop.AddDataSet(inputData);
                            returnVal = excelop.GenerateExcel();
                            if (returnVal)
                                System.Diagnostics.Process.Start(outputFilePath + reportName + ".xlsx");
                            break;
                    }
                    return returnVal;
                }
                return returnVal;
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                return false;
            }
        }
    }    
}