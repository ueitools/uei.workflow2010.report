using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.Workflow2010.Report.ExportToExcel
{
    public class MSExcelOperation
    {
        #region Variables
        #endregion

        #region Properties
        private String m_fileName = String.Empty;
        public String FileName
        {
            get { return m_fileName; }
            set { m_fileName = value+".xls"; }
        }
        private String m_filePath = String.Empty;
        public String FilePath
        {
            get { return m_filePath; }
            set { m_filePath = value; }
        }
        private Boolean m_suppressemptyColumns = false;
        public Boolean SuppressEmptyColumn
        {
            get { return m_suppressemptyColumns; }
            set { m_suppressemptyColumns = value; }
        }
        #endregion

        #region Constructor
        public MSExcelOperation()
        {
        }
        #endregion
    }
}