using System;
using System.Collections.Generic;
using System.Text;
using CarlosAg.ExcelXmlWriter;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;

namespace UEI.Workflow2010.Report.ExportToExcel
{
    public class ExcelGenerator : IDisposable
    {
        #region Vaiables
        private Workbook excelWorkbook = null;
        private Worksheet excelWorksheet = null;
        private WorksheetStyle excelWorksheetStyle = null;

        #endregion

        private String strFilePath = String.Empty;
        public String FilePath
        {
            get
            {
                return strFilePath;

            }
            set
            {
                strFilePath = value;
            }
        }
        private String strFileName = String.Empty;
        public String FileName
        {
            get
            {
                return strFileName;
            }
            set
            {
                strFileName = value + ".xls";
            }
        }
        private Boolean bolSupressEmptyColumn = false;
        public Boolean SupressEmptyColumn
        {
            get
            {
                return bolSupressEmptyColumn;
            }
            set
            {
                bolSupressEmptyColumn = value;
            }
        }

        #region Constructor
        public ExcelGenerator()
        {
            excelWorkbook = new Workbook();
            //Optional properties of the Document
            excelWorkbook.Properties.Title = "";
            excelWorkbook.Properties.Created = DateTime.Now;
            excelWorkbook.Properties.Subject = "Report";
            excelWorkbook.Properties.Author = "UEI";
            //Append Style To Work Book
            WorkSheetStyle();
        }
        #endregion

        #region Work Sheet Style
        private void WorkSheetStyle()
        {
            // Add some styles to the Workbook
            excelWorksheetStyle = excelWorkbook.Styles.Add("HeaderStyle");
            excelWorksheetStyle.Font.FontName = "Tahoma";
            excelWorksheetStyle.Font.Size = 10;
            excelWorksheetStyle.Font.Bold = true;
            excelWorksheetStyle.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            excelWorksheetStyle.Font.Color = "Black";
            excelWorksheetStyle.Interior.Color = "LightGray";
            excelWorksheetStyle.Interior.Pattern = StyleInteriorPattern.Solid;
            //excelWorksheetStyle.Alignment.Rotate = 90;

            excelWorksheetStyle.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1);

            // Create the Default Style to use for everyone
            excelWorksheetStyle = excelWorkbook.Styles.Add("Default");
            excelWorksheetStyle.Font.FontName = "Tahoma";
            excelWorksheetStyle.Font.Size = 10;
            excelWorksheetStyle.Font.Color = "Black";
            excelWorksheetStyle.Interior.Color = "White";
            //excelWorksheetStyle.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1);//, "#FFFFFF");
            //excelWorksheetStyle.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1);//, "#FFFFFF");
            //excelWorksheetStyle.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1);//, "#FFFFFF");
            //excelWorksheetStyle.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1);//, "#FFFFFF");

            //For All Cells
            excelWorksheetStyle = excelWorkbook.Styles.Add("DefaultCellStyle");
            excelWorksheetStyle.Font.FontName = "Tahoma";
            excelWorksheetStyle.Font.Size = 10;
            //excelWorksheetStyle.Alignment.WrapText = true;
            excelWorksheetStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            excelWorksheetStyle.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1);

            //for specific reports
            excelWorksheetStyle = excelWorkbook.Styles.Add("SpecificCellStyle");
            excelWorksheetStyle.Font.FontName = "Tahoma";
            excelWorksheetStyle.Font.Size = 10;
            excelWorksheetStyle.Alignment.WrapText = true;
            excelWorksheetStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            excelWorksheetStyle.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1);


            //For All Cells
            excelWorksheetStyle = excelWorkbook.Styles.Add("HighlightCellStyle");
            excelWorksheetStyle.Font.FontName = "Tahoma";
            excelWorksheetStyle.Font.Size = 10;
            excelWorksheetStyle.Font.Bold = true;
            //excelWorksheetStyle.Alignment.WrapText = true;
            excelWorksheetStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            excelWorksheetStyle.Interior.Color = "LightGray";
            excelWorksheetStyle.Interior.Pattern = StyleInteriorPattern.Solid;
            excelWorksheetStyle.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1);
            excelWorksheetStyle.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1);
        }
        #endregion

        #region Generate Work Sheets Based on DataTables in A DataSet
        public void AddDataSet(DataSet dsDataSet)
        {
            for (int i = 0; i < dsDataSet.Tables.Count; i++)
            {
                //Remove Empty Columns
                DataTable dtDataTable = new DataTable();
                dtDataTable = dsDataSet.Tables[i];
                //if (SupressEmptyColumn == true)
                //{
                //    //Remove Empty Columns
                //    dtDataTable = SupressEmptyColumns((DataTable)dsDataSet.Tables[i]);
                //}
                if (dtDataTable.Rows.Count > 0)
                {
                    excelWorksheet = excelWorkbook.Worksheets.Add(dtDataTable.TableName.ToString().ToUpper());
                    //Create Header and Sheet           
                    WorksheetRow excelWorksheetRow = excelWorksheet.Table.Rows.Add();
                    for (int intCol = 0; intCol < dtDataTable.Columns.Count; intCol++)
                    {
                        excelWorksheetRow.Cells.Add(dtDataTable.Columns[intCol].ColumnName.ToString().Trim(), DataType.String, "HeaderStyle");

                        if (intCol == 1 && dtDataTable.Columns[intCol].ColumnName != "DeviceCode" && dtDataTable.Columns[intCol].ColumnName != "Brand Number" && dtDataTable.Columns[intCol].ColumnName != "Standard Brand" && dtDataTable.Columns[intCol].ColumnName != "T/R" && dtDataTable.Columns[intCol].ColumnName != "Model Count" && dtDataTable.Columns[intCol].ColumnName != "UnitsSold" && dtDataTable.Columns[intCol].ColumnName != "Search Count" && dsDataSet.DataSetName.Contains("BrandBasedSearch") == false && dsDataSet.DataSetName.Contains("ExecIdMapSet") == false && dsDataSet.DataSetName.Contains("SynthesizerTable") == false && dsDataSet.DataSetName.Contains("NewDataSet") == false && dsDataSet.DataSetName.Contains("EDID Summary") == false && dsDataSet.DataSetName.Contains("Grouped EDID Summary") == false && dsDataSet.DataSetName.Contains("XBox EDID Summary") == false)
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(350));
                        else if (intCol == 2 && dsDataSet.DataSetName == "BSCList" && dtDataTable.Columns[intCol].ColumnName == "ID")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(350));
                        }
                        else if (dtDataTable.TableName == "BSC")
                        {
                            if (dtDataTable.Columns[intCol].ColumnName == "ID")
                                excelWorksheet.Table.Columns.Add(new WorksheetColumn(350));
                            else
                                excelWorksheet.Table.Columns.Add(new WorksheetColumn(150));
                        }
                        else if (dsDataSet.DataSetName == "IDSelection" && intCol == 1)
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(70));
                        }
                        else if (intCol == 0 && dsDataSet.DataSetName == "BSCList" && dtDataTable.Columns[intCol].ColumnName == "Brand")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(150));
                        }
                        else if (intCol == 0 && dtDataTable.TableName == "BSC" && dtDataTable.Columns[intCol].ColumnName == "Brand")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(150));
                        }
                        else if (dsDataSet.DataSetName == "ID-Brand" && (dtDataTable.Columns[intCol].ColumnName == "Brand Count" || dtDataTable.Columns[intCol].ColumnName == "Model Count"))
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(80));
                        }
                        else if (dsDataSet.DataSetName == "ID-Brand" && dtDataTable.Columns[intCol].ColumnName == "ID")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(100));
                        }
                        else if (dsDataSet.DataSetName == "ModelInfo" && dtDataTable.Columns[intCol].ColumnName == "Brand" || dtDataTable.Columns[intCol].ColumnName == "DataSource")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(150));
                        }
                        else if (dsDataSet.DataSetName == "ModelInfo" && dtDataTable.Columns[intCol].ColumnName == "DataSource")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(80));
                        }
                        else if (dsDataSet.DataSetName == "ModelInfo" && dtDataTable.Columns[intCol].ColumnName == "Model")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else if (dsDataSet.DataSetName == "BrandBasedSearch" && dtDataTable.Columns[intCol].ColumnName == "Models")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else if (dsDataSet.DataSetName == "BrandBasedSearch" && dtDataTable.Columns[intCol].ColumnName == "Ids")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else if (dsDataSet.DataSetName == "BrandSearch" && dtDataTable.Columns[intCol].ColumnName == "Region")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else if (dsDataSet.DataSetName == "BrandSearch" && dtDataTable.Columns[intCol].ColumnName == "Country")
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else if (dsDataSet.DataSetName == "ExecIdMapSet" && (dtDataTable.Columns[intCol].ColumnName == "ID" || dtDataTable.Columns[intCol].ColumnName == "Brand"))
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else if (dsDataSet.DataSetName == "ExPrefixData" && (dtDataTable.Columns[intCol].ColumnName == "ID"))
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn(250));
                        }
                        else
                        {
                            excelWorksheet.Table.Columns.Add(new WorksheetColumn());
                            excelWorksheet.Table.Columns[intCol].AutoFitWidth = true;
                            //excelWorksheet.Table.Columns[intCol].Width = 35;
                        }
                    }
                    //Create Excel Freeze Pane
                    //excelWorksheet.Options.SplitHorizontal = 1;
                    //excelWorksheet.Options.FreezePanes = true;
                    //excelWorksheet.Options.TopRowBottomPane = 1;
                    //Build the Sheet Contain
                    for (int intRows = 0; intRows < dtDataTable.Rows.Count; intRows++)
                    {
                        excelWorksheetRow = excelWorksheet.Table.Rows.Add();
                        excelWorksheetRow.AutoFitHeight = true;
                        string style = "DefaultCellStyle";
                        if (dsDataSet.DataSetName == "BSCList" && dtDataTable.Columns.Contains("Brand") == true)
                            style = "SpecificCellStyle";
                        if (dtDataTable.TableName == "BSC" && dtDataTable.Columns.Contains("Brand") == true)
                            style = "SpecificCellStyle";
                        if (dsDataSet.DataSetName == "ID-Brand" && dtDataTable.Columns.Contains("Brands") == true)
                            style = "SpecificCellStyle";
                        if (dsDataSet.DataSetName == "BrandSearch")
                            style = "SpecificCellStyle";
                        if (dsDataSet.DataSetName == "BrandBasedSearch" && dtDataTable.Columns.Contains("Models") == true || dtDataTable.Columns.Contains("Ids") == true)
                            style = "SpecificCellStyle";
                        if (dsDataSet.DataSetName == "ExecIdMapSet" && (dtDataTable.Columns.Contains("ID") == true || dtDataTable.Columns.Contains("Brand") == true))
                            style = "SpecificCellStyle";
                        if (dsDataSet.DataSetName == "ExPrefixData" && (dtDataTable.Columns.Contains("ID") == true))
                            style = "SpecificCellStyle";
                        for (int intCol = 0; intCol < dtDataTable.Columns.Count; intCol++)
                        {
                            if (intCol < dtDataTable.Columns.Count - 1 && dtDataTable.Rows[intRows][intCol + 1].ToString().Trim() == String.Empty && dtDataTable.TableName.Contains("ModelInfoReport") == false && dsDataSet.DataSetName.Contains("LabelIntron") == false && dsDataSet.DataSetName.Contains("BrandBasedSearch") == false && dsDataSet.DataSetName.Contains("BrandSearch") == false && dsDataSet.DataSetName.Contains("ExecIdMapSet") == false && dsDataSet.DataSetName.Contains("EDID Summary") == false && dsDataSet.DataSetName.Contains("Grouped EDID Summary") == false && dsDataSet.DataSetName.Contains("XBox EDID Summary") == false && dtDataTable.TableName.Contains("LocationSearch") == false)
                            {
                                style = "HighlightCellStyle";
                            }
                            //if (dtDataTable.Columns.Contains("Group"))
                            //{
                            //    style = "HighlightCellStyle";
                            //}
                            Boolean m_NumericColumn = false;

                            if (dsDataSet.DataSetName == "BSCList" || dsDataSet.DataSetName == "ModelInfo" || dtDataTable.Columns[intCol].ColumnName.ToLower().Contains("synth") || dtDataTable.Columns[intCol].ColumnName.ToLower().Contains("prefix") || dtDataTable.Columns[intCol].ColumnName.ToLower().Contains("data") || dsDataSet.DataSetName.Contains("EDID Summary") || dsDataSet.DataSetName.Contains("SynthesizerTable") || dsDataSet.DataSetName.Contains("XBox EDID Summary") || dsDataSet.DataSetName.Contains("EDID Summary"))
                            {
                                m_NumericColumn = false;
                            }
                            else if (dtDataTable.TableName == "BSC" || dtDataTable.Columns[intCol].ColumnName.Contains("Brand") || dtDataTable.Columns[intCol].ColumnName.Contains("Brands") || dtDataTable.Columns[intCol].ColumnName.Contains("Models"))
                            {
                                m_NumericColumn = false;
                            }
                            else if (dsDataSet.DataSetName == "LabelIntron" || dtDataTable.TableName == "LabelIntron" || dtDataTable.Columns[intCol].ColumnName.Contains("Label") || dtDataTable.Columns[intCol].ColumnName.Contains("Data"))
                            {
                                m_NumericColumn = false;
                            }
                            else if (dsDataSet.DataSetName.Contains("EDID Summary") && (dtDataTable.Columns[intCol].ColumnName.Contains("Console Count")))
                            {
                                if (IsNumeric(dtDataTable.Rows[intRows][intCol].ToString().Trim()))
                                {
                                    m_NumericColumn = true;
                                }
                            }
                            else if (dsDataSet.DataSetName.Contains("Grouped EDID Summary") && (dtDataTable.Columns[intCol].ColumnName.Contains("Console Count")))
                            {
                                if (IsNumeric(dtDataTable.Rows[intRows][intCol].ToString().Trim()))
                                {
                                    m_NumericColumn = true;
                                }
                            }
                            else if (dtDataTable.Rows[intRows][intCol].ToString().Trim() != String.Empty)
                            {
                                if (IsNumeric(dtDataTable.Rows[intRows][intCol].ToString().Trim()))
                                {
                                    m_NumericColumn = true;
                                }
                            }
                            if (m_NumericColumn == true)
                            {
                                excelWorksheetRow.Cells.Add(new WorksheetCell(dtDataTable.Rows[intRows][intCol].ToString().Trim(), DataType.Number, style));
                            }
                            else
                                excelWorksheetRow.Cells.Add(new WorksheetCell(dtDataTable.Rows[intRows][intCol].ToString().Trim(), DataType.String, style));
                        }
                    }
                }
            }
        }
        #endregion

        #region Generate Excel File
        public bool GenerateExcel()
        {
            bool returnVal = false;

            if (!IsFileOpen())
            {
                // Specify which Sheet should be opened and the size of window by default
                excelWorkbook.ExcelWorkbook.ActiveSheetIndex = 0;
                //Remove  the Excel File if Exists            
                RemoveFiles();
                excelWorkbook.Save(FilePath + "\\" + FileName);

                returnVal = true;
            }

            if (excelWorkbook != null)
                excelWorkbook = null;

            return returnVal;
        }
        #endregion

        #region Remove File
        private void RemoveFiles()
        {
            DirectoryInfo dir = new DirectoryInfo(FilePath);
            FileInfo[] fiArr = dir.GetFiles(FileName);
            foreach (FileInfo fi in fiArr)
            {
                if (fi.Extension.ToString() == ".xls")
                {
                    fi.Delete();
                }
            }

        }

        private bool IsFileOpen()
        {
            try
            {
                FileInfo fileInfo = new FileInfo(FilePath + "\\" + FileName);

                if (fileInfo.Exists)
                {
                    FileStream stream = fileInfo.Open(FileMode.Open);

                    stream.Close();
                    stream.Dispose();
                }

                return false;
            }
            catch (IOException)
            {
                throw;
            }
        }
        #endregion

        #region Check if string is numeric
        public static bool IsNumeric(string strToCheck)
        {
            if (strToCheck.Contains(","))
                return false;
            //return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
            int i; float f; decimal d; double d1;
            return int.TryParse(strToCheck, out i) || float.TryParse(strToCheck, out f) || decimal.TryParse(strToCheck, out d) || double.TryParse(strToCheck, out d1);
        }
        #endregion

        #region Suppress Empty Columns from DataTable
        private DataTable SupressEmptyColumns(DataTable dtSource)
        {
            System.Collections.ArrayList columnsToRemove = new System.Collections.ArrayList();
            foreach (DataColumn dc in dtSource.Columns)
            {
                bool colEmpty = true;
                foreach (DataRow dr in dtSource.Rows)
                {
                    if (dr[dc.ColumnName].ToString() != String.Empty)
                    {
                        colEmpty = false;
                    }
                }
                if (colEmpty == true)
                {
                    columnsToRemove.Add(dc.ColumnName);
                }
            }
            foreach (String columnName in columnsToRemove)
            {
                if (dtSource.Columns.Contains(columnName) == true)
                    dtSource.Columns.Remove(columnName);
            }
            return dtSource;
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            //Dispose the Objects
            excelWorkbook = null;
            excelWorksheet = null;
        }
        #endregion
    }
}