﻿using System;
using System.Collections.Generic;
using System.Text;
//using CarlosAg.ExcelXmlWriter;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Runtime.InteropServices;
namespace UEI.Workflow2010.Report.ExportToExcel
{
    public class ExcelOperation: IDisposable
    {
        #region Vaiables
        private Excel.Application excelApplication = null;
        private Excel.Workbook excelWorkbook = null;
        private Excel.Worksheet excelWorksheet = null;
        private Excel.Range excelWorksheetRange = null;
        #endregion

        private String strFilePath = String.Empty;        
        public String FilePath
        {
            get
            {
                return strFilePath;

            }
            set
            {
                strFilePath = value;
            }
        }
        private String strFileName = String.Empty;
        public String FileName
        {
            get
            {
                return strFileName;
            }
            set
            {
                strFileName = value + ".xlsx";
            }
        }
        private Boolean bolSupressEmptyColumn = false;
        public Boolean SupressEmptyColumn
        {
            get
            {
                return bolSupressEmptyColumn;
            }
            set
            {
                bolSupressEmptyColumn = value;
            }
        }

        #region Constructor
        public ExcelOperation()
        {
            excelApplication = new Excel.Application();
            excelApplication.Visible = false;
            excelWorkbook = excelApplication.Application.Workbooks.Add(Type.Missing);
            //Optional properties of the Document
            //excelWorkbook.CustomDocumentProperties..Properties.Title = "";
            //excelWorkbook.Properties.Created = DateTime.Now;
            //excelWorkbook.Properties.Subject = "Report";
            //excelWorkbook.Properties.Author = "UEI";
            //Append Style To Work Book
        }
        #endregion        

        #region Generate Work Sheets Based on DataTables in A DataSet
        public void AddDataSet(DataSet dsDataSet)
        {
            //Temporary DataTable and DataRow
            int intexcelWorksheetIndex = 0;

            for (int i = 0; i < dsDataSet.Tables.Count; i++)
            {
                //Remove Empty Columns
                DataTable dtDataTable = new DataTable();
                dtDataTable = dsDataSet.Tables[i];
                //if (SupressEmptyColumn == true)
                //{
                //    //Remove Empty Columns
                //    dtDataTable = SupressEmptyColumns((DataTable)dsDataSet.Tables[i]);
                //}
                if (dtDataTable.Rows.Count > 0)
                {
                    //Get the Worksheet index                        
                    intexcelWorksheetIndex = (i + 1);

                    if (i <= 3)
                    {
                        excelWorksheet = (Excel.Worksheet)excelWorkbook.Worksheets[intexcelWorksheetIndex];
                        excelWorksheet.Name = dsDataSet.Tables[i].TableName.ToUpper();
                        excelWorksheet.Tab.Color = Color.Tomato.ToArgb();
                    }
                    else
                    {
                        excelWorksheet = (Excel.Worksheet)excelWorkbook.Worksheets.Add(Type.Missing, excelWorkbook.Worksheets[(intexcelWorksheetIndex - 1)], 1, Type.Missing);
                        excelWorksheet.Name = dsDataSet.Tables[i].TableName.ToUpper();
                        excelWorksheet.Tab.Color = Color.Tomato.ToArgb();
                    }

                    //Create Header and Sheet                               
                    for (int intCol = 0; intCol < dtDataTable.Columns.Count; intCol++)
                    {
                        if (intCol == 1 && dtDataTable.Columns[intCol].ColumnName != "DeviceCode" && dtDataTable.Columns[intCol].ColumnName != "Brand Number" && dtDataTable.Columns[intCol].ColumnName != "Standard Brand" && dtDataTable.Columns[intCol].ColumnName != "T/R" && dtDataTable.Columns[intCol].ColumnName != "Model Count" && dtDataTable.Columns[intCol].ColumnName != "UnitsSold" && dtDataTable.Columns[intCol].ColumnName != "Search Count" && dsDataSet.DataSetName.Contains("BrandBasedSearch") == false && dsDataSet.DataSetName.Contains("ExecIdMapSet") == false && dsDataSet.DataSetName.Contains("SynthesizerTable") == false && dsDataSet.DataSetName.Contains("NewDataSet") == false && dsDataSet.DataSetName.Contains("EDID Summary") == false && dsDataSet.DataSetName.Contains("Grouped EDID Summary") == false && dsDataSet.DataSetName.Contains("XBox EDID Summary") == false)
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;                            
                        }
                        else if (intCol == 2 && dsDataSet.DataSetName == "BSCList" && dtDataTable.Columns[intCol].ColumnName == "ID")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dtDataTable.TableName == "BSC")
                        {
                            if (dtDataTable.Columns[intCol].ColumnName == "ID")
                            {
                                excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                                Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                                rg.EntireColumn.ColumnWidth = 250;
                            }
                            else
                            {
                                excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                                Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                                rg.EntireColumn.ColumnWidth = 150;
                            }
                        }
                        else if (dsDataSet.DataSetName == "IDSelection" && intCol == 1)
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 70;
                        }
                        else if (intCol == 0 && dsDataSet.DataSetName == "BSCList" && dtDataTable.Columns[intCol].ColumnName == "Brand")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 150;
                        }
                        else if (intCol == 0 && dtDataTable.TableName == "BSC" && dtDataTable.Columns[intCol].ColumnName == "Brand")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 150;
                        }
                        else if (dsDataSet.DataSetName == "ID-Brand" && (dtDataTable.Columns[intCol].ColumnName == "Brand Count" || dtDataTable.Columns[intCol].ColumnName == "Model Count"))
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 80;
                        }
                        else if (dsDataSet.DataSetName == "ID-Brand" && dtDataTable.Columns[intCol].ColumnName == "ID")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 100;
                        }
                        else if (dsDataSet.DataSetName == "ModelInfo" && dtDataTable.Columns[intCol].ColumnName == "Brand" || dtDataTable.Columns[intCol].ColumnName == "DataSource")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 150;
                        }
                        else if (dsDataSet.DataSetName == "ModelInfo" && dtDataTable.Columns[intCol].ColumnName == "DataSource")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 100;
                        }
                        else if (dsDataSet.DataSetName == "ModelInfo" && dtDataTable.Columns[intCol].ColumnName == "Model")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dsDataSet.DataSetName == "BrandBasedSearch" && dtDataTable.Columns[intCol].ColumnName == "Models")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dsDataSet.DataSetName == "BrandBasedSearch" && dtDataTable.Columns[intCol].ColumnName == "Ids")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dsDataSet.DataSetName == "BrandSearch" && dtDataTable.Columns[intCol].ColumnName == "Region")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dsDataSet.DataSetName == "BrandSearch" && dtDataTable.Columns[intCol].ColumnName == "Country")
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dsDataSet.DataSetName == "ExecIdMapSet" && (dtDataTable.Columns[intCol].ColumnName == "ID" || dtDataTable.Columns[intCol].ColumnName == "Brand"))
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else if (dsDataSet.DataSetName == "ExPrefixData" && (dtDataTable.Columns[intCol].ColumnName == "ID"))
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.ColumnWidth = 250;
                        }
                        else
                        {
                            excelWorksheet.Cells[1, intCol + 1] = dtDataTable.Columns[intCol].ColumnName.ToString().Trim();
                            Excel.Range rg = (Excel.Range)excelWorksheet.Cells[1, intCol + 1];
                            rg.EntireColumn.Columns.AutoFit();
                        }
                        
                    }
                    //Create Excel Freeze Pane
                    //excelWorksheet.Options.SplitHorizontal = 1;
                    //excelWorksheet.Options.FreezePanes = true;
                    //excelWorksheet.Options.TopRowBottomPane = 1;
                    //Build the Sheet Contain

                    //Take the Row index for Excel Sheet
                    int iRow = 2;
                    try
                    {
                        for (int intRows = 0; intRows < dtDataTable.Rows.Count; intRows++)
                        {
                            for (int intCol = 0; intCol < dtDataTable.Columns.Count; intCol++)
                            {
                                Boolean m_NumericColumn = false;
                                if (dsDataSet.DataSetName == "BSCList" || dsDataSet.DataSetName == "ModelInfo" || dtDataTable.Columns[intCol].ColumnName.ToLower().Contains("synth") || dtDataTable.Columns[intCol].ColumnName.ToLower().Contains("prefix") || dtDataTable.Columns[intCol].ColumnName.ToLower().Contains("data") || dsDataSet.DataSetName.Contains("EDID Summary") || dsDataSet.DataSetName.Contains("SynthesizerTable") || dsDataSet.DataSetName.Contains("XBox EDID Summary") || dsDataSet.DataSetName.Contains("EDID Summary"))
                                {
                                    m_NumericColumn = false;
                                }
                                else if (dtDataTable.TableName == "BSC" || dtDataTable.Columns[intCol].ColumnName.Contains("Brand") || dtDataTable.Columns[intCol].ColumnName.Contains("Brands") || dtDataTable.Columns[intCol].ColumnName.Contains("Models"))
                                {
                                    m_NumericColumn = false;
                                }
                                else if (dtDataTable.TableName == "LabelIntron" || dtDataTable.Columns[intCol].ColumnName.Contains("Label") || dtDataTable.Columns[intCol].ColumnName.Contains("Data"))
                                {
                                    m_NumericColumn = false;
                                }
                                else if (dsDataSet.DataSetName.Contains("EDID Summary") && (dtDataTable.Columns[intCol].ColumnName.Contains("Console Count")))
                                {
                                    if (IsNumeric(dtDataTable.Rows[intRows][intCol].ToString().Trim()))
                                    {
                                        m_NumericColumn = true;
                                    }
                                }
                                else if (dsDataSet.DataSetName.Contains("Grouped EDID Summary") && (dtDataTable.Columns[intCol].ColumnName.Contains("Console Count")))
                                {
                                    if (IsNumeric(dtDataTable.Rows[intRows][intCol].ToString().Trim()))
                                    {
                                        m_NumericColumn = true;
                                    }
                                }
                                else if (dtDataTable.Rows[intRows][intCol].ToString().Trim() != String.Empty)
                                {
                                    if (IsNumeric(dtDataTable.Rows[intRows][intCol].ToString().Trim()))
                                    {
                                        m_NumericColumn = true;
                                    }
                                }
                                if (m_NumericColumn == true)
                                {
                                    Excel.Range rg = (Excel.Range)excelWorksheet.Cells[iRow, intCol + 1];
                                    rg.NumberFormat = 0;
                                    excelWorksheet.Cells[iRow, intCol + 1] = dtDataTable.Rows[intRows][intCol].ToString().Trim();
                                }
                                else
                                    excelWorksheet.Cells[iRow, intCol + 1] = dtDataTable.Rows[intRows][intCol].ToString().Trim();
                            }
                            iRow++;
                        }
                    }
                    catch (Exception ex)
                    {
                        String s = ex.Message;
                    }

                    //Format A1:Z1 as bold, vertical alignment = center.
                    Excel.Range startRange;
                    Excel.Range rightRange;
                    // get a range to work with  
                    startRange = excelWorksheet.get_Range("A1", Type.Missing);
                    // get the end of values to the right  
                    // (will stop at the first empty cell)  
                    rightRange = startRange.get_End(Excel.XlDirection.xlToRight);
                    excelWorksheet.get_Range(startRange, rightRange).Font.Bold = true;
                    excelWorksheet.get_Range(startRange, rightRange).Font.FontStyle = "Arial Black";
                    excelWorksheet.get_Range(startRange, rightRange).Font.Color = Color.White.ToArgb();
                    excelWorksheet.get_Range(startRange, rightRange).Font.Size = 9;
                    excelWorksheet.get_Range(startRange, rightRange).Orientation = 90;
                    excelWorksheet.get_Range(startRange, rightRange).Interior.Color = Color.Tomato.ToArgb();
                    excelWorksheet.get_Range(startRange, rightRange).VerticalAlignment = Excel.Constants.xlBottom;
                    excelWorksheet.get_Range(startRange, rightRange).HorizontalAlignment = Excel.Constants.xlCenter;
                    excelWorksheet.get_Range(startRange, rightRange).WrapText = false;
                    excelWorksheet.get_Range(startRange, rightRange).EntireRow.AutoFit();

                    //AutoFit columns A1:Z1
                    excelWorksheetRange = getValidRange(excelWorksheet);
                    excelWorksheetRange.EntireColumn.AutoFit();
                    excelWorksheetRange.Font.Name = "Aerial";
                    excelWorksheetRange.VerticalAlignment = Excel.Constants.xlCenter;
                    excelWorksheetRange.Font.Size = 8;
                    excelWorksheetRange.Borders.Color = Color.Black.ToArgb();
                    excelWorksheetRange.Borders.LineStyle = Excel.Constants.xlSolid;

                    excelWorksheetRange.Cells.WrapText = false;

                    //Need all the folllowing code to clean up
                    if (startRange != null)
                        Marshal.ReleaseComObject(startRange);
                    startRange = null;

                    if (rightRange != null)
                        Marshal.ReleaseComObject(rightRange);
                    rightRange = null;

                    if (excelWorksheetRange != null)
                        Marshal.ReleaseComObject(excelWorksheetRange);
                    excelWorksheetRange = null;
                    
                    if (excelWorksheet != null)
                    Marshal.ReleaseComObject(excelWorksheet);                    
                        excelWorksheet = null;
                }
            }
        }
        #endregion

        #region Generate Excel File
        public bool GenerateExcel()
        {
            bool returnVal = false;

            if (!IsFileOpen())
            {
                excelApplication.Visible = false;
                excelApplication.UserControl = false;

                // Specify which Sheet should be opened and the size of window by default
                //excelWorkbook.ExcelWorkbook.ActiveSheetIndex = 0;
                //Remove  the Excel File if Exists            
                RemoveFiles();
                
                if (FilePath.EndsWith("\\"))
                    excelApplication.ActiveWorkbook.SaveCopyAs(FilePath + FileName);
                else
                    excelApplication.ActiveWorkbook.SaveCopyAs(FilePath + "\\" + FileName);
                excelApplication.ActiveWorkbook.Saved = true;                
                returnVal = true;
            }

            excelWorkbook.Close(null,null,null);
            if (excelWorkbook != null)
            {
                excelApplication.Workbooks.Close();                
                Marshal.ReleaseComObject(excelWorkbook);
            }
            excelWorkbook = null;
            excelApplication.Quit();

            if (excelApplication != null)
                Marshal.ReleaseComObject(excelApplication);
            excelApplication = null;

            GC.Collect(); // force final cleanup!                    
            Dispose();
            return returnVal;
        }
        #endregion

        #region Remove File
        private void RemoveFiles()
        {
            DirectoryInfo dir = new DirectoryInfo(FilePath);
            FileInfo[] fiArr = dir.GetFiles(FileName);
            foreach (FileInfo fi in fiArr)
            {
                //if (fi.Extension.ToString() == ".xls")
                //{
                //  fi.Delete();
                //}
                if (fi.Extension.ToString() == ".xlsx")
                {
                    fi.Delete();
                }
            }

        }

        private bool IsFileOpen()
        {
            try
            {
                FileInfo fileInfo = new FileInfo(FilePath + "\\" + FileName);                
                if (fileInfo.Exists)
                {
                    FileStream stream = fileInfo.Open(FileMode.Open);
                    stream.Close();
                    stream.Dispose();
                }
                return false;
            }
            catch (IOException)
            {
                throw;
            }
        }
        #endregion

        #region Check if string is numeric
        public static bool IsNumeric(string strToCheck)
        {
            if (strToCheck.Contains(","))
                return false;
            //return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
            int i; float f; decimal d; double d1;
            return int.TryParse(strToCheck, out i) || float.TryParse(strToCheck, out f) || decimal.TryParse(strToCheck, out d) || double.TryParse(strToCheck, out d1);
        }
        #endregion

        #region Suppress Empty Columns from DataTable
        private DataTable SupressEmptyColumns(DataTable dtSource)
        {
            System.Collections.ArrayList columnsToRemove = new System.Collections.ArrayList();
            foreach (DataColumn dc in dtSource.Columns)
            {
                bool colEmpty = true;
                foreach (DataRow dr in dtSource.Rows)
                {
                    if (dr[dc.ColumnName].ToString() != String.Empty)
                    {
                        colEmpty = false;
                    }
                }
                if (colEmpty == true)
                {
                    columnsToRemove.Add(dc.ColumnName);
                }
            }
            foreach (String columnName in columnsToRemove)
            {
                if (dtSource.Columns.Contains(columnName) == true)
                    dtSource.Columns.Remove(columnName);
            }
            return dtSource;
        }
        #endregion

        #region Get Valid Range for your Excel Work Sheet
        /// <summary>
        /// Get the Range of the Excel Sheet
        /// </summary>
        /// <param name="sheet">Get the Individual Sheet to find out the Range</param>
        /// <returns>Returns the Range of Excel Sheet from Left to right and Top to Bottom</returns>
        private Excel.Range getValidRange(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            string downAddress = ""; string rightAddress = "";
            long indexOld = 0; long index = 0;
            Excel.Range startRange;
            Excel.Range rightRange;
            Excel.Range downRange;
            try
            {
                // get a range to work with  
                startRange = sheet.get_Range("A1", Type.Missing);

                // get the end of values to the right  
                // (will stop at the first empty cell)  
                rightRange = startRange.get_End(Excel.XlDirection.xlToRight);

                /*get_End method scans the sheet in the direction specified *until it finds an empty cell and returns 		
                 * the previous cell. *We need to scan all the columns and find the column with *the highest number of 		
                 * rows (row count). *Then use the Prefix character on the right cell and the *row count to determine the 		
                 * address for the valid range. */
                while (true)
                {
                    downRange = rightRange.get_End(Excel.XlDirection.xlDown);
                    downAddress = downRange.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                    index = getIndex(downAddress);

                    //if (index >= 65536)
                    if(index >= 1048576)
                        index = 0;
                    if (index > indexOld)
                        indexOld = index;
                    if (rightRange.Column == 1)
                        break;

                    rightRange = rightRange.Previous;
                }
                rightRange = startRange.get_End(Excel.XlDirection.xlToRight);
                rightAddress = rightRange.get_Address(false, false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                return sheet.get_Range("A1", getPrefix(rightAddress) + indexOld);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                startRange = null;
                rightRange = null;
                downRange = null;
            }
        }
        #endregion

        #region Get the Index for the Excel Sheet
        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        private long getIndex(string address)
        {
            string outStr = "";
            for (int i = 0; i < address.Length; i++)
            {
                if (!Char.IsLetter(address[i]))
                {
                    outStr += address[i].ToString();
                }
            }
            return Convert.ToInt64(outStr);
        }
        #endregion

        #region Get the Prefix
        /// <summary>
        /// Gets the prefix.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        private string getPrefix(string address)
        {
            string outStr = "";
            for (int i = 0; i < address.Length; i++)
            {
                if (Char.IsLetter(address[i]))
                {
                    outStr += address[i].ToString();
                }
            }

            return outStr;
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            //Dispose the Objects            
            if (excelApplication != null)
            {
                Marshal.ReleaseComObject(excelApplication);
                excelApplication = null;
            }
            if (excelWorksheet != null)
            {
                Marshal.ReleaseComObject(excelWorksheet);
                excelWorksheet = null;
            }
            if (excelWorkbook != null)
            {
                Marshal.ReleaseComObject(excelWorkbook);
                excelWorkbook = null;
            }
            if (excelWorksheetRange != null)
            {
                Marshal.ReleaseComObject(excelWorksheetRange);
                excelWorksheetRange = null;
            }
            GC.Collect();   //force to cleanup
        }
        #endregion
    }
}
